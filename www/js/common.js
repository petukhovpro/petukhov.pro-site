// Sets equal height for all sections
function resizeTitleSection(force) {
    if (force || window.innerWidth < 1200) {
        var windowHeight = $(window).height();
        $("section.title").outerHeight(windowHeight);
    }
}

$(document).ready(function () {
    //enable sticky main menu while scrolling
    sticky = new Waypoint.Sticky({
        element: $('#navbar')
    });

    //enable smoolt scroll
    smoothScroll.init();

    //init the burger button collapsed it by default
    $('.collapse').collapse({
        toggle: false
    });

    //hide the main menu
    $(".nav a").click(function () {
        $('.collapse').collapse('toggle');
    });

    $('.contacts').waypoint({
        handler: function () {
            $('.contacts').addClass('visited');
        },
        offset: 150
    });

    resizeTitleSection();

    $(window).resize(function() {
        resizeTitleSection(true);
    });
});