-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.11.3-MariaDB-log - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             12.4.0.6659
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for petukhov
CREATE DATABASE IF NOT EXISTS `petukhov` /*!40100 DEFAULT CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci */;
USE `petukhov`;

-- Dumping structure for table petukhov.modx_access_actiondom
CREATE TABLE IF NOT EXISTS `modx_access_actiondom` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `target` varchar(100) NOT NULL DEFAULT '',
  `principal_class` varchar(100) NOT NULL DEFAULT 'MODX\\Revolution\\modPrincipal',
  `principal` int(10) unsigned NOT NULL DEFAULT 0,
  `authority` int(10) unsigned NOT NULL DEFAULT 9999,
  `policy` int(10) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `target` (`target`),
  KEY `principal_class` (`principal_class`),
  KEY `principal` (`principal`),
  KEY `authority` (`authority`),
  KEY `policy` (`policy`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table petukhov.modx_access_actiondom: 0 rows
/*!40000 ALTER TABLE `modx_access_actiondom` DISABLE KEYS */;
/*!40000 ALTER TABLE `modx_access_actiondom` ENABLE KEYS */;

-- Dumping structure for table petukhov.modx_access_actions
CREATE TABLE IF NOT EXISTS `modx_access_actions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `target` varchar(100) NOT NULL DEFAULT '',
  `principal_class` varchar(100) NOT NULL DEFAULT 'modPrincipal',
  `principal` int(10) unsigned NOT NULL DEFAULT 0,
  `authority` int(10) unsigned NOT NULL DEFAULT 9999,
  `policy` int(10) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `target` (`target`),
  KEY `principal_class` (`principal_class`),
  KEY `principal` (`principal`),
  KEY `authority` (`authority`),
  KEY `policy` (`policy`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table petukhov.modx_access_actions: 0 rows
/*!40000 ALTER TABLE `modx_access_actions` DISABLE KEYS */;
/*!40000 ALTER TABLE `modx_access_actions` ENABLE KEYS */;

-- Dumping structure for table petukhov.modx_access_category
CREATE TABLE IF NOT EXISTS `modx_access_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `target` varchar(100) NOT NULL DEFAULT '',
  `principal_class` varchar(100) NOT NULL DEFAULT 'MODX\\Revolution\\modPrincipal',
  `principal` int(10) unsigned NOT NULL DEFAULT 0,
  `authority` int(10) unsigned NOT NULL DEFAULT 9999,
  `policy` int(10) unsigned NOT NULL DEFAULT 0,
  `context_key` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `target` (`target`),
  KEY `principal_class` (`principal_class`),
  KEY `principal` (`principal`),
  KEY `authority` (`authority`),
  KEY `policy` (`policy`),
  KEY `context_key` (`context_key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table petukhov.modx_access_category: 0 rows
/*!40000 ALTER TABLE `modx_access_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `modx_access_category` ENABLE KEYS */;

-- Dumping structure for table petukhov.modx_access_context
CREATE TABLE IF NOT EXISTS `modx_access_context` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `target` varchar(100) NOT NULL DEFAULT '',
  `principal_class` varchar(100) NOT NULL DEFAULT 'MODX\\Revolution\\modPrincipal',
  `principal` int(10) unsigned NOT NULL DEFAULT 0,
  `authority` int(10) unsigned NOT NULL DEFAULT 9999,
  `policy` int(10) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `target` (`target`),
  KEY `principal_class` (`principal_class`),
  KEY `principal` (`principal`),
  KEY `authority` (`authority`),
  KEY `policy` (`policy`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table petukhov.modx_access_context: 3 rows
/*!40000 ALTER TABLE `modx_access_context` DISABLE KEYS */;
INSERT INTO `modx_access_context` (`id`, `target`, `principal_class`, `principal`, `authority`, `policy`) VALUES
	(1, 'web', 'MODX\\Revolution\\modUserGroup', 0, 9999, 15),
	(2, 'mgr', 'MODX\\Revolution\\modUserGroup', 1, 0, 2),
	(3, 'web', 'MODX\\Revolution\\modUserGroup', 1, 0, 2);
/*!40000 ALTER TABLE `modx_access_context` ENABLE KEYS */;

-- Dumping structure for table petukhov.modx_access_elements
CREATE TABLE IF NOT EXISTS `modx_access_elements` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `target` varchar(100) NOT NULL DEFAULT '',
  `principal_class` varchar(100) NOT NULL DEFAULT 'MODX\\Revolution\\modPrincipal',
  `principal` int(10) unsigned NOT NULL DEFAULT 0,
  `authority` int(10) unsigned NOT NULL DEFAULT 9999,
  `policy` int(10) unsigned NOT NULL DEFAULT 0,
  `context_key` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `target` (`target`),
  KEY `principal_class` (`principal_class`),
  KEY `principal` (`principal`),
  KEY `authority` (`authority`),
  KEY `policy` (`policy`),
  KEY `context_key` (`context_key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table petukhov.modx_access_elements: 0 rows
/*!40000 ALTER TABLE `modx_access_elements` DISABLE KEYS */;
/*!40000 ALTER TABLE `modx_access_elements` ENABLE KEYS */;

-- Dumping structure for table petukhov.modx_access_media_source
CREATE TABLE IF NOT EXISTS `modx_access_media_source` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `target` varchar(100) NOT NULL DEFAULT '',
  `principal_class` varchar(100) NOT NULL DEFAULT 'MODX\\Revolution\\modPrincipal',
  `principal` int(10) unsigned NOT NULL DEFAULT 0,
  `authority` int(10) unsigned NOT NULL DEFAULT 9999,
  `policy` int(10) unsigned NOT NULL DEFAULT 0,
  `context_key` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `target` (`target`),
  KEY `principal_class` (`principal_class`),
  KEY `principal` (`principal`),
  KEY `authority` (`authority`),
  KEY `policy` (`policy`),
  KEY `context_key` (`context_key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table petukhov.modx_access_media_source: 0 rows
/*!40000 ALTER TABLE `modx_access_media_source` DISABLE KEYS */;
/*!40000 ALTER TABLE `modx_access_media_source` ENABLE KEYS */;

-- Dumping structure for table petukhov.modx_access_menus
CREATE TABLE IF NOT EXISTS `modx_access_menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `target` varchar(100) NOT NULL DEFAULT '',
  `principal_class` varchar(100) NOT NULL DEFAULT 'MODX\\Revolution\\modPrincipal',
  `principal` int(10) unsigned NOT NULL DEFAULT 0,
  `authority` int(10) unsigned NOT NULL DEFAULT 9999,
  `policy` int(10) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `target` (`target`),
  KEY `principal_class` (`principal_class`),
  KEY `principal` (`principal`),
  KEY `authority` (`authority`),
  KEY `policy` (`policy`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table petukhov.modx_access_menus: 0 rows
/*!40000 ALTER TABLE `modx_access_menus` DISABLE KEYS */;
/*!40000 ALTER TABLE `modx_access_menus` ENABLE KEYS */;

-- Dumping structure for table petukhov.modx_access_namespace
CREATE TABLE IF NOT EXISTS `modx_access_namespace` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `target` varchar(100) NOT NULL DEFAULT '',
  `principal_class` varchar(100) NOT NULL DEFAULT 'MODX\\Revolution\\modPrincipal',
  `principal` int(10) unsigned NOT NULL DEFAULT 0,
  `authority` int(10) unsigned NOT NULL DEFAULT 9999,
  `policy` int(10) unsigned NOT NULL DEFAULT 0,
  `context_key` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `target` (`target`),
  KEY `principal_class` (`principal_class`),
  KEY `principal` (`principal`),
  KEY `authority` (`authority`),
  KEY `policy` (`policy`),
  KEY `context_key` (`context_key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table petukhov.modx_access_namespace: 0 rows
/*!40000 ALTER TABLE `modx_access_namespace` DISABLE KEYS */;
/*!40000 ALTER TABLE `modx_access_namespace` ENABLE KEYS */;

-- Dumping structure for table petukhov.modx_access_permissions
CREATE TABLE IF NOT EXISTS `modx_access_permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `template` int(10) unsigned NOT NULL DEFAULT 0,
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `value` tinyint(1) unsigned NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  KEY `template` (`template`),
  KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=237 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table petukhov.modx_access_permissions: 233 rows
/*!40000 ALTER TABLE `modx_access_permissions` DISABLE KEYS */;
INSERT INTO `modx_access_permissions` (`id`, `template`, `name`, `description`, `value`) VALUES
	(1, 1, 'about', 'perm.about_desc', 1),
	(2, 1, 'access_permissions', 'perm.access_permissions_desc', 1),
	(3, 1, 'actions', 'perm.actions_desc', 1),
	(4, 1, 'change_password', 'perm.change_password_desc', 1),
	(5, 1, 'change_profile', 'perm.change_profile_desc', 1),
	(6, 1, 'charsets', 'perm.charsets_desc', 1),
	(7, 1, 'class_map', 'perm.class_map_desc', 1),
	(8, 1, 'components', 'perm.components_desc', 1),
	(9, 1, 'content_types', 'perm.content_types_desc', 1),
	(10, 1, 'countries', 'perm.countries_desc', 1),
	(11, 1, 'create', 'perm.create_desc', 1),
	(12, 1, 'credits', 'perm.credits_desc', 1),
	(13, 1, 'customize_forms', 'perm.customize_forms_desc', 1),
	(14, 1, 'dashboards', 'perm.dashboards_desc', 1),
	(15, 1, 'database', 'perm.database_desc', 1),
	(16, 1, 'database_truncate', 'perm.database_truncate_desc', 1),
	(17, 1, 'delete_category', 'perm.delete_category_desc', 1),
	(18, 1, 'delete_chunk', 'perm.delete_chunk_desc', 1),
	(19, 1, 'delete_context', 'perm.delete_context_desc', 1),
	(20, 1, 'delete_document', 'perm.delete_document_desc', 1),
	(21, 1, 'delete_eventlog', 'perm.delete_eventlog_desc', 1),
	(22, 1, 'delete_plugin', 'perm.delete_plugin_desc', 1),
	(23, 1, 'delete_propertyset', 'perm.delete_propertyset_desc', 1),
	(24, 1, 'delete_snippet', 'perm.delete_snippet_desc', 1),
	(25, 1, 'delete_template', 'perm.delete_template_desc', 1),
	(26, 1, 'delete_tv', 'perm.delete_tv_desc', 1),
	(27, 1, 'delete_role', 'perm.delete_role_desc', 1),
	(28, 1, 'delete_user', 'perm.delete_user_desc', 1),
	(29, 1, 'directory_chmod', 'perm.directory_chmod_desc', 1),
	(30, 1, 'directory_create', 'perm.directory_create_desc', 1),
	(31, 1, 'directory_list', 'perm.directory_list_desc', 1),
	(32, 1, 'directory_remove', 'perm.directory_remove_desc', 1),
	(33, 1, 'directory_update', 'perm.directory_update_desc', 1),
	(34, 1, 'edit_category', 'perm.edit_category_desc', 1),
	(35, 1, 'edit_chunk', 'perm.edit_chunk_desc', 1),
	(36, 1, 'edit_context', 'perm.edit_context_desc', 1),
	(37, 1, 'edit_document', 'perm.edit_document_desc', 1),
	(38, 1, 'edit_locked', 'perm.edit_locked_desc', 1),
	(39, 1, 'edit_plugin', 'perm.edit_plugin_desc', 1),
	(40, 1, 'edit_propertyset', 'perm.edit_propertyset_desc', 1),
	(41, 1, 'edit_role', 'perm.edit_role_desc', 1),
	(42, 1, 'edit_snippet', 'perm.edit_snippet_desc', 1),
	(43, 1, 'edit_template', 'perm.edit_template_desc', 1),
	(44, 1, 'edit_tv', 'perm.edit_tv_desc', 1),
	(45, 1, 'edit_user', 'perm.edit_user_desc', 1),
	(46, 1, 'element_tree', 'perm.element_tree_desc', 1),
	(47, 1, 'empty_cache', 'perm.empty_cache_desc', 1),
	(48, 1, 'error_log_erase', 'perm.error_log_erase_desc', 1),
	(49, 1, 'error_log_view', 'perm.error_log_view_desc', 1),
	(50, 1, 'export_static', 'perm.export_static_desc', 1),
	(51, 1, 'file_create', 'perm.file_create_desc', 1),
	(52, 1, 'file_list', 'perm.file_list_desc', 1),
	(53, 1, 'file_manager', 'perm.file_manager_desc', 1),
	(54, 1, 'file_remove', 'perm.file_remove_desc', 1),
	(55, 1, 'file_tree', 'perm.file_tree_desc', 1),
	(56, 1, 'file_update', 'perm.file_update_desc', 1),
	(57, 1, 'file_upload', 'perm.file_upload_desc', 1),
	(58, 1, 'file_unpack', 'perm.file_unpack_desc', 1),
	(59, 1, 'file_view', 'perm.file_view_desc', 1),
	(60, 1, 'flush_sessions', 'perm.flush_sessions_desc', 1),
	(61, 1, 'frames', 'perm.frames_desc', 1),
	(62, 1, 'help', 'perm.help_desc', 1),
	(63, 1, 'home', 'perm.home_desc', 1),
	(64, 1, 'import_static', 'perm.import_static_desc', 1),
	(65, 1, 'languages', 'perm.languages_desc', 1),
	(66, 1, 'lexicons', 'perm.lexicons_desc', 1),
	(67, 1, 'list', 'perm.list_desc', 1),
	(68, 1, 'load', 'perm.load_desc', 1),
	(69, 1, 'logout', 'perm.logout_desc', 1),
	(234, 1, 'language', 'perm.language_desc', 1),
	(71, 1, 'menu_reports', 'perm.menu_reports_desc', 1),
	(72, 1, 'menu_security', 'perm.menu_security_desc', 1),
	(73, 1, 'menu_site', 'perm.menu_site_desc', 1),
	(74, 1, 'menu_support', 'perm.menu_support_desc', 1),
	(75, 1, 'menu_system', 'perm.menu_system_desc', 1),
	(76, 1, 'menu_tools', 'perm.menu_tools_desc', 1),
	(77, 1, 'menu_user', 'perm.menu_user_desc', 1),
	(78, 1, 'menus', 'perm.menus_desc', 1),
	(79, 1, 'messages', 'perm.messages_desc', 1),
	(80, 1, 'namespaces', 'perm.namespaces_desc', 1),
	(81, 1, 'new_category', 'perm.new_category_desc', 1),
	(82, 1, 'new_chunk', 'perm.new_chunk_desc', 1),
	(83, 1, 'new_context', 'perm.new_context_desc', 1),
	(84, 1, 'new_document', 'perm.new_document_desc', 1),
	(85, 1, 'new_static_resource', 'perm.new_static_resource_desc', 1),
	(86, 1, 'new_symlink', 'perm.new_symlink_desc', 1),
	(87, 1, 'new_weblink', 'perm.new_weblink_desc', 1),
	(88, 1, 'new_document_in_root', 'perm.new_document_in_root_desc', 1),
	(89, 1, 'new_plugin', 'perm.new_plugin_desc', 1),
	(90, 1, 'new_propertyset', 'perm.new_propertyset_desc', 1),
	(91, 1, 'new_role', 'perm.new_role_desc', 1),
	(92, 1, 'new_snippet', 'perm.new_snippet_desc', 1),
	(93, 1, 'new_template', 'perm.new_template_desc', 1),
	(94, 1, 'new_tv', 'perm.new_tv_desc', 1),
	(95, 1, 'new_user', 'perm.new_user_desc', 1),
	(96, 1, 'packages', 'perm.packages_desc', 1),
	(97, 1, 'policy_delete', 'perm.policy_delete_desc', 1),
	(98, 1, 'policy_edit', 'perm.policy_edit_desc', 1),
	(99, 1, 'policy_new', 'perm.policy_new_desc', 1),
	(100, 1, 'policy_save', 'perm.policy_save_desc', 1),
	(101, 1, 'policy_view', 'perm.policy_view_desc', 1),
	(102, 1, 'policy_template_delete', 'perm.policy_template_delete_desc', 1),
	(103, 1, 'policy_template_edit', 'perm.policy_template_edit_desc', 1),
	(104, 1, 'policy_template_new', 'perm.policy_template_new_desc', 1),
	(105, 1, 'policy_template_save', 'perm.policy_template_save_desc', 1),
	(106, 1, 'policy_template_view', 'perm.policy_template_view_desc', 1),
	(107, 1, 'property_sets', 'perm.property_sets_desc', 1),
	(108, 1, 'providers', 'perm.providers_desc', 1),
	(109, 1, 'publish_document', 'perm.publish_document_desc', 1),
	(110, 1, 'purge_deleted', 'perm.purge_deleted_desc', 1),
	(111, 1, 'remove', 'perm.remove_desc', 1),
	(112, 1, 'remove_locks', 'perm.remove_locks_desc', 1),
	(113, 1, 'resource_duplicate', 'perm.resource_duplicate_desc', 1),
	(114, 1, 'resourcegroup_delete', 'perm.resourcegroup_delete_desc', 1),
	(115, 1, 'resourcegroup_edit', 'perm.resourcegroup_edit_desc', 1),
	(116, 1, 'resourcegroup_new', 'perm.resourcegroup_new_desc', 1),
	(117, 1, 'resourcegroup_resource_edit', 'perm.resourcegroup_resource_edit_desc', 1),
	(118, 1, 'resourcegroup_resource_list', 'perm.resourcegroup_resource_list_desc', 1),
	(119, 1, 'resourcegroup_save', 'perm.resourcegroup_save_desc', 1),
	(120, 1, 'resourcegroup_view', 'perm.resourcegroup_view_desc', 1),
	(121, 1, 'resource_quick_create', 'perm.resource_quick_create_desc', 1),
	(122, 1, 'resource_quick_update', 'perm.resource_quick_update_desc', 1),
	(123, 1, 'resource_tree', 'perm.resource_tree_desc', 1),
	(124, 1, 'save', 'perm.save_desc', 1),
	(125, 1, 'save_category', 'perm.save_category_desc', 1),
	(126, 1, 'save_chunk', 'perm.save_chunk_desc', 1),
	(127, 1, 'save_context', 'perm.save_context_desc', 1),
	(128, 1, 'save_document', 'perm.save_document_desc', 1),
	(129, 1, 'save_plugin', 'perm.save_plugin_desc', 1),
	(130, 1, 'save_propertyset', 'perm.save_propertyset_desc', 1),
	(131, 1, 'save_role', 'perm.save_role_desc', 1),
	(132, 1, 'save_snippet', 'perm.save_snippet_desc', 1),
	(133, 1, 'save_template', 'perm.save_template_desc', 1),
	(134, 1, 'save_tv', 'perm.save_tv_desc', 1),
	(135, 1, 'save_user', 'perm.save_user_desc', 1),
	(136, 1, 'search', 'perm.search_desc', 1),
	(137, 1, 'settings', 'perm.settings_desc', 1),
	(138, 1, 'events', 'perm.events_desc', 1),
	(139, 1, 'source_save', 'perm.source_save_desc', 1),
	(140, 1, 'source_delete', 'perm.source_delete_desc', 1),
	(141, 1, 'source_edit', 'perm.source_edit_desc', 1),
	(142, 1, 'source_view', 'perm.source_view_desc', 1),
	(143, 1, 'sources', 'perm.sources_desc', 1),
	(144, 1, 'steal_locks', 'perm.steal_locks_desc', 1),
	(145, 1, 'tree_show_element_ids', 'perm.tree_show_element_ids_desc', 1),
	(146, 1, 'tree_show_resource_ids', 'perm.tree_show_resource_ids_desc', 1),
	(147, 1, 'undelete_document', 'perm.undelete_document_desc', 1),
	(148, 1, 'unpublish_document', 'perm.unpublish_document_desc', 1),
	(149, 1, 'unlock_element_properties', 'perm.unlock_element_properties_desc', 1),
	(150, 1, 'usergroup_delete', 'perm.usergroup_delete_desc', 1),
	(151, 1, 'usergroup_edit', 'perm.usergroup_edit_desc', 1),
	(152, 1, 'usergroup_new', 'perm.usergroup_new_desc', 1),
	(153, 1, 'usergroup_save', 'perm.usergroup_save_desc', 1),
	(154, 1, 'usergroup_user_edit', 'perm.usergroup_user_edit_desc', 1),
	(155, 1, 'usergroup_user_list', 'perm.usergroup_user_list_desc', 1),
	(156, 1, 'usergroup_view', 'perm.usergroup_view_desc', 1),
	(157, 1, 'view', 'perm.view_desc', 1),
	(158, 1, 'view_category', 'perm.view_category_desc', 1),
	(159, 1, 'view_chunk', 'perm.view_chunk_desc', 1),
	(160, 1, 'view_context', 'perm.view_context_desc', 1),
	(161, 1, 'view_document', 'perm.view_document_desc', 1),
	(162, 1, 'view_element', 'perm.view_element_desc', 1),
	(163, 1, 'view_eventlog', 'perm.view_eventlog_desc', 1),
	(164, 1, 'view_offline', 'perm.view_offline_desc', 1),
	(165, 1, 'view_plugin', 'perm.view_plugin_desc', 1),
	(166, 1, 'view_propertyset', 'perm.view_propertyset_desc', 1),
	(167, 1, 'view_role', 'perm.view_role_desc', 1),
	(168, 1, 'view_snippet', 'perm.view_snippet_desc', 1),
	(169, 1, 'view_sysinfo', 'perm.view_sysinfo_desc', 1),
	(170, 1, 'view_template', 'perm.view_template_desc', 1),
	(171, 1, 'view_tv', 'perm.view_tv_desc', 1),
	(172, 1, 'view_user', 'perm.view_user_desc', 1),
	(173, 1, 'view_unpublished', 'perm.view_unpublished_desc', 1),
	(174, 1, 'workspaces', 'perm.workspaces_desc', 1),
	(175, 2, 'add_children', 'perm.add_children_desc', 1),
	(176, 2, 'copy', 'perm.copy_desc', 1),
	(177, 2, 'create', 'perm.create_desc', 1),
	(178, 2, 'delete', 'perm.delete_desc', 1),
	(179, 2, 'list', 'perm.list_desc', 1),
	(180, 2, 'load', 'perm.load_desc', 1),
	(181, 2, 'move', 'perm.move_desc', 1),
	(182, 2, 'publish', 'perm.publish_desc', 1),
	(183, 2, 'remove', 'perm.remove_desc', 1),
	(184, 2, 'save', 'perm.save_desc', 1),
	(185, 2, 'steal_lock', 'perm.steal_lock_desc', 1),
	(186, 2, 'undelete', 'perm.undelete_desc', 1),
	(187, 2, 'unpublish', 'perm.unpublish_desc', 1),
	(188, 2, 'view', 'perm.view_desc', 1),
	(189, 3, 'load', 'perm.load_desc', 1),
	(190, 3, 'list', 'perm.list_desc', 1),
	(191, 3, 'view', 'perm.view_desc', 1),
	(192, 3, 'save', 'perm.save_desc', 1),
	(193, 3, 'remove', 'perm.remove_desc', 1),
	(194, 4, 'add_children', 'perm.add_children_desc', 1),
	(195, 4, 'create', 'perm.create_desc', 1),
	(196, 4, 'copy', 'perm.copy_desc', 1),
	(197, 4, 'delete', 'perm.delete_desc', 1),
	(198, 4, 'list', 'perm.list_desc', 1),
	(199, 4, 'load', 'perm.load_desc', 1),
	(200, 4, 'remove', 'perm.remove_desc', 1),
	(201, 4, 'save', 'perm.save_desc', 1),
	(202, 4, 'view', 'perm.view_desc', 1),
	(203, 5, 'create', 'perm.create_desc', 1),
	(204, 5, 'copy', 'perm.copy_desc', 1),
	(205, 5, 'list', 'perm.list_desc', 1),
	(206, 5, 'load', 'perm.load_desc', 1),
	(207, 5, 'remove', 'perm.remove_desc', 1),
	(208, 5, 'save', 'perm.save_desc', 1),
	(209, 5, 'view', 'perm.view_desc', 1),
	(210, 6, 'load', 'perm.load_desc', 1),
	(211, 6, 'list', 'perm.list_desc', 1),
	(212, 6, 'view', 'perm.view_desc', 1),
	(213, 6, 'save', 'perm.save_desc', 1),
	(214, 6, 'remove', 'perm.remove_desc', 1),
	(215, 6, 'view_unpublished', 'perm.view_unpublished_desc', 1),
	(216, 6, 'copy', 'perm.copy_desc', 1),
	(217, 7, 'list', 'perm.list_desc', 1),
	(218, 7, 'load', 'perm.load_desc', 1),
	(219, 7, 'view', 'perm.view_desc', 1),
	(222, 9, 'load', 'Возможность «загружать» объекты, или быть в состоянии вернуть их как экземпляр объекта вообще.', 1),
	(223, 9, 'view_unpublished', 'Просматривать неопубликованные ресурсы.', 1),
	(224, 1, 'delete_weblink', 'perm.delete_weblink_desc', 1),
	(225, 1, 'delete_symlink', 'perm.delete_symlink_desc', 1),
	(226, 1, 'delete_static_resource', 'perm.delete_static_resource_desc', 1),
	(227, 1, 'edit_weblink', 'perm.edit_weblink_desc', 1),
	(228, 1, 'edit_symlink', 'perm.edit_symlink_desc', 1),
	(229, 1, 'edit_static_resource', 'perm.edit_static_resource_desc', 1),
	(230, 1, 'menu_trash', 'perm.menu_trash_desc', 1),
	(231, 1, 'set_sudo', 'perm.set_sudo_desc', 1),
	(232, 1, 'formit', 'To view the formit package.', 1),
	(233, 1, 'formit_encryptions', 'To view the formit package, encriptions part.', 1),
	(235, 1, 'mgr_log_view', 'perm.mgr_log_view_desc', 1),
	(236, 1, 'mgr_log_erase', 'perm.mgr_log_erase_desc', 1);
/*!40000 ALTER TABLE `modx_access_permissions` ENABLE KEYS */;

-- Dumping structure for table petukhov.modx_access_policies
CREATE TABLE IF NOT EXISTS `modx_access_policies` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` mediumtext DEFAULT NULL,
  `parent` int(10) unsigned NOT NULL DEFAULT 0,
  `template` int(10) unsigned NOT NULL DEFAULT 0,
  `class` varchar(255) NOT NULL DEFAULT '',
  `data` text DEFAULT NULL,
  `lexicon` varchar(255) NOT NULL DEFAULT 'permissions',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `parent` (`parent`),
  KEY `class` (`class`),
  KEY `template` (`template`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table petukhov.modx_access_policies: 13 rows
/*!40000 ALTER TABLE `modx_access_policies` DISABLE KEYS */;
INSERT INTO `modx_access_policies` (`id`, `name`, `description`, `parent`, `template`, `class`, `data`, `lexicon`) VALUES
	(1, 'Resource', 'policy_resource_desc', 0, 2, '', '{"add_children":true,"create":true,"copy":true,"delete":true,"list":true,"load":true,"move":true,"publish":true,"remove":true,"save":true,"steal_lock":true,"undelete":true,"unpublish":true,"view":true}', 'permissions'),
	(2, 'Administrator', 'policy_administrator_desc', 0, 1, '', '{"about":true,"access_permissions":true,"actions":true,"change_password":true,"change_profile":true,"charsets":true,"class_map":true,"components":true,"content_types":true,"countries":true,"create":true,"credits":true,"customize_forms":true,"dashboards":true,"database":true,"database_truncate":true,"delete_category":true,"delete_chunk":true,"delete_context":true,"delete_document":true,"delete_eventlog":true,"delete_plugin":true,"delete_propertyset":true,"delete_role":true,"delete_snippet":true,"delete_static_resource":true,"delete_symlink":true,"delete_template":true,"delete_tv":true,"delete_user":true,"delete_weblink":true,"directory_chmod":true,"directory_create":true,"directory_list":true,"directory_remove":true,"directory_update":true,"edit_category":true,"edit_chunk":true,"edit_context":true,"edit_document":true,"edit_locked":true,"edit_plugin":true,"edit_propertyset":true,"edit_role":true,"edit_snippet":true,"edit_static_resource":true,"edit_symlink":true,"edit_template":true,"edit_tv":true,"edit_user":true,"edit_weblink":true,"element_tree":true,"empty_cache":true,"error_log_erase":true,"error_log_view":true,"events":true,"export_static":true,"file_create":true,"file_list":true,"file_manager":true,"file_remove":true,"file_tree":true,"file_unpack":true,"file_update":true,"file_upload":true,"file_view":true,"flush_sessions":true,"frames":true,"help":true,"home":true,"language":true,"languages":true,"lexicons":true,"list":true,"load":true,"logout":true,"mgr_log_view":true,"mgr_log_erase":true,"menu_reports":true,"menu_security":true,"menu_site":true,"menu_support":true,"menu_system":true,"menu_tools":true,"menu_trash":true,"menu_user":true,"menus":true,"messages":true,"namespaces":true,"new_category":true,"new_chunk":true,"new_context":true,"new_document":true,"new_document_in_root":true,"new_plugin":true,"new_propertyset":true,"new_role":true,"new_snippet":true,"new_static_resource":true,"new_symlink":true,"new_template":true,"new_tv":true,"new_user":true,"new_weblink":true,"packages":true,"policy_delete":true,"policy_edit":true,"policy_new":true,"policy_save":true,"policy_template_delete":true,"policy_template_edit":true,"policy_template_new":true,"policy_template_save":true,"policy_template_view":true,"policy_view":true,"property_sets":true,"providers":true,"publish_document":true,"purge_deleted":true,"remove":true,"remove_locks":true,"resource_duplicate":true,"resource_quick_create":true,"resource_quick_update":true,"resource_tree":true,"resourcegroup_delete":true,"resourcegroup_edit":true,"resourcegroup_new":true,"resourcegroup_resource_edit":true,"resourcegroup_resource_list":true,"resourcegroup_save":true,"resourcegroup_view":true,"save":true,"save_category":true,"save_chunk":true,"save_context":true,"save_document":true,"save_plugin":true,"save_propertyset":true,"save_role":true,"save_snippet":true,"save_template":true,"save_tv":true,"save_user":true,"search":true,"set_sudo":true,"settings":true,"source_delete":true,"source_edit":true,"source_save":true,"source_view":true,"sources":true,"steal_locks":true,"tree_show_element_ids":true,"tree_show_resource_ids":true,"undelete_document":true,"unlock_element_properties":true,"unpublish_document":true,"usergroup_delete":true,"usergroup_edit":true,"usergroup_new":true,"usergroup_save":true,"usergroup_user_edit":true,"usergroup_user_list":true,"usergroup_view":true,"view":true,"view_category":true,"view_chunk":true,"view_context":true,"view_document":true,"view_element":true,"view_eventlog":true,"view_offline":true,"view_plugin":true,"view_propertyset":true,"view_role":true,"view_snippet":true,"view_sysinfo":true,"view_template":true,"view_tv":true,"view_unpublished":true,"view_user":true,"workspaces":true}', 'permissions'),
	(3, 'Load Only', 'policy_load_only_desc', 0, 3, '', '{"load":true}', 'permissions'),
	(4, 'Load, List and View', 'policy_load_list_and_view_desc', 0, 3, '', '{"load":true,"list":true,"view":true}', 'permissions'),
	(5, 'Object', 'policy_object_desc', 0, 3, '', '{"load":true,"list":true,"view":true,"save":true,"remove":true}', 'permissions'),
	(6, 'Element', 'policy_element_desc', 0, 4, '', '{"add_children":true,"create":true,"delete":true,"list":true,"load":true,"remove":true,"save":true,"view":true,"copy":true}', 'permissions'),
	(7, 'Content Editor', 'policy_content_editor_desc', 0, 1, '', '{"change_profile":true,"class_map":true,"countries":true,"delete_document":true,"delete_static_resource":true,"delete_symlink":true,"delete_weblink":true,"edit_document":true,"edit_static_resource":true,"edit_symlink":true,"edit_weblink":true,"frames":true,"help":true,"home":true,"language":true,"list":true,"load":true,"logout":true,"menu_reports":true,"menu_site":true,"menu_support":true,"menu_tools":true,"menu_user":true,"new_document":true,"new_static_resource":true,"new_symlink":true,"new_weblink":true,"resource_duplicate":true,"resource_tree":true,"save_document":true,"source_view":true,"tree_show_resource_ids":true,"view":true,"view_document":true,"view_template":true}', 'permissions'),
	(8, 'Media Source Admin', 'policy_media_source_admin_desc', 0, 5, '', '{"create":true,"copy":true,"load":true,"list":true,"save":true,"remove":true,"view":true}', 'permissions'),
	(9, 'Media Source User', 'policy_media_source_user_desc', 0, 5, '', '{"load":true,"list":true,"view":true}', 'permissions'),
	(10, 'Developer', 'policy_developer_desc', 0, 1, '', '{"about":true,"change_password":true,"change_profile":true,"charsets":true,"class_map":true,"components":true,"content_types":true,"countries":true,"create":true,"credits":true,"customize_forms":true,"dashboards":true,"database":true,"delete_category":true,"delete_chunk":true,"delete_context":true,"delete_document":true,"delete_eventlog":true,"delete_plugin":true,"delete_propertyset":true,"delete_role":true,"delete_snippet":true,"delete_template":true,"delete_tv":true,"delete_user":true,"directory_chmod":true,"directory_create":true,"directory_list":true,"directory_remove":true,"directory_update":true,"edit_category":true,"edit_chunk":true,"edit_context":true,"edit_document":true,"edit_locked":true,"edit_plugin":true,"edit_propertyset":true,"edit_role":true,"edit_snippet":true,"edit_static_resource":true,"edit_symlink":true,"edit_template":true,"edit_tv":true,"edit_user":true,"edit_weblink":true,"element_tree":true,"empty_cache":true,"error_log_erase":true,"error_log_view":true,"export_static":true,"file_create":true,"file_list":true,"file_manager":true,"file_remove":true,"file_tree":true,"file_unpack":true,"file_update":true,"file_upload":true,"file_view":true,"frames":true,"help":true,"home":true,"language":true,"languages":true,"lexicons":true,"list":true,"load":true,"logout":true,"mgr_log_view":true,"mgr_log_erase":true,"menu_reports":true,"menu_site":true,"menu_support":true,"menu_system":true,"menu_tools":true,"menu_user":true,"menus":true,"messages":true,"namespaces":true,"new_category":true,"new_chunk":true,"new_context":true,"new_document":true,"new_document_in_root":true,"new_plugin":true,"new_propertyset":true,"new_role":true,"new_snippet":true,"new_static_resource":true,"new_symlink":true,"new_template":true,"new_tv":true,"new_user":true,"new_weblink":true,"packages":true,"property_sets":true,"providers":true,"publish_document":true,"purge_deleted":true,"remove":true,"resource_duplicate":true,"resource_quick_create":true,"resource_quick_update":true,"resource_tree":true,"save":true,"save_category":true,"save_chunk":true,"save_context":true,"save_document":true,"save_plugin":true,"save_propertyset":true,"save_snippet":true,"save_template":true,"save_tv":true,"save_user":true,"search":true,"settings":true,"source_delete":true,"source_edit":true,"source_save":true,"source_view":true,"sources":true,"tree_show_element_ids":true,"tree_show_resource_ids":true,"undelete_document":true,"unlock_element_properties":true,"unpublish_document":true,"view":true,"view_category":true,"view_chunk":true,"view_context":true,"view_document":true,"view_element":true,"view_eventlog":true,"view_offline":true,"view_plugin":true,"view_propertyset":true,"view_role":true,"view_snippet":true,"view_sysinfo":true,"view_template":true,"view_tv":true,"view_unpublished":true,"view_user":true,"workspaces":true}', 'permissions'),
	(11, 'Context', 'policy_context_desc', 0, 6, '', '{"load":true,"list":true,"view":true,"save":true,"remove":true,"copy":true,"view_unpublished":true}', 'permissions'),
	(12, 'Hidden Namespace', 'policy_hidden_namespace_desc', 0, 7, '', '{"load":false,"list":false,"view":true}', 'permissions'),
	(15, 'Load Only Published', '', 0, 9, '', '{"load":true,"view_unpublished":false,"formit":true,"formit_encryptions":false}', 'permissions');
/*!40000 ALTER TABLE `modx_access_policies` ENABLE KEYS */;

-- Dumping structure for table petukhov.modx_access_policy_templates
CREATE TABLE IF NOT EXISTS `modx_access_policy_templates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `template_group` int(10) unsigned NOT NULL DEFAULT 0,
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` mediumtext DEFAULT NULL,
  `lexicon` varchar(255) NOT NULL DEFAULT 'permissions',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table petukhov.modx_access_policy_templates: 8 rows
/*!40000 ALTER TABLE `modx_access_policy_templates` DISABLE KEYS */;
INSERT INTO `modx_access_policy_templates` (`id`, `template_group`, `name`, `description`, `lexicon`) VALUES
	(1, 1, 'AdministratorTemplate', 'policy_template_administrator_desc', 'permissions'),
	(2, 3, 'ResourceTemplate', 'policy_template_resource_desc', 'permissions'),
	(3, 2, 'ObjectTemplate', 'policy_template_object_desc', 'permissions'),
	(4, 4, 'ElementTemplate', 'policy_template_element_desc', 'permissions'),
	(5, 5, 'MediaSourceTemplate', 'policy_template_mediasource_desc', 'permissions'),
	(6, 7, 'ContextTemplate', 'policy_template_context_desc', 'permissions'),
	(7, 6, 'NamespaceTemplate', 'policy_template_namespace_desc', 'permissions'),
	(9, 2, 'Load Only Published', '', 'permissions');
/*!40000 ALTER TABLE `modx_access_policy_templates` ENABLE KEYS */;

-- Dumping structure for table petukhov.modx_access_policy_template_groups
CREATE TABLE IF NOT EXISTS `modx_access_policy_template_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` mediumtext DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table petukhov.modx_access_policy_template_groups: 7 rows
/*!40000 ALTER TABLE `modx_access_policy_template_groups` DISABLE KEYS */;
INSERT INTO `modx_access_policy_template_groups` (`id`, `name`, `description`) VALUES
	(1, 'Administrator', 'policy_template_group_administrator_desc'),
	(2, 'Object', 'policy_template_group_object_desc'),
	(3, 'Resource', 'policy_template_group_resource_desc'),
	(4, 'Element', 'policy_template_group_element_desc'),
	(5, 'MediaSource', 'policy_template_group_mediasource_desc'),
	(6, 'Namespace', 'policy_template_group_namespace_desc'),
	(7, 'Context', 'policy_template_group_context_desc');
/*!40000 ALTER TABLE `modx_access_policy_template_groups` ENABLE KEYS */;

-- Dumping structure for table petukhov.modx_access_resources
CREATE TABLE IF NOT EXISTS `modx_access_resources` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `target` varchar(100) NOT NULL DEFAULT '',
  `principal_class` varchar(100) NOT NULL DEFAULT 'MODX\\Revolution\\modPrincipal',
  `principal` int(10) unsigned NOT NULL DEFAULT 0,
  `authority` int(10) unsigned NOT NULL DEFAULT 9999,
  `policy` int(10) unsigned NOT NULL DEFAULT 0,
  `context_key` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `target` (`target`),
  KEY `principal_class` (`principal_class`),
  KEY `principal` (`principal`),
  KEY `authority` (`authority`),
  KEY `policy` (`policy`),
  KEY `context_key` (`context_key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table petukhov.modx_access_resources: 0 rows
/*!40000 ALTER TABLE `modx_access_resources` DISABLE KEYS */;
/*!40000 ALTER TABLE `modx_access_resources` ENABLE KEYS */;

-- Dumping structure for table petukhov.modx_access_resource_groups
CREATE TABLE IF NOT EXISTS `modx_access_resource_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `target` varchar(100) NOT NULL DEFAULT '',
  `principal_class` varchar(100) NOT NULL DEFAULT 'MODX\\Revolution\\modPrincipal',
  `principal` int(10) unsigned NOT NULL DEFAULT 0,
  `authority` int(10) unsigned NOT NULL DEFAULT 9999,
  `policy` int(10) unsigned NOT NULL DEFAULT 0,
  `context_key` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `target` (`target`),
  KEY `principal_class` (`principal_class`,`target`,`principal`,`authority`),
  KEY `principal` (`principal`),
  KEY `authority` (`authority`),
  KEY `policy` (`policy`),
  KEY `context_key` (`context_key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table petukhov.modx_access_resource_groups: 0 rows
/*!40000 ALTER TABLE `modx_access_resource_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `modx_access_resource_groups` ENABLE KEYS */;

-- Dumping structure for table petukhov.modx_access_templatevars
CREATE TABLE IF NOT EXISTS `modx_access_templatevars` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `target` varchar(100) NOT NULL DEFAULT '',
  `principal_class` varchar(100) NOT NULL DEFAULT 'MODX\\Revolution\\modPrincipal',
  `principal` int(10) unsigned NOT NULL DEFAULT 0,
  `authority` int(10) unsigned NOT NULL DEFAULT 9999,
  `policy` int(10) unsigned NOT NULL DEFAULT 0,
  `context_key` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `target` (`target`),
  KEY `principal_class` (`principal_class`),
  KEY `principal` (`principal`),
  KEY `authority` (`authority`),
  KEY `policy` (`policy`),
  KEY `context_key` (`context_key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table petukhov.modx_access_templatevars: 0 rows
/*!40000 ALTER TABLE `modx_access_templatevars` DISABLE KEYS */;
/*!40000 ALTER TABLE `modx_access_templatevars` ENABLE KEYS */;

-- Dumping structure for table petukhov.modx_actiondom
CREATE TABLE IF NOT EXISTS `modx_actiondom` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `set` int(11) NOT NULL DEFAULT 0,
  `action` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` text DEFAULT NULL,
  `xtype` varchar(100) NOT NULL DEFAULT '',
  `container` varchar(255) NOT NULL DEFAULT '',
  `rule` varchar(100) NOT NULL DEFAULT '',
  `value` text NOT NULL,
  `constraint` varchar(255) NOT NULL DEFAULT '',
  `constraint_field` varchar(100) NOT NULL DEFAULT '',
  `constraint_class` varchar(100) NOT NULL DEFAULT '',
  `active` tinyint(1) unsigned NOT NULL DEFAULT 1,
  `for_parent` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `rank` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `set` (`set`),
  KEY `action` (`action`),
  KEY `name` (`name`),
  KEY `active` (`active`),
  KEY `for_parent` (`for_parent`),
  KEY `rank` (`rank`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table petukhov.modx_actiondom: 0 rows
/*!40000 ALTER TABLE `modx_actiondom` DISABLE KEYS */;
/*!40000 ALTER TABLE `modx_actiondom` ENABLE KEYS */;

-- Dumping structure for table petukhov.modx_actions
CREATE TABLE IF NOT EXISTS `modx_actions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `namespace` varchar(100) NOT NULL DEFAULT 'core',
  `controller` varchar(255) NOT NULL,
  `haslayout` tinyint(1) unsigned NOT NULL DEFAULT 1,
  `lang_topics` text NOT NULL,
  `assets` text NOT NULL,
  `help_url` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `namespace` (`namespace`),
  KEY `controller` (`controller`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table petukhov.modx_actions: 1 rows
/*!40000 ALTER TABLE `modx_actions` DISABLE KEYS */;
INSERT INTO `modx_actions` (`id`, `namespace`, `controller`, `haslayout`, `lang_topics`, `assets`, `help_url`) VALUES
	(1, 'migx', 'index', 0, 'example:default', '', '');
/*!40000 ALTER TABLE `modx_actions` ENABLE KEYS */;

-- Dumping structure for table petukhov.modx_actions_fields
CREATE TABLE IF NOT EXISTS `modx_actions_fields` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `action` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `type` varchar(100) NOT NULL DEFAULT 'field',
  `tab` varchar(255) NOT NULL DEFAULT '',
  `form` varchar(255) NOT NULL DEFAULT '',
  `other` varchar(255) NOT NULL DEFAULT '',
  `rank` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `action` (`action`),
  KEY `type` (`type`),
  KEY `tab` (`tab`)
) ENGINE=MyISAM AUTO_INCREMENT=245 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table petukhov.modx_actions_fields: 88 rows
/*!40000 ALTER TABLE `modx_actions_fields` DISABLE KEYS */;
INSERT INTO `modx_actions_fields` (`id`, `action`, `name`, `type`, `tab`, `form`, `other`, `rank`) VALUES
	(221, 'resource/create', 'menutitle', 'field', 'modx-resource-main-right-bottom', 'modx-panel-resource', '', 1),
	(220, 'resource/create', 'hidemenu', 'field', 'modx-resource-main-right-bottom', 'modx-panel-resource', '', 0),
	(219, 'resource/create', 'modx-resource-main-right-bottom', 'tab', '', 'modx-panel-resource', '', 6),
	(218, 'resource/create', 'template', 'field', 'modx-resource-main-right-middle', 'modx-panel-resource', '', 0),
	(217, 'resource/create', 'modx-resource-main-right-middle', 'tab', '', 'modx-panel-resource', '', 5),
	(216, 'resource/create', 'unpub_date', 'field', 'modx-resource-main-right-top', 'modx-panel-resource', '', 4),
	(215, 'resource/create', 'pub_date', 'field', 'modx-resource-main-right-top', 'modx-panel-resource', '', 3),
	(214, 'resource/create', 'publishedon', 'field', 'modx-resource-main-right-top', 'modx-panel-resource', '', 2),
	(213, 'resource/create', 'deleted', 'field', 'modx-resource-main-right-top', 'modx-panel-resource', '', 1),
	(212, 'resource/create', 'published', 'field', 'modx-resource-main-right-top', 'modx-panel-resource', '', 0),
	(211, 'resource/create', 'modx-resource-main-right-top', 'tab', '', 'modx-panel-resource', '', 4),
	(210, 'resource/create', 'modx-resource-main-right', 'tab', '', 'modx-panel-resource', '', 3),
	(209, 'resource/create', 'modx-resource-content', 'field', 'modx-resource-content', 'modx-panel-resource', '', 0),
	(208, 'resource/create', 'introtext', 'field', 'modx-resource-main-left', 'modx-panel-resource', '', 5),
	(207, 'resource/create', 'description', 'field', 'modx-resource-main-left', 'modx-panel-resource', '', 4),
	(206, 'resource/create', 'longtitle', 'field', 'modx-resource-main-left', 'modx-panel-resource', '', 3),
	(205, 'resource/create', 'alias', 'field', 'modx-resource-main-left', 'modx-panel-resource', '', 2),
	(204, 'resource/create', 'pagetitle', 'field', 'modx-resource-main-left', 'modx-panel-resource', '', 1),
	(203, 'resource/create', 'id', 'field', 'modx-resource-main-left', 'modx-panel-resource', '', 0),
	(202, 'resource/create', 'modx-resource-main-left', 'tab', '', 'modx-panel-resource', '', 1),
	(201, 'resource/create', 'modx-resource-settings', 'tab', '', 'modx-panel-resource', '', 0),
	(200, 'resource/update', 'modx-resource-access-permissions', 'tab', '', 'modx-panel-resource', '', 13),
	(199, 'resource/update', 'modx-panel-resource-tv', 'tab', '', 'modx-panel-resource', 'tv', 12),
	(198, 'resource/update', 'syncsite', 'field', 'modx-page-settings-box-right', 'modx-panel-resource', '', 3),
	(197, 'resource/update', 'searchable', 'field', 'modx-page-settings-box-right', 'modx-panel-resource', '', 2),
	(196, 'resource/update', 'cacheable', 'field', 'modx-page-settings-box-right', 'modx-panel-resource', '', 1),
	(195, 'resource/update', 'richtext', 'field', 'modx-page-settings-box-right', 'modx-panel-resource', '', 0),
	(194, 'resource/update', 'modx-page-settings-box-right', 'tab', '', 'modx-panel-resource', '', 11),
	(193, 'resource/update', 'uri', 'field', 'modx-page-settings-box-left', 'modx-panel-resource', '', 5),
	(192, 'resource/update', 'uri_override', 'field', 'modx-page-settings-box-left', 'modx-panel-resource', '', 4),
	(191, 'resource/update', 'alias_visible', 'field', 'modx-page-settings-box-left', 'modx-panel-resource', '', 3),
	(190, 'resource/update', 'hide_children_in_tree', 'field', 'modx-page-settings-box-left', 'modx-panel-resource', '', 2),
	(189, 'resource/update', 'show_in_tree', 'field', 'modx-page-settings-box-left', 'modx-panel-resource', '', 1),
	(188, 'resource/update', 'isfolder', 'field', 'modx-page-settings-box-left', 'modx-panel-resource', '', 0),
	(187, 'resource/update', 'modx-page-settings-box-left', 'tab', '', 'modx-panel-resource', '', 10),
	(186, 'resource/update', 'content_dispo', 'field', 'modx-page-settings-right', 'modx-panel-resource', '', 1),
	(185, 'resource/update', 'parent-cmb', 'field', 'modx-page-settings-right', 'modx-panel-resource', '', 0),
	(184, 'resource/update', 'modx-page-settings-right', 'tab', '', 'modx-panel-resource', '', 9),
	(183, 'resource/update', 'content_type', 'field', 'modx-page-settings-left', 'modx-panel-resource', '', 1),
	(182, 'resource/update', 'class_key', 'field', 'modx-page-settings-left', 'modx-panel-resource', '', 0),
	(181, 'resource/update', 'modx-page-settings-left', 'tab', '', 'modx-panel-resource', '', 8),
	(180, 'resource/update', 'modx-page-settings', 'tab', '', 'modx-panel-resource', '', 7),
	(179, 'resource/update', 'menuindex', 'field', 'modx-resource-main-right-bottom', 'modx-panel-resource', '', 3),
	(178, 'resource/update', 'link_attributes', 'field', 'modx-resource-main-right-bottom', 'modx-panel-resource', '', 2),
	(177, 'resource/update', 'menutitle', 'field', 'modx-resource-main-right-bottom', 'modx-panel-resource', '', 1),
	(176, 'resource/update', 'hidemenu', 'field', 'modx-resource-main-right-bottom', 'modx-panel-resource', '', 0),
	(175, 'resource/update', 'modx-resource-main-right-bottom', 'tab', '', 'modx-panel-resource', '', 6),
	(174, 'resource/update', 'template', 'field', 'modx-resource-main-right-middle', 'modx-panel-resource', '', 0),
	(173, 'resource/update', 'modx-resource-main-right-middle', 'tab', '', 'modx-panel-resource', '', 5),
	(172, 'resource/update', 'unpub_date', 'field', 'modx-resource-main-right-top', 'modx-panel-resource', '', 4),
	(171, 'resource/update', 'pub_date', 'field', 'modx-resource-main-right-top', 'modx-panel-resource', '', 3),
	(170, 'resource/update', 'publishedon', 'field', 'modx-resource-main-right-top', 'modx-panel-resource', '', 2),
	(169, 'resource/update', 'deleted', 'field', 'modx-resource-main-right-top', 'modx-panel-resource', '', 1),
	(168, 'resource/update', 'published', 'field', 'modx-resource-main-right-top', 'modx-panel-resource', '', 0),
	(167, 'resource/update', 'modx-resource-main-right-top', 'tab', '', 'modx-panel-resource', '', 4),
	(166, 'resource/update', 'modx-resource-main-right', 'tab', '', 'modx-panel-resource', '', 3),
	(165, 'resource/update', 'modx-resource-content', 'field', 'modx-resource-content', 'modx-panel-resource', '', 0),
	(164, 'resource/update', 'introtext', 'field', 'modx-resource-main-left', 'modx-panel-resource', '', 5),
	(163, 'resource/update', 'description', 'field', 'modx-resource-main-left', 'modx-panel-resource', '', 4),
	(162, 'resource/update', 'longtitle', 'field', 'modx-resource-main-left', 'modx-panel-resource', '', 3),
	(161, 'resource/update', 'alias', 'field', 'modx-resource-main-left', 'modx-panel-resource', '', 2),
	(160, 'resource/update', 'pagetitle', 'field', 'modx-resource-main-left', 'modx-panel-resource', '', 1),
	(159, 'resource/update', 'id', 'field', 'modx-resource-main-left', 'modx-panel-resource', '', 0),
	(158, 'resource/update', 'modx-resource-main-left', 'tab', '', 'modx-panel-resource', '', 1),
	(157, 'resource/update', 'modx-resource-settings', 'tab', '', 'modx-panel-resource', '', 0),
	(222, 'resource/create', 'link_attributes', 'field', 'modx-resource-main-right-bottom', 'modx-panel-resource', '', 2),
	(223, 'resource/create', 'menuindex', 'field', 'modx-resource-main-right-bottom', 'modx-panel-resource', '', 3),
	(224, 'resource/create', 'modx-page-settings', 'tab', '', 'modx-panel-resource', '', 7),
	(225, 'resource/create', 'modx-page-settings-left', 'tab', '', 'modx-panel-resource', '', 8),
	(226, 'resource/create', 'class_key', 'field', 'modx-page-settings-left', 'modx-panel-resource', '', 0),
	(227, 'resource/create', 'content_type', 'field', 'modx-page-settings-left', 'modx-panel-resource', '', 1),
	(228, 'resource/create', 'modx-page-settings-right', 'tab', '', 'modx-panel-resource', '', 9),
	(229, 'resource/create', 'parent-cmb', 'field', 'modx-page-settings-right', 'modx-panel-resource', '', 0),
	(230, 'resource/create', 'content_dispo', 'field', 'modx-page-settings-right', 'modx-panel-resource', '', 1),
	(231, 'resource/create', 'modx-page-settings-box-left', 'tab', '', 'modx-panel-resource', '', 10),
	(232, 'resource/create', 'isfolder', 'field', 'modx-page-settings-box-left', 'modx-panel-resource', '', 0),
	(233, 'resource/create', 'show_in_tree', 'field', 'modx-page-settings-box-left', 'modx-panel-resource', '', 1),
	(234, 'resource/create', 'hide_children_in_tree', 'field', 'modx-page-settings-box-left', 'modx-panel-resource', '', 2),
	(235, 'resource/create', 'alias_visible', 'field', 'modx-page-settings-box-left', 'modx-panel-resource', '', 3),
	(236, 'resource/create', 'uri_override', 'field', 'modx-page-settings-box-left', 'modx-panel-resource', '', 4),
	(237, 'resource/create', 'uri', 'field', 'modx-page-settings-box-left', 'modx-panel-resource', '', 5),
	(238, 'resource/create', 'modx-page-settings-box-right', 'tab', '', 'modx-panel-resource', '', 11),
	(239, 'resource/create', 'richtext', 'field', 'modx-page-settings-box-right', 'modx-panel-resource', '', 0),
	(240, 'resource/create', 'cacheable', 'field', 'modx-page-settings-box-right', 'modx-panel-resource', '', 1),
	(241, 'resource/create', 'searchable', 'field', 'modx-page-settings-box-right', 'modx-panel-resource', '', 2),
	(242, 'resource/create', 'syncsite', 'field', 'modx-page-settings-box-right', 'modx-panel-resource', '', 3),
	(243, 'resource/create', 'modx-panel-resource-tv', 'tab', '', 'modx-panel-resource', 'tv', 12),
	(244, 'resource/create', 'modx-resource-access-permissions', 'tab', '', 'modx-panel-resource', '', 13);
/*!40000 ALTER TABLE `modx_actions_fields` ENABLE KEYS */;

-- Dumping structure for table petukhov.modx_active_users
CREATE TABLE IF NOT EXISTS `modx_active_users` (
  `internalKey` int(9) NOT NULL DEFAULT 0,
  `username` varchar(50) NOT NULL DEFAULT '',
  `lasthit` int(20) NOT NULL DEFAULT 0,
  `id` int(10) DEFAULT NULL,
  `action` varchar(255) NOT NULL DEFAULT '',
  `ip` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`internalKey`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table petukhov.modx_active_users: 0 rows
/*!40000 ALTER TABLE `modx_active_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `modx_active_users` ENABLE KEYS */;

-- Dumping structure for table petukhov.modx_categories
CREATE TABLE IF NOT EXISTS `modx_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent` int(10) unsigned DEFAULT 0,
  `category` varchar(45) NOT NULL DEFAULT '',
  `rank` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `category` (`parent`,`category`),
  KEY `parent` (`parent`),
  KEY `rank` (`rank`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table petukhov.modx_categories: 5 rows
/*!40000 ALTER TABLE `modx_categories` DISABLE KEYS */;
INSERT INTO `modx_categories` (`id`, `parent`, `category`, `rank`) VALUES
	(1, 0, 'Common', 0),
	(2, 0, 'English', 0),
	(7, 0, 'MIGX', 0),
	(4, 0, 'Pictures', 0),
	(8, 0, 'TinyMCE Rich Text Editor', 0);
/*!40000 ALTER TABLE `modx_categories` ENABLE KEYS */;

-- Dumping structure for table petukhov.modx_categories_closure
CREATE TABLE IF NOT EXISTS `modx_categories_closure` (
  `ancestor` int(10) unsigned NOT NULL DEFAULT 0,
  `descendant` int(10) unsigned NOT NULL DEFAULT 0,
  `depth` int(10) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`ancestor`,`descendant`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table petukhov.modx_categories_closure: 10 rows
/*!40000 ALTER TABLE `modx_categories_closure` DISABLE KEYS */;
INSERT INTO `modx_categories_closure` (`ancestor`, `descendant`, `depth`) VALUES
	(1, 1, 0),
	(0, 1, 0),
	(2, 2, 0),
	(0, 2, 0),
	(0, 7, 0),
	(7, 7, 0),
	(4, 4, 0),
	(0, 4, 0),
	(8, 8, 0),
	(0, 8, 0);
/*!40000 ALTER TABLE `modx_categories_closure` ENABLE KEYS */;

-- Dumping structure for table petukhov.modx_class_map
CREATE TABLE IF NOT EXISTS `modx_class_map` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class` varchar(120) NOT NULL DEFAULT '',
  `parent_class` varchar(120) NOT NULL DEFAULT '',
  `name_field` varchar(255) NOT NULL DEFAULT 'name',
  `path` tinytext DEFAULT NULL,
  `lexicon` varchar(255) NOT NULL DEFAULT 'core:resource',
  PRIMARY KEY (`id`),
  UNIQUE KEY `class` (`class`),
  KEY `parent_class` (`parent_class`),
  KEY `name_field` (`name_field`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table petukhov.modx_class_map: 9 rows
/*!40000 ALTER TABLE `modx_class_map` DISABLE KEYS */;
INSERT INTO `modx_class_map` (`id`, `class`, `parent_class`, `name_field`, `path`, `lexicon`) VALUES
	(1, 'modDocument', 'modResource', 'pagetitle', '', 'core:resource'),
	(2, 'modWebLink', 'modResource', 'pagetitle', '', 'core:resource'),
	(3, 'modSymLink', 'modResource', 'pagetitle', '', 'core:resource'),
	(4, 'modStaticResource', 'modResource', 'pagetitle', '', 'core:resource'),
	(5, 'modTemplate', 'modElement', 'templatename', '', 'core:resource'),
	(6, 'modTemplateVar', 'modElement', 'name', '', 'core:resource'),
	(7, 'modChunk', 'modElement', 'name', '', 'core:resource'),
	(8, 'modSnippet', 'modElement', 'name', '', 'core:resource'),
	(9, 'modPlugin', 'modElement', 'name', '', 'core:resource');
/*!40000 ALTER TABLE `modx_class_map` ENABLE KEYS */;

-- Dumping structure for table petukhov.modx_content_type
CREATE TABLE IF NOT EXISTS `modx_content_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` tinytext DEFAULT NULL,
  `mime_type` tinytext DEFAULT NULL,
  `file_extensions` tinytext DEFAULT NULL,
  `headers` mediumtext DEFAULT NULL,
  `binary` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `icon` tinytext DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table petukhov.modx_content_type: 8 rows
/*!40000 ALTER TABLE `modx_content_type` DISABLE KEYS */;
INSERT INTO `modx_content_type` (`id`, `name`, `description`, `mime_type`, `file_extensions`, `headers`, `binary`, `icon`) VALUES
	(1, 'HTML', 'HTML content', 'text/html', '.html', NULL, 0, ''),
	(2, 'XML', 'XML content', 'text/xml', '.xml', NULL, 0, 'icon-xml'),
	(3, 'text', 'plain text content', 'text/plain', '.txt', NULL, 0, 'icon-txt'),
	(4, 'CSS', 'CSS content', 'text/css', '.css', NULL, 0, 'icon-css'),
	(5, 'javascript', 'javascript content', 'text/javascript', '.js', NULL, 0, 'icon-js'),
	(6, 'RSS', 'For RSS feeds', 'application/rss+xml', '.rss', NULL, 0, 'icon-rss'),
	(7, 'JSON', 'JSON', 'application/json', '.json', NULL, 0, 'icon-json'),
	(8, 'PDF', 'PDF Files', 'application/pdf', '.pdf', NULL, 1, 'icon-pdf');
/*!40000 ALTER TABLE `modx_content_type` ENABLE KEYS */;

-- Dumping structure for table petukhov.modx_context
CREATE TABLE IF NOT EXISTS `modx_context` (
  `key` varchar(100) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` tinytext DEFAULT NULL,
  `rank` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`key`),
  KEY `name` (`name`),
  KEY `rank` (`rank`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table petukhov.modx_context: 2 rows
/*!40000 ALTER TABLE `modx_context` DISABLE KEYS */;
INSERT INTO `modx_context` (`key`, `name`, `description`, `rank`) VALUES
	('web', 'Website', 'The default front-end context for your web site.', 0),
	('mgr', 'Manager', 'The default manager or administration context for content management activity.', 0);
/*!40000 ALTER TABLE `modx_context` ENABLE KEYS */;

-- Dumping structure for table petukhov.modx_context_resource
CREATE TABLE IF NOT EXISTS `modx_context_resource` (
  `context_key` varchar(255) NOT NULL,
  `resource` int(11) unsigned NOT NULL,
  PRIMARY KEY (`context_key`,`resource`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table petukhov.modx_context_resource: 0 rows
/*!40000 ALTER TABLE `modx_context_resource` DISABLE KEYS */;
/*!40000 ALTER TABLE `modx_context_resource` ENABLE KEYS */;

-- Dumping structure for table petukhov.modx_context_setting
CREATE TABLE IF NOT EXISTS `modx_context_setting` (
  `context_key` varchar(255) NOT NULL,
  `key` varchar(50) NOT NULL,
  `value` mediumtext DEFAULT NULL,
  `xtype` varchar(75) NOT NULL DEFAULT 'textfield',
  `namespace` varchar(40) NOT NULL DEFAULT 'core',
  `area` varchar(255) NOT NULL DEFAULT '',
  `editedon` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`context_key`,`key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table petukhov.modx_context_setting: 6 rows
/*!40000 ALTER TABLE `modx_context_setting` DISABLE KEYS */;
INSERT INTO `modx_context_setting` (`context_key`, `key`, `value`, `xtype`, `namespace`, `area`, `editedon`) VALUES
	('mgr', 'allow_tags_in_post', '1', 'combo-boolean', 'core', 'system', NULL),
	('mgr', 'modRequest.class', 'MODX\\Revolution\\modManagerRequest', 'textfield', 'core', 'system', '2022-10-04 19:32:09'),
	('web', 'portfolio_id', '9', 'textfield', 'core', '', NULL),
	('web', 'english_id', '2', 'textfield', 'core', '', NULL),
	('web', 'code_samples_id', '14', 'textfield', 'core', '', NULL),
	('web', 'main_page_id', '1', 'textfield', 'core', '', NULL);
/*!40000 ALTER TABLE `modx_context_setting` ENABLE KEYS */;

-- Dumping structure for table petukhov.modx_dashboard
CREATE TABLE IF NOT EXISTS `modx_dashboard` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` text DEFAULT NULL,
  `hide_trees` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `customizable` tinyint(1) unsigned NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  KEY `name` (`name`),
  KEY `hide_trees` (`hide_trees`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table petukhov.modx_dashboard: 1 rows
/*!40000 ALTER TABLE `modx_dashboard` DISABLE KEYS */;
INSERT INTO `modx_dashboard` (`id`, `name`, `description`, `hide_trees`, `customizable`) VALUES
	(1, 'Default', '', 0, 1);
/*!40000 ALTER TABLE `modx_dashboard` ENABLE KEYS */;

-- Dumping structure for table petukhov.modx_dashboard_widget
CREATE TABLE IF NOT EXISTS `modx_dashboard_widget` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` text DEFAULT NULL,
  `type` varchar(100) NOT NULL,
  `content` mediumtext DEFAULT NULL,
  `namespace` varchar(255) NOT NULL DEFAULT '',
  `lexicon` varchar(255) NOT NULL DEFAULT 'core:dashboards',
  `size` varchar(255) NOT NULL DEFAULT 'half',
  `permission` varchar(255) NOT NULL DEFAULT '',
  `properties` text DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`),
  KEY `type` (`type`),
  KEY `namespace` (`namespace`),
  KEY `lexicon` (`lexicon`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table petukhov.modx_dashboard_widget: 7 rows
/*!40000 ALTER TABLE `modx_dashboard_widget` DISABLE KEYS */;
INSERT INTO `modx_dashboard_widget` (`id`, `name`, `description`, `type`, `content`, `namespace`, `lexicon`, `size`, `permission`, `properties`) VALUES
	(1, 'w_newsfeed', 'w_newsfeed_desc', 'file', '[[++manager_path]]controllers/default/dashboard/widget.modx-news.php', 'core', 'core:dashboards', 'half', '', NULL),
	(2, 'w_securityfeed', 'w_securityfeed_desc', 'file', '[[++manager_path]]controllers/default/dashboard/widget.modx-security.php', 'core', 'core:dashboards', 'half', '', NULL),
	(3, 'w_whosonline', 'w_whosonline_desc', 'file', '[[++manager_path]]controllers/default/dashboard/widget.grid-online.php', 'core', 'core:dashboards', 'half', '', NULL),
	(4, 'w_recentlyeditedresources', 'w_recentlyeditedresources_desc', 'file', '[[++manager_path]]controllers/default/dashboard/widget.grid-rer.php', 'core', 'core:dashboards', 'half', 'view_document', NULL),
	(5, 'w_configcheck', 'w_configcheck_desc', 'file', '[[++manager_path]]controllers/default/dashboard/widget.configcheck.php', 'core', 'core:dashboards', 'full', '', NULL),
	(6, 'w_buttons', 'w_buttons_desc', 'file', '[[++manager_path]]controllers/default/dashboard/widget.buttons.php', 'core', 'core:dashboards', 'full', '', '{"create-resource":{"link":"[[++manager_url]]?a=resource\\/create","icon":"file-o","title":"[[%action_new_resource]]","description":"[[%action_new_resource_desc]]"},"view-site":{"link":"[[++site_url]]","icon":"eye","title":"[[%action_view_website]]","description":"[[%action_view_website_desc]]","target":"_blank"},"advanced-search":{"link":"[[++manager_url]]?a=search","icon":"search","title":"[[%action_advanced_search]]","description":"[[%action_advanced_search_desc]]"},"manage-users":{"link":"[[++manager_url]]?a=security\\/user","icon":"user","title":"[[%action_manage_users]]","description":"[[%action_manage_users_desc]]"}}'),
	(7, 'w_updates', 'w_updates_desc', 'file', '[[++manager_path]]controllers/default/dashboard/widget.updates.php', 'core', 'core:dashboards', 'one-third', 'workspaces', NULL);
/*!40000 ALTER TABLE `modx_dashboard_widget` ENABLE KEYS */;

-- Dumping structure for table petukhov.modx_dashboard_widget_placement
CREATE TABLE IF NOT EXISTS `modx_dashboard_widget_placement` (
  `dashboard` int(10) unsigned NOT NULL DEFAULT 0,
  `widget` int(10) unsigned NOT NULL DEFAULT 0,
  `rank` int(10) unsigned NOT NULL DEFAULT 0,
  `user` int(10) unsigned NOT NULL DEFAULT 0,
  `size` varchar(255) NOT NULL DEFAULT 'half',
  PRIMARY KEY (`user`,`dashboard`,`widget`),
  KEY `rank` (`rank`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table petukhov.modx_dashboard_widget_placement: 14 rows
/*!40000 ALTER TABLE `modx_dashboard_widget_placement` DISABLE KEYS */;
INSERT INTO `modx_dashboard_widget_placement` (`dashboard`, `widget`, `rank`, `user`, `size`) VALUES
	(1, 5, 0, 0, 'half'),
	(1, 1, 2, 0, 'half'),
	(1, 2, 3, 0, 'half'),
	(1, 3, 4, 0, 'half'),
	(1, 4, 5, 0, 'half'),
	(1, 6, 1, 0, 'full'),
	(1, 7, 6, 0, 'one-third'),
	(1, 5, 0, 1, 'half'),
	(1, 1, 2, 1, 'half'),
	(1, 2, 3, 1, 'half'),
	(1, 3, 4, 1, 'half'),
	(1, 4, 5, 1, 'half'),
	(1, 6, 1, 1, 'full'),
	(1, 7, 6, 1, 'one-third');
/*!40000 ALTER TABLE `modx_dashboard_widget_placement` ENABLE KEYS */;

-- Dumping structure for table petukhov.modx_deprecated_call
CREATE TABLE IF NOT EXISTS `modx_deprecated_call` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `method` int(11) unsigned NOT NULL DEFAULT 0,
  `call_count` int(11) unsigned NOT NULL DEFAULT 0,
  `caller` varchar(191) NOT NULL DEFAULT '',
  `caller_file` varchar(191) NOT NULL DEFAULT '',
  `caller_line` int(11) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `method` (`method`),
  KEY `call_count` (`call_count`),
  KEY `caller` (`caller`),
  KEY `caller_file` (`caller_file`),
  KEY `caller_line` (`caller_line`)
) ENGINE=InnoDB AUTO_INCREMENT=362 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

-- Dumping data for table petukhov.modx_deprecated_call: ~1 rows (approximately)
INSERT INTO `modx_deprecated_call` (`id`, `method`, `call_count`, `caller`, `caller_file`, `caller_line`) VALUES
	(364, 121, 10, 'MODX\\Revolution\\modX::_initSession', 'D:\\PHP websites\\home\\petukhov.local\\www\\core\\src\\Revolution\\modX.php', 2755);

-- Dumping structure for table petukhov.modx_deprecated_method
CREATE TABLE IF NOT EXISTS `modx_deprecated_method` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `definition` varchar(191) NOT NULL DEFAULT '',
  `since` varchar(191) NOT NULL DEFAULT '',
  `recommendation` varchar(1024) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `definition` (`definition`)
) ENGINE=InnoDB AUTO_INCREMENT=119 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

-- Dumping data for table petukhov.modx_deprecated_method: ~1 rows (approximately)
INSERT INTO `modx_deprecated_method` (`id`, `definition`, `since`, `recommendation`) VALUES
	(121, 'modSessionHandler', '3.0', 'Replace references to class modSessionHandler with MODX\\Revolution\\modSessionHandler to take advantage of PSR-4 autoloading.');

-- Dumping structure for table petukhov.modx_documentgroup_names
CREATE TABLE IF NOT EXISTS `modx_documentgroup_names` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `private_memgroup` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `private_webgroup` tinyint(1) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table petukhov.modx_documentgroup_names: 0 rows
/*!40000 ALTER TABLE `modx_documentgroup_names` DISABLE KEYS */;
/*!40000 ALTER TABLE `modx_documentgroup_names` ENABLE KEYS */;

-- Dumping structure for table petukhov.modx_document_groups
CREATE TABLE IF NOT EXISTS `modx_document_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `document_group` int(10) NOT NULL DEFAULT 0,
  `document` int(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `document_group` (`document_group`),
  KEY `document` (`document`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table petukhov.modx_document_groups: 0 rows
/*!40000 ALTER TABLE `modx_document_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `modx_document_groups` ENABLE KEYS */;

-- Dumping structure for table petukhov.modx_element_property_sets
CREATE TABLE IF NOT EXISTS `modx_element_property_sets` (
  `element` int(10) unsigned NOT NULL DEFAULT 0,
  `element_class` varchar(100) NOT NULL DEFAULT '',
  `property_set` int(10) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`element`,`element_class`,`property_set`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table petukhov.modx_element_property_sets: 0 rows
/*!40000 ALTER TABLE `modx_element_property_sets` DISABLE KEYS */;
/*!40000 ALTER TABLE `modx_element_property_sets` ENABLE KEYS */;

-- Dumping structure for table petukhov.modx_extension_packages
CREATE TABLE IF NOT EXISTS `modx_extension_packages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `namespace` varchar(40) NOT NULL DEFAULT 'core',
  `name` varchar(100) NOT NULL DEFAULT 'core',
  `path` text DEFAULT NULL,
  `table_prefix` varchar(255) NOT NULL DEFAULT '',
  `service_class` varchar(255) NOT NULL DEFAULT '',
  `service_name` varchar(255) NOT NULL DEFAULT '',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `namespace` (`namespace`),
  KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table petukhov.modx_extension_packages: 0 rows
/*!40000 ALTER TABLE `modx_extension_packages` DISABLE KEYS */;
/*!40000 ALTER TABLE `modx_extension_packages` ENABLE KEYS */;

-- Dumping structure for table petukhov.modx_fc_profiles
CREATE TABLE IF NOT EXISTS `modx_fc_profiles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 0,
  `rank` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `name` (`name`),
  KEY `rank` (`rank`),
  KEY `active` (`active`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table petukhov.modx_fc_profiles: 0 rows
/*!40000 ALTER TABLE `modx_fc_profiles` DISABLE KEYS */;
/*!40000 ALTER TABLE `modx_fc_profiles` ENABLE KEYS */;

-- Dumping structure for table petukhov.modx_fc_profiles_usergroups
CREATE TABLE IF NOT EXISTS `modx_fc_profiles_usergroups` (
  `usergroup` int(11) NOT NULL DEFAULT 0,
  `profile` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`usergroup`,`profile`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table petukhov.modx_fc_profiles_usergroups: 0 rows
/*!40000 ALTER TABLE `modx_fc_profiles_usergroups` DISABLE KEYS */;
/*!40000 ALTER TABLE `modx_fc_profiles_usergroups` ENABLE KEYS */;

-- Dumping structure for table petukhov.modx_fc_sets
CREATE TABLE IF NOT EXISTS `modx_fc_sets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `profile` int(11) NOT NULL DEFAULT 0,
  `action` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 0,
  `template` int(11) NOT NULL DEFAULT 0,
  `constraint` varchar(255) NOT NULL DEFAULT '',
  `constraint_field` varchar(100) NOT NULL DEFAULT '',
  `constraint_class` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `profile` (`profile`),
  KEY `action` (`action`),
  KEY `active` (`active`),
  KEY `template` (`template`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table petukhov.modx_fc_sets: 0 rows
/*!40000 ALTER TABLE `modx_fc_sets` DISABLE KEYS */;
/*!40000 ALTER TABLE `modx_fc_sets` ENABLE KEYS */;

-- Dumping structure for table petukhov.modx_formit_forms
CREATE TABLE IF NOT EXISTS `modx_formit_forms` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `form` varchar(255) NOT NULL DEFAULT '',
  `context_key` varchar(100) NOT NULL DEFAULT '',
  `values` text NOT NULL,
  `ip` varchar(100) NOT NULL DEFAULT '',
  `date` int(11) NOT NULL DEFAULT 0,
  `encrypted` tinyint(1) NOT NULL DEFAULT 0,
  `hash` varchar(255) NOT NULL DEFAULT '',
  `encryption_type` int(2) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

-- Dumping data for table petukhov.modx_formit_forms: 8 rows
/*!40000 ALTER TABLE `modx_formit_forms` DISABLE KEYS */;
INSERT INTO `modx_formit_forms` (`id`, `form`, `context_key`, `values`, `ip`, `date`, `encrypted`, `hash`, `encryption_type`) VALUES
	(1, 'form-1', 'web', '{"name":"\\u0415\\u0432\\u0433\\u0435\\u043d\\u0438\\u0439","email":"petukhoven@gmail.com","confirmemail":"","login":"","password":"","message":"fantastic!"}', '90.189.173.149', 1496318539, 0, 'c52d8de5da974537cae1f0767296c0ff', 1),
	(2, 'form-1', 'web', '{"name":"\\u0415\\u0432\\u0433\\u0435\\u043d\\u0438\\u0439","email":"petukhoven@gmail.com","confirmemail":"","login":"","password":"","message":"test","g-recaptcha-response":"03AOPBWq9wgEFGoGa09V65qHfT8nZ9xAIyAd8vP4xApCSfAmm-ZP_ixe8mj_fJvmcuS89il6nNjsLHtFEaSfz3rRXRhrplmV4l9oLUJrY_ov2Y4BPhQZ4lJo_WaehA-IWC5TNI9EJ2LO-WnmSJl83PigHimfxCtgCmASKB11Ea_NzR9ZuKr-9NgztdgwvbgtdSnivo7scqFoNGrzpnZuJqgwFVUyTnsoAmrsNSdfBjbH_NaoWBYkRFa2ZAY0cvmfLWV3vsCgA-Il_nhPsI7cqMG5VXadqLykOyeVPZcPBXLeOzlyYuwYrEGSSX4VUonRdv9h2b_IGD20RtBxsFOo4xPAb1xYuW5_hO5XFzcb08OIaYQYp6k9stkrC4yDpGRWc2yWU7zaAM35SyjK9zPshJOxcFa2TS0QeLlBz109ollQMJwkhYlA_uUa924J8E6IA_qEPfZfH0Xa5drVDVV8D4hm1LaJNgOl_k8A"}', '90.189.173.149', 1496319083, 0, 'addbac441d4146c0a0fb7d3972e9f3d3', 1),
	(3, 'form-1', 'web', '{"name":"\\u0415\\u0432\\u0433\\u0435\\u043d\\u0438\\u0439","email":"petukhoven@gmail.com","confirmemail":"","login":"","password":"","message":"\\u044d\\u0442\\u043e \\u043c\\u043e\\u0451 \\u0441\\u043e\\u043e\\u0431\\u0449\\u0435\\u043d\\u0438\\u0435","g-recaptcha-response":"03AOPBWq-2G3Trt1ga-YpEmnZ-B-xMZqwraV5xREK72udr4Lkyd7zMvYmXBvrJXTZiikOP6AbBSQTLLF1odyZY1OFxhBZxCpfQ1URN2ceEx1ji8YXIEz-l2Yt1wVqw4NHGJ5j6VMOFDVlOVntuekXASKKzIdxKXSJB4V3PYDe-nuXDkkts_WN2RrFAd6G-CpWhfFNUnWrm9sDMHv3eIIBP_C5bibIFWlpFDnbnBgmFJIOZThIs9GETcW9DGLW792QZxTPe60AUGOnijcoptTaumDdMENsz9FjBba6drFuhCLNEgUd7LSbFLchp9eR2E9D9RchGq_x819Goy_7bvwp8aLV0fDiXG-I5XMybRXuOOt0vzRBLkzIdgRdOgpZefqiyKU6Qe8K3vDX2mouK7Y_HGIcrkWgcJoGCCkQeyzu2OK4xmfK5iapGdmWVYktnaeDjE2BiORNtCZGZ9CMlz56ZRdjZjXYgic1aUw"}', '90.189.173.149', 1496319358, 0, '23c09cedae259d7e4588a4a279dc8277', 1),
	(4, 'form-2', 'web', '{"name":"Dmitrii","email":"petukhoven@gmail.com","confirmemail":"","login":"","password":"","message":"this is my message","g-recaptcha-response":"03AOPBWq-GYoxprvRJi3uQFkGMkdIYRZoZ7MwqsdONy0FiKv3TeM1BVjkNcf573ealtGBXFg_u72jXU3zBMgNrhfZHHX7Th7xcPpKPh_HGwz8KrzDkF-kTAqwwZFUk5BaM2bfacg1B1BiHubzHSK2ZGm2j7ug_7l8tFKo3TcHgmMXb8_H3ZXuGRf1UumheWYqTM1EgJmI5UXYdan4xMDEe1RD5GjR4wPAGE8lOmte1GuNDwM2jY9DZ0tNtIOljLGFe0uiMENikN5M3ATh2-jOnJISHHwgG3pZzgdViQZheeEfuNvcGb6VQWHBxPOeHWuFoK6w06XUPI2J7MIADJf10Eg5G2sWb6W-ORXT6x016KjFeTWz0MPzgyimBqw3Ez8QxOv2z3759km6fQvFH8evZQmVDeYKfoOXboR6bpB0GjYH2_8Q8b2_Cs4YBRSmWSJcT-TO0yLWeTVrf6TggHtmkAWP1NYW3iIEc2A"}', '90.189.173.149', 1496319385, 0, '75a4c00154e61bf20894510eacd22744', 1),
	(5, 'form-1', 'web', '{"name":"Maksim Tatarinov","email":"makctt@gmail.com","confirmemail":"","login":"","password":"","message":"\\u041e\\u043a","g-recaptcha-response":"03AEHxwuyOZihn1yEVgRTBaXzWW4S2uE8y1LqC_y_7IKRaiLRDUgeKlea6IcW5aa7TbuHVMFYtzUFo9oKX-McxFeCk_G2_lnb06Bx9Z9ON1YgBQqqGfAgm5lrWqCIJEki3EM3oP7OYbVYNFJmthzwtKuPSgzMd9uXhgZyECT2XTJcWB2KmUcloQ6qsXyVcrVH11IO2jHsr5YrqMD7Hjavd7fGF0kXqAt7GFDS4pUtLU_R_gHZX6df0ieqhJlSXyGV0xQ58P57d2FrQhCQq-7XFmTxp_DWygvRoxEPU_9iL-6BaqUG4bW4JbCJ3taHdJQ68Y-foruSvaIGGvMTXwIlngfYn8O8rb9gbXlbyMFpSZWoJPnJ4Y6884zJApMJYOQQGlVSb4zB4-ALaLGdg_2b2bC59pTOwNLn4Mc9_s8yxzT5nsy0pfoIQYRLVy9wf1kNEt0HqPMDfwXfnN3iAd4adJLhOUtfx7T7-_g"}', '178.66.62.127', 1499521307, 0, 'a7a6bd90b99c5ef8aa46d161f490b307', 1),
	(6, 'form-1', 'web', '{"name":"\\u0412\\u0438\\u043a\\u0442\\u043e\\u0440 \\u0413\\u0435\\u043e\\u0440\\u0433\\u0438\\u0435\\u0432\\u0438\\u0447","email":"maltsev1948@rambler.ru","confirmemail":"","login":"","password":"","message":"\\u0416\\u0435\\u043d\\u044f, \\u0434\\u043e\\u0431\\u0440\\u044b\\u0439 \\u0432\\u0435\\u0447\\u0435\\u0440! ","g-recaptcha-response":"03AOmkcwKvCxfc6hA6A_VqqKzzFWhGygraULQmDJTN0kD181RhVjt9b1D5xNO0Aw8jtEQi-T_lS1MvAZn3lXPw3HgHXOmb_TGfs64hVnKQs-Wy7yj_oydME0hwGr083FTbL0xqPTKFDv9kjWY8Y6ws7V2dGRw2P2IAo3GXGXbT-GVJdhUHmdbgK7uwtmkVbIed_MjwcKA-TyGIXCYlVWQxhTlMih4yReQ7oFIVvQc34pZpyDbyTj9o0AnUqrB-kx-4v9jfC8AHTj9p0ucWdXRcmzxdaVJqdWNszx95va_5UCbvZPHV2xHNRLX7oSe6JP_dqiNn6d6tIVx8UcFVqR_NxO94An4R_7FeRIguUYMXO78a5VdJDNutLydVjcaBHZwTxPpLDXwsnCjFlt5KCchSvoFPxe8qrGodiH-W5RijLDf4IQ9rLfiD1CDZR9KFOwRcqGRuDFM7cEbOH_SOvVVMMf0U_14mBR0NDA"}', '46.158.200.84', 1505571883, 0, '113cb9fab4bd909bec1b1e29e5a288c9', 1),
	(7, 'form-1', 'web', '{"name":"\\u0415\\u043b\\u0435\\u043d\\u0430","email":"Lena-rasdk@narod.ru","confirmemail":"","login":"","password":"","message":"\\u041c\\u0435\\u043d\\u044f \\u0438\\u043d\\u0442\\u0435\\u0440\\u0435\\u0441\\u0443\\u0435\\u0442 \\u0440\\u0430\\u0437\\u0440\\u0430\\u0431\\u043e\\u0442\\u043a\\u0430 \\u0441\\u0430\\u0439\\u0442\\u0430 \\u0430 \\u043a\\u043e\\u043d\\u043a\\u0440\\u0435\\u0442\\u043d\\u043e \\u0438\\u043d\\u0442\\u0435\\u0440\\u043d\\u0435\\u0442 \\u043c\\u0430\\u0433\\u0430\\u0437\\u0438\\u043d\\u0430. \\u041d\\u043e\\u0432\\u043e\\u0433\\u043e \\u0438\\u043b\\u0438 \\u043f\\u0435\\u0440\\u0435\\u0434\\u0435\\u043b\\u043a\\u0430 \\u0438\\u043d\\u0442\\u0435\\u0440\\u043d\\u0435\\u0442 \\u043c\\u0430\\u0433\\u0430\\u0437\\u0438\\u043d\\u0430 \\u043b\\u0438\\u043d\\u0437\\u043e\\u0447\\u043a\\u0438. \\u041c\\u043d\\u0435 \\u0441\\u043a\\u0430\\u0437\\u0430\\u043b\\u0438 \\u0447\\u0442\\u043e \\u044d\\u0442\\u043e \\u0432\\u044b \\u0435\\u0433\\u043e \\u0434\\u0435\\u043b\\u0430\\u043b\\u0438. ","g-recaptcha-response":"03AEMEkEkx4CG0PKPKJC3oAS0gxZUzwQbp-db61V4Qu7ufbUAM08bailBucgPyID7MQTuwHCte1adWz2gw5agcPiJD9Kt-0yAOOD0dKrnp-_93h-XQyk0olRWapUftYFgdHJfL-R3E0ngoVl0KxD6GhQ4b_z9XLCfifWQB99qv4MEkkriO8MlRHZafITv_00_xz1tTznTJGkhzVnk6r7ePjp6zojVBGEiK3kplDBiEhRKLfXax03YymXU3R0Stl-VLYwoUPU14iN-qJbpxhtaWL2Itg6T0Eu2ZAAdCZxs92sMW37YW887zMHRsrBcmZ1rparIPYVjglAmg04rfsSQmq1u7daGqdoTBrQ"}', '176.59.140.124', 1529847346, 0, '19a24334663763f9ced9305b787786aa', 1),
	(8, 'form-1', 'web', '{"name":"\\u0415\\u0432\\u0433\\u0435\\u043d\\u0438\\u0439 \\u041f\\u0435\\u0442\\u0443\\u0445\\u043e\\u0432","email":"petrovich2k@yandex.ru","confirmemail":"","login":"","password":"","message":"\\u0442\\u0435\\u0441\\u0442","g-recaptcha-response":"03AOLTBLSwX1-moUAs6iwsGwd7VYLt4bgdPbyH1qK9s5ByDU87l4STN_HoS7rBcWqiINDIGWQXxPcsTFzDqoX0qilSG3Pqy08Dx-55UsOTZhGweYkE2R94raFD_VQhU40iKMjvhrQ3a7OgbFKFjRHrH3BdZMSQNd91rPZBX8IA8QtMIcwylLEkXeLjADzmZXW9lDLD9fYRYa5eLm2TjVvKLgOnmDZXU6It8lkTRV_qYbY4AtSLwmXflXvcfbfmzPrd1bVsun40ige46D6T_pvs2s9hjaNAIGpvanzNv04LGsF6VbVzOMB4sUlX_npXE8SzNgr7rjxKY7B_"}', '185.134.74.5', 1561539436, 0, '4bce0bb2db36a671fdc86e3a26377938', 1);
/*!40000 ALTER TABLE `modx_formit_forms` ENABLE KEYS */;

-- Dumping structure for table petukhov.modx_lexicon_entries
CREATE TABLE IF NOT EXISTS `modx_lexicon_entries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `value` text NOT NULL,
  `topic` varchar(255) NOT NULL DEFAULT 'default',
  `namespace` varchar(40) NOT NULL DEFAULT 'core',
  `language` varchar(20) NOT NULL DEFAULT 'en',
  `createdon` datetime DEFAULT NULL,
  `editedon` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `name` (`name`),
  KEY `topic` (`topic`),
  KEY `namespace` (`namespace`),
  KEY `language` (`language`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table petukhov.modx_lexicon_entries: 1 rows
/*!40000 ALTER TABLE `modx_lexicon_entries` DISABLE KEYS */;
INSERT INTO `modx_lexicon_entries` (`id`, `name`, `value`, `topic`, `namespace`, `language`, `createdon`, `editedon`) VALUES
	(1, 'setting_main_page_id', 'main_page_id', 'setting', 'core', 'ru', '2021-11-28 20:45:41', NULL);
/*!40000 ALTER TABLE `modx_lexicon_entries` ENABLE KEYS */;

-- Dumping structure for table petukhov.modx_manager_log
CREATE TABLE IF NOT EXISTS `modx_manager_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user` int(10) unsigned NOT NULL DEFAULT 0,
  `occurred` datetime DEFAULT NULL,
  `action` varchar(100) NOT NULL DEFAULT '',
  `classKey` varchar(100) NOT NULL DEFAULT '',
  `item` varchar(255) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_occurred` (`user`,`occurred`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table petukhov.modx_manager_log: 0 rows
/*!40000 ALTER TABLE `modx_manager_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `modx_manager_log` ENABLE KEYS */;

-- Dumping structure for table petukhov.modx_media_sources
CREATE TABLE IF NOT EXISTS `modx_media_sources` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` text DEFAULT NULL,
  `class_key` varchar(100) NOT NULL DEFAULT 'MODX\\Revolution\\Sources\\modFileMediaSource',
  `properties` mediumtext DEFAULT NULL,
  `is_stream` tinyint(1) unsigned NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  KEY `name` (`name`),
  KEY `class_key` (`class_key`),
  KEY `is_stream` (`is_stream`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table petukhov.modx_media_sources: 1 rows
/*!40000 ALTER TABLE `modx_media_sources` DISABLE KEYS */;
INSERT INTO `modx_media_sources` (`id`, `name`, `description`, `class_key`, `properties`, `is_stream`) VALUES
	(1, 'Filesystem', '', 'MODX\\Revolution\\Sources\\modFileMediaSource', 'a:0:{}', 1);
/*!40000 ALTER TABLE `modx_media_sources` ENABLE KEYS */;

-- Dumping structure for table petukhov.modx_media_sources_contexts
CREATE TABLE IF NOT EXISTS `modx_media_sources_contexts` (
  `source` int(11) NOT NULL DEFAULT 0,
  `context_key` varchar(100) NOT NULL DEFAULT 'web',
  PRIMARY KEY (`source`,`context_key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table petukhov.modx_media_sources_contexts: 0 rows
/*!40000 ALTER TABLE `modx_media_sources_contexts` DISABLE KEYS */;
/*!40000 ALTER TABLE `modx_media_sources_contexts` ENABLE KEYS */;

-- Dumping structure for table petukhov.modx_media_sources_elements
CREATE TABLE IF NOT EXISTS `modx_media_sources_elements` (
  `source` int(11) unsigned NOT NULL DEFAULT 0,
  `object_class` varchar(100) NOT NULL DEFAULT 'modTemplateVar',
  `object` int(11) unsigned NOT NULL DEFAULT 0,
  `context_key` varchar(100) NOT NULL DEFAULT 'web',
  PRIMARY KEY (`source`,`object`,`object_class`,`context_key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table petukhov.modx_media_sources_elements: 12 rows
/*!40000 ALTER TABLE `modx_media_sources_elements` DISABLE KEYS */;
INSERT INTO `modx_media_sources_elements` (`source`, `object_class`, `object`, `context_key`) VALUES
	(1, 'modTemplateVar', 1, 'web'),
	(1, 'modTemplateVar', 2, 'web'),
	(1, 'modTemplateVar', 3, 'web'),
	(1, 'modTemplateVar', 4, 'web'),
	(1, 'modTemplateVar', 5, 'web'),
	(1, 'modTemplateVar', 6, 'web'),
	(1, 'modTemplateVar', 7, 'web'),
	(1, 'modTemplateVar', 8, 'web'),
	(1, 'modTemplateVar', 9, 'web'),
	(1, 'modTemplateVar', 10, 'web'),
	(1, 'modTemplateVar', 11, 'web'),
	(1, 'modTemplateVar', 12, 'web');
/*!40000 ALTER TABLE `modx_media_sources_elements` ENABLE KEYS */;

-- Dumping structure for table petukhov.modx_membergroup_names
CREATE TABLE IF NOT EXISTS `modx_membergroup_names` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` text DEFAULT NULL,
  `parent` int(10) unsigned NOT NULL DEFAULT 0,
  `rank` int(10) unsigned NOT NULL DEFAULT 0,
  `dashboard` int(10) unsigned NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `parent` (`parent`),
  KEY `rank` (`rank`),
  KEY `dashboard` (`dashboard`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table petukhov.modx_membergroup_names: 1 rows
/*!40000 ALTER TABLE `modx_membergroup_names` DISABLE KEYS */;
INSERT INTO `modx_membergroup_names` (`id`, `name`, `description`, `parent`, `rank`, `dashboard`) VALUES
	(1, 'Administrator', NULL, 0, 0, 1);
/*!40000 ALTER TABLE `modx_membergroup_names` ENABLE KEYS */;

-- Dumping structure for table petukhov.modx_member_groups
CREATE TABLE IF NOT EXISTS `modx_member_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_group` int(10) unsigned NOT NULL DEFAULT 0,
  `member` int(10) unsigned NOT NULL DEFAULT 0,
  `role` int(10) unsigned NOT NULL DEFAULT 1,
  `rank` int(10) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `role` (`role`),
  KEY `rank` (`rank`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table petukhov.modx_member_groups: 1 rows
/*!40000 ALTER TABLE `modx_member_groups` DISABLE KEYS */;
INSERT INTO `modx_member_groups` (`id`, `user_group`, `member`, `role`, `rank`) VALUES
	(1, 1, 1, 2, 0);
/*!40000 ALTER TABLE `modx_member_groups` ENABLE KEYS */;

-- Dumping structure for table petukhov.modx_menus
CREATE TABLE IF NOT EXISTS `modx_menus` (
  `text` varchar(255) NOT NULL DEFAULT '',
  `parent` varchar(255) NOT NULL DEFAULT '',
  `action` varchar(255) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `icon` varchar(255) NOT NULL DEFAULT '',
  `menuindex` int(11) unsigned NOT NULL DEFAULT 0,
  `params` text NOT NULL,
  `handler` text NOT NULL,
  `permissions` text NOT NULL,
  `namespace` varchar(100) NOT NULL DEFAULT 'core',
  PRIMARY KEY (`text`),
  KEY `parent` (`parent`),
  KEY `action` (`action`),
  KEY `namespace` (`namespace`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table petukhov.modx_menus: 42 rows
/*!40000 ALTER TABLE `modx_menus` DISABLE KEYS */;
INSERT INTO `modx_menus` (`text`, `parent`, `action`, `description`, `icon`, `menuindex`, `params`, `handler`, `permissions`, `namespace`) VALUES
	('topnav', '', '', 'topnav_desc', '', 0, '', '', '', 'core'),
	('site', 'topnav', '', '', '<i class="icon-file-text-o icon icon-large"></i>', 0, '', '', 'menu_site', 'core'),
	('new_resource', 'site', 'resource/create', 'new_resource_desc', '', 0, '', '', 'new_document', 'core'),
	('preview', 'site', '', 'preview_desc', '', 1, '', 'MODx.preview(); return false;', '', 'core'),
	('language', 'user', '', 'language_desc', '', 2, '', '', 'language', 'core'),
	('{$username}', 'user', 'security/profile', 'profile_desc', '', 0, '', '', 'change_profile', 'core'),
	('resource_groups', 'site', 'security/resourcegroup', 'resource_groups_desc', '', 2, '', '', 'access_permissions', 'core'),
	('content_types', 'site', 'system/contenttype', 'content_types_desc', '', 3, '', '', 'content_types', 'core'),
	('media', 'topnav', '', '', '<i class="icon-file-image-o icon icon-large"></i>', 1, '', '', 'file_manager', 'core'),
	('file_browser', 'media', 'media/browser', 'file_browser_desc', '', 0, '', '', 'file_manager', 'core'),
	('sources', 'media', 'source', 'sources_desc', '', 1, '', '', 'sources', 'core'),
	('components', 'topnav', '', '', '<i class="icon-cube icon icon-large"></i>', 2, '', '', 'components', 'core'),
	('installer', 'components', 'workspaces', 'installer_desc', '', 0, '', '', 'packages', 'core'),
	('manage', 'topnav', '', '', '<i class="icon-sliders icon icon-large"></i>', 3, '', '', 'menu_tools', 'core'),
	('users', 'manage', 'security/user', 'user_management_desc', '', 0, '', '', 'view_user', 'core'),
	('refresh_site', 'manage', '', 'refresh_site_desc', '', 1, '', 'MODx.clearCache(); return false;', 'empty_cache', 'core'),
	('refreshuris', 'refresh_site', '', 'refreshuris_desc', '', 0, '', 'MODx.refreshURIs(); return false;', 'empty_cache', 'core'),
	('remove_locks', 'manage', '', 'remove_locks_desc', '', 2, '', 'MODx.removeLocks();return false;', 'remove_locks', 'core'),
	('flush_access', 'manage', '', 'flush_access_desc', '', 3, '', 'MODx.msg.confirm({\n    title: _(\'flush_access\')\n    ,text: _(\'flush_access_confirm\')\n    ,url: MODx.config.connector_url\n    ,params: {\n        action: \'security/access/flush\'\n    }\n    ,listeners: {\n        \'success\': {fn:function() { location.href = \'./\'; },scope:this},\n        \'failure\': {fn:function(response) { Ext.MessageBox.alert(\'failure\', response.responseText); },scope:this},\n    }\n});', 'access_permissions', 'core'),
	('flush_sessions', 'manage', '', 'flush_sessions_desc', '', 4, '', 'MODx.msg.confirm({\n    title: _(\'flush_sessions\')\n    ,text: _(\'flush_sessions_confirm\')\n    ,url: MODx.config.connector_url\n    ,params: {\n        action: \'security/flush\'\n    }\n    ,listeners: {\n        \'success\': {fn:function() { location.href = \'./\'; },scope:this}\n    }\n});', 'flush_sessions', 'core'),
	('reports', 'manage', '', 'reports_desc', '', 5, '', '', 'menu_reports', 'core'),
	('site_schedule', 'reports', 'resource/site_schedule', 'site_schedule_desc', '', 0, '', '', 'view_document', 'core'),
	('view_logging', 'reports', 'system/logs', 'view_logging_desc', '', 1, '', '', 'mgr_log_view', 'core'),
	('eventlog_viewer', 'reports', 'system/event', 'eventlog_viewer_desc', '', 2, '', '', 'view_eventlog', 'core'),
	('view_sysinfo', 'reports', 'system/info', 'view_sysinfo_desc', '', 3, '', '', 'view_sysinfo', 'core'),
	('usernav', '', '', 'usernav_desc', '', 0, '', '', '', 'core'),
	('user', 'usernav', '', '', '<span id="user-avatar">{$userImage}</span> <span id="user-username">{$username}</span>', 5, '', '', 'menu_user', 'core'),
	('profile', 'user', 'security/profile', 'profile_desc', '', 0, '', '', 'change_profile', 'core'),
	('messages', 'user', 'security/message', 'messages_desc', '', 1, '', '', 'messages', 'core'),
	('logout', 'user', 'security/logout', 'logout_desc', '', 3, '', 'MODx.logout(); return false;', 'logout', 'core'),
	('admin', 'usernav', '', '', '<i class="icon-gear icon icon-large"></i>', 6, '', '', 'settings', 'core'),
	('system_settings', 'admin', 'system/settings', 'system_settings_desc', '', 0, '', '', 'settings', 'core'),
	('dashboards', 'admin', 'system/dashboards', 'dashboards_desc', '', 2, '', '', 'dashboards', 'core'),
	('contexts', 'admin', 'context', 'contexts_desc', '', 3, '', '', 'view_context', 'core'),
	('edit_menu', 'admin', 'system/action', 'edit_menu_desc', '', 4, '', '', 'actions', 'core'),
	('acls', 'admin', 'security/permission', 'acls_desc', '', 5, '', '', 'access_permissions', 'core'),
	('propertysets', 'admin', 'element/propertyset', 'propertysets_desc', '', 6, '', '', 'property_sets', 'core'),
	('lexicon_management', 'admin', 'workspaces/lexicon', 'lexicon_management_desc', '', 7, '', '', 'lexicons', 'core'),
	('namespaces', 'admin', 'workspaces/namespace', 'namespaces_desc', '', 8, '', '', 'namespaces', 'core'),
	('about', 'usernav', 'help', '', '<i class="icon-question-circle icon icon-large"></i>', 8, '', '', 'help', 'core'),
	('MIGX', 'components', 'index', '', '', 1, '&configs=migxconfigs||packagemanager', '', '', 'migx'),
	('form_customization', 'admin', 'security/forms', 'form_customization_desc', '', 1, '', '', 'customize_forms', 'core');
/*!40000 ALTER TABLE `modx_menus` ENABLE KEYS */;

-- Dumping structure for table petukhov.modx_migx_configs
CREATE TABLE IF NOT EXISTS `modx_migx_configs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL DEFAULT '',
  `formtabs` text NOT NULL,
  `contextmenus` text NOT NULL,
  `actionbuttons` text NOT NULL,
  `columnbuttons` text NOT NULL,
  `filters` text NOT NULL,
  `extended` text NOT NULL,
  `columns` text NOT NULL,
  `createdby` int(10) NOT NULL DEFAULT 0,
  `createdon` datetime DEFAULT NULL,
  `editedby` int(10) NOT NULL DEFAULT 0,
  `editedon` datetime DEFAULT NULL,
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `deletedon` datetime DEFAULT NULL,
  `deletedby` int(10) NOT NULL DEFAULT 0,
  `published` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `publishedon` datetime DEFAULT NULL,
  `publishedby` int(10) NOT NULL DEFAULT 0,
  `permissions` text NOT NULL,
  `fieldpermissions` text NOT NULL,
  `category` varchar(191) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

-- Dumping data for table petukhov.modx_migx_configs: 0 rows
/*!40000 ALTER TABLE `modx_migx_configs` DISABLE KEYS */;
/*!40000 ALTER TABLE `modx_migx_configs` ENABLE KEYS */;

-- Dumping structure for table petukhov.modx_migx_config_elements
CREATE TABLE IF NOT EXISTS `modx_migx_config_elements` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `config_id` int(10) NOT NULL DEFAULT 0,
  `element_id` int(10) NOT NULL DEFAULT 0,
  `rank` int(10) NOT NULL DEFAULT 0,
  `createdby` int(10) NOT NULL DEFAULT 0,
  `createdon` datetime NOT NULL,
  `editedby` int(10) NOT NULL DEFAULT 0,
  `editedon` datetime NOT NULL,
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `deletedon` datetime NOT NULL,
  `deletedby` int(10) NOT NULL DEFAULT 0,
  `published` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `publishedon` datetime NOT NULL,
  `publishedby` int(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

-- Dumping data for table petukhov.modx_migx_config_elements: 0 rows
/*!40000 ALTER TABLE `modx_migx_config_elements` DISABLE KEYS */;
/*!40000 ALTER TABLE `modx_migx_config_elements` ENABLE KEYS */;

-- Dumping structure for table petukhov.modx_migx_elements
CREATE TABLE IF NOT EXISTS `modx_migx_elements` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(100) NOT NULL DEFAULT '',
  `content` text NOT NULL,
  `createdby` int(10) NOT NULL DEFAULT 0,
  `createdon` datetime NOT NULL,
  `editedby` int(10) NOT NULL DEFAULT 0,
  `editedon` datetime NOT NULL,
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `deletedon` datetime NOT NULL,
  `deletedby` int(10) NOT NULL DEFAULT 0,
  `published` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `publishedon` datetime NOT NULL,
  `publishedby` int(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

-- Dumping data for table petukhov.modx_migx_elements: 0 rows
/*!40000 ALTER TABLE `modx_migx_elements` DISABLE KEYS */;
/*!40000 ALTER TABLE `modx_migx_elements` ENABLE KEYS */;

-- Dumping structure for table petukhov.modx_migx_formtabs
CREATE TABLE IF NOT EXISTS `modx_migx_formtabs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `config_id` int(10) NOT NULL DEFAULT 0,
  `caption` varchar(255) NOT NULL DEFAULT '',
  `pos` int(10) NOT NULL DEFAULT 0,
  `print_before_tabs` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `extended` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

-- Dumping data for table petukhov.modx_migx_formtabs: 0 rows
/*!40000 ALTER TABLE `modx_migx_formtabs` DISABLE KEYS */;
/*!40000 ALTER TABLE `modx_migx_formtabs` ENABLE KEYS */;

-- Dumping structure for table petukhov.modx_migx_formtab_fields
CREATE TABLE IF NOT EXISTS `modx_migx_formtab_fields` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `config_id` int(10) NOT NULL DEFAULT 0,
  `formtab_id` int(10) NOT NULL DEFAULT 0,
  `field` varchar(191) NOT NULL DEFAULT '',
  `caption` varchar(255) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `pos` int(10) NOT NULL DEFAULT 0,
  `description_is_code` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `inputTV` varchar(255) NOT NULL DEFAULT '',
  `inputTVtype` varchar(255) NOT NULL DEFAULT '',
  `validation` text NOT NULL,
  `configs` varchar(255) NOT NULL DEFAULT '',
  `restrictive_condition` text NOT NULL,
  `display` varchar(255) NOT NULL DEFAULT '',
  `sourceFrom` varchar(255) NOT NULL DEFAULT '',
  `sources` varchar(255) NOT NULL DEFAULT '',
  `inputOptionValues` text NOT NULL,
  `default` text NOT NULL,
  `extended` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

-- Dumping data for table petukhov.modx_migx_formtab_fields: 0 rows
/*!40000 ALTER TABLE `modx_migx_formtab_fields` DISABLE KEYS */;
/*!40000 ALTER TABLE `modx_migx_formtab_fields` ENABLE KEYS */;

-- Dumping structure for table petukhov.modx_namespaces
CREATE TABLE IF NOT EXISTS `modx_namespaces` (
  `name` varchar(40) NOT NULL DEFAULT '',
  `path` text DEFAULT NULL,
  `assets_path` text DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table petukhov.modx_namespaces: 6 rows
/*!40000 ALTER TABLE `modx_namespaces` DISABLE KEYS */;
INSERT INTO `modx_namespaces` (`name`, `path`, `assets_path`) VALUES
	('core', '{core_path}', '{assets_path}'),
	('migx', '{core_path}components/migx/', '{assets_path}components/migx/'),
	('tinymcerte', '{core_path}components/tinymcerte/', '{assets_path}components/tinymcerte/'),
	('translit', '{core_path}components/translit/', ''),
	('if', '{core_path}components/if/', ''),
	('ace', '{core_path}components/ace/', '');
/*!40000 ALTER TABLE `modx_namespaces` ENABLE KEYS */;

-- Dumping structure for table petukhov.modx_property_set
CREATE TABLE IF NOT EXISTS `modx_property_set` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '',
  `category` int(10) NOT NULL DEFAULT 0,
  `description` varchar(255) NOT NULL DEFAULT '',
  `properties` text DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `category` (`category`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table petukhov.modx_property_set: 0 rows
/*!40000 ALTER TABLE `modx_property_set` DISABLE KEYS */;
/*!40000 ALTER TABLE `modx_property_set` ENABLE KEYS */;

-- Dumping structure for table petukhov.modx_register_messages
CREATE TABLE IF NOT EXISTS `modx_register_messages` (
  `topic` int(10) unsigned NOT NULL,
  `id` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `valid` datetime NOT NULL,
  `accessed` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `accesses` int(10) unsigned NOT NULL DEFAULT 0,
  `expires` int(20) NOT NULL DEFAULT 0,
  `payload` mediumtext NOT NULL,
  `kill` tinyint(1) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`topic`,`id`),
  KEY `created` (`created`),
  KEY `valid` (`valid`),
  KEY `accessed` (`accessed`),
  KEY `accesses` (`accesses`),
  KEY `expires` (`expires`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table petukhov.modx_register_messages: 21 rows
/*!40000 ALTER TABLE `modx_register_messages` DISABLE KEYS */;
INSERT INTO `modx_register_messages` (`topic`, `id`, `created`, `valid`, `accessed`, `accesses`, `expires`, `payload`, `kill`) VALUES
	(1, 'eccbc87e4b5ce2fe28308fd9f2a7baf3', '2023-04-15 05:37:44', '2023-04-15 05:37:44', NULL, 0, 1681530224, 'if (time() > 1681530224) return null;\nreturn 1;\n', 0),
	(1, 'a87ff679a2f3e71d9181a67b7542122c', '2023-04-15 05:37:44', '2023-04-15 05:37:44', NULL, 0, 1681530224, 'if (time() > 1681530224) return null;\nreturn 1;\n', 0),
	(1, 'e4da3b7fbbce2345d7772b0674a318d5', '2023-04-15 05:37:44', '2023-04-15 05:37:44', NULL, 0, 1681530224, 'if (time() > 1681530224) return null;\nreturn 1;\n', 0),
	(1, '1679091c5a880faf6fb5e6087eb1b2dc', '2023-04-15 05:37:44', '2023-04-15 05:37:44', NULL, 0, 1681530224, 'if (time() > 1681530224) return null;\nreturn 1;\n', 0),
	(1, '8f14e45fceea167a5a36dedd4bea2543', '2023-04-15 05:37:44', '2023-04-15 05:37:44', NULL, 0, 1681530224, 'if (time() > 1681530224) return null;\nreturn 1;\n', 0),
	(1, 'c9f0f895fb98ab9159f51fd0297e236d', '2023-04-15 05:37:44', '2023-04-15 05:37:44', NULL, 0, 1681530224, 'if (time() > 1681530224) return null;\nreturn 1;\n', 0),
	(1, 'd3d9446802a44259755d38e6d163e820', '2023-04-15 05:37:44', '2023-04-15 05:37:44', NULL, 0, 1681530224, 'if (time() > 1681530224) return null;\nreturn 1;\n', 0),
	(1, '6512bd43d9caa6e02c990b0a82652dca', '2023-04-15 05:37:44', '2023-04-15 05:37:44', NULL, 0, 1681530224, 'if (time() > 1681530224) return null;\nreturn 1;\n', 0),
	(1, 'c20ad4d76fe97759aa27a0c99bff6710', '2023-04-15 05:37:44', '2023-04-15 05:37:44', NULL, 0, 1681530224, 'if (time() > 1681530224) return null;\nreturn 1;\n', 0),
	(1, 'c51ce410c124a10e0db5e4b97fc2af39', '2023-04-15 05:37:44', '2023-04-15 05:37:44', NULL, 0, 1681530224, 'if (time() > 1681530224) return null;\nreturn 1;\n', 0),
	(1, '9bf31c7ff062936a96d3c8bd1f8f2ff3', '2023-04-15 05:38:35', '2023-04-15 05:38:35', NULL, 0, 1681530275, 'if (time() > 1681530275) return null;\nreturn 1;\n', 0),
	(1, '02e74f10e0327ad868d138f2b4fdd6f0', '2023-04-15 05:38:35', '2023-04-15 05:38:35', NULL, 0, 1681530275, 'if (time() > 1681530275) return null;\nreturn 1;\n', 0),
	(1, '1ff1de774005f8da13f42943881c655f', '2023-04-15 05:38:35', '2023-04-15 05:38:35', NULL, 0, 1681530275, 'if (time() > 1681530275) return null;\nreturn 1;\n', 0),
	(1, '8e296a067a37563370ded05f5a3bf3ec', '2023-04-15 05:38:35', '2023-04-15 05:38:35', NULL, 0, 1681530275, 'if (time() > 1681530275) return null;\nreturn 1;\n', 0),
	(1, '4e732ced3463d06de0ca9a15b6153677', '2023-04-15 05:38:35', '2023-04-15 05:38:35', NULL, 0, 1681530275, 'if (time() > 1681530275) return null;\nreturn 1;\n', 0),
	(1, 'c74d97b01eae257e44aa9d5bade97baf', '2023-04-15 05:38:35', '2023-04-15 05:38:35', NULL, 0, 1681530275, 'if (time() > 1681530275) return null;\nreturn 1;\n', 0),
	(1, 'b6d767d2f8ed5d21a44b0e5886680cb9', '2023-04-15 05:38:35', '2023-04-15 05:38:35', NULL, 0, 1681530275, 'if (time() > 1681530275) return null;\nreturn 1;\n', 0),
	(1, '1f0e3dad99908345f7439f8ffabdffc4', '2023-04-15 05:38:35', '2023-04-15 05:38:35', NULL, 0, 1681530275, 'if (time() > 1681530275) return null;\nreturn 1;\n', 0),
	(1, '98f13708210194c475687be6106a3b84', '2023-04-15 05:38:35', '2023-04-15 05:38:35', NULL, 0, 1681530275, 'if (time() > 1681530275) return null;\nreturn 1;\n', 0),
	(1, '3c59dc048e8850243be8079a5c74d079', '2023-04-15 05:38:35', '2023-04-15 05:38:35', NULL, 0, 1681530275, 'if (time() > 1681530275) return null;\nreturn 1;\n', 0),
	(1, '37693cfc748049e45d87b8c7d8b9aacd', '2023-04-15 05:38:35', '2023-04-15 05:38:35', NULL, 0, 1681530275, 'if (time() > 1681530275) return null;\nreturn 1;\n', 0);
/*!40000 ALTER TABLE `modx_register_messages` ENABLE KEYS */;

-- Dumping structure for table petukhov.modx_register_queues
CREATE TABLE IF NOT EXISTS `modx_register_queues` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `options` mediumtext DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table petukhov.modx_register_queues: 2 rows
/*!40000 ALTER TABLE `modx_register_queues` DISABLE KEYS */;
INSERT INTO `modx_register_queues` (`id`, `name`, `options`) VALUES
	(1, 'locks', 'a:1:{s:9:"directory";s:5:"locks";}'),
	(2, 'resource_reload', 'a:1:{s:9:"directory";s:15:"resource_reload";}');
/*!40000 ALTER TABLE `modx_register_queues` ENABLE KEYS */;

-- Dumping structure for table petukhov.modx_register_topics
CREATE TABLE IF NOT EXISTS `modx_register_topics` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `queue` int(10) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `updated` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `options` mediumtext DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `queue` (`queue`),
  KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table petukhov.modx_register_topics: 2 rows
/*!40000 ALTER TABLE `modx_register_topics` DISABLE KEYS */;
INSERT INTO `modx_register_topics` (`id`, `queue`, `name`, `created`, `updated`, `options`) VALUES
	(1, 1, '/resource/', '2016-08-04 09:53:41', NULL, NULL),
	(2, 2, '/resourcereload/', '2016-08-04 09:55:12', NULL, NULL);
/*!40000 ALTER TABLE `modx_register_topics` ENABLE KEYS */;

-- Dumping structure for table petukhov.modx_session
CREATE TABLE IF NOT EXISTS `modx_session` (
  `id` varchar(255) NOT NULL DEFAULT '',
  `access` int(20) unsigned NOT NULL,
  `data` mediumtext DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `access` (`access`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table petukhov.modx_session: 2 rows
/*!40000 ALTER TABLE `modx_session` DISABLE KEYS */;
INSERT INTO `modx_session` (`id`, `access`, `data`) VALUES
	('qq8ef75j95ossfnuvu969dedsn', 1681530560, 'modx.user.0.resourceGroups|a:1:{s:3:"mgr";a:0:{}}modx.user.0.attributes|a:1:{s:3:"mgr";a:5:{s:32:"MODX\\Revolution\\modAccessContext";a:1:{s:3:"web";a:1:{i:0;a:3:{s:9:"principal";i:0;s:9:"authority";s:1:"0";s:6:"policy";a:4:{s:4:"load";b:1;s:16:"view_unpublished";b:0;s:6:"formit";b:1;s:18:"formit_encryptions";b:0;}}}}s:38:"MODX\\Revolution\\modAccessResourceGroup";a:0:{}s:33:"MODX\\Revolution\\modAccessCategory";a:0:{}s:44:"MODX\\Revolution\\Sources\\modAccessMediaSource";a:0:{}s:34:"MODX\\Revolution\\modAccessNamespace";a:0:{}}}modx.user.contextTokens|a:1:{s:3:"mgr";i:1;}manager_language|s:2:"en";login_failed|i:1;modx.mgr.user.token|s:52:"modx633c8a376161a9.15858990_1643a0cfeeded08.64634688";modx.mgr.session.cookie.lifetime|i:0;modx.mgr.user.config|a:0:{}newResourceTokens|a:8:{i:0;s:23:"643a1d97673502.56217971";i:1;s:23:"643a1d9c644932.94724091";i:2;s:23:"643a1da08b7230.01184118";i:3;s:23:"643a1da4c7a116.15196489";i:4;s:23:"643a1dabc945e1.44534007";i:5;s:23:"643a1db0b9a602.12193523";i:6;s:23:"643a1ddb4c4fb2.44191782";i:7;s:23:"643a1ec039e3a9.38348317";}'),
	('la53vebfeb0hcpe78q16dua2k4', 1693900244, 'modx.user.0.resourceGroups|a:1:{s:3:"mgr";a:0:{}}modx.user.0.attributes|a:1:{s:3:"mgr";a:5:{s:32:"MODX\\Revolution\\modAccessContext";a:1:{s:3:"web";a:1:{i:0;a:3:{s:9:"principal";i:0;s:9:"authority";s:1:"0";s:6:"policy";a:4:{s:4:"load";b:1;s:16:"view_unpublished";b:0;s:6:"formit";b:1;s:18:"formit_encryptions";b:0;}}}}s:38:"MODX\\Revolution\\modAccessResourceGroup";a:0:{}s:33:"MODX\\Revolution\\modAccessCategory";a:0:{}s:44:"MODX\\Revolution\\Sources\\modAccessMediaSource";a:0:{}s:34:"MODX\\Revolution\\modAccessNamespace";a:0:{}}}modx.user.contextTokens|a:1:{s:3:"mgr";i:1;}manager_language|s:2:"en";modx.mgr.user.token|s:52:"modx633c8a376161a9.15858990_164f6ddd42206f8.30739916";modx.mgr.session.cookie.lifetime|i:0;modx.mgr.user.config|a:0:{}');
/*!40000 ALTER TABLE `modx_session` ENABLE KEYS */;

-- Dumping structure for table petukhov.modx_site_content
CREATE TABLE IF NOT EXISTS `modx_site_content` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(20) NOT NULL DEFAULT 'document',
  `pagetitle` varchar(255) NOT NULL DEFAULT '',
  `longtitle` varchar(255) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `alias` varchar(255) DEFAULT '',
  `link_attributes` varchar(255) NOT NULL DEFAULT '',
  `published` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `pub_date` int(20) NOT NULL DEFAULT 0,
  `unpub_date` int(20) NOT NULL DEFAULT 0,
  `parent` int(10) NOT NULL DEFAULT 0,
  `isfolder` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `introtext` text DEFAULT NULL,
  `content` mediumtext DEFAULT NULL,
  `richtext` tinyint(1) unsigned NOT NULL DEFAULT 1,
  `template` int(10) NOT NULL DEFAULT 0,
  `menuindex` int(10) NOT NULL DEFAULT 0,
  `searchable` tinyint(1) unsigned NOT NULL DEFAULT 1,
  `cacheable` tinyint(1) unsigned NOT NULL DEFAULT 1,
  `createdby` int(10) NOT NULL DEFAULT 0,
  `createdon` int(20) NOT NULL DEFAULT 0,
  `editedby` int(10) NOT NULL DEFAULT 0,
  `editedon` int(20) NOT NULL DEFAULT 0,
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `deletedon` int(20) NOT NULL DEFAULT 0,
  `deletedby` int(10) NOT NULL DEFAULT 0,
  `publishedon` int(20) NOT NULL DEFAULT 0,
  `publishedby` int(10) NOT NULL DEFAULT 0,
  `menutitle` varchar(255) NOT NULL DEFAULT '',
  `donthit` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `privateweb` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `privatemgr` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `content_dispo` tinyint(1) NOT NULL DEFAULT 0,
  `hidemenu` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `class_key` varchar(100) NOT NULL DEFAULT 'MODX\\Revolution\\modDocument',
  `context_key` varchar(100) NOT NULL DEFAULT 'web',
  `content_type` int(11) unsigned NOT NULL DEFAULT 1,
  `uri` text DEFAULT NULL,
  `uri_override` tinyint(1) NOT NULL DEFAULT 0,
  `hide_children_in_tree` tinyint(1) NOT NULL DEFAULT 0,
  `show_in_tree` tinyint(1) NOT NULL DEFAULT 1,
  `properties` mediumtext DEFAULT NULL,
  `alias_visible` tinyint(1) unsigned NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  KEY `alias` (`alias`),
  KEY `published` (`published`),
  KEY `pub_date` (`pub_date`),
  KEY `unpub_date` (`unpub_date`),
  KEY `parent` (`parent`),
  KEY `isfolder` (`isfolder`),
  KEY `template` (`template`),
  KEY `menuindex` (`menuindex`),
  KEY `searchable` (`searchable`),
  KEY `cacheable` (`cacheable`),
  KEY `hidemenu` (`hidemenu`),
  KEY `class_key` (`class_key`),
  KEY `context_key` (`context_key`),
  KEY `uri` (`uri`(333)),
  KEY `uri_override` (`uri_override`),
  KEY `hide_children_in_tree` (`hide_children_in_tree`),
  KEY `show_in_tree` (`show_in_tree`),
  KEY `cache_refresh_idx` (`parent`,`menuindex`,`id`),
  FULLTEXT KEY `content_ft_idx` (`pagetitle`,`longtitle`,`description`,`introtext`,`content`)
) ENGINE=MyISAM AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table petukhov.modx_site_content: 3 rows
/*!40000 ALTER TABLE `modx_site_content` DISABLE KEYS */;
INSERT INTO `modx_site_content` (`id`, `type`, `pagetitle`, `longtitle`, `description`, `alias`, `link_attributes`, `published`, `pub_date`, `unpub_date`, `parent`, `isfolder`, `introtext`, `content`, `richtext`, `template`, `menuindex`, `searchable`, `cacheable`, `createdby`, `createdon`, `editedby`, `editedon`, `deleted`, `deletedon`, `deletedby`, `publishedon`, `publishedby`, `menutitle`, `donthit`, `privateweb`, `privatemgr`, `content_dispo`, `hidemenu`, `class_key`, `context_key`, `content_type`, `uri`, `uri_override`, `hide_children_in_tree`, `show_in_tree`, `properties`, `alias_visible`) VALUES
	(1, 'document', 'en', 'Evgenii Petukhov | Full-Stack Web Developer', '', 'index', '', 1, 0, 0, 0, 0, '', '', 1, 1, 0, 1, 0, 1, 1470279139, 1, 1665156183, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 'MODX\\Revolution\\modDocument', 'web', 1, 'index.html', 0, 0, 1, NULL, 1),
	(2, 'document', 'ru', 'Evgenii Petukhov | Full-Stack Web Developer', '', 'ru', '', 0, 0, 0, 0, 1, '', '', 1, 1, 0, 1, 0, 1, 1470297221, 1, 1681531601, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 'MODX\\Revolution\\modDocument', 'web', 1, 'ru/', 0, 0, 1, NULL, 1),
	(18, 'document', '404', '404', '', '404', '', 1, 0, 0, 0, 1, '', '', 1, 4, 4, 1, 1, 1, 1482942771, 1, 1482945061, 0, 0, 0, 1482942720, 1, '', 0, 0, 0, 0, 0, 'MODX\\Revolution\\modDocument', 'web', 1, '404/', 0, 0, 1, NULL, 1);
/*!40000 ALTER TABLE `modx_site_content` ENABLE KEYS */;

-- Dumping structure for table petukhov.modx_site_htmlsnippets
CREATE TABLE IF NOT EXISTS `modx_site_htmlsnippets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `source` int(10) unsigned NOT NULL DEFAULT 0,
  `property_preprocess` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `name` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT 'Chunk',
  `editor_type` int(11) NOT NULL DEFAULT 0,
  `category` int(11) NOT NULL DEFAULT 0,
  `cache_type` tinyint(1) NOT NULL DEFAULT 0,
  `snippet` mediumtext DEFAULT NULL,
  `locked` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `properties` text DEFAULT NULL,
  `static` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `static_file` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `category` (`category`),
  KEY `locked` (`locked`),
  KEY `static` (`static`)
) ENGINE=MyISAM AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table petukhov.modx_site_htmlsnippets: 11 rows
/*!40000 ALTER TABLE `modx_site_htmlsnippets` DISABLE KEYS */;
INSERT INTO `modx_site_htmlsnippets` (`id`, `source`, `property_preprocess`, `name`, `description`, `editor_type`, `category`, `cache_type`, `snippet`, `locked`, `properties`, `static`, `static_file`) VALUES
	(30, 1, 0, 'tplMainMenuLanding', '', 0, 0, 0, '<li><a data-scroll href="#about">[[*lang:is=`ru`:then=`О себе`:else=`About me`]]</a></li>\n<li><a data-scroll href="#education">[[*lang:is=`ru`:then=`Образование`:else=`Education`]]</a></li>\n<li><a data-scroll href="#experience">[[*lang:is=`ru`:then=`Опыт работы`:else=`Experience`]]</a></li>\n<li><a data-scroll href="#github">GitHub</a></li>\n<li><a data-scroll href="#contacts">[[*lang:is=`ru`:then=`Контакты`:else=`Contacts`]]</a></li>', 0, 'a:0:{}', 0, ''),
	(6, 1, 0, 'tplHeadCommon', '', 0, 0, 0, '<!DOCTYPE html>\n<html lang="[[*lang]]">\n  <head>\n    <meta charset="[[++modx_charset]]" />\n    <meta http-equiv="X-UA-Compatible" content="IE=edge">\n    <meta name="viewport" content="width=device-width, initial-scale=1">\n    <meta property="og:url" content="[[++site_url]][[*lang:is=`ru`:then=``:else=`[[~[[*id]]]]`]]" />\n    <meta property="og:type" content="profile" />\n    <meta property="og:title" content="[[*lang:is=`ru`:then=`Евгений Петухов: онлайн резюме`:else=`Evgenii Petukhov CV website`]]" />\n    <meta property="og:description" content="[[*lang:is=`ru`:then=`Здесь вы сможете найти информацию о моём образовании, опыте работы и портфолио`:else=`You\'re able to find the information about my education, experience and portfolio on this website.`]]" />\n    <meta property="og:image" content="[[++site_url]]images/[[*lang:is=`ru`:then=`og-image-ru6.jpg`:else=`og-image-en6.jpg`]]" />\n    <meta property="og:image:width" content="1600" />\n    <meta property="og:image:height" content="838" />\n    <meta property="fb:app_id" content="1783793708500879" />\n    <meta name="color-scheme" content="light only">\n    <link rel="icon" type="image/png" href="[[++site_url]]images/favicon.png">\n    <title>[[*longtitle]]</title>\n    <link href="[[++site_url]]css/fonts.css" rel="stylesheet">\n    <link href="[[++site_url]]css/style.css?[[!Random]]" rel="stylesheet">\n    <!--[if lt IE 9]>\n      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>\n      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>\n    <![endif]-->\n    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>\n    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.4.1/js/bootstrap.min.js"></script>\n		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/jquery.waypoints.min.js"></script>\n		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/shortcuts/sticky.min.js"></script>\n		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/smooth-scroll/9.4.3/js/smooth-scroll.js"></script>\n		<script type="text/javascript" src="[[++site_url]]js/common.js?[[!Random]]"></script>\n        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-82498017-1"></script>\n        <script>\n            window.dataLayer = window.dataLayer || [];\n            function gtag(){dataLayer.push(arguments);}\n            gtag(\'js\', new Date());\n            gtag(\'config\', \'UA-82498017-1\');\n        </script>', 0, 'a:0:{}', 0, ''),
	(7, 1, 0, 'tplFooter', '', 0, 0, 0, '		<footer class = "gradient">\n			<div class = "container">\n				<div class = "row">\n					<p>[[*lang:is=`ru`:then=`Фотографии взяты из следующих аккаунтов на`:else=`Images have been downloaded from the following accounts on`]] <a href = "http://pexels.com" target = "_blank" rel="noopener">Pexels</a>:&nbsp;\n					<a href = "https://www.pexels.com/@nick-shul/" target = "_blank" rel="noopener">Nikolay Shulga</a>&nbsp;\n					<a href = "https://www.pexels.com/@vincent-wei-64074344/" target = "_blank" rel="noopener">Vincent Wei</a>&nbsp;\n					<a href = "https://www.pexels.com/@goumbik/" target = "_blank" rel="noopener">Lukas</a>&nbsp;\n					<a href = "https://www.pexels.com/@eliza-craciunescu-2243791/" target = "_blank" rel="noopener">Eliza Craciunescu</a>&nbsp;\n					<a href = "https://www.pexels.com/@balidmr/" target = "_blank" rel="noopener">Bali Demiri</a>&nbsp;\n					<a href = "https://www.pexels.com/@valeriya-kobzar-42371713/" target = "_blank" rel="noopener">Valeriya Kobzar</a></p>\n					<p>&copy; 2016-2023 [[*lang:is=`ru`:then=`Евгений Петухов. Все права защищены`:else=`Evgenii Petukhov. All rights reserved`]]</p>						\n				</div>\n			</div>\n		</footer>\n  </body>\n</html>', 0, 'a:0:{}', 0, ''),
	(9, 1, 0, 'tplMainMenu', '', 0, 0, 0, '<nav class="navbar navbar-default" id = "navbar">\n  <div class="container-fluid">\n    <div class="navbar-header">\n      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false">\n        <span class="sr-only">Toggle navigation</span>\n        <span class="icon-bar"></span>\n        <span class="icon-bar"></span>\n        <span class="icon-bar"></span>\n      </button>\n      <a [[*template:is=`1`:then=`data-scroll`]] class="navbar-brand" href="[[++site_url]]"><h1>[[*lang:is=`ru`:then=`Евгений<br />Петухов`:else=`Evgenii<br />Petukhov`]]</h1></a>\n      <div class = "speciality hidden-sm hidden-xs">Full-Stack Web Developer</div>\n    </div>\n    <div class="collapse navbar-collapse" id="navbar-collapse">\n      <ul class="nav navbar-nav navbar-right">\n[[*template:is=`4`:then=``:else=`[[$tplMainMenuLanding]]`]]\n      </ul>\n    </div>\n  </div>\n</nav>', 0, 'a:0:{}', 0, ''),
	(20, 1, 0, 'tplExperienceNorilskNickelRu', '', 0, 0, 0, '<p>Занимался разработкой WPF-приложений, веб-приложений ASP.Net MVC, а также приложений Windows Forms applications. Наиболее интересный проектом было интерактивное тестирование &laquo;Предсменный экзаменатор&raquo;. Рабочие, занятые на подземных рудниках, обязаны пройти проверку знаний по технике безопасности перед началом работы с целью снижения вероятности травматизма. На предприятиях установлены около ста touchscreen устройств, так называемых &laquo;информационных киосков&raquo;. Внутри киоска запущено WPF-приложение. Киоски получают информацию через API-интерфейс, который реализован в приложении ASP.Net MVC. Клиентское приложение выполняет запросы асинхронно при помощи класса Task и ключевых слов &laquo;async&raquo; и &laquo;await&raquo;, введенных в C# 5.0.</p>', 0, 'a:0:{}', 0, ''),
	(21, 1, 0, 'tplExperienceNorilskNickelEn', '', 0, 0, 0, '<p>C# / ASP.NET / WCF / SQL Server</p>\n<p>I led the development of a safety training system. This project had a profound impact on enhancing safety awareness and significantly reducing the risk of injuries caused by negligence. The system comprised a robust web server built on ASP.NET Web API, complemented by a user-friendly WPF client. This experience not only honed my technical skills but also deepened my commitment to creating solutions that have a positive and tangible impact.</p>', 0, 'a:0:{}', 0, ''),
	(26, 1, 0, 'tplExperienceSoftwareCountryEn', '', 0, 0, 0, '<p>In my capacity as a team leader, I undertook various leadership responsibilities, including sprint planning, prioritizing tasks to meet sprint objectives, inspiring team members to reach their goals, providing coaching to improve team members\' skills, facilitating the smooth onboarding of new team members, and skillfully addressing conflicts within the team.</p>\n<p>Since a number of key employees in my current company had to leave the firm recently, I’ve become the go-to guy which everyone turns to in order to fill the void left behind. If you want to get something done, you come to me - all my teammates know that. However, this requires a huge amount of time and effort, which many people are unwilling to make.</p>', 0, 'a:0:{}', 0, ''),
	(27, 1, 0, 'tplExperienceArcadiaEn', '', 0, 0, 0, '<p>C# / ASP.NET Core / EF Core / SQL Server / React + Redux / Angular + NgRx</p>\n<p>I dedicated over five years of my career to an esteemed international IT company. During this tenure, I was heavily involved in the development of a comprehensive computer-based assessment platform for online examination centers in the UK and the US. My contributions included the introduction of innovative question formats, enabling audio recording functionality, implementing offline test-taking functionality, and resolving numerous software issues which demonstrated my commitment to delivering quality software solutions.</p>', 0, 'a:0:{}', 0, ''),
	(28, 1, 0, 'tplExperienceWpfMvvmPracticeEn', '', 0, 0, 0, '<p>(WPF MVVM, NHibernate, gRPC, PostgreSQL) Developed a vector editor for mapping a mine. Enhanced the map rendering performance for deep and complex mines with a large number of elements.</p>', 0, 'a:0:{}', 0, ''),
	(29, 1, 0, 'tplExperienceAngularPracticeEn', '', 0, 0, 0, '<p>(Angular) Contributed to the <a href = "https://github.com/abacritt/angularx-social-login" target = "_blank" rel="noopener">angularx-social-login</a> GitHub project which entails login via Facebook and Google. Added a new login provider to log in on a website via VK (social media).</p>', 0, 'a:0:{}', 0, ''),
	(32, 1, 0, 'tplExperienceGameteqEn', '', 0, 0, 0, '<p>C# / ASP.NET Core / gRPC / PostgreSQL, MongoDB / Apache Kafka / Angular</p>\n<p>Currently, I am employed as a full-stack developer at a game development company, where I have taken on significant responsibilities in the development of event-driven content management systems. I have played a key role in architecting microservices that seamlessly communicate through Apache Kafka, resulting in highly efficient data flow and substantial enhancements in system performance. I also successfully identified and resolved several issues that were causing performance declines on the backend, showcasing my problem-solving and troubleshooting skills.</p>\n<p>Additionally, I took the initiative to participate in the upgrade of front-end applications to the latest Angular version, scrupulously resolving deprecations and maintaining the platform\'s robustness. My ability to navigate both frontend and backend components reflects my versatility as a full-stack developer.</p>', 0, 'a:0:{}', 0, '');
/*!40000 ALTER TABLE `modx_site_htmlsnippets` ENABLE KEYS */;

-- Dumping structure for table petukhov.modx_site_plugins
CREATE TABLE IF NOT EXISTS `modx_site_plugins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `source` int(10) unsigned NOT NULL DEFAULT 0,
  `property_preprocess` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `name` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `editor_type` int(11) NOT NULL DEFAULT 0,
  `category` int(11) NOT NULL DEFAULT 0,
  `cache_type` tinyint(1) NOT NULL DEFAULT 0,
  `plugincode` mediumtext NOT NULL,
  `locked` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `properties` text DEFAULT NULL,
  `disabled` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `moduleguid` varchar(32) NOT NULL DEFAULT '',
  `static` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `static_file` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `category` (`category`),
  KEY `locked` (`locked`),
  KEY `disabled` (`disabled`),
  KEY `static` (`static`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table petukhov.modx_site_plugins: 6 rows
/*!40000 ALTER TABLE `modx_site_plugins` DISABLE KEYS */;
INSERT INTO `modx_site_plugins` (`id`, `source`, `property_preprocess`, `name`, `description`, `editor_type`, `category`, `cache_type`, `plugincode`, `locked`, `properties`, `disabled`, `moduleguid`, `static`, `static_file`) VALUES
	(12, 0, 0, 'migxResizeOnUpload', '', 0, 7, 0, '/**\n * migxResizeOnUpload Plugin\n *\n * Events: OnFileManagerUpload\n * Author: Bruno Perner <b.perner@gmx.de>\n * Modified to read multiple configs from mediasource-property\n * \n * First Author: Vasiliy Naumkin <bezumkin@yandex.ru>\n * Required: PhpThumbOf snippet for resizing images\n * \n * Example: mediasource - property \'resizeConfig\':\n * [{"alias":"origin","w":"500","h":"500","far":1},{"alias":"thumb","w":"150","h":"150","far":1}]\n */\n\nif ($modx->event->name != \'OnFileManagerUpload\') {\n    return;\n}\n\n\n$file = $modx->event->params[\'files\'][\'file\'];\n$directory = $modx->event->params[\'directory\'];\n\nif ($file[\'error\'] != 0) {\n    return;\n}\n\n$name = $file[\'name\'];\n//$extensions = explode(\',\', $modx->getOption(\'upload_images\'));\n\n$source = $modx->event->params[\'source\'];\n\nif ($source instanceof modMediaSource) {\n    //$dirTree = $modx->getOption(\'dirtree\', $_REQUEST, \'\');\n    //$modx->setPlaceholder(\'docid\', $resource_id);\n    $source->initialize();\n    $basePath = str_replace(\'/./\', \'/\', $source->getBasePath());\n    //$cachepath = $cachepath . $dirTree;\n    $baseUrl = $modx->getOption(\'site_url\') . $source->getBaseUrl();\n    //$baseUrl = $baseUrl . $dirTree;\n    $sourceProperties = $source->getPropertyList();\n\n    //echo \'<pre>\' . print_r($sourceProperties, 1) . \'</pre>\';\n    //$allowedExtensions = $modx->getOption(\'allowedFileTypes\', $sourceProperties, \'\');\n    //$allowedExtensions = empty($allowedExtensions) ? \'jpg,jpeg,png,gif\' : $allowedExtensions;\n    //$maxFilesizeMb = $modx->getOption(\'maxFilesizeMb\', $sourceProperties, \'8\');\n    //$maxFiles = $modx->getOption(\'maxFiles\', $sourceProperties, \'0\');\n    //$thumbX = $modx->getOption(\'thumbX\', $sourceProperties, \'100\');\n    //$thumbY = $modx->getOption(\'thumbY\', $sourceProperties, \'100\');\n    $resizeConfigs = $modx->getOption(\'resizeConfigs\', $sourceProperties, \'\');\n    $resizeConfigs = $modx->fromJson($resizeConfigs);\n    $thumbscontainer = $modx->getOption(\'thumbscontainer\', $sourceProperties, \'thumbs/\');\n    $imageExtensions = $modx->getOption(\'imageExtensions\', $sourceProperties, \'jpg,jpeg,png,gif,JPG\');\n    $imageExtensions = explode(\',\', $imageExtensions);\n    //$uniqueFilenames = $modx->getOption(\'uniqueFilenames\', $sourceProperties, false);\n    //$onImageUpload = $modx->getOption(\'onImageUpload\', $sourceProperties, \'\');\n    //$onImageRemove = $modx->getOption(\'onImageRemove\', $sourceProperties, \'\');\n    $cleanalias = $modx->getOption(\'cleanFilename\', $sourceProperties, false);\n\n}\n\nif (is_array($resizeConfigs) && count($resizeConfigs) > 0) {\n    foreach ($resizeConfigs as $rc) {\n        if (isset($rc[\'alias\'])) {\n            $filePath = $basePath . $directory;\n            $filePath = str_replace(\'//\',\'/\',$filePath);\n            if ($rc[\'alias\'] == \'origin\') {\n                $thumbPath = $filePath;\n            } else {\n                $thumbPath = $filePath . $rc[\'alias\'] . \'/\';\n                $permissions = octdec(\'0\' . (int)($modx->getOption(\'new_folder_permissions\', null, \'755\', true)));\n                if (!@mkdir($thumbPath, $permissions, true)) {\n                    $modx->log(MODX_LOG_LEVEL_ERROR, sprintf(\'[migxResourceMediaPath]: could not create directory %s).\', $thumbPath));\n                } else {\n                    chmod($thumbPath, $permissions);\n                }\n\n            }\n\n\n            $filename = $filePath . $name;\n            $thumbname = $thumbPath . $name;\n            $ext = substr(strrchr($name, \'.\'), 1);\n            if (in_array($ext, $imageExtensions)) {\n                $sizes = getimagesize($filename);\n                echo $sizes[0]; \n                //$format = substr($sizes[\'mime\'], 6);\n                if ($sizes[0] > $rc[\'w\'] || $sizes[1] > $rc[\'h\']) {\n                    if ($sizes[0] < $rc[\'w\']) {\n                        $rc[\'w\'] = $sizes[0];\n                    }\n                    if ($sizes[1] < $rc[\'h\']) {\n                        $rc[\'h\'] = $sizes[1];\n                    }\n                    $type = $sizes[0] > $sizes[1] ? \'landscape\' : \'portrait\';\n                    if (isset($rc[\'far\']) && $rc[\'far\'] == \'1\' && isset($rc[\'w\']) && isset($rc[\'h\'])) {\n                        if ($type = \'landscape\') {\n                            unset($rc[\'h\']);\n                        }else {\n                            unset($rc[\'w\']);\n                        }\n                    }\n\n                    $options = \'\';\n                    foreach ($rc as $k => $v) {\n                        if ($k != \'alias\') {\n                            $options .= \'&\' . $k . \'=\' . $v;\n                        }\n                    }\n                    $resized = $modx->runSnippet(\'phpthumbof\', array(\'input\' => $filePath . $name, \'options\' => $options));\n                    rename(MODX_BASE_PATH . substr($resized, 1), $thumbname);\n                }\n            }\n\n\n        }\n    }\n}', 0, 'a:0:{}', 0, '', 0, ''),
	(13, 0, 0, 'TinyMCE Rich Text Editor', 'TinyMCE Rich Text Editor runtime hooks - registers and includes javascripts on document edit pages', 0, 8, 0, '/**\n * TinyMCE Rich Tech Editor Plugin\n *\n * @package tinymcerte\n * @subpackage plugin\n *\n * @var modX $modx\n * @var array $scriptProperties\n */\n\n$className = \'TinyMCERTE\\Plugins\\Events\\\\\' . $modx->event->name;\n\n$corePath = $modx->getOption(\'tinymcerte.core_path\', null, $modx->getOption(\'core_path\') . \'components/tinymcerte/\');\n/** @var TinyMCERTE $tinymcerte */\n$tinymcerte = $modx->getService(\'tinymcerte\', \'TinyMCERTE\', $corePath . \'model/tinymcerte/\', [\n    \'core_path\' => $corePath\n]);\n\nif ($tinymcerte) {\n    if (class_exists($className)) {\n        $handler = new $className($modx, $scriptProperties);\n        if (get_class($handler) == $className) {\n            $handler->run();\n        } else {\n            $modx->log(xPDO::LOG_LEVEL_ERROR, $className. \' could not be initialized!\', \'\', \'TinyMCE RTE Plugin\');\n        }\n    } else {\n        $modx->log(xPDO::LOG_LEVEL_ERROR, $className. \' was not found!\', \'\', \'TinyMCE RTE Plugin\');\n    }\n}\n\nreturn;', 0, 'a:0:{}', 0, '', 0, ''),
	(11, 0, 0, 'MIGXquip', '', 0, 7, 0, '$quipCorePath = $modx->getOption(\'quip.core_path\', null, $modx->getOption(\'core_path\') . \'components/quip/\');\n//$assetsUrl = $modx->getOption(\'migx.assets_url\', null, $modx->getOption(\'assets_url\') . \'components/migx/\');\nswitch ($modx->event->name)\n{\n\n    case \'OnDocFormPrerender\':\n\n        \n        require_once $quipCorePath . \'model/quip/quip.class.php\';\n        $modx->quip = new Quip($modx);\n\n        $modx->lexicon->load(\'quip:default\');\n        $quipconfig = $modx->toJson($modx->quip->config);\n        \n        $js = "\n        Quip.config = Ext.util.JSON.decode(\'{$quipconfig}\');\n        console.log(Quip);";\n\n        //$modx->controller->addCss($assetsUrl . \'css/mgr.css\');\n        $modx->controller->addJavascript($modx->quip->config[\'jsUrl\'].\'quip.js\');\n        $modx->controller->addHtml(\'<script type="text/javascript">\' . $js . \'</script>\');\n        break;\n\n}\nreturn;', 0, 'a:0:{}', 1, '', 0, ''),
	(10, 0, 0, 'MIGX', '', 0, 7, 0, '$corePath = $modx->getOption(\'migx.core_path\',null,$modx->getOption(\'core_path\').\'components/migx/\');\n$assetsUrl = $modx->getOption(\'migx.assets_url\', null, $modx->getOption(\'assets_url\') . \'components/migx/\');\nswitch ($modx->event->name) {\n    case \'OnTVInputRenderList\':\n        $modx->event->output($corePath.\'elements/tv/input/\');\n        break;\n    case \'OnTVInputPropertiesList\':\n        $modx->event->output($corePath.\'elements/tv/inputoptions/\');\n        break;\n\n        case \'OnDocFormPrerender\':\n        $modx->controller->addCss($assetsUrl.\'css/mgr.css\');\n        break; \n \n    /*          \n    case \'OnTVOutputRenderList\':\n        $modx->event->output($corePath.\'elements/tv/output/\');\n        break;\n    case \'OnTVOutputRenderPropertiesList\':\n        $modx->event->output($corePath.\'elements/tv/properties/\');\n        break;\n    \n    case \'OnDocFormPrerender\':\n        $assetsUrl = $modx->getOption(\'colorpicker.assets_url\',null,$modx->getOption(\'assets_url\').\'components/colorpicker/\'); \n        $modx->regClientStartupHTMLBlock(\'<script type="text/javascript">\n        Ext.onReady(function() {\n            \n        });\n        </script>\');\n        $modx->regClientStartupScript($assetsUrl.\'sources/ColorPicker.js\');\n        $modx->regClientStartupScript($assetsUrl.\'sources/ColorMenu.js\');\n        $modx->regClientStartupScript($assetsUrl.\'sources/ColorPickerField.js\');		\n        $modx->regClientCSS($assetsUrl.\'resources/css/colorpicker.css\');\n        break;\n     */\n}\nreturn;', 0, 'a:0:{}', 0, '', 0, ''),
	(6, 1, 0, 'RestrictUnpublished', '', 0, 0, 0, '$e = $modx->Event;\nif ($modx->context->key == \'web\') {\n  $id = $modx->resourceIdentifier;\n  $page = $modx->getObject(\'modResource\', $id);\n  if (!$page) return;\n  $published = $page->get(\'published\');\n  if (empty($published)) $modx->sendForward(18);\n}', 0, 'a:0:{}', 0, '', 0, ''),
	(9, 0, 0, 'Ace', 'Ace code editor plugin for MODx Revolution', 0, 0, 0, '/**\n * Ace Source Editor Plugin\n *\n * Events: OnManagerPageBeforeRender, OnRichTextEditorRegister, OnSnipFormPrerender,\n * OnTempFormPrerender, OnChunkFormPrerender, OnPluginFormPrerender,\n * OnFileCreateFormPrerender, OnFileEditFormPrerender, OnDocFormPrerender\n *\n * @author Danil Kostin <danya.postfactum(at)gmail.com>\n *\n * @package ace\n *\n * @var array $scriptProperties\n * @var Ace $ace\n */\nif ($modx->event->name == \'OnRichTextEditorRegister\') {\n    $modx->event->output(\'Ace\');\n    return;\n}\n\nif ($modx->getOption(\'which_element_editor\', null, \'Ace\') !== \'Ace\') {\n    return;\n}\n\n$corePath = $modx->getOption(\'ace.core_path\', null, $modx->getOption(\'core_path\').\'components/ace/\');\n$ace = $modx->getService(\'ace\', \'Ace\', $corePath.\'model/ace/\');\n$ace->initialize();\n\n$extensionMap = array(\n    \'tpl\'   => \'text/x-smarty\',\n    \'htm\'   => \'text/html\',\n    \'html\'  => \'text/html\',\n    \'css\'   => \'text/css\',\n    \'scss\'  => \'text/x-scss\',\n    \'less\'  => \'text/x-less\',\n    \'svg\'   => \'image/svg+xml\',\n    \'xml\'   => \'application/xml\',\n    \'xsl\'   => \'application/xml\',\n    \'js\'    => \'application/javascript\',\n    \'json\'  => \'application/json\',\n    \'php\'   => \'application/x-php\',\n    \'sql\'   => \'text/x-sql\',\n    \'md\'    => \'text/x-markdown\',\n    \'txt\'   => \'text/plain\',\n    \'twig\'  => \'text/x-twig\'\n);\n\n// Define default mime for html elements(templates, chunks and html resources)\n$html_elements_mime=$modx->getOption(\'ace.html_elements_mime\',null,false);\nif(!$html_elements_mime){\n    // this may deprecated in future because components may set ace.html_elements_mime option now\n    switch (true) {\n        case $modx->getOption(\'twiggy_class\'):\n            $html_elements_mime = \'text/x-twig\';\n            break;\n        case $modx->getOption(\'pdotools_fenom_parser\'):\n            $html_elements_mime = \'text/x-smarty\';\n            break;\n        default:\n            $html_elements_mime = \'text/html\';\n    }\n}\n\n// Defines wether we should highlight modx tags\n$modxTags = false;\nswitch ($modx->event->name) {\n    case \'OnSnipFormPrerender\':\n        $field = \'modx-snippet-snippet\';\n        $mimeType = \'application/x-php\';\n        break;\n    case \'OnTempFormPrerender\':\n        $field = \'modx-template-content\';\n        $modxTags = true;\n        $mimeType = $html_elements_mime;\n        break;\n    case \'OnChunkFormPrerender\':\n        $field = \'modx-chunk-snippet\';\n        if ($modx->controller->chunk && $modx->controller->chunk->isStatic()) {\n            $extension = pathinfo($modx->controller->chunk->name, PATHINFO_EXTENSION);\n            if(!$extension||!isset($extensionMap[$extension])){\n                $extension = pathinfo($modx->controller->chunk->getSourceFile(), PATHINFO_EXTENSION);\n            }\n            $mimeType = isset($extensionMap[$extension]) ? $extensionMap[$extension] : \'text/plain\';\n        } else {\n            $mimeType = $html_elements_mime;\n        }\n        $modxTags = true;\n        break;\n    case \'OnPluginFormPrerender\':\n        $field = \'modx-plugin-plugincode\';\n        $mimeType = \'application/x-php\';\n        break;\n    case \'OnFileCreateFormPrerender\':\n        $field = \'modx-file-content\';\n        $mimeType = \'text/plain\';\n        break;\n    case \'OnFileEditFormPrerender\':\n        $field = \'modx-file-content\';\n        $extension = pathinfo($scriptProperties[\'file\'], PATHINFO_EXTENSION);\n        $mimeType = isset($extensionMap[$extension])\n            ? $extensionMap[$extension]\n            : (\'@FILE:\'.pathinfo($scriptProperties[\'file\'], PATHINFO_BASENAME));\n        $modxTags = $extension == \'tpl\';\n        break;\n    case \'OnDocFormPrerender\':\n        if (!$modx->controller->resourceArray) {\n            return;\n        }\n        $field = \'ta\';\n        $mimeType = $modx->getObject(\'modContentType\', $modx->controller->resourceArray[\'content_type\'])->get(\'mime_type\');\n\n        if($mimeType == \'text/html\')$mimeType = $html_elements_mime;\n\n        if ($modx->getOption(\'use_editor\')){\n            $richText = $modx->controller->resourceArray[\'richtext\'];\n            $classKey = $modx->controller->resourceArray[\'class_key\'];\n            if ($richText || in_array($classKey, array(\'modStaticResource\',\'modSymLink\',\'modWebLink\',\'modXMLRPCResource\'))) {\n                $field = false;\n            }\n        }\n        $modxTags = true;\n        break;\n    case \'OnTVInputRenderList\':\n        $modx->event->output($corePath . \'elements/tv/input/\');\n        break;\n    default:\n        return;\n}\n\n$modxTags = (int) $modxTags;\n$script = \'\';\nif (!empty($field)) {\n    $script .= "MODx.ux.Ace.replaceComponent(\'$field\', \'$mimeType\', $modxTags);";\n}\n\nif ($modx->event->name == \'OnDocFormPrerender\' && !$modx->getOption(\'use_editor\')) {\n    $script .= "MODx.ux.Ace.replaceTextAreas(Ext.query(\'.modx-richtext\'));";\n}\n\nif ($script) {\n    $modx->controller->addHtml(\'<script>Ext.onReady(function() {\' . $script . \'});</script>\');\n}', 0, NULL, 0, '', 0, 'ace/elements/plugins/ace.plugin.php');
/*!40000 ALTER TABLE `modx_site_plugins` ENABLE KEYS */;

-- Dumping structure for table petukhov.modx_site_plugin_events
CREATE TABLE IF NOT EXISTS `modx_site_plugin_events` (
  `pluginid` int(10) NOT NULL DEFAULT 0,
  `event` varchar(255) NOT NULL DEFAULT '',
  `priority` int(10) NOT NULL DEFAULT 0,
  `propertyset` int(10) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`pluginid`,`event`),
  KEY `priority` (`priority`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table petukhov.modx_site_plugin_events: 20 rows
/*!40000 ALTER TABLE `modx_site_plugin_events` DISABLE KEYS */;
INSERT INTO `modx_site_plugin_events` (`pluginid`, `event`, `priority`, `propertyset`) VALUES
	(9, 'OnManagerPageBeforeRender', 0, 0),
	(9, 'OnRichTextEditorRegister', 0, 0),
	(9, 'OnTempFormPrerender', 0, 0),
	(9, 'OnFileEditFormPrerender', 0, 0),
	(9, 'OnFileCreateFormPrerender', 0, 0),
	(9, 'OnDocFormPrerender', 0, 0),
	(13, 'OnRichTextEditorRegister', 0, 0),
	(13, 'OnManagerPageBeforeRender', 0, 0),
	(9, 'OnSnipFormPrerender', 0, 0),
	(9, 'OnPluginFormPrerender', 0, 0),
	(6, 'OnWebPageInit', 0, 0),
	(9, 'OnChunkFormPrerender', 0, 0),
	(9, 'OnTVInputRenderList', 0, 0),
	(10, 'OnDocFormPrerender', 0, 0),
	(10, 'OnTVInputPropertiesList', 0, 0),
	(10, 'OnTVInputRenderList', 0, 0),
	(11, 'OnDocFormPrerender', 0, 0),
	(12, 'OnFileManagerUpload', 0, 0),
	(13, 'OnRichTextEditorInit', 0, 0),
	(13, 'OnRichTextBrowserInit', 0, 0);
/*!40000 ALTER TABLE `modx_site_plugin_events` ENABLE KEYS */;

-- Dumping structure for table petukhov.modx_site_snippets
CREATE TABLE IF NOT EXISTS `modx_site_snippets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `source` int(10) unsigned NOT NULL DEFAULT 0,
  `property_preprocess` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `name` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `editor_type` int(11) NOT NULL DEFAULT 0,
  `category` int(11) NOT NULL DEFAULT 0,
  `cache_type` tinyint(1) NOT NULL DEFAULT 0,
  `snippet` mediumtext DEFAULT NULL,
  `locked` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `properties` text DEFAULT NULL,
  `moduleguid` varchar(32) NOT NULL DEFAULT '',
  `static` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `static_file` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `category` (`category`),
  KEY `locked` (`locked`),
  KEY `moduleguid` (`moduleguid`),
  KEY `static` (`static`)
) ENGINE=MyISAM AUTO_INCREMENT=64 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table petukhov.modx_site_snippets: 27 rows
/*!40000 ALTER TABLE `modx_site_snippets` DISABLE KEYS */;
INSERT INTO `modx_site_snippets` (`id`, `source`, `property_preprocess`, `name`, `description`, `editor_type`, `category`, `cache_type`, `snippet`, `locked`, `properties`, `moduleguid`, `static`, `static_file`) VALUES
	(48, 0, 0, 'migxImageUpload', '', 0, 7, 0, 'return include $modx->getOption(\'core_path\').\'components/migx/model/imageupload/imageupload.php\';', 0, 'a:0:{}', '', 0, ''),
	(49, 0, 0, 'migxChunklistToJson', '', 0, 7, 0, '$category = $modx->getOption(\'category\', $scriptProperties, \'\');\n$format = $modx->getOption(\'format\', $scriptProperties, \'json\');\n\n$classname = \'modChunk\';\n$rows = array();\n$output = \'\';\n\n$c = $modx->newQuery($classname);\n$c->select($modx->getSelectColumns($classname, $classname, \'\', array(\'id\', \'name\')));\n$c->sortby(\'name\');\n\nif (!empty($category)) {\n    $c->where(array(\'category\' => $category));\n}\n//$c->prepare();echo $c->toSql();\nif ($collection = $modx->getCollection($classname, $c)) {\n    $i = 0;\n\n    switch ($format) {\n        case \'json\':\n            foreach ($collection as $object) {\n                $row[\'MIGX_id\'] = (string )$i;\n                $row[\'name\'] = $object->get(\'name\');\n                $row[\'selected\'] = \'0\';\n                $rows[] = $row;\n                $i++;\n            }\n            $output = $modx->toJson($rows);\n            break;\n        \n        case \'optionlist\':\n            foreach ($collection as $object) {\n                $rows[] = $object->get(\'name\');\n                $i++;\n            }\n            $output = implode(\'||\',$rows);      \n        break;\n            \n    }\n\n\n}\n\nreturn $output;', 0, 'a:0:{}', '', 0, ''),
	(50, 0, 0, 'migxSwitchDetailChunk', '', 0, 7, 0, '//[[migxSwitchDetailChunk? &detailChunk=`detailChunk` &listingChunk=`listingChunk`]]\n\n\n$properties[\'migx_id\'] = $modx->getOption(\'migx_id\',$_GET,\'\');\n\nif (!empty($properties[\'migx_id\'])){\n    $output = $modx->getChunk($detailChunk,$properties);\n}\nelse{\n    $output = $modx->getChunk($listingChunk);\n}\n\nreturn $output;', 0, 'a:0:{}', '', 0, ''),
	(51, 0, 0, 'getSwitchColumnCol', '', 0, 7, 0, '$scriptProperties = $_REQUEST;\n$col = \'\';\n// special actions, for example the showSelector - action\n$tempParams = $modx->getOption(\'tempParams\', $scriptProperties, \'\');\n\nif (!empty($tempParams)) {\n    $tempParams = $modx->fromJson($tempParams);\n    $col = $modx->getOption(\'col\', $tempParams, \'\');\n}\n\nreturn $col;', 0, 'a:0:{}', '', 0, ''),
	(52, 0, 0, 'getDayliMIGXrecord', '', 0, 7, 0, '/**\n * getDayliMIGXrecord\n *\n * Copyright 2009-2011 by Bruno Perner <b.perner@gmx.de>\n *\n * getDayliMIGXrecord is free software; you can redistribute it and/or modify it\n * under the terms of the GNU General Public License as published by the Free\n * Software Foundation; either version 2 of the License, or (at your option) any\n * later version.\n *\n * getDayliMIGXrecord is distributed in the hope that it will be useful, but WITHOUT ANY\n * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR\n * A PARTICULAR PURPOSE. See the GNU General Public License for more details.\n *\n * You should have received a copy of the GNU General Public License along with\n * getDayliMIGXrecord; if not, write to the Free Software Foundation, Inc., 59 Temple Place,\n * Suite 330, Boston, MA 02111-1307 USA\n *\n * @package migx\n */\n/**\n * getDayliMIGXrecord\n *\n * display Items from outputvalue of TV with custom-TV-input-type MIGX or from other JSON-string for MODx Revolution \n *\n * @version 1.0\n * @author Bruno Perner <b.perner@gmx.de>\n * @copyright Copyright &copy; 2012\n * @license http://www.gnu.org/licenses/old-licenses/gpl-2.0.html GNU General Public License\n * version 2 or (at your option) any later version.\n * @package migx\n */\n\n/*example: [[!getDayliMIGXrecord? &tvname=`myTV`&tpl=`@CODE:<img src="[[+image]]"/>` &randomize=`1`]]*/\n/* get default properties */\n\n\n$tvname = $modx->getOption(\'tvname\', $scriptProperties, \'\');\n$tpl = $modx->getOption(\'tpl\', $scriptProperties, \'\');\n$randomize = $modx->getOption(\'randomize\', $scriptProperties, false);\n$where = $modx->getOption(\'where\', $scriptProperties, \'\');\n$where = !empty($where) ? $modx->fromJSON($where) : array();\n$sort = $modx->getOption(\'sort\', $scriptProperties, \'\');\n$sort = !empty($sort) ? $modx->fromJSON($sort) : array();\n$toPlaceholder = $modx->getOption(\'toPlaceholder\', $scriptProperties, false);\n$docid = $modx->getOption(\'docid\', $scriptProperties, (isset($modx->resource) ? $modx->resource->get(\'id\') : 1));\n$processTVs = $modx->getOption(\'processTVs\', $scriptProperties, \'1\');\n\n$migx = $modx->getService(\'migx\', \'Migx\', $modx->getOption(\'migx.core_path\', null, $modx->getOption(\'core_path\') . \'components/migx/\') . \'model/migx/\', $scriptProperties);\nif (!($migx instanceof Migx))\n    return \'\';\n$migx->working_context = $modx->resource->get(\'context_key\');\n\nif (!empty($tvname)) {\n    if ($tv = $modx->getObject(\'modTemplateVar\', array(\'name\' => $tvname))) {\n\n        /*\n        *   get inputProperties\n        */\n\n\n        $properties = $tv->get(\'input_properties\');\n        $properties = isset($properties[\'formtabs\']) ? $properties : $tv->getProperties();\n\n        $migx->config[\'configs\'] = $properties[\'configs\'];\n        $migx->loadConfigs();\n        // get tabs from file or migx-config-table\n        $formtabs = $migx->getTabs();\n        if (empty($formtabs)) {\n            //try to get formtabs and its fields from properties\n            $formtabs = $modx->fromJSON($properties[\'formtabs\']);\n        }\n\n        //$tv->setCacheable(false);\n        //$outputvalue = $tv->renderOutput($docid);\n        \n        $tvresource = $modx->getObject(\'modTemplateVarResource\', array(\n            \'tmplvarid\' => $tv->get(\'id\'),\n            \'contentid\' => $docid,\n            ));\n\n\n        $outputvalue = $tvresource->get(\'value\');\n        \n        /*\n        *   get inputTvs \n        */\n        $inputTvs = array();\n        if (is_array($formtabs)) {\n\n            //multiple different Forms\n            // Note: use same field-names and inputTVs in all forms\n            $inputTvs = $migx->extractInputTvs($formtabs);\n        }\n        $migx->source = $tv->getSource($migx->working_context, false);\n\n        if (empty($outputvalue)) {\n            return \'\';\n        }\n\n        $items = $modx->fromJSON($outputvalue);\n\n\n        //is there an active item for the current date?\n        $activedate = $modx->getOption(\'activedate\', $scriptProperties, strftime(\'%Y/%m/%d\'));\n        //$activedate = $modx->getOption(\'activedate\', $_GET, strftime(\'%Y/%m/%d\'));\n        $activewhere = array();\n        $activewhere[\'activedate\'] = $activedate;\n        $activewhere[\'activated\'] = \'1\';\n        $activeitems = $migx->filterItems($activewhere, $items);\n\n        if (count($activeitems) == 0) {\n\n            $activeitems = array();\n            // where filter\n            if (is_array($where) && count($where) > 0) {\n                $items = $migx->filterItems($where, $items);\n            }\n\n            $tempitems = array();\n            $count = count($items);\n            $emptycount = 0;\n            $latestdate = $activedate;\n            $nextdate = strtotime($latestdate);\n            foreach ($items as $item) {\n                //empty all dates and active-states which are older than today\n                if (!empty($item[\'activedate\']) && $item[\'activedate\'] < $activedate) {\n                    $item[\'activated\'] = \'0\';\n                    $item[\'activedate\'] = \'\';\n                }\n                if (empty($item[\'activedate\'])) {\n                    $emptycount++;\n                }\n                if ($item[\'activedate\'] > $latestdate) {\n                    $latestdate = $item[\'activedate\'];\n                    $nextdate = strtotime($latestdate) + (24 * 60 * 60);\n                }\n                if ($item[\'activedate\'] == $activedate) {\n                    $item[\'activated\'] = \'1\';\n                    $activeitems[] = $item;\n                }\n                $tempitems[] = $item;\n            }\n\n            //echo \'<pre>\' . print_r($tempitems, 1) . \'</pre>\';\n\n            $items = $tempitems;\n\n\n            //are there more than half of all items with empty activedates\n\n            if ($emptycount >= $count / 2) {\n\n                // sort items\n                if (is_array($sort) && count($sort) > 0) {\n                    $items = $migx->sortDbResult($items, $sort);\n                }\n                if (count($items) > 0) {\n                    //shuffle items\n                    if ($randomize) {\n                        shuffle($items);\n                    }\n                }\n\n                $tempitems = array();\n                foreach ($items as $item) {\n                    if (empty($item[\'activedate\'])) {\n                        $item[\'activedate\'] = strftime(\'%Y/%m/%d\', $nextdate);\n                        $nextdate = $nextdate + (24 * 60 * 60);\n                        if ($item[\'activedate\'] == $activedate) {\n                            $item[\'activated\'] = \'1\';\n                            $activeitems[] = $item;\n                        }\n                    }\n\n                    $tempitems[] = $item;\n                }\n\n                $items = $tempitems;\n            }\n\n            //$resource = $modx->getObject(\'modResource\', $docid);\n            //echo $modx->toJson($items);\n            $sort = \'[{"sortby":"activedate"}]\';\n            $items = $migx->sortDbResult($items, $modx->fromJson($sort));\n\n            //echo \'<pre>\' . print_r($items, 1) . \'</pre>\';\n\n            $tv->setValue($docid, $modx->toJson($items));\n            $tv->save();\n\n        }\n    }\n\n}\n\n\n$properties = array();\nforeach ($scriptProperties as $property => $value) {\n    $properties[\'property.\' . $property] = $value;\n}\n\n$output = \'\';\n\nforeach ($activeitems as $key => $item) {\n\n    $fields = array();\n    foreach ($item as $field => $value) {\n        $value = is_array($value) ? implode(\'||\', $value) : $value; //handle arrays (checkboxes, multiselects)\n        if ($processTVs && isset($inputTvs[$field])) {\n            if ($tv = $modx->getObject(\'modTemplateVar\', array(\'name\' => $inputTvs[$field][\'inputTV\']))) {\n\n            } else {\n                $tv = $modx->newObject(\'modTemplateVar\');\n                $tv->set(\'type\', $inputTvs[$field][\'inputTVtype\']);\n            }\n            $inputTV = $inputTvs[$field];\n\n            $mTypes = $modx->getOption(\'manipulatable_url_tv_output_types\', null, \'image,file\');\n            //don\'t manipulate any urls here\n            $modx->setOption(\'manipulatable_url_tv_output_types\', \'\');\n            $tv->set(\'default_text\', $value);\n            $value = $tv->renderOutput($docid);\n            //set option back\n            $modx->setOption(\'manipulatable_url_tv_output_types\', $mTypes);\n            //now manipulate urls\n            if ($mediasource = $migx->getFieldSource($inputTV, $tv)) {\n                $mTypes = explode(\',\', $mTypes);\n                if (!empty($value) && in_array($tv->get(\'type\'), $mTypes)) {\n                    //$value = $mediasource->prepareOutputUrl($value);\n                    $value = str_replace(\'/./\', \'/\', $mediasource->prepareOutputUrl($value));\n                }\n            }\n\n        }\n        $fields[$field] = $value;\n\n    }\n\n    $rowtpl = $tpl;\n    //get changing tpls from field\n    if (substr($tpl, 0, 7) == "@FIELD:") {\n        $tplField = substr($tpl, 7);\n        $rowtpl = $fields[$tplField];\n    }\n\n    if (!isset($template[$rowtpl])) {\n        if (substr($rowtpl, 0, 6) == "@FILE:") {\n            $template[$rowtpl] = file_get_contents($modx->config[\'base_path\'] . substr($rowtpl, 6));\n        } elseif (substr($rowtpl, 0, 6) == "@CODE:") {\n            $template[$rowtpl] = substr($tpl, 6);\n        } elseif ($chunk = $modx->getObject(\'modChunk\', array(\'name\' => $rowtpl), true)) {\n            $template[$rowtpl] = $chunk->getContent();\n        } else {\n            $template[$rowtpl] = false;\n        }\n    }\n\n    $fields = array_merge($fields, $properties);\n\n    if ($template[$rowtpl]) {\n        $chunk = $modx->newObject(\'modChunk\');\n        $chunk->setCacheable(false);\n        $chunk->setContent($template[$rowtpl]);\n        $output .= $chunk->process($fields);\n\n    } else {\n        $output .= \'<pre>\' . print_r($fields, 1) . \'</pre>\';\n\n    }\n\n\n}\n\n\nif (!empty($toPlaceholder)) {\n    $modx->setPlaceholder($toPlaceholder, $output);\n    return \'\';\n}\n\nreturn $output;', 0, 'a:0:{}', '', 0, ''),
	(53, 0, 0, 'filterbytag', '', 0, 7, 0, 'if (!is_array($subject)) {\n    $subject = explode(\',\',str_replace(array(\'||\',\' \'),array(\',\',\'\'),$subject));\n}\n\nreturn (in_array($operand,$subject));', 0, 'a:0:{}', '', 0, ''),
	(54, 0, 0, 'migxObjectMediaPath', '', 0, 7, 0, '$pathTpl = $modx->getOption(\'pathTpl\', $scriptProperties, \'\');\n$objectid = $modx->getOption(\'objectid\', $scriptProperties, \'\');\n$createfolder = $modx->getOption(\'createFolder\', $scriptProperties, \'1\');\n$path = \'\';\n$createpath = false;\nif (empty($objectid) && $modx->getPlaceholder(\'objectid\')) {\n    // placeholder was set by some script on frontend for example\n    $objectid = $modx->getPlaceholder(\'objectid\');\n}\nif (empty($objectid) && isset($_REQUEST[\'object_id\'])) {\n    $objectid = $_REQUEST[\'object_id\'];\n}\n\n\n\nif (empty($objectid)) {\n\n    //set Session - var in fields.php - processor\n    if (isset($_SESSION[\'migxWorkingObjectid\'])) {\n        $objectid = $_SESSION[\'migxWorkingObjectid\'];\n        $createpath = !empty($createfolder);\n    }\n\n}\n\n\n$path = str_replace(\'{id}\', $objectid, $pathTpl);\n\n$fullpath = $modx->getOption(\'base_path\') . $path;\n\nif ($createpath && !file_exists($fullpath)) {\n        $permissions = octdec(\'0\' . (int)($modx->getOption(\'new_folder_permissions\', null, \'755\', true)));\n        if (!@mkdir($fullpath, $permissions, true)) {\n            $modx->log(MODX_LOG_LEVEL_ERROR, sprintf(\'[migxResourceMediaPath]: could not create directory %s).\', $fullpath));\n        }\n        else{\n            chmod($fullpath, $permissions); \n        }\n}\n\nreturn $path;', 0, 'a:0:{}', '', 0, ''),
	(55, 0, 0, 'exportMIGX2db', '', 0, 7, 0, '/**\n * exportMIGX2db\n *\n * Copyright 2014 by Bruno Perner <b.perner@gmx.de>\n * \n * Sponsored by Simon Wurster <info@wurster-medien.de>\n *\n * exportMIGX2db is free software; you can redistribute it and/or modify it\n * under the terms of the GNU General Public License as published by the Free\n * Software Foundation; either version 2 of the License, or (at your option) any\n * later version.\n *\n * exportMIGX2db is distributed in the hope that it will be useful, but WITHOUT ANY\n * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR\n * A PARTICULAR PURPOSE. See the GNU General Public License for more details.\n *\n * You should have received a copy of the GNU General Public License along with\n * exportMIGX2db; if not, write to the Free Software Foundation, Inc., 59 Temple Place,\n * Suite 330, Boston, MA 02111-1307 USA\n *\n * @package migx\n */\n/**\n * exportMIGX2db\n *\n * export Items from outputvalue of TV with custom-TV-input-type MIGX or from other JSON-string to db-table \n *\n * @version 1.0\n * @author Bruno Perner <b.perner@gmx.de>\n * @copyright Copyright &copy; 2014\n * @license http://www.gnu.org/licenses/old-licenses/gpl-2.0.html GNU General Public License\n * version 2 or (at your option) any later version.\n * @package migx\n */\n\n/*\n[[!exportMIGX2db? \n&tvname=`references` \n&resources=`25` \n&packageName=`projekte`\n&classname=`Projekt` \n&migx_id_field=`migx_id` \n&renamed_fields=`{"Firmen-URL":"Firmen_url","Projekt-URL":"Projekt_URL","main-image":"main_image"}`\n]]\n*/\n\n\n$tvname = $modx->getOption(\'tvname\', $scriptProperties, \'\');\n$resources = $modx->getOption(\'resources\', $scriptProperties, (isset($modx->resource) ? $modx->resource->get(\'id\') : \'\'));\n$resources = explode(\',\', $resources);\n$prefix = isset($scriptProperties[\'prefix\']) ? $scriptProperties[\'prefix\'] : null;\n$packageName = $modx->getOption(\'packageName\', $scriptProperties, \'\');\n$classname = $modx->getOption(\'classname\', $scriptProperties, \'\');\n$value = $modx->getOption(\'value\', $scriptProperties, \'\');\n$migx_id_field = $modx->getOption(\'migx_id_field\', $scriptProperties, \'\');\n$pos_field = $modx->getOption(\'pos_field\', $scriptProperties, \'\');\n$renamed_fields = $modx->getOption(\'renamed_fields\', $scriptProperties, \'\');\n\n$packagepath = $modx->getOption(\'core_path\') . \'components/\' . $packageName .\n    \'/\';\n$modelpath = $packagepath . \'model/\';\n\n$modx->addPackage($packageName, $modelpath, $prefix);\n$added = 0;\n$modified = 0;\n\nforeach ($resources as $docid) {\n    \n    $outputvalue = \'\';\n    if (count($resources)==1){\n        $outputvalue = $value;    \n    }\n    \n    if (!empty($tvname)) {\n        if ($tv = $modx->getObject(\'modTemplateVar\', array(\'name\' => $tvname))) {\n\n            $outputvalue = empty($outputvalue) ? $tv->renderOutput($docid) : $outputvalue;\n        }\n    }\n\n    if (!empty($outputvalue)) {\n        $renamed = !empty($renamed_fields) ? $modx->fromJson($renamed_fields) : array();\n\n        $items = $modx->fromJSON($outputvalue);\n        $pos = 1;\n        $searchfields = array();\n        if (is_array($items)) {\n            foreach ($items as $fields) {\n                $search = array();\n                if (!empty($migx_id_field)) {\n                    $search[$migx_id_field] = $fields[\'MIGX_id\'];\n                }\n                if (!empty($resource_id_field)) {\n                    $search[$resource_id_field] = $docid;\n                }\n                if (!empty($migx_id_field) && $object = $modx->getObject($classname, $search)) {\n                    $mode = \'mod\';\n                } else {\n                    $object = $modx->newObject($classname);\n                    $object->fromArray($search);\n                    $mode = \'add\';\n                }\n                foreach ($fields as $field => $value) {\n                    $fieldname = array_key_exists($field, $renamed) ? $renamed[$field] : $field;\n                    $object->set($fieldname, $value);\n                }\n                if (!empty($pos_field)) {\n                    $object->set($pos_field,$pos) ;\n                }                \n                if ($object->save()) {\n                    if ($mode == \'add\') {\n                        $added++;\n                    } else {\n                        $modified++;\n                    }\n                }\n                $pos++;\n            }\n            \n        }\n    }\n}\n\n\nreturn $added . \' rows added to db, \' . $modified . \' existing rows actualized\';', 0, 'a:0:{}', '', 0, ''),
	(56, 0, 0, 'preparedatewhere', '', 0, 7, 0, '$name = $modx->getOption(\'name\', $scriptProperties, \'\');\n$date = $modx->getOption($name . \'_date\', $_REQUEST, \'\');\n$dir = str_replace(\'T\', \' \', $modx->getOption($name . \'_dir\', $_REQUEST, \'\'));\n\nif (!empty($date) && !empty($dir) && $dir != \'all\') {\n    switch ($dir) {\n        case \'=\':\n            $where = array(\n            \'enddate:>=\' => strftime(\'%Y-%m-%d 00:00:00\',strtotime($date)),\n            \'startdate:<=\' => strftime(\'%Y-%m-%d 23:59:59\',strtotime($date))\n            );\n            break;\n        case \'>=\':\n            $where = array(\n            \'enddate:>=\' => strftime(\'%Y-%m-%d 00:00:00\',strtotime($date))\n            );\n            break;\n        case \'<=\':\n            $where = array(\n            \'startdate:<=\' => strftime(\'%Y-%m-%d 23:59:59\',strtotime($date))\n            );            \n            break;\n\n    }\n\n    return $modx->toJson($where);\n}', 0, 'a:0:{}', '', 0, ''),
	(57, 0, 0, 'migxJsonToPlaceholders', '', 0, 7, 0, '$value = $modx->getOption(\'value\',$scriptProperties,\'\');\r\n$prefix = $modx->getOption(\'prefix\',$scriptProperties,\'\');\r\n\r\n//$modx->setPlaceholders($modx->fromJson($value),$prefix,\'\',true);\r\n\r\n$values = json_decode($value, true);\r\n\r\n$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($values));\r\n\r\nif (is_array($values)){\r\n    foreach ($it as $key => $value){\r\n        $value = $value == null ? \'\' : $value;\r\n        $modx->setPlaceholder($prefix . $key, $value);\r\n    }\r\n}', 0, 'a:0:{}', '', 0, ''),
	(58, 0, 0, 'migxGetCollectionTree', '', 0, 7, 0, '/**\n * migxGetCollectionTree\n *\n * Copyright 2014 by Bruno Perner <b.perner@gmx.de>\n *\n * migxGetCollectionTree is free software; you can redistribute it and/or modify it\n * under the terms of the GNU General Public License as published by the Free\n * Software Foundation; either version 2 of the License, or (at your option) any\n * later version.\n *\n * migxGetCollectionTree is distributed in the hope that it will be useful, but WITHOUT ANY\n * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR\n * A PARTICULAR PURPOSE. See the GNU General Public License for more details.\n *\n * You should have received a copy of the GNU General Public License along with\n * migxGetCollectionTree; if not, write to the Free Software Foundation, Inc., 59 Temple Place,\n * Suite 330, Boston, MA 02111-1307 USA\n *\n * @package migx\n */\n/**\n * migxGetCollectionTree\n *\n *          display nested items from different objects. The tree-schema is defined by a json-property. \n *\n * @version 1.0.0\n * @author Bruno Perner <b.perner@gmx.de>\n * @copyright Copyright &copy; 2014\n * @license http://www.gnu.org/licenses/old-licenses/gpl-2.0.html GNU General Public License\n * version 2 or (at your option) any later version.\n * @package migx\n */\n\n$treeSchema = $modx->getOption(\'treeSchema\', $scriptProperties, \'\');\n$treeSchema = $modx->fromJson($treeSchema);\n\n$scriptProperties[\'current\'] = $modx->getOption(\'current\', $scriptProperties, \'\');\n$scriptProperties[\'currentClassname\'] = $modx->getOption(\'currentClassname\', $scriptProperties, \'\');\n$scriptProperties[\'currentKeyField\'] = $modx->getOption(\'currentKeyField\', $scriptProperties, \'id\');\n$return = $modx->getOption(\'return\', $scriptProperties, \'parsed\'); //parsed,json,arrayprint\n\n/*\nExamples:\n\nGet Resource-Tree, 4 levels deep\n\n[[!migxGetCollectionTree?\n&current=`57`\n&currentClassname=`modResource`\n&treeSchema=`\n{\n"classname": "modResource",\n"debug": "1",\n"tpl": "mgctResourceTree",\n"wrapperTpl": "@CODE:<ul>[[+output]]</ul>",\n"selectfields": "id,pagetitle",\n"where": {\n"parent": "0",\n"published": "1",\n"deleted": "0"\n},\n"_branches": [{\n"alias": "children",\n"classname": "modResource",\n"local": "parent",\n"foreign": "id",\n"tpl": "mgctResourceTree",\n"debug": "1",\n"selectfields": "id,pagetitle,parent",\n"_branches": [{\n"alias": "children",\n"classname": "modResource",\n"local": "parent",\n"foreign": "id",\n"tpl": "mgctResourceTree",\n"debug": "1",\n"selectfields": "id,pagetitle,parent",\n"where": {\n"published": "1",\n"deleted": "0"\n},\n"_branches": [{\n"alias": "children",\n"classname": "modResource",\n"local": "parent",\n"foreign": "id",\n"tpl": "mgctResourceTree",\n"debug": "1",\n"selectfields": "id,pagetitle,parent",\n"where": {\n"published": "1",\n"deleted": "0"\n}\n}]\n}]\n}]\n}\n`]]\n\nthe chunk mgctResourceTree:\n<li class="[[+_activelabel]] [[+_currentlabel]]" ><a href="[[~[[+id]]]]">[[+pagetitle]]([[+id]])</a></li>\n[[+innercounts.children:gt=`0`:then=`\n<ul>[[+innerrows.children]]</ul>\n`:else=``]]\n\nget all Templates and its Resources:\n\n[[!migxGetCollectionTree?\n&treeSchema=`\n{\n"classname": "modTemplate",\n"debug": "1",\n"tpl": "@CODE:<h3>[[+templatename]]</h3><ul>[[+innerrows.resource]]</ul>",\n"selectfields": "id,templatename",\n"_branches": [{\n"alias": "resource",\n"classname": "modResource",\n"local": "template",\n"foreign": "id",\n"tpl": "@CODE:<li>[[+pagetitle]]([[+id]])</li>",\n"debug": "1",\n"selectfields": "id,pagetitle,template"\n}]\n}\n`]]\n*/\n\nif (!class_exists(\'MigxGetCollectionTree\')) {\n    class MigxGetCollectionTree\n    {\n        function __construct(modX & $modx, array $config = array())\n        {\n            $this->modx = &$modx;\n            $this->config = $config;\n        }\n\n        function getBranch($branch, $foreigns = array(), $level = 1)\n        {\n\n            $rows = array();\n\n            if (count($foreigns) > 0) {\n                $modx = &$this->modx;\n\n                $local = $modx->getOption(\'local\', $branch, \'\');\n                $where = $modx->getOption(\'where\', $branch, array());\n                $where = !empty($where) && !is_array($where) ? $modx->fromJSON($where) : $where;\n                $where[] = array($local . \':IN\' => $foreigns);\n\n                $branch[\'where\'] = $modx->toJson($where);\n\n                $level++;\n                /*\n                if ($levelFromCurrent > 0){\n                $levelFromCurrent++;    \n                }\n                */\n\n                $rows = $this->getRows($branch, $level);\n            }\n\n            return $rows;\n        }\n\n        function getRows($scriptProperties, $level)\n        {\n            $migx = &$this->migx;\n            $modx = &$this->modx;\n\n            $current = $modx->getOption(\'current\', $this->config, \'\');\n            $currentKeyField = $modx->getOption(\'currentKeyField\', $this->config, \'id\');\n            $currentlabel = $modx->getOption(\'currentlabel\', $this->config, \'current\');\n            $classname = $modx->getOption(\'classname\', $scriptProperties, \'\');\n			$sortResult = $modx->getOption(\'sortResult\', $scriptProperties, \'\');\n            $currentClassname = !empty($this->config[\'currentClassname\']) ? $this->config[\'currentClassname\'] : $classname;\n\n            $activelabel = $modx->getOption(\'activelabel\', $this->config, \'active\');\n            $return = $modx->getOption(\'return\', $this->config, \'parsed\');\n\n            $xpdo = $migx->getXpdoInstanceAndAddPackage($scriptProperties);\n            $c = $migx->prepareQuery($xpdo, $scriptProperties);\n            $rows = $migx->getCollection($c);\n\n            $branches = $modx->getOption(\'_branches\', $scriptProperties, array());\n\n            $collectedSubrows = array();\n            foreach ($branches as $branch) {\n                $foreign = $modx->getOption(\'foreign\', $branch, \'\');\n                $local = $modx->getOption(\'local\', $branch, \'\');\n                $alias = $modx->getOption(\'alias\', $branch, \'\');\n                //$activeonly = $modx->getOption(\'activeonly\', $branch, \'\');\n                $foreigns = array();\n                foreach ($rows as $row) {\n                    $foreigns[] = $row[$foreign];\n                }\n\n                $subrows = $this->getBranch($branch, $foreigns, $level);\n                foreach ($subrows as $subrow) {\n\n                    $collectedSubrows[$subrow[$local]][] = $subrow;\n                    $subrow[\'_active\'] = $modx->getOption(\'_active\', $subrow, \'0\');\n                    /*\n                    if (!empty($activeonly) && $subrow[\'_active\'] != \'1\') {\n                    $output = \'\';\n                    } else {\n                    $collectedSubrows[$subrow[$local]][] = $subrow;\n                    }\n                    */\n                    if ($subrow[\'_active\'] == \'1\') {\n                        //echo \'active subrow:<pre>\' . print_r($subrow,1) . \'</pre>\';\n                        $activesubrow[$subrow[$local]] = true;\n                    }\n                    if ($subrow[\'_current\'] == \'1\') {\n                        //echo \'active subrow:<pre>\' . print_r($subrow,1) . \'</pre>\';\n                        $currentsubrow[$subrow[$local]] = true;\n                    }\n\n\n                }\n                //insert subrows\n                $temprows = $rows;\n                $rows = array();\n                foreach ($temprows as $row) {\n                    if (isset($collectedSubrows[$row[$foreign]])) {\n                        $row[\'_active\'] = \'0\';\n                        $row[\'_currentparent\'] = \'0\';\n                        if (isset($activesubrow[$row[$foreign]]) && $activesubrow[$row[$foreign]]) {\n                            $row[\'_active\'] = \'1\';\n                            //echo \'active row:<pre>\' . print_r($row,1) . \'</pre>\';\n                        }\n                        if (isset($currentsubrow[$row[$foreign]]) && $currentsubrow[$row[$foreign]]) {\n                            $row[\'_currentparent\'] = \'1\';\n                            //echo \'active row:<pre>\' . print_r($row,1) . \'</pre>\';\n                        }\n\n                        //render innerrows\n                        //$output = $migx->renderOutput($collectedSubrows[$row[$foreign]],$scriptProperties);\n                        //$output = $collectedSubrows[$row[$foreign]];\n\n                        $row[\'innercounts.\' . $alias] = count($collectedSubrows[$row[$foreign]]);\n                        $row[\'_scriptProperties\'][$alias] = $branch;\n                        /*\n                        switch ($return) {\n                        case \'parsed\':\n                        $output = $migx->renderOutput($collectedSubrows[$row[$foreign]], $branch);\n                        //$subbranches = $modx->getOption(\'_branches\', $branch, array());\n                        //if there are any placeholders left with the same alias from subbranch, remove them\n                        $output = str_replace(\'[[+innerrows.\' . $alias . \']]\', \'\', $output);\n                        break;\n                        case \'json\':\n                        case \'arrayprint\':\n                        $output = $collectedSubrows[$row[$foreign]];\n                        break;\n                        }\n                        */\n                        $output = $collectedSubrows[$row[$foreign]];\n\n                        $row[\'innerrows.\' . $alias] = $output;\n\n                    }\n                    $rows[] = $row;\n                }\n\n            }\n\n            $temprows = $rows;\n            $rows = array();\n            foreach ($temprows as $row) {\n                //add additional placeholders\n                $row[\'_level\'] = $level;\n                $row[\'_active\'] = $modx->getOption(\'_active\', $row, \'0\');\n                if ($currentClassname == $classname && $row[$currentKeyField] == $current) {\n                    $row[\'_current\'] = \'1\';\n                    $row[\'_currentlabel\'] = $currentlabel;\n                    $row[\'_active\'] = \'1\';\n                } else {\n                    $row[\'_current\'] = \'0\';\n                    $row[\'_currentlabel\'] = \'\';\n                }\n                if ($row[\'_active\'] == \'1\') {\n                    $row[\'_activelabel\'] = $activelabel;\n                } else {\n                    $row[\'_activelabel\'] = \'\';\n                }\n                $rows[] = $row;\n            }\n\n            if (!empty($sortResult) && is_array($sortResult)){\n                $rows = $migx->sortDbResult($rows, $sortResult);\n			}\n\n            return $rows;\n        }\n\n        function renderRow($row, $levelFromCurrent = 0)\n        {\n            $migx = &$this->migx;\n            $modx = &$this->modx;\n            $return = $modx->getOption(\'return\', $this->config, \'parsed\');\n            $branchProperties = $modx->getOption(\'_scriptProperties\', $row, array());\n            $current = $modx->getOption(\'_current\', $row, \'0\');\n            $currentparent = $modx->getOption(\'_currentparent\', $row, \'0\');\n            $levelFromCurrent = $current == \'1\' ? 1 : $levelFromCurrent;\n            $row[\'_levelFromCurrent\'] = $levelFromCurrent;\n            foreach ($branchProperties as $alias => $properties) {\n                $innerrows = $modx->getOption(\'innerrows.\' . $alias, $row, array());\n                $subrows = $this->renderRows($innerrows, $properties, $levelFromCurrent, $currentparent);\n                if ($return == \'parsed\') {\n                    $subrows = $migx->renderOutput($subrows, $properties);\n                }\n                $row[\'innerrows.\' . $alias] = $subrows;\n            }\n\n            return $row;\n        }\n\n        function renderRows($rows, $scriptProperties, $levelFromCurrent = 0, $siblingOfCurrent = \'0\')\n        {\n\n            $modx = &$this->modx;\n            $temprows = $rows;\n            $rows = array();\n            if ($levelFromCurrent > 0) {\n                $levelFromCurrent++;\n            }\n            foreach ($temprows as $row) {\n                $row[\'_siblingOfCurrent\'] = $siblingOfCurrent;\n                $row = $this->renderRow($row, $levelFromCurrent);\n                $rows[] = $row;\n            }\n            return $rows;\n        }\n    }\n}\n\n$instance = new MigxGetCollectionTree($modx, $scriptProperties);\n\nif (is_array($treeSchema)) {\n    $scriptProperties = $treeSchema;\n\n    $migx = $modx->getService(\'migx\', \'Migx\', $modx->getOption(\'migx.core_path\', null, $modx->getOption(\'core_path\') . \'components/migx/\') . \'model/migx/\', $scriptProperties);\n    if (!($migx instanceof Migx))\n        return \'\';\n\n    $defaultcontext = \'web\';\n    $migx->working_context = isset($modx->resource) ? $modx->resource->get(\'context_key\') : $defaultcontext;\n    $instance->migx = &$migx;\n\n    $level = 1;\n    $scriptProperties[\'alias\'] = \'row\';\n    $rows = $instance->getRows($scriptProperties, $level);\n\n    $row = array();\n    $row[\'innercounts.row\'] = count($rows);\n    $row[\'innerrows.row\'] = $rows;\n    $row[\'_scriptProperties\'][\'row\'] = $scriptProperties;\n\n    $rows = $instance->renderRow($row);\n\n    $output = \'\';\n    switch ($return) {\n        case \'parsed\':\n            $output = $modx->getOption(\'innerrows.row\', $rows, \'\');\n            break;\n        case \'json\':\n            $output = $modx->toJson($rows);\n            break;\n        case \'arrayprint\':\n            $output = \'<pre>\' . print_r($rows, 1) . \'</pre>\';\n            break;\n    }\n\n    return $output;\n\n}', 0, 'a:0:{}', '', 0, ''),
	(59, 0, 0, 'migxIsNewObject', '', 0, 7, 0, 'if (isset($_REQUEST[\'object_id\']) && $_REQUEST[\'object_id\']==\'new\'){\n    return 1;\n}', 0, 'a:0:{}', '', 0, ''),
	(60, 0, 0, 'migx_example_validate', '', 0, 7, 0, '$properties = &$modx->getOption(\'scriptProperties\', $scriptProperties, array());\n$object = &$modx->getOption(\'object\', $scriptProperties, null);\n$postvalues = &$modx->getOption(\'postvalues\', $scriptProperties, array());\n$form_field = $modx->getOption(\'form_field\', $scriptProperties, array());\n$value = $modx->getOption(\'value\', $scriptProperties, \'\');\n$validation_type = $modx->getOption(\'validation_type\', $scriptProperties, \'\');\n$result = \'\';\nswitch ($validation_type) {\n    case \'gt25\':\n        if ((int) $value > 25) {\n        } else {\n            $error_message = $validation_type; // may be custom validation message\n            $error[\'caption\'] = $form_field;\n            $error[\'validation_type\'] = $error_message;\n            $result[\'error\'] = $error;\n            $result = $modx->toJson($result);\n        }\n        break;\n}\nreturn $result;', 0, 'a:0:{}', '', 0, ''),
	(61, 0, 0, 'migxHookAftercollectmigxitems', '', 0, 7, 0, '$configs = $modx->getOption(\'configs\', $_REQUEST, \'\');\n\n$rows = $modx->getOption(\'rows\', $scriptProperties, array());\n$newrows = array();\n\n\nif (is_array($rows)) {\n    $max_id = 0;\n    $dbfields = array();\n    $existing_dbfields = array();\n    foreach ($rows as $key => $row) {\n        if (isset($row[\'MIGX_id\']) && $row[\'MIGX_id\'] > $max_id) {\n            $max_id = $row[\'MIGX_id\'];\n        }\n        if (isset($row[\'selected_dbfields\']) && isset($row[\'existing_dbfields\'])) {\n            $dbfields = is_array($row[\'selected_dbfields\']) ? $row[\'selected_dbfields\'] : array($row[\'selected_dbfields\']);\n            \n            $existing_dbfields = explode(\'||\', $row[\'existing_dbfields\']);\n            //echo \'<pre>\' . print_r($existing_dbfields,1) . \'</pre>\';die();\n\n        } else {\n            $newrows[] = $row;\n        }\n\n    }\n\n    foreach ($dbfields as $dbfield) {\n        if (!empty($dbfield) && !in_array($dbfield, $existing_dbfields)) {\n            $max_id++;\n            $newrow = array();\n            $newrow[\'MIGX_id\'] = $max_id;\n\n            switch ($configs) {\n                case \'migxformtabfields\':\n                    $newrow[\'field\'] = $dbfield;\n                    $newrow[\'caption\'] = $dbfield;\n                    break;\n                case \'migxcolumns\':\n                    $newrow[\'dataIndex\'] = $dbfield;\n                    $newrow[\'header\'] = $dbfield;\n                    break;                    \n            }\n\n\n            $newrows[] = $newrow;\n        }\n    }\n\n\n}\n\n\nreturn json_encode($newrows);', 0, 'a:0:{}', '', 0, ''),
	(62, 0, 0, 'migxAutoPublish', '', 0, 7, 0, '$classnames = explode(\',\', $modx->getOption(\'classnames\',$scriptProperties,\'\'));\n$packageName = $modx->getOption(\'packageName\',$scriptProperties,\'\');\n\nswitch ($mode) {\n    case \'datetime\' :\n        $timeNow = strftime(\'%Y-%m-%d %H:%M:%S\');\n        break;\n    case \'unixtime\' :\n        $timeNow = time();\n        break;\n    default :\n        $timeNow = strftime(\'%Y-%m-%d %H:%M:%S\');\n        break;\n}\n\n$modx->addPackage($packageName, $modx->getOption(\'core_path\') . \'components/\' . $packageName . \'/model/\');\n\nforeach ($classnames as $classname) {\n    if (!empty($classname)) {\n        $tblResource = $modx->getTableName($classname);\n        if (!$result = $modx->exec("UPDATE {$tblResource} SET published=1,publishedon=pub_date,pub_date=null WHERE pub_date < \'{$timeNow}\' AND pub_date > 0 AND published=0")) {\n            $modx->log(modX::LOG_LEVEL_ERROR, \'Error while refreshing resource publishing data: \' . print_r($modx->errorInfo(), true));\n        }\n        if (!$result = $modx->exec("UPDATE $tblResource SET published=0,unpub_date=null WHERE unpub_date < \'{$timeNow}\' AND unpub_date IS NOT NULL AND unpub_date > 0 AND published=1")) {\n            $modx->log(modX::LOG_LEVEL_ERROR, \'Error while refreshing resource unpublishing data: \' . print_r($modx->errorInfo(), true));\n        }\n    }\n\n}\n$modx->cacheManager->refresh();', 0, 'a:0:{}', '', 0, ''),
	(63, 0, 0, 'migxGetObject', '', 0, 7, 0, '/*\ngetXpdoInstanceAndAddPackage - properties\n\n$prefix\n$usecustomprefix\n$packageName\n\n\nprepareQuery - properties:\n\n$limit\n$offset\n$totalVar\n$where\n$queries\n$sortConfig\n$groupby\n$joins\n$selectfields\n$classname\n$debug\n\nrenderOutput - properties:\n\n$tpl\n$wrapperTpl\n$toSeparatePlaceholders\n$toPlaceholder\n$outputSeparator\n$placeholdersKeyField\n$toJsonPlaceholder\n$jsonVarKey\n$addfields\n\nmigxGetObject - properties\n\n$id\n$toPlaceholders - if not empty, output to placeholders with specified prefix or print the array, if \'print_r\' is the property-value\n\n*/\n\n$id = $modx->getOption(\'id\',$scriptProperties,\'\');\n$scriptProperties[\'limit\'] = 1;\n\n$migx = $modx->getService(\'migx\', \'Migx\', $modx->getOption(\'migx.core_path\', null, $modx->getOption(\'core_path\') . \'components/migx/\') . \'model/migx/\', $scriptProperties);\nif (!($migx instanceof Migx))\n    return \'\';\n//$modx->migx = &$migx;\n\n$xpdo = $migx->getXpdoInstanceAndAddPackage($scriptProperties);\n\n$defaultcontext = \'web\';\n$migx->working_context = isset($modx->resource) ? $modx->resource->get(\'context_key\') : $defaultcontext;\n\n$c = $migx->prepareQuery($xpdo,$scriptProperties);\nif (!empty($id)){\n    $c->where(array(\'id\'=>$id));\n	$c->prepare();\n}	\n$rows = $migx->getCollection($c);\n\n$output = $migx->renderOutput($rows,$scriptProperties);\n\nreturn $output;', 0, 'a:0:{}', '', 0, ''),
	(3, 0, 0, 'If', 'Simple if (conditional) snippet', 0, 0, 0, '/**\n * If\n *\n * Copyright 2009-2010 by Jason Coward <jason@modx.com> and Shaun McCormick\n * <shaun@modx.com>\n *\n * If is free software; you can redistribute it and/or modify it under the terms\n * of the GNU General Public License as published by the Free Software\n * Foundation; either version 2 of the License, or (at your option) any later\n * version.\n *\n * If is distributed in the hope that it will be useful, but WITHOUT ANY\n * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR\n * A PARTICULAR PURPOSE. See the GNU General Public License for more details.\n *\n * You should have received a copy of the GNU General Public License along with\n * If; if not, write to the Free Software Foundation, Inc., 59 Temple Place,\n * Suite 330, Boston, MA 02111-1307 USA\n *\n * @package if\n */\n/**\n * Simple if (conditional) snippet\n *\n * @package if\n */\nif (!empty($debug)) {\n    print_r($scriptProperties);\n    if (!empty($die)) die();\n}\n$modx->parser->processElementTags(\'\',$subject,true,true);\n\n$output = \'\';\n$operator = !empty($operator) ? $operator : \'\';\n$operand = !isset($operand) ? \'\' : $operand;\nif (isset($subject)) {\n    if (!empty($operator)) {\n        $operator = strtolower($operator);\n        switch ($operator) {\n            case \'!=\':\n            case \'neq\':\n            case \'not\':\n            case \'isnot\':\n            case \'isnt\':\n            case \'unequal\':\n            case \'notequal\':\n                $output = (($subject != $operand) ? $then : (isset($else) ? $else : \'\'));\n                break;\n            case \'<\':\n            case \'lt\':\n            case \'less\':\n            case \'lessthan\':\n                $output = (($subject < $operand) ? $then : (isset($else) ? $else : \'\'));\n                break;\n            case \'>\':\n            case \'gt\':\n            case \'greater\':\n            case \'greaterthan\':\n                $output = (($subject > $operand) ? $then : (isset($else) ? $else : \'\'));\n                break;\n            case \'<=\':\n            case \'lte\':\n            case \'lessthanequals\':\n            case \'lessthanorequalto\':\n                $output = (($subject <= $operand) ? $then : (isset($else) ? $else : \'\'));\n                break;\n            case \'>=\':\n            case \'gte\':\n            case \'greaterthanequals\':\n            case \'greaterthanequalto\':\n                $output = (($subject >= $operand) ? $then : (isset($else) ? $else : \'\'));\n                break;\n            case \'isempty\':\n            case \'empty\':\n                $output = empty($subject) ? $then : (isset($else) ? $else : \'\');\n                break;\n            case \'!empty\':\n            case \'notempty\':\n            case \'isnotempty\':\n                $output = !empty($subject) && $subject != \'\' ? $then : (isset($else) ? $else : \'\');\n                break;\n            case \'isnull\':\n            case \'null\':\n                $output = $subject == null || strtolower($subject) == \'null\' ? $then : (isset($else) ? $else : \'\');\n                break;\n            case \'inarray\':\n            case \'in_array\':\n            case \'ia\':\n                $operand = explode(\',\',$operand);\n                $output = in_array($subject,$operand) ? $then : (isset($else) ? $else : \'\');\n                break;\n            case \'==\':\n            case \'=\':\n            case \'eq\':\n            case \'is\':\n            case \'equal\':\n            case \'equals\':\n            case \'equalto\':\n            default:\n                $output = (($subject == $operand) ? $then : (isset($else) ? $else : \'\'));\n                break;\n        }\n    }\n}\nif (!empty($debug)) { var_dump($output); }\nunset($subject);\nreturn $output;', 0, 'a:6:{s:7:"subject";a:7:{s:4:"name";s:7:"subject";s:4:"desc";s:24:"The data being affected.";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";N;s:4:"area";s:0:"";}s:8:"operator";a:7:{s:4:"name";s:8:"operator";s:4:"desc";s:24:"The type of conditional.";s:4:"type";s:4:"list";s:7:"options";a:10:{i:0;a:2:{s:5:"value";s:2:"EQ";s:4:"text";s:2:"EQ";}i:1;a:2:{s:5:"value";s:3:"NEQ";s:4:"text";s:3:"NEQ";}i:2;a:2:{s:5:"value";s:2:"LT";s:4:"text";s:2:"LT";}i:3;a:2:{s:5:"value";s:2:"GT";s:4:"text";s:2:"GT";}i:4;a:2:{s:5:"value";s:3:"LTE";s:4:"text";s:3:"LTE";}i:5;a:2:{s:5:"value";s:2:"GT";s:4:"text";s:3:"GTE";}i:6;a:2:{s:5:"value";s:5:"EMPTY";s:4:"text";s:5:"EMPTY";}i:7;a:2:{s:5:"value";s:8:"NOTEMPTY";s:4:"text";s:8:"NOTEMPTY";}i:8;a:2:{s:5:"value";s:6:"ISNULL";s:4:"text";s:6:"ISNULL";}i:9;a:2:{s:5:"value";s:7:"inarray";s:4:"text";s:7:"INARRAY";}}s:5:"value";s:2:"EQ";s:7:"lexicon";N;s:4:"area";s:0:"";}s:7:"operand";a:7:{s:4:"name";s:7:"operand";s:4:"desc";s:62:"When comparing to the subject, this is the data to compare to.";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";N;s:4:"area";s:0:"";}s:4:"then";a:7:{s:4:"name";s:4:"then";s:4:"desc";s:43:"If conditional was successful, output this.";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";N;s:4:"area";s:0:"";}s:4:"else";a:7:{s:4:"name";s:4:"else";s:4:"desc";s:45:"If conditional was unsuccessful, output this.";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";N;s:4:"area";s:0:"";}s:5:"debug";a:7:{s:4:"name";s:5:"debug";s:4:"desc";s:92:"Will output the parameters passed in, as well as the end output. Leave off when not testing.";s:4:"type";s:13:"combo-boolean";s:7:"options";s:0:"";s:5:"value";b:0;s:7:"lexicon";N;s:4:"area";s:0:"";}}', '', 0, ''),
	(4, 1, 0, 'splitTags', '', 0, 0, 0, '$result = \'\';\n    $tags = ((isset($tags))?($tags):(\'\'));\n    $tags_array = explode(\',\', $tags);\n    foreach($tags_array as $tag)\n    {\n        $result .= sprintf(\'<li class = "%s"></li>\', trim($tag));\n    \n    }\n    $result = \'<ul>\'.$result.\'</ul>\';\n    return $result;', 0, 'a:0:{}', '', 0, ''),
	(6, 1, 0, 'Random', '', 0, 0, 0, 'return rand(2000, 3000);', 0, 'a:0:{}', '', 0, ''),
	(44, 0, 0, 'migxGetRelations', '', 0, 7, 0, '$id = $modx->getOption(\'id\', $scriptProperties, $modx->resource->get(\'id\'));\n$toPlaceholder = $modx->getOption(\'toPlaceholder\', $scriptProperties, \'\');\n$element = $modx->getOption(\'element\', $scriptProperties, \'getResources\');\n$outputSeparator = $modx->getOption(\'outputSeparator\', $scriptProperties, \',\');\n$sourceWhere = $modx->getOption(\'sourceWhere\', $scriptProperties, \'\');\n$ignoreRelationIfEmpty = $modx->getOption(\'ignoreRelationIfEmpty\', $scriptProperties, false);\n$inheritFromParents = $modx->getOption(\'inheritFromParents\', $scriptProperties, false);\n$parentIDs = $inheritFromParents ? array_merge(array($id), $modx->getParentIds($id)) : array($id);\n\n$packageName = \'resourcerelations\';\n\n$packagepath = $modx->getOption(\'core_path\') . \'components/\' . $packageName . \'/\';\n$modelpath = $packagepath . \'model/\';\n\n$modx->addPackage($packageName, $modelpath, $prefix);\n$classname = \'rrResourceRelation\';\n$output = \'\';\n\nforeach ($parentIDs as $id) {\n    if (!empty($id)) {\n        $output = \'\';\n                \n        $c = $modx->newQuery($classname, array(\'target_id\' => $id, \'published\' => \'1\'));\n        $c->select($modx->getSelectColumns($classname, $classname));\n\n        if (!empty($sourceWhere)) {\n            $sourceWhere_ar = $modx->fromJson($sourceWhere);\n            if (is_array($sourceWhere_ar)) {\n                $where = array();\n                foreach ($sourceWhere_ar as $key => $value) {\n                    $where[\'Source.\' . $key] = $value;\n                }\n                $joinclass = \'modResource\';\n                $joinalias = \'Source\';\n                $selectfields = \'id\';\n                $selectfields = !empty($selectfields) ? explode(\',\', $selectfields) : null;\n                $c->leftjoin($joinclass, $joinalias);\n                $c->select($modx->getSelectColumns($joinclass, $joinalias, $joinalias . \'_\', $selectfields));\n                $c->where($where);\n            }\n        }\n\n        //$c->prepare(); echo $c->toSql();\n        if ($c->prepare() && $c->stmt->execute()) {\n            $collection = $c->stmt->fetchAll(PDO::FETCH_ASSOC);\n        }\n        \n        foreach ($collection as $row) {\n            $ids[] = $row[\'source_id\'];\n        }\n        $output = implode($outputSeparator, $ids);\n    }\n    if (!empty($output)){\n        break;\n    }\n}\n\n\nif (!empty($element)) {\n    if (empty($output) && $ignoreRelationIfEmpty) {\n        return $modx->runSnippet($element, $scriptProperties);\n    } else {\n        $scriptProperties[\'resources\'] = $output;\n        $scriptProperties[\'parents\'] = \'9999999\';\n        return $modx->runSnippet($element, $scriptProperties);\n    }\n\n\n}\n\nif (!empty($toPlaceholder)) {\n    $modx->setPlaceholder($toPlaceholder, $output);\n    return \'\';\n}\n\nreturn $output;', 0, 'a:0:{}', '', 0, ''),
	(45, 0, 0, 'migx', '', 0, 7, 0, '$tvname = $modx->getOption(\'tvname\', $scriptProperties, \'\');\n$tpl = $modx->getOption(\'tpl\', $scriptProperties, \'\');\n$limit = $modx->getOption(\'limit\', $scriptProperties, \'0\');\n$offset = $modx->getOption(\'offset\', $scriptProperties, 0);\n$totalVar = $modx->getOption(\'totalVar\', $scriptProperties, \'total\');\n$randomize = $modx->getOption(\'randomize\', $scriptProperties, false);\n$preselectLimit = $modx->getOption(\'preselectLimit\', $scriptProperties, 0); // when random preselect important images\n$where = $modx->getOption(\'where\', $scriptProperties, \'\');\n$where = !empty($where) ? $modx->fromJSON($where) : array();\n$sortConfig = $modx->getOption(\'sortConfig\', $scriptProperties, \'\');\n$sortConfig = !empty($sortConfig) ? $modx->fromJSON($sortConfig) : array();\n$configs = $modx->getOption(\'configs\', $scriptProperties, \'\');\n$configs = !empty($configs) ? explode(\',\',$configs):array();\n$toSeparatePlaceholders = $modx->getOption(\'toSeparatePlaceholders\', $scriptProperties, false);\n$toPlaceholder = $modx->getOption(\'toPlaceholder\', $scriptProperties, false);\n$outputSeparator = $modx->getOption(\'outputSeparator\', $scriptProperties, \'\');\n//$placeholdersKeyField = $modx->getOption(\'placeholdersKeyField\', $scriptProperties, \'MIGX_id\');\n$placeholdersKeyField = $modx->getOption(\'placeholdersKeyField\', $scriptProperties, \'id\');\n$toJsonPlaceholder = $modx->getOption(\'toJsonPlaceholder\', $scriptProperties, false);\n$jsonVarKey = $modx->getOption(\'jsonVarKey\', $scriptProperties, \'migx_outputvalue\');\n$outputvalue = $modx->getOption(\'value\', $scriptProperties, \'\');\n$outputvalue = isset($_REQUEST[$jsonVarKey]) ? $_REQUEST[$jsonVarKey] : $outputvalue;\n$docidVarKey = $modx->getOption(\'docidVarKey\', $scriptProperties, \'migx_docid\');\n$docid = $modx->getOption(\'docid\', $scriptProperties, (isset($modx->resource) ? $modx->resource->get(\'id\') : 1));\n$docid = isset($_REQUEST[$docidVarKey]) ? $_REQUEST[$docidVarKey] : $docid;\n$processTVs = $modx->getOption(\'processTVs\', $scriptProperties, \'1\');\n\n$base_path = $modx->getOption(\'base_path\', null, MODX_BASE_PATH);\n$base_url = $modx->getOption(\'base_url\', null, MODX_BASE_URL);\n\n$migx = $modx->getService(\'migx\', \'Migx\', $modx->getOption(\'migx.core_path\', null, $modx->getOption(\'core_path\') . \'components/migx/\') . \'model/migx/\', $scriptProperties);\nif (!($migx instanceof Migx))\n    return \'\';\n//$modx->migx = &$migx;\n$defaultcontext = \'web\';\n$migx->working_context = isset($modx->resource) ? $modx->resource->get(\'context_key\') : $defaultcontext;\n\nif (!empty($tvname))\n{\n    if ($tv = $modx->getObject(\'modTemplateVar\', array(\'name\' => $tvname)))\n    {\n\n        /*\n        *   get inputProperties\n        */\n\n\n        $properties = $tv->get(\'input_properties\');\n        $properties = isset($properties[\'configs\']) ? $properties : $tv->getProperties();\n        $cfgs = $modx->getOption(\'configs\',$properties,\'\');\n        if (!empty($cfgs)){\n            $cfgs = explode(\',\',$cfgs);\n            $configs = array_merge($configs,$cfgs);\n           \n        }\n        \n    }\n}\n\n\n\n//$migx->config[\'configs\'] = implode(\',\',$configs);\n$migx->loadConfigs(false,true,array(\'configs\'=>implode(\',\',$configs)));\n$migx->customconfigs = array_merge($migx->customconfigs,$scriptProperties);\n\n\n\n// get tabs from file or migx-config-table\n$formtabs = $migx->getTabs();\nif (empty($formtabs))\n{\n    //try to get formtabs and its fields from properties\n    $formtabs = $modx->fromJSON($properties[\'formtabs\']);\n}\n\nif ($jsonVarKey == \'migx_outputvalue\' && !empty($properties[\'jsonvarkey\']))\n{\n    $jsonVarKey = $properties[\'jsonvarkey\'];\n    $outputvalue = isset($_REQUEST[$jsonVarKey]) ? $_REQUEST[$jsonVarKey] : $outputvalue;\n}\n\n$outputvalue = $tv && empty($outputvalue) ? $tv->renderOutput($docid) : $outputvalue;\n/*\n*   get inputTvs \n*/\n$inputTvs = array();\nif (is_array($formtabs))\n{\n\n    //multiple different Forms\n    // Note: use same field-names and inputTVs in all forms\n    $inputTvs = $migx->extractInputTvs($formtabs);\n}\n\nif ($tv)\n{\n    $migx->source = $tv->getSource($migx->working_context, false);\n}\n\n//$task = $modx->migx->getTask();\n$filename = \'getlist.php\';\n$processorspath = $migx->config[\'processorsPath\'] . \'mgr/\';\n$filenames = array();\n$scriptProperties[\'start\'] = $modx->getOption(\'offset\', $scriptProperties, 0);\nif ($processor_file = $migx->findProcessor($processorspath, $filename, $filenames))\n{\n    include ($processor_file);\n    //todo: add getlist-processor for default-MIGX-TV\n}\n\n$items = isset($rows) && is_array($rows) ? $rows : array();\n$modx->setPlaceholder($totalVar, isset($count) ? $count : 0);\n\n$properties = array();\nforeach ($scriptProperties as $property => $value)\n{\n    $properties[\'property.\' . $property] = $value;\n}\n\n$idx = 0;\n$output = array();\nforeach ($items as $key => $item)\n{\n\n    $fields = array();\n    foreach ($item as $field => $value)\n    {\n        $value = is_array($value) ? implode(\'||\', $value) : $value; //handle arrays (checkboxes, multiselects)\n        if ($processTVs && isset($inputTvs[$field]))\n        {\n            if ($tv = $modx->getObject(\'modTemplateVar\', array(\'name\' => $inputTvs[$field][\'inputTV\'])))\n            {\n\n            } else\n            {\n                $tv = $modx->newObject(\'modTemplateVar\');\n                $tv->set(\'type\', $inputTvs[$field][\'inputTVtype\']);\n            }\n            $inputTV = $inputTvs[$field];\n\n            $mTypes = $modx->getOption(\'manipulatable_url_tv_output_types\', null, \'image,file\');\n            //don\'t manipulate any urls here\n            $modx->setOption(\'manipulatable_url_tv_output_types\', \'\');\n            $tv->set(\'default_text\', $value);\n            $value = $tv->renderOutput($docid);\n            //set option back\n            $modx->setOption(\'manipulatable_url_tv_output_types\', $mTypes);\n            //now manipulate urls\n            if ($mediasource = $migx->getFieldSource($inputTV, $tv))\n            {\n                $mTypes = explode(\',\', $mTypes);\n                if (!empty($value) && in_array($tv->get(\'type\'), $mTypes))\n                {\n                    //$value = $mediasource->prepareOutputUrl($value);\n                    $value = str_replace(\'/./\', \'/\', $mediasource->prepareOutputUrl($value));\n                }\n            }\n\n        }\n        $fields[$field] = $value;\n\n    }\n    if ($toJsonPlaceholder)\n    {\n        $output[] = $fields;\n    } else\n    {\n        $fields[\'_alt\'] = $idx % 2;\n        $idx++;\n        $fields[\'_first\'] = $idx == 1 ? true : \'\';\n        $fields[\'_last\'] = $idx == $limit ? true : \'\';\n        $fields[\'idx\'] = $idx;\n        $rowtpl = $tpl;\n        //get changing tpls from field\n        if (substr($tpl, 0, 7) == "@FIELD:")\n        {\n            $tplField = substr($tpl, 7);\n            $rowtpl = $fields[$tplField];\n        }\n\n        if (!isset($template[$rowtpl]))\n        {\n            if (substr($rowtpl, 0, 6) == "@FILE:")\n            {\n                $template[$rowtpl] = file_get_contents($modx->config[\'base_path\'] . substr($rowtpl, 6));\n            } elseif (substr($rowtpl, 0, 6) == "@CODE:")\n            {\n                $template[$rowtpl] = substr($tpl, 6);\n            } elseif ($chunk = $modx->getObject(\'modChunk\', array(\'name\' => $rowtpl), true))\n            {\n                $template[$rowtpl] = $chunk->getContent();\n            } else\n            {\n                $template[$rowtpl] = false;\n            }\n        }\n\n        $fields = array_merge($fields, $properties);\n\n        if ($template[$rowtpl])\n        {\n            $chunk = $modx->newObject(\'modChunk\');\n            $chunk->setCacheable(false);\n            $chunk->setContent($template[$rowtpl]);\n            if (!empty($placeholdersKeyField) && isset($fields[$placeholdersKeyField]))\n            {\n                $output[$fields[$placeholdersKeyField]] = $chunk->process($fields);\n            } else\n            {\n                $output[] = $chunk->process($fields);\n            }\n        } else\n        {\n            if (!empty($placeholdersKeyField))\n            {\n                $output[$fields[$placeholdersKeyField]] = \'<pre>\' . print_r($fields, 1) . \'</pre>\';\n            } else\n            {\n                $output[] = \'<pre>\' . print_r($fields, 1) . \'</pre>\';\n            }\n        }\n    }\n\n\n}\n\n\nif ($toJsonPlaceholder)\n{\n    $modx->setPlaceholder($toJsonPlaceholder, $modx->toJson($output));\n    return \'\';\n}\n\nif (!empty($toSeparatePlaceholders))\n{\n    $modx->toPlaceholders($output, $toSeparatePlaceholders);\n    return \'\';\n}\n/*\nif (!empty($outerTpl))\n$o = parseTpl($outerTpl, array(\'output\'=>implode($outputSeparator, $output)));\nelse \n*/\nif (is_array($output))\n{\n    $o = implode($outputSeparator, $output);\n} else\n{\n    $o = $output;\n}\n\nif (!empty($toPlaceholder))\n{\n    $modx->setPlaceholder($toPlaceholder, $o);\n    return \'\';\n}\n\nreturn $o;', 0, 'a:0:{}', '', 0, ''),
	(46, 0, 0, 'migxLoopCollection', '', 0, 7, 0, '/*\ngetXpdoInstanceAndAddPackage - properties\n\n$prefix\n$usecustomprefix\n$packageName\n\n\nprepareQuery - properties:\n\n$limit\n$offset\n$totalVar\n$where\n$queries\n$sortConfig\n$groupby\n$joins\n$selectfields\n$classname\n$debug\n\nrenderOutput - properties:\n\n$tpl\n$wrapperTpl\n$toSeparatePlaceholders\n$toPlaceholder\n$outputSeparator\n$placeholdersKeyField\n$toJsonPlaceholder\n$jsonVarKey\n$addfields\n\n*/\n\n\n$migx = $modx->getService(\'migx\', \'Migx\', $modx->getOption(\'migx.core_path\', null, $modx->getOption(\'core_path\') . \'components/migx/\') . \'model/migx/\', $scriptProperties);\nif (!($migx instanceof Migx))\n    return \'\';\n//$modx->migx = &$migx;\n\n$xpdo = $migx->getXpdoInstanceAndAddPackage($scriptProperties);\n\n$defaultcontext = \'web\';\n$migx->working_context = isset($modx->resource) ? $modx->resource->get(\'context_key\') : $defaultcontext;\n\n$c = $migx->prepareQuery($xpdo,$scriptProperties);\n$rows = $migx->getCollection($c);\n\n$output = $migx->renderOutput($rows,$scriptProperties);\n\nreturn $output;', 0, 'a:0:{}', '', 0, ''),
	(47, 0, 0, 'migxResourceMediaPath', '', 0, 7, 0, '/**\n * @name migxResourceMediaPath\n * @description Dynamically calculates the upload path for a given resource\n * \n * This Snippet is meant to dynamically calculate your baseBath attribute\n * for custom Media Sources.  This is useful if you wish to shepard uploaded\n * images to a folder dedicated to a given resource.  E.g. page 123 would \n * have its own images that page 456 could not reference.\n *\n * USAGE:\n * [[migxResourceMediaPath? &pathTpl=`assets/businesses/{id}/`]]\n * [[migxResourceMediaPath? &pathTpl=`assets/test/{breadcrumb}`]]\n * [[migxResourceMediaPath? &pathTpl=`assets/test/{breadcrumb}` &breadcrumbdepth=`2`]]\n *\n * PARAMETERS\n * &pathTpl string formatting string specifying the file path. \n *		Relative to MODX base_path\n *		Available placeholders: {id}, {pagetitle}, {parent}\n * &docid (optional) integer page id\n * &createFolder (optional) boolean whether or not to create\n */\n$pathTpl = $modx->getOption(\'pathTpl\', $scriptProperties, \'\');\n$docid = $modx->getOption(\'docid\', $scriptProperties, \'\');\n$createfolder = $modx->getOption(\'createFolder\', $scriptProperties, false);\n$tvname = $modx->getOption(\'tvname\', $scriptProperties, \'\');\n\n$path = \'\';\n$createpath = false;\n\nif (empty($pathTpl)) {\n    $modx->log(MODX_LOG_LEVEL_ERROR, \'[migxResourceMediaPath]: pathTpl not specified.\');\n    return;\n}\n\nif (empty($docid) && $modx->getPlaceholder(\'mediasource_docid\')) {\n    // placeholder was set by some script\n    // warning: the parser may not render placeholders, e.g. &docid=`[[*parent]]` may fail\n    $docid = $modx->getPlaceholder(\'mediasource_docid\');\n}\n\nif (empty($docid) && $modx->getPlaceholder(\'docid\')) {\n    // placeholder was set by some script\n    // warning: the parser may not render placeholders, e.g. &docid=`[[*parent]]` may fail\n    $docid = $modx->getPlaceholder(\'docid\');\n}\nif (empty($docid)) {\n\n    //on frontend\n    if (is_object($modx->resource)) {\n        $docid = $modx->resource->get(\'id\');\n    }\n    //on backend\n    else {\n        $createpath = $createfolder;\n        // We do this to read the &id param from an Ajax request\n        $parsedUrl = parse_url($_SERVER[\'HTTP_REFERER\']);\n        parse_str($parsedUrl[\'query\'], $parsedQuery);\n\n        if (isset($parsedQuery[\'amp;id\'])) {\n            $docid = (int)$parsedQuery[\'amp;id\'];\n        } elseif (isset($parsedQuery[\'id\'])) {\n            $docid = (int)$parsedQuery[\'id\'];\n        }\n    }\n}\n\nif (empty($docid)) {\n    $modx->log(MODX_LOG_LEVEL_ERROR, \'[migxResourceMediaPath]: docid could not be determined.\');\n    return;\n}\n\nif ($resource = $modx->getObject(\'modResource\', $docid)) {\n    $path = $pathTpl;\n    $ultimateParent = \'\';\n    if (strstr($path, \'{breadcrumb}\') || strstr($path, \'{ultimateparent}\')) {\n        $depth = $modx->getOption(\'breadcrumbdepth\', $scriptProperties, 10);\n        $ctx = $resource->get(\'context_key\');\n        $parentids = $modx->getParentIds($docid, $depth, array(\'context\' => $ctx));\n        $breadcrumbdepth = $modx->getOption(\'breadcrumbdepth\', $scriptProperties, count($parentids));\n        $breadcrumbdepth = $breadcrumbdepth > count($parentids) ? count($parentids) : $breadcrumbdepth;\n        if (count($parentids) > 1) {\n            $parentids = array_reverse($parentids);\n            $parentids[] = $docid;\n            $ultimateParent = $parentids[1];\n        } else {\n            $ultimateParent = $docid;\n            $parentids = array();\n            $parentids[] = $docid;\n        }\n    }\n\n    if (strstr($path, \'{breadcrumb}\')) {\n        $breadcrumbpath = \'\';\n        for ($i = 1; $i <= $breadcrumbdepth; $i++) {\n            $breadcrumbpath .= $parentids[$i] . \'/\';\n        }\n        $path = str_replace(\'{breadcrumb}\', $breadcrumbpath, $path);\n    }\n    \n    if (!empty($tvname)){\n        $path = str_replace(\'{tv_value}\', $resource->getTVValue($tvname), $path);    \n    }\n    $path = str_replace(\'{id}\', $docid, $path);\n    $path = str_replace(\'{pagetitle}\', $resource->get(\'pagetitle\'), $path);\n    $path = str_replace(\'{alias}\', $resource->get(\'alias\'), $path);\n    $path = str_replace(\'{parent}\', $resource->get(\'parent\'), $path);\n    $path = str_replace(\'{context_key}\', $resource->get(\'context_key\'), $path);\n    $path = str_replace(\'{ultimateparent}\', $ultimateParent, $path);\n    if ($template = $resource->getOne(\'Template\')) {\n        $path = str_replace(\'{templatename}\', $template->get(\'templatename\'), $path);\n    }\n    if ($user = $modx->user) {\n        $path = str_replace(\'{username}\', $modx->user->get(\'username\'), $path);\n        $path = str_replace(\'{userid}\', $modx->user->get(\'id\'), $path);\n    }\n\n    $fullpath = $modx->getOption(\'base_path\') . $path;\n\n    if ($createpath && !file_exists($fullpath)) {\n\n        $permissions = octdec(\'0\' . (int)($modx->getOption(\'new_folder_permissions\', null, \'755\', true)));\n        if (!@mkdir($fullpath, $permissions, true)) {\n            $modx->log(MODX_LOG_LEVEL_ERROR, sprintf(\'[migxResourceMediaPath]: could not create directory %s).\', $fullpath));\n        } else {\n            chmod($fullpath, $permissions);\n        }\n    }\n\n    return $path;\n} else {\n    $modx->log(MODX_LOG_LEVEL_ERROR, sprintf(\'[migxResourceMediaPath]: resource not found (page id %s).\', $docid));\n    return;\n}', 0, 'a:0:{}', '', 0, ''),
	(43, 0, 0, 'getImageList', '', 0, 7, 0, '/**\n * getImageList\n *\n * Copyright 2009-2014 by Bruno Perner <b.perner@gmx.de>\n *\n * getImageList is free software; you can redistribute it and/or modify it\n * under the terms of the GNU General Public License as published by the Free\n * Software Foundation; either version 2 of the License, or (at your option) any\n * later version.\n *\n * getImageList is distributed in the hope that it will be useful, but WITHOUT ANY\n * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR\n * A PARTICULAR PURPOSE. See the GNU General Public License for more details.\n *\n * You should have received a copy of the GNU General Public License along with\n * getImageList; if not, write to the Free Software Foundation, Inc., 59 Temple Place,\n * Suite 330, Boston, MA 02111-1307 USA\n *\n * @package migx\n */\n/**\n * getImageList\n *\n * display Items from outputvalue of TV with custom-TV-input-type MIGX or from other JSON-string for MODx Revolution \n *\n * @version 1.4\n * @author Bruno Perner <b.perner@gmx.de>\n * @copyright Copyright &copy; 2009-2014\n * @license http://www.gnu.org/licenses/old-licenses/gpl-2.0.html GNU General Public License\n * version 2 or (at your option) any later version.\n * @package migx\n */\n\n/*example: <ul>[[!getImageList? &tvname=`myTV`&tpl=`@CODE:<li>[[+idx]]<img src="[[+imageURL]]"/><p>[[+imageAlt]]</p></li>`]]</ul>*/\n/* get default properties */\n\n$allow_request = (bool)$modx->getOption(\'allowRequest\', $scriptProperties, false);\n$tvname = $modx->getOption(\'tvname\', $scriptProperties, \'\');\n$inherit_children_tvname = $modx->getOption(\'inherit_children_tvname\', $scriptProperties, \'\');\n$tpl = $modx->getOption(\'tpl\', $scriptProperties, \'\');\n$wrapperTpl = $modx->getOption(\'wrapperTpl\', $scriptProperties, \'\');\n$emptyTpl = $modx->getOption(\'emptyTpl\', $scriptProperties, \'\'); \n$limit = $modx->getOption(\'limit\', $scriptProperties, \'0\');\n$offset = $modx->getOption(\'offset\', $scriptProperties, 0);\n$totalVar = $modx->getOption(\'totalVar\', $scriptProperties, \'total\');\n$randomize = $modx->getOption(\'randomize\', $scriptProperties, false);\n$preselectLimit = $modx->getOption(\'preselectLimit\', $scriptProperties, 0); // when random preselect important images\n$where = $modx->getOption(\'where\', $scriptProperties, \'\');\n$where = !empty($where) ? $modx->fromJSON($where) : array();\n$sort = $modx->getOption(\'sort\', $scriptProperties, \'\');\n$sort = !empty($sort) ? $modx->fromJSON($sort) : array();\n$toSeparatePlaceholders = $modx->getOption(\'toSeparatePlaceholders\', $scriptProperties, false);\n$toPlaceholder = $modx->getOption(\'toPlaceholder\', $scriptProperties, false);\n$outputSeparator = $modx->getOption(\'outputSeparator\', $scriptProperties, \'\');\n$splitSeparator = $modx->getOption(\'splitSeparator\', $scriptProperties, \'\');\n$placeholdersKeyField = $modx->getOption(\'placeholdersKeyField\', $scriptProperties, \'MIGX_id\');\n$toJsonPlaceholder = $modx->getOption(\'toJsonPlaceholder\', $scriptProperties, false);\n$jsonVarKey = $modx->getOption(\'jsonVarKey\', $scriptProperties, \'migx_outputvalue\');\n$outputvalue = $modx->getOption(\'value\', $scriptProperties, \'\');\nif ($allow_request) {\n    $outputvalue = isset($_REQUEST[$jsonVarKey]) ? $_REQUEST[$jsonVarKey] : $outputvalue;\n}\n$docidVarKey = $modx->getOption(\'docidVarKey\', $scriptProperties, \'migx_docid\');\n$docid = $modx->getOption(\'docid\', $scriptProperties, (isset($modx->resource) ? $modx->resource->get(\'id\') : 1));\nif ($allow_request) {\n    $docid = isset($_REQUEST[$docidVarKey]) ? $_REQUEST[$docidVarKey] : $docid;\n}\n$processTVs = $modx->getOption(\'processTVs\', $scriptProperties, \'1\');\n$reverse = $modx->getOption(\'reverse\', $scriptProperties, \'0\');\n$sumFields = $modx->getOption(\'sumFields\', $scriptProperties, \'\');\n$sumPrefix = $modx->getOption(\'sumPrefix\', $scriptProperties, \'summary_\');\n$addfields = $modx->getOption(\'addfields\', $scriptProperties, \'\');\n$addfields = !empty($addfields) ? explode(\',\', $addfields) : null;\n//split json into parts\n$splits = $modx->fromJson($modx->getOption(\'splits\', $scriptProperties, 0));\n$splitTpl = $modx->getOption(\'splitTpl\', $scriptProperties, \'\');\n$splitSeparator = $modx->getOption(\'splitSeparator\', $scriptProperties, \'\');\n$inheritFrom = $modx->getOption(\'inheritFrom\', $scriptProperties, \'\'); //commaseparated list of resource-ids or/and the keyword \'parents\' where to inherit from\n$inheritFrom = !empty($inheritFrom) ? explode(\',\', $inheritFrom) : \'\';\n\n$modx->setPlaceholder(\'docid\', $docid);\n\n$base_path = $modx->getOption(\'base_path\', null, MODX_BASE_PATH);\n$base_url = $modx->getOption(\'base_url\', null, MODX_BASE_URL);\n\n$migx = $modx->getService(\'migx\', \'Migx\', $modx->getOption(\'migx.core_path\', null, $modx->getOption(\'core_path\') . \'components/migx/\') . \'model/migx/\', $scriptProperties);\nif (!($migx instanceof Migx))\n    return \'\';\n$migx->working_context = isset($modx->resource) ? $modx->resource->get(\'context_key\') : \'web\';\n\nif (!empty($tvname)) {\n    if ($tv = $modx->getObject(\'modTemplateVar\', array(\'name\' => $tvname))) {\n\n        /*\n        *   get inputProperties\n        */\n\n\n        $properties = $tv->get(\'input_properties\');\n        $properties = isset($properties[\'formtabs\']) ? $properties : $tv->getProperties();\n\n        $migx->config[\'configs\'] = $modx->getOption(\'configs\', $properties, \'\');\n        if (!empty($migx->config[\'configs\'])) {\n            $migx->loadConfigs();\n            // get tabs from file or migx-config-table\n            $formtabs = $migx->getTabs();\n        }\n        if (empty($formtabs) && isset($properties[\'formtabs\'])) {\n            //try to get formtabs and its fields from properties\n            $formtabs = $modx->fromJSON($properties[\'formtabs\']);\n        }\n\n        if (!empty($properties[\'basePath\'])) {\n            if ($properties[\'autoResourceFolders\'] == \'true\') {\n                $scriptProperties[\'base_path\'] = $base_path . $properties[\'basePath\'] . $docid . \'/\';\n                $scriptProperties[\'base_url\'] = $base_url . $properties[\'basePath\'] . $docid . \'/\';\n            } else {\n                $scriptProperties[\'base_path\'] = $base_path . $properties[\'base_path\'];\n                $scriptProperties[\'base_url\'] = $base_url . $properties[\'basePath\'];\n            }\n        }\n        if ($jsonVarKey == \'migx_outputvalue\' && !empty($properties[\'jsonvarkey\'])) {\n            $jsonVarKey = $properties[\'jsonvarkey\'];\n            $outputvalue = $allow_request && isset($_REQUEST[$jsonVarKey]) ? $_REQUEST[$jsonVarKey] : $outputvalue;\n        }\n\n        if (empty($outputvalue)) {\n            $outputvalue = $tv->renderOutput($docid);\n            if (empty($outputvalue) && !empty($inheritFrom)) {\n                foreach ($inheritFrom as $from) {\n                    if ($from == \'parents\') {\n                        if (!empty($inherit_children_tvname)){\n                            //try to get items from optional MIGX-TV for children\n                            if ($inh_tv = $modx->getObject(\'modTemplateVar\', array(\'name\' => $inherit_children_tvname))) {\n                                $outputvalue = $inh_tv->processInheritBinding(\'\', $docid);    \n                            }\n                        }\n                        $outputvalue = empty($outputvalue) ? $tv->processInheritBinding(\'\', $docid) : $outputvalue;\n                    } else {\n                        $outputvalue = $tv->renderOutput($from);\n                    }\n                    if (!empty($outputvalue)) {\n                        break;\n                    }\n                }\n            }\n        }\n\n\n        /*\n        *   get inputTvs \n        */\n        $inputTvs = array();\n        if (is_array($formtabs)) {\n\n            //multiple different Forms\n            // Note: use same field-names and inputTVs in all forms\n            $inputTvs = $migx->extractInputTvs($formtabs);\n        }\n        if ($migx->source = $tv->getSource($migx->working_context, false)) {\n            $migx->source->initialize();\n        }\n\n    }\n\n\n}\n\nif (empty($outputvalue)) {\n    $modx->setPlaceholder($totalVar, 0);\n    return \'\';\n}\n\n//echo $outputvalue.\'<br/><br/>\';\n\n$items = $modx->fromJSON($outputvalue);\n\n// where filter\nif (is_array($where) && count($where) > 0) {\n    $items = $migx->filterItems($where, $items);\n}\n$modx->setPlaceholder($totalVar, count($items));\n\nif (!empty($reverse)) {\n    $items = array_reverse($items);\n}\n\n// sort items\nif (is_array($sort) && count($sort) > 0) {\n    $items = $migx->sortDbResult($items, $sort);\n}\n\n$summaries = array();\n$output = \'\';\n$items = $offset > 0 ? array_slice($items, $offset) : $items;\n$count = count($items);\n\nif ($count > 0) {\n    $limit = $limit == 0 || $limit > $count ? $count : $limit;\n    $preselectLimit = $preselectLimit > $count ? $count : $preselectLimit;\n    //preselect important items\n    $preitems = array();\n    if ($randomize && $preselectLimit > 0) {\n        for ($i = 0; $i < $preselectLimit; $i++) {\n            $preitems[] = $items[$i];\n            unset($items[$i]);\n        }\n        $limit = $limit - count($preitems);\n    }\n\n    //shuffle items\n    if ($randomize) {\n        shuffle($items);\n    }\n\n    //limit items\n    $count = count($items);\n    $tempitems = array();\n\n    for ($i = 0; $i < $limit; $i++) {\n        if ($i >= $count) {\n            break;\n        }\n        $tempitems[] = $items[$i];\n    }\n    $items = $tempitems;\n\n    //add preselected items and schuffle again\n    if ($randomize && $preselectLimit > 0) {\n        $items = array_merge($preitems, $items);\n        shuffle($items);\n    }\n\n    $properties = array();\n    foreach ($scriptProperties as $property => $value) {\n        $properties[\'property.\' . $property] = $value;\n    }\n\n    $idx = 0;\n    $output = array();\n    $template = array();\n    $count = count($items);\n\n    foreach ($items as $key => $item) {\n        $formname = isset($item[\'MIGX_formname\']) ? $item[\'MIGX_formname\'] . \'_\' : \'\';\n        $fields = array();\n        foreach ($item as $field => $value) {\n            if (is_array($value)) {\n                if (is_array($value[0])) {\n                    //nested array - convert to json\n                    $value = $modx->toJson($value);\n                } else {\n                    $value = implode(\'||\', $value); //handle arrays (checkboxes, multiselects)\n                }\n            }\n\n\n            $inputTVkey = $formname . $field;\n\n            if ($processTVs && isset($inputTvs[$inputTVkey])) {\n                if (isset($inputTvs[$inputTVkey][\'inputTV\']) && $tv = $modx->getObject(\'modTemplateVar\', array(\'name\' => $inputTvs[$inputTVkey][\'inputTV\']))) {\n\n                } else {\n                    $tv = $modx->newObject(\'modTemplateVar\');\n                    $tv->set(\'type\', $inputTvs[$inputTVkey][\'inputTVtype\']);\n                }\n                $inputTV = $inputTvs[$inputTVkey];\n\n                $mTypes = $modx->getOption(\'manipulatable_url_tv_output_types\', null, \'image,file\');\n                //don\'t manipulate any urls here\n                $modx->setOption(\'manipulatable_url_tv_output_types\', \'\');\n                $tv->set(\'default_text\', $value);\n\n                // $value = $tv->renderOutput($docid); breaks if the TV used in MIGX is also assigned to this Template,\n                // example tv: imageLogo is assigned to the template and imageLogo is assigned to the MIGX TV as a result\n                // only the value of the imageLogo is returned for the MIGX TV instance\n                // need to override default MODX method: $value = $tv->renderOutput($docid);\n                /* process any TV commands in value */\n                $tv_value = $tv->processBindings($value, $docid);\n                $params = $tv->get(\'output_properties\');\n                if (empty($params) || $params === null) {\n                    $params = [];\n                }\n                /* run prepareOutput to allow for custom overriding */\n                $tv_value = $tv->prepareOutput($tv_value, $docid);\n                /* find the render */\n                $outputRenderPaths = $tv->getRenderDirectories(\'OnTVOutputRenderList\',\'output\');\n                $value = $tv->getRender($params, $tv_value, $outputRenderPaths, \'output\', $docid, $tv->get(\'display\'));\n                // End override of $value = $tv->renderOutput($docid);\n				\n                //set option back\n                $modx->setOption(\'manipulatable_url_tv_output_types\', $mTypes);\n                //now manipulate urls\n                if ($mediasource = $migx->getFieldSource($inputTV, $tv)) {\n                    $mTypes = explode(\',\', $mTypes);\n                    if (!empty($value) && in_array($tv->get(\'type\'), $mTypes)) {\n                        //$value = $mediasource->prepareOutputUrl($value);\n                        $value = str_replace(\'/./\', \'/\', $mediasource->prepareOutputUrl($value));\n                    }\n                }\n\n            }\n            $fields[$field] = $value;\n\n        }\n\n        if (!empty($addfields)) {\n            foreach ($addfields as $addfield) {\n                $addfield = explode(\':\', $addfield);\n                $addname = $addfield[0];\n                $adddefault = isset($addfield[1]) ? $addfield[1] : \'\';\n                $fields[$addname] = $adddefault;\n            }\n        }\n\n        if (!empty($sumFields)) {\n            $sumFields = is_array($sumFields) ? $sumFields : explode(\',\', $sumFields);\n            foreach ($sumFields as $sumField) {\n                if (isset($fields[$sumField])) {\n                    $summaries[$sumPrefix . $sumField] = $summaries[$sumPrefix . $sumField] + $fields[$sumField];\n                    $fields[$sumPrefix . $sumField] = $summaries[$sumPrefix . $sumField];\n                }\n            }\n        }\n\n\n        if ($toJsonPlaceholder) {\n            $output[] = $fields;\n        } else {\n            $fields[\'_alt\'] = $idx % 2;\n            $idx++;\n            $fields[\'_first\'] = $idx == 1 ? true : \'\';\n            $fields[\'_last\'] = $idx == $limit ? true : \'\';\n            $fields[\'idx\'] = $idx;\n            $rowtpl = \'\';\n            //get changing tpls from field\n            if (substr($tpl, 0, 7) == "@FIELD:") {\n                $tplField = substr($tpl, 7);\n                $rowtpl = $fields[$tplField];\n            }\n\n            if ($fields[\'_first\'] && !empty($tplFirst)) {\n                $rowtpl = $tplFirst;\n            }\n            if ($fields[\'_last\'] && empty($rowtpl) && !empty($tplLast)) {\n                $rowtpl = $tplLast;\n            }\n            $tplidx = \'tpl_\' . $idx;\n            if (empty($rowtpl) && !empty($$tplidx)) {\n                $rowtpl = $$tplidx;\n            }\n            if ($idx > 1 && empty($rowtpl)) {\n                $divisors = $migx->getDivisors($idx);\n                if (!empty($divisors)) {\n                    foreach ($divisors as $divisor) {\n                        $tplnth = \'tpl_n\' . $divisor;\n                        if (!empty($$tplnth)) {\n                            $rowtpl = $$tplnth;\n                            if (!empty($rowtpl)) {\n                                break;\n                            }\n                        }\n                    }\n                }\n            }\n\n            if ($count == 1 && isset($tpl_oneresult)) {\n                $rowtpl = $tpl_oneresult;\n            }\n\n            $fields = array_merge($fields, $properties);\n\n            if (!empty($rowtpl)) {\n                $template = $migx->getTemplate($tpl, $template);\n                $fields[\'_tpl\'] = $template[$tpl];\n            } else {\n                $rowtpl = $tpl;\n\n            }\n            $template = $migx->getTemplate($rowtpl, $template);\n\n\n            if ($template[$rowtpl]) {\n                $chunk = $modx->newObject(\'modChunk\');\n                $chunk->setCacheable(false);\n                $chunk->setContent($template[$rowtpl]);\n\n\n                if (!empty($placeholdersKeyField) && isset($fields[$placeholdersKeyField])) {\n                    $output[$fields[$placeholdersKeyField]] = $chunk->process($fields);\n                } else {\n                    $output[] = $chunk->process($fields);\n                }\n            } else {\n                if (!empty($placeholdersKeyField)) {\n                    $output[$fields[$placeholdersKeyField]] = \'<pre>\' . print_r($fields, 1) . \'</pre>\';\n                } else {\n                    $output[] = \'<pre>\' . print_r($fields, 1) . \'</pre>\';\n                }\n            }\n        }\n\n\n    }\n}\n\nif (count($summaries) > 0) {\n    $modx->toPlaceholders($summaries);\n}\n\n\nif ($toJsonPlaceholder) {\n    $modx->setPlaceholder($toJsonPlaceholder, $modx->toJson($output));\n    return \'\';\n}\n\nif (!empty($toSeparatePlaceholders)) {\n    $modx->toPlaceholders($output, $toSeparatePlaceholders);\n    return \'\';\n}\n/*\nif (!empty($outerTpl))\n$o = parseTpl($outerTpl, array(\'output\'=>implode($outputSeparator, $output)));\nelse \n*/\n\nif ($count > 0 && $splits > 0) {\n    $size = ceil($count / $splits);\n    $chunks = array_chunk($output, $size);\n    $output = array();\n    foreach ($chunks as $chunk) {\n        $o = implode($outputSeparator, $chunk);\n        $output[] = $modx->getChunk($splitTpl, array(\'output\' => $o));\n    }\n    $outputSeparator = $splitSeparator;\n}\n\nif (is_array($output)) {\n    $o = implode($outputSeparator, $output);\n} else {\n    $o = $output;\n}\n\nif (!empty($o) && !empty($wrapperTpl)) {\n    $template = $migx->getTemplate($wrapperTpl);\n    if ($template[$wrapperTpl]) {\n        $chunk = $modx->newObject(\'modChunk\');\n        $chunk->setCacheable(false);\n        $chunk->setContent($template[$wrapperTpl]);\n        $properties[\'output\'] = $o;\n        $o = $chunk->process($properties);\n    }\n}\n\nif (empty($o) && !empty($emptyTpl)) {\n    $template = $migx->getTemplate($emptyTpl);\n    if ($template[$emptyTpl]) {\n        $chunk = $modx->newObject(\'modChunk\');\n        $chunk->setCacheable(false);\n        $chunk->setContent($template[$emptyTpl]);\n        $o = $chunk->process($properties);\n    }\n}\n\nif (!empty($toPlaceholder)) {\n    $modx->setPlaceholder($toPlaceholder, $o);\n    return \'\';\n}\n\nreturn $o;', 0, 'a:0:{}', '', 0, ''),
	(26, 0, 0, 'FormItAutoResponder', 'Custom hook for FormIt to handle Auto-Response emails.', 0, 0, 0, '/**\r\n * FormIt\r\n *\r\n * Copyright 2009-2012 by Shaun McCormick <shaun@modx.com>\r\n *\r\n * FormIt is free software; you can redistribute it and/or modify it\r\n * under the terms of the GNU General Public License as published by the Free\r\n * Software Foundation; either version 2 of the License, or (at your option) any\r\n * later version.\r\n *\r\n * FormIt is distributed in the hope that it will be useful, but WITHOUT ANY\r\n * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR\r\n * A PARTICULAR PURPOSE. See the GNU General Public License for more details.\r\n *\r\n * You should have received a copy of the GNU General Public License along with\r\n * FormIt; if not, write to the Free Software Foundation, Inc., 59 Temple Place,\r\n * Suite 330, Boston, MA 02111-1307 USA\r\n *\r\n * @package formit\r\n */\r\n/**\r\n * A custom FormIt hook for auto-responders.\r\n *\r\n * @var modX $modx\r\n * @var array $scriptProperties\r\n * @var FormIt $formit\r\n * @var fiHooks $hook\r\n * \r\n * @package formit\r\n */\r\n/* setup default properties */\r\n$tpl = $modx->getOption(\'fiarTpl\',$scriptProperties,\'fiarTpl\');\r\n$mailFrom = $modx->getOption(\'fiarFrom\',$scriptProperties,$modx->getOption(\'emailsender\'));\r\n$mailFromName = $modx->getOption(\'fiarFromName\',$scriptProperties,$modx->getOption(\'site_name\'));\r\n$mailSender = $modx->getOption(\'fiarSender\',$scriptProperties,$modx->getOption(\'emailsender\'));\r\n$mailSubject = $modx->getOption(\'fiarSubject\',$scriptProperties,\'[[++site_name]] Auto-Responder\');\r\n$mailSubject = str_replace(array(\'[[++site_name]]\',\'[[++emailsender]]\'),array($modx->getOption(\'site_name\'),$modx->getOption(\'emailsender\')),$mailSubject);\r\n$fiarFiles = $modx->getOption(\'fiarFiles\',$scriptProperties,false);\r\n$isHtml = $modx->getOption(\'fiarHtml\',$scriptProperties,true);\r\n$toField = $modx->getOption(\'fiarToField\',$scriptProperties,\'email\');\r\n$multiSeparator = $modx->getOption(\'fiarMultiSeparator\',$formit->config,"\\n");\r\n$multiWrapper = $modx->getOption(\'fiarMultiWrapper\',$formit->config,"[[+value]]");\r\n$required = $modx->getOption(\'fiarRequired\',$scriptProperties,true);\r\nif (empty($fields[$toField])) {\r\n    if ($required) {\r\n        $modx->log(modX::LOG_LEVEL_ERROR,\'[FormIt] Auto-responder could not find field `\'.$toField.\'` in form submission.\');\r\n        return false;\r\n    } else {\r\n        return true;\r\n    }\r\n}\r\n\r\n/* handle checkbox and array fields */\r\nforeach ($fields as $k => &$v) {\r\n    if (is_array($v) && !empty($v[\'name\']) && isset($v[\'error\']) && $v[\'error\'] == UPLOAD_ERR_OK) {\r\n        $fields[$k] = $v[\'name\'];\r\n    } else if (is_array($v)) {\r\n        $vOpts = array();\r\n        foreach ($v as $vKey => $vValue) {\r\n            if (is_string($vKey) && !empty($vKey)) {\r\n                $vKey = $k.\'.\'.$vKey;\r\n                $fields[$vKey] = $vValue;\r\n            } else {\r\n                $vOpts[] = str_replace(\'[[+value]]\',$vValue,$multiWrapper);\r\n            }\r\n        }\r\n        $newValue = implode($multiSeparator,$vOpts);\r\n        if (!empty($vOpts)) {\r\n            $fields[$k] = $newValue;\r\n        }\r\n    }\r\n}\r\n\r\n/* setup placeholders */\r\n$placeholders = $fields;\r\n$mailTo= $fields[$toField];\r\n\r\n$message = $formit->getChunk($tpl,$placeholders);\r\n$modx->parser->processElementTags(\'\',$message,true,false);\r\n\r\n$modx->getService(\'mail\', \'mail.modPHPMailer\');\r\n$modx->mail->reset();\r\n$modx->mail->set(modMail::MAIL_BODY,$message);\r\n$modx->mail->set(modMail::MAIL_FROM,$hook->_process($mailFrom,$placeholders));\r\n$modx->mail->set(modMail::MAIL_FROM_NAME,$hook->_process($mailFromName,$placeholders));\r\n$modx->mail->set(modMail::MAIL_SENDER,$hook->_process($mailSender,$placeholders));\r\n$modx->mail->set(modMail::MAIL_SUBJECT,$hook->_process($mailSubject,$placeholders));\r\n$modx->mail->address(\'to\',$mailTo);\r\n$modx->mail->setHTML($isHtml);\r\n\r\n/* add attachments */\r\nif($fiarFiles){\r\n    $fiarFiles = explode(\',\', $fiarFiles);\r\n    foreach($fiarFiles AS $file){\r\n        $modx->mail->mailer->AddAttachment($file);\r\n    }\r\n}\r\n\r\n/* reply to */\r\n$emailReplyTo = $modx->getOption(\'fiarReplyTo\',$scriptProperties,$mailFrom);\r\n$emailReplyTo = $hook->_process($emailReplyTo,$fields);\r\n$emailReplyToName = $modx->getOption(\'fiarReplyToName\',$scriptProperties,$mailFromName);\r\n$emailReplyToName = $hook->_process($emailReplyToName,$fields);\r\nif (!empty($emailReplyTo)) {\r\n    $modx->mail->address(\'reply-to\',$emailReplyTo,$emailReplyToName);\r\n}\r\n\r\n/* cc */\r\n$emailCC = $modx->getOption(\'fiarCC\',$scriptProperties,\'\');\r\nif (!empty($emailCC)) {\r\n    $emailCCName = $modx->getOption(\'fiarCCName\',$scriptProperties,\'\');\r\n    $emailCC = explode(\',\',$emailCC);\r\n    $emailCCName = explode(\',\',$emailCCName);\r\n    $numAddresses = count($emailCC);\r\n    for ($i=0;$i<$numAddresses;$i++) {\r\n        $etn = !empty($emailCCName[$i]) ? $emailCCName[$i] : \'\';\r\n        if (!empty($etn)) $etn = $hook->_process($etn,$fields);\r\n        $emailCC[$i] = $hook->_process($emailCC[$i],$fields);\r\n        if (!empty($emailCC[$i])) {\r\n            $modx->mail->address(\'cc\',$emailCC[$i],$etn);\r\n        }\r\n    }\r\n}\r\n\r\n/* bcc */\r\n$emailBCC = $modx->getOption(\'fiarBCC\',$scriptProperties,\'\');\r\nif (!empty($emailBCC)) {\r\n    $emailBCCName = $modx->getOption(\'fiarBCCName\',$scriptProperties,\'\');\r\n    $emailBCC = explode(\',\',$emailBCC);\r\n    $emailBCCName = explode(\',\',$emailBCCName);\r\n    $numAddresses = count($emailBCC);\r\n    for ($i=0;$i<$numAddresses;$i++) {\r\n        $etn = !empty($emailBCCName[$i]) ? $emailBCCName[$i] : \'\';\r\n        if (!empty($etn)) $etn = $hook->_process($etn,$fields);\r\n        $emailBCC[$i] = $hook->_process($emailBCC[$i],$fields);\r\n        if (!empty($emailBCC[$i])) {\r\n            $modx->mail->address(\'bcc\',$emailBCC[$i],$etn);\r\n        }\r\n    }\r\n}\r\n\r\nif (!$formit->inTestMode) {\r\n    if (!$modx->mail->send()) {\r\n        $modx->log(modX::LOG_LEVEL_ERROR,\'[FormIt] An error occurred while trying to send the auto-responder email: \'.$modx->mail->mailer->ErrorInfo);\r\n        return false;\r\n    }\r\n}\r\n$modx->mail->reset();\r\nreturn true;', 0, 'a:0:{}', '', 0, ''),
	(32, 0, 0, 'FormItSaveForm', 'Custom hook for FormIt to save the form.', 0, 0, 0, '/**\r\n * FormIt\r\n *\r\n * Copyright 2011-12 by SCHERP Ontwikkeling <info@scherpontwikkeling.nl>\r\n * Copyright 2015 by Wieger Sloot <modx@sterc.nl>\r\n *\r\n * FormIt is free software; you can redistribute it and/or modify it\r\n * under the terms of the GNU General Public License as published by the Free\r\n * Software Foundation; either version 2 of the License, or (at your option) any\r\n * later version.\r\n *\r\n * FormIt is distributed in the hope that it will be useful, but WITHOUT ANY\r\n * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR\r\n * A PARTICULAR PURPOSE. See the GNU General Public License for more details.\r\n *\r\n * You should have received a copy of the GNU General Public License along with\r\n * FormIt; if not, write to the Free Software Foundation, Inc., 59 Temple Place,\r\n * Suite 330, Boston, MA 02111-1307 USA\r\n *\r\n * @package formit\r\n */\r\n/**\r\n * A custom FormIt hook for saving filled-in forms. - Based on FormSave\r\n *\r\n * @var modX $modx\r\n * @var array $scriptProperties\r\n * @var FormIt $formit\r\n * @var fiHooks $hook\r\n * \r\n * @package formit\r\n */\r\n/* setup default properties */\r\n$values = $hook->getValues();\r\n$formName = $modx->getOption(\'formName\', $formit->config, \'form-\'.$modx->resource->get(\'id\'));\r\n// process formName. Pick a value from the form\r\n// Inspired from the email\'s hook of formit (fihooks.class.php)\r\nif (is_string($formName)) {\r\n    foreach ($fields as $k => $v) {\r\n        if (is_scalar($k) && is_scalar($v)) {\r\n            $formName = str_replace(\'[[+\'.$k.\']]\',$v,$formName);\r\n        }\r\n    }\r\n}\r\n\r\n$formEncrypt = $modx->getOption(\'formEncrypt\', $formit->config, false);\r\n$formFields = $modx->getOption(\'formFields\', $formit->config, false);\r\n$fieldNames = $modx->getOption(\'fieldNames\', $formit->config, false);\r\n$updateSavedForm = $modx->getOption(\'updateSavedForm\', $formit->config, false); // true, false, \'1\', \'0\', or \'values\'\r\n// In order to use update process, you need to provide the hash key to the user somehow\r\n// Usually with [[!FormItRetriever]] to populate this form field:\r\n$formHashKeyField = $modx->getOption(\'savedFormHashKeyField\', $formit->config, \'savedFormHashKey\');\r\n// Disable if you want to use the session_id() in your hash, like FormIt does\r\n// WARNING: this can cause potential hash key collisions and overwriting of the wrong form record!!\r\n$formHashKeyRandom = $modx->getOption(\'formHashKeyRandom\', $formit->config, true);\r\n// process formHashKeyField into variable for later use\r\n$formHashKey = (isset($values[$formHashKeyField])) ? (string) $values[$formHashKeyField] : \'\';\r\n// our hashing methods return 32 chars\r\nif (strlen($formHashKey) !== 32) $formHashKey = \'\';\r\nunset($values[$formHashKeyField]);\r\n\r\nif ($formFields) {\r\n    $formFields = explode(\',\', $formFields);\r\n    foreach($formFields as $k => $v) {\r\n        $formFields[$k] = trim($v);\r\n    }\r\n}\r\n// Build the data array\r\n$dataArray = array();\r\nif($formFields){\r\n    foreach($formFields as $field) {\r\n        $dataArray[$field] = (!isset($values[$field])) ? \'\' : $values[$field];\r\n    }\r\n}else{\r\n    $dataArray = $values;\r\n}\r\n//Change the fieldnames\r\nif($fieldNames){\r\n    $newDataArray = array();\r\n    $fieldLabels = array();\r\n    $formFieldNames = explode(\',\', $fieldNames);\r\n    foreach($formFieldNames as $formFieldName){\r\n        list($name, $label) = explode(\'==\', $formFieldName);\r\n        $fieldLabels[trim($name)] = trim($label);\r\n    }\r\n    foreach ($dataArray as $key => $value) {\r\n        if($fieldLabels[$key]){\r\n            $newDataArray[$fieldLabels[$key]] = $value;\r\n        }else{\r\n            $newDataArray[$key] = $value;\r\n        }\r\n    }\r\n    $dataArray = $newDataArray;\r\n}\r\n// We only enter update mode if we already have a valid formHashKey (tested above)\r\n// AND the updateSavedForm param was set to a truth-y value.\r\n$mode = ($updateSavedForm && $formHashKey) ? \'update\' : \'create\';\r\n// Create/get obj\r\n$newForm = null;\r\nif ($mode === \'update\') {\r\n    $newForm = $modx->getObject(\'FormItForm\', array(\'hash\' => $formHashKey));\r\n}\r\nif ($newForm === null) $newForm = $modx->newObject(\'FormItForm\');\r\n\r\n// Handle encryption\r\nif($formEncrypt){\r\n    $dataArray = $newForm->encrypt($modx->toJSON($dataArray));\r\n}else{\r\n    $dataArray = $modx->toJSON($dataArray);\r\n}\r\n\r\n// Create new hash key on create mode, and handle invalid hash keys. \r\nif ($mode === \'create\') {\r\n    $formHashKey = ($formHashKeyRandom) ? $newForm->generatePseudoRandomHash() : pathinfo($formit->getStoreKey(), PATHINFO_BASENAME);\r\n}\r\n\r\n// Array from which to populate form record\r\n$newFormArray = array();\r\n\r\n// Special case: if updateSavedForm has the flag \'values\' we only merge in\r\n// the form values, not the other stuff\r\nif ($mode === \'update\' && $updateSavedForm === \'values\') {\r\n    $newFormArray = $newForm->toArray();\r\n    $newFormArray = array_merge($newFormArray, array(\r\n        \'values\' => $dataArray,\r\n    ));       \r\n} else {\r\n    // In all other cases, we overwrite the record completely!\r\n    // In create mode we must save the hash. In update mode, the \r\n    // formHashKey will be valid so we can also save it, again.\r\n    $newFormArray = array(\r\n        \'form\' => $formName,\r\n        \'date\' => time(),\r\n        \'values\' => $dataArray,\r\n        \'ip\' => $modx->getOption(\'REMOTE_ADDR\', $_SERVER, \'\'),\r\n        \'context_key\' => $modx->resource->get(\'context_key\'),\r\n        \'encrypted\' => $formEncrypt,\r\n        \'hash\' => $formHashKey,\r\n    );\r\n}\r\n// Convert to object\r\n$newForm->fromArray($newFormArray);\r\n// Attempt to save\r\nif (!$newForm->save()) {\r\n    $modx->log(modX::LOG_LEVEL_ERROR, \'[FormItSaveForm] An error occurred while trying to save the submitted form: \' . print_r($newForm->toArray(), true));\r\n    return false;\r\n}\r\n// Pass the hash and form data back to the user\r\n$hook->setValue(\'savedForm\', $newForm->toArray());\r\n$hook->setValue($formHashKeyField, $newForm->get(\'hash\'));\r\nreturn true;', 0, 'a:0:{}', '', 0, ''),
	(42, 0, 0, 'getResources', '<strong>1.7.0-pl</strong> A general purpose Resource listing and summarization snippet for MODX Revolution', 0, 0, 0, '/**\n * getResources\n *\n * A general purpose Resource listing and summarization snippet for MODX 2.x.\n *\n * @author Jason Coward\n * @copyright Copyright 2010-2015, Jason Coward\n *\n * TEMPLATES\n *\n * tpl - Name of a chunk serving as a resource template\n * [NOTE: if not provided, properties are dumped to output for each resource]\n *\n * tplOdd - (Opt) Name of a chunk serving as resource template for resources with an odd idx value\n * (see idx property)\n * tplFirst - (Opt) Name of a chunk serving as resource template for the first resource (see first\n * property)\n * tplLast - (Opt) Name of a chunk serving as resource template for the last resource (see last\n * property)\n * tpl_{n} - (Opt) Name of a chunk serving as resource template for the nth resource\n *\n * tplCondition - (Opt) Defines a field of the resource to evaluate against keys defined in the\n * conditionalTpls property. Must be a resource field; does not work with Template Variables.\n * conditionalTpls - (Opt) A JSON object defining a map of field values and the associated tpl to\n * use when the field defined by tplCondition matches the value. [NOTE: tplOdd, tplFirst, tplLast,\n * and tpl_{n} will take precedence over any defined conditionalTpls]\n *\n * tplWrapper - (Opt) Name of a chunk serving as a wrapper template for the output\n * [NOTE: Does not work with toSeparatePlaceholders]\n *\n * SELECTION\n *\n * parents - Comma-delimited list of ids serving as parents\n *\n * context - (Opt) Comma-delimited list of context keys to limit results by; if empty, contexts for all specified\n * parents will be used (all contexts if 0 is specified) [default=]\n *\n * depth - (Opt) Integer value indicating depth to search for resources from each parent [default=10]\n *\n * tvFilters - (Opt) Delimited-list of TemplateVar values to filter resources by. Supports two\n * delimiters and two value search formats. The first delimiter || represents a logical OR and the\n * primary grouping mechanism.  Within each group you can provide a comma-delimited list of values.\n * These values can be either tied to a specific TemplateVar by name, e.g. myTV==value, or just the\n * value, indicating you are searching for the value in any TemplateVar tied to the Resource. An\n * example would be &tvFilters=`filter2==one,filter1==bar%||filter1==foo`\n * [NOTE: filtering by values uses a LIKE query and % is considered a wildcard.]\n * [NOTE: this only looks at the raw value set for specific Resource, i. e. there must be a value\n * specifically set for the Resource and it is not evaluated.]\n *\n * tvFiltersAndDelimiter - (Opt) Custom delimiter for logical AND, default \',\', in case you want to\n * match a literal comma in the tvFilters. E.g. &tvFiltersAndDelimiter=`&&`\n * &tvFilters=`filter1==foo,bar&&filter2==baz` [default=,]\n *\n * tvFiltersOrDelimiter - (Opt) Custom delimiter for logical OR, default \'||\', in case you want to\n * match a literal \'||\' in the tvFilters. E.g. &tvFiltersOrDelimiter=`|OR|`\n * &tvFilters=`filter1==foo||bar|OR|filter2==baz` [default=||]\n *\n * where - (Opt) A JSON expression of criteria to build any additional where clauses from. An example would be\n * &where=`{{"alias:LIKE":"foo%", "OR:alias:LIKE":"%bar"},{"OR:pagetitle:=":"foobar", "AND:description:=":"raboof"}}`\n *\n * sortby - (Opt) Field to sort by or a JSON array, e.g. {"publishedon":"ASC","createdon":"DESC"} [default=publishedon]\n * sortbyTV - (opt) A Template Variable name to sort by (if supplied, this precedes the sortby value) [default=]\n * sortbyTVType - (Opt) A data type to CAST a TV Value to in order to sort on it properly [default=string]\n * sortbyAlias - (Opt) Query alias for sortby field [default=]\n * sortbyEscaped - (Opt) Escapes the field name(s) specified in sortby [default=0]\n * sortdir - (Opt) Order which to sort by [default=DESC]\n * sortdirTV - (Opt) Order which to sort by a TV [default=DESC]\n * limit - (Opt) Limits the number of resources returned [default=5]\n * offset - (Opt) An offset of resources returned by the criteria to skip [default=0]\n * dbCacheFlag - (Opt) Controls caching of db queries; 0|false = do not cache result set; 1 = cache result set\n * according to cache settings, any other integer value = number of seconds to cache result set [default=0]\n *\n * OPTIONS\n *\n * includeContent - (Opt) Indicates if the content of each resource should be returned in the\n * results [default=0]\n * includeTVs - (Opt) Indicates if TemplateVar values should be included in the properties available\n * to each resource template [default=0]\n * includeTVList - (Opt) Limits the TemplateVars that are included if includeTVs is true to those specified\n * by name in a comma-delimited list [default=]\n * prepareTVs - (Opt) Prepares media-source dependent TemplateVar values [default=1]\n * prepareTVList - (Opt) Limits the TVs that are prepared to those specified by name in a comma-delimited\n * list [default=]\n * processTVs - (Opt) Indicates if TemplateVar values should be rendered as they would on the\n * resource being summarized [default=0]\n * processTVList - (opt) Limits the TemplateVars that are processed if included to those specified\n * by name in a comma-delimited list [default=]\n * tvPrefix - (Opt) The prefix for TemplateVar properties [default=tv.]\n * idx - (Opt) You can define the starting idx of the resources, which is an property that is\n * incremented as each resource is rendered [default=1]\n * first - (Opt) Define the idx which represents the first resource (see tplFirst) [default=1]\n * last - (Opt) Define the idx which represents the last resource (see tplLast) [default=# of\n * resources being summarized + first - 1]\n * outputSeparator - (Opt) An optional string to separate each tpl instance [default="\\n"]\n * wrapIfEmpty - (Opt) Indicates if the tplWrapper should be applied if the output is empty [default=0]\n *\n */\n$output = array();\n$outputSeparator = isset($outputSeparator) ? $outputSeparator : "\\n";\n\n/* set default properties */\n$tpl = !empty($tpl) ? $tpl : \'\';\n$includeContent = !empty($includeContent) ? true : false;\n$includeTVs = !empty($includeTVs) ? true : false;\n$includeTVList = !empty($includeTVList) ? explode(\',\', $includeTVList) : array();\n$processTVs = !empty($processTVs) ? true : false;\n$processTVList = !empty($processTVList) ? explode(\',\', $processTVList) : array();\n$prepareTVs = !empty($prepareTVs) ? true : false;\n$prepareTVList = !empty($prepareTVList) ? explode(\',\', $prepareTVList) : array();\n$tvPrefix = isset($tvPrefix) ? $tvPrefix : \'tv.\';\n$parents = (!empty($parents) || $parents === \'0\') ? explode(\',\', $parents) : array($modx->resource->get(\'id\'));\narray_walk($parents, \'trim\');\n$parents = array_unique($parents);\n$depth = isset($depth) ? (integer) $depth : 10;\n\n$tvFiltersOrDelimiter = isset($tvFiltersOrDelimiter) ? $tvFiltersOrDelimiter : \'||\';\n$tvFiltersAndDelimiter = isset($tvFiltersAndDelimiter) ? $tvFiltersAndDelimiter : \',\';\n$tvFilters = !empty($tvFilters) ? explode($tvFiltersOrDelimiter, $tvFilters) : array();\n\n$where = !empty($where) ? $modx->fromJSON($where) : array();\n$showUnpublished = !empty($showUnpublished) ? true : false;\n$showDeleted = !empty($showDeleted) ? true : false;\n\n$sortby = isset($sortby) ? $sortby : \'publishedon\';\n$sortbyTV = isset($sortbyTV) ? $sortbyTV : \'\';\n$sortbyAlias = isset($sortbyAlias) ? $sortbyAlias : \'modResource\';\n$sortbyEscaped = !empty($sortbyEscaped) ? true : false;\n$sortdir = isset($sortdir) ? $sortdir : \'DESC\';\n$sortdirTV = isset($sortdirTV) ? $sortdirTV : \'DESC\';\n$limit = isset($limit) ? (integer) $limit : 5;\n$offset = isset($offset) ? (integer) $offset : 0;\n$totalVar = !empty($totalVar) ? $totalVar : \'total\';\n\n$dbCacheFlag = !isset($dbCacheFlag) ? false : $dbCacheFlag;\nif (is_string($dbCacheFlag) || is_numeric($dbCacheFlag)) {\n    if ($dbCacheFlag == \'0\') {\n        $dbCacheFlag = false;\n    } elseif ($dbCacheFlag == \'1\') {\n        $dbCacheFlag = true;\n    } else {\n        $dbCacheFlag = (integer) $dbCacheFlag;\n    }\n}\n\n/* multiple context support */\n$contextArray = array();\n$contextSpecified = false;\nif (!empty($context)) {\n    $contextArray = explode(\',\',$context);\n    array_walk($contextArray, \'trim\');\n    $contexts = array();\n    foreach ($contextArray as $ctx) {\n        $contexts[] = $modx->quote($ctx);\n    }\n    $context = implode(\',\',$contexts);\n    $contextSpecified = true;\n    unset($contexts,$ctx);\n} else {\n    $context = $modx->quote($modx->context->get(\'key\'));\n}\n\n$pcMap = array();\n$pcQuery = $modx->newQuery(\'modResource\', array(\'id:IN\' => $parents), $dbCacheFlag);\n$pcQuery->select(array(\'id\', \'context_key\'));\nif ($pcQuery->prepare() && $pcQuery->stmt->execute()) {\n    foreach ($pcQuery->stmt->fetchAll(PDO::FETCH_ASSOC) as $pcRow) {\n        $pcMap[(integer) $pcRow[\'id\']] = $pcRow[\'context_key\'];\n    }\n}\n\n$children = array();\n$parentArray = array();\nforeach ($parents as $parent) {\n    $parent = (integer) $parent;\n    if ($parent === 0) {\n        $pchildren = array();\n        if ($contextSpecified) {\n            foreach ($contextArray as $pCtx) {\n                if (!in_array($pCtx, $contextArray)) {\n                    continue;\n                }\n                $options = $pCtx !== $modx->context->get(\'key\') ? array(\'context\' => $pCtx) : array();\n                $pcchildren = $modx->getChildIds($parent, $depth, $options);\n                if (!empty($pcchildren)) $pchildren = array_merge($pchildren, $pcchildren);\n            }\n        } else {\n            $cQuery = $modx->newQuery(\'modContext\', array(\'key:!=\' => \'mgr\'));\n            $cQuery->select(array(\'key\'));\n            if ($cQuery->prepare() && $cQuery->stmt->execute()) {\n                foreach ($cQuery->stmt->fetchAll(PDO::FETCH_COLUMN) as $pCtx) {\n                    $options = $pCtx !== $modx->context->get(\'key\') ? array(\'context\' => $pCtx) : array();\n                    $pcchildren = $modx->getChildIds($parent, $depth, $options);\n                    if (!empty($pcchildren)) $pchildren = array_merge($pchildren, $pcchildren);\n                }\n            }\n        }\n        $parentArray[] = $parent;\n    } else {\n        $pContext = array_key_exists($parent, $pcMap) ? $pcMap[$parent] : false;\n        if ($debug) $modx->log(modX::LOG_LEVEL_ERROR, "context for {$parent} is {$pContext}");\n        if ($pContext && $contextSpecified && !in_array($pContext, $contextArray, true)) {\n            $parent = next($parents);\n            continue;\n        }\n        $parentArray[] = $parent;\n        $options = !empty($pContext) && $pContext !== $modx->context->get(\'key\') ? array(\'context\' => $pContext) : array();\n        $pchildren = $modx->getChildIds($parent, $depth, $options);\n    }\n    if (!empty($pchildren)) $children = array_merge($children, $pchildren);\n    $parent = next($parents);\n}\n$parents = array_merge($parentArray, $children);\n\n/* build query */\n$criteria = array("modResource.parent IN (" . implode(\',\', $parents) . ")");\nif ($contextSpecified) {\n    $contextResourceTbl = $modx->getTableName(\'modContextResource\');\n    $criteria[] = "(modResource.context_key IN ({$context}) OR EXISTS(SELECT 1 FROM {$contextResourceTbl} ctx WHERE ctx.resource = modResource.id AND ctx.context_key IN ({$context})))";\n}\nif (empty($showDeleted)) {\n    $criteria[\'deleted\'] = \'0\';\n}\nif (empty($showUnpublished)) {\n    $criteria[\'published\'] = \'1\';\n}\nif (empty($showHidden)) {\n    $criteria[\'hidemenu\'] = \'0\';\n}\nif (!empty($hideContainers)) {\n    $criteria[\'isfolder\'] = \'0\';\n}\n$criteria = $modx->newQuery(\'modResource\', $criteria);\nif (!empty($tvFilters)) {\n    $tmplVarTbl = $modx->getTableName(\'modTemplateVar\');\n    $tmplVarResourceTbl = $modx->getTableName(\'modTemplateVarResource\');\n    $conditions = array();\n    $operators = array(\n        \'<=>\' => \'<=>\',\n        \'===\' => \'=\',\n        \'!==\' => \'!=\',\n        \'<>\' => \'<>\',\n        \'==\' => \'LIKE\',\n        \'!=\' => \'NOT LIKE\',\n        \'<<\' => \'<\',\n        \'<=\' => \'<=\',\n        \'=<\' => \'=<\',\n        \'>>\' => \'>\',\n        \'>=\' => \'>=\',\n        \'=>\' => \'=>\'\n    );\n    foreach ($tvFilters as $fGroup => $tvFilter) {\n        $filterGroup = array();\n        $filters = explode($tvFiltersAndDelimiter, $tvFilter);\n        $multiple = count($filters) > 0;\n        foreach ($filters as $filter) {\n            $operator = \'==\';\n            $sqlOperator = \'LIKE\';\n            foreach ($operators as $op => $opSymbol) {\n                if (strpos($filter, $op, 1) !== false) {\n                    $operator = $op;\n                    $sqlOperator = $opSymbol;\n                    break;\n                }\n            }\n            $tvValueField = \'tvr.value\';\n            $tvDefaultField = \'tv.default_text\';\n            $f = explode($operator, $filter);\n            if (count($f) >= 2) {\n                if (count($f) > 2) {\n                    $k = array_shift($f);\n                    $b = join($operator, $f);\n                    $f = array($k, $b);\n                }\n                $tvName = $modx->quote($f[0]);\n                if (is_numeric($f[1]) && !in_array($sqlOperator, array(\'LIKE\', \'NOT LIKE\'))) {\n                    $tvValue = $f[1];\n                    if ($f[1] == (integer)$f[1]) {\n                        $tvValueField = "CAST({$tvValueField} AS SIGNED INTEGER)";\n                        $tvDefaultField = "CAST({$tvDefaultField} AS SIGNED INTEGER)";\n                    } else {\n                        $tvValueField = "CAST({$tvValueField} AS DECIMAL)";\n                        $tvDefaultField = "CAST({$tvDefaultField} AS DECIMAL)";\n                    }\n                } else {\n                    $tvValue = $modx->quote($f[1]);\n                }\n                if ($multiple) {\n                    $filterGroup[] =\n                        "(EXISTS (SELECT 1 FROM {$tmplVarResourceTbl} tvr JOIN {$tmplVarTbl} tv ON {$tvValueField} {$sqlOperator} {$tvValue} AND tv.name = {$tvName} AND tv.id = tvr.tmplvarid WHERE tvr.contentid = modResource.id) " .\n                        "OR EXISTS (SELECT 1 FROM {$tmplVarTbl} tv WHERE tv.name = {$tvName} AND {$tvDefaultField} {$sqlOperator} {$tvValue} AND tv.id NOT IN (SELECT tmplvarid FROM {$tmplVarResourceTbl} WHERE contentid = modResource.id)) " .\n                        ")";\n                } else {\n                    $filterGroup =\n                        "(EXISTS (SELECT 1 FROM {$tmplVarResourceTbl} tvr JOIN {$tmplVarTbl} tv ON {$tvValueField} {$sqlOperator} {$tvValue} AND tv.name = {$tvName} AND tv.id = tvr.tmplvarid WHERE tvr.contentid = modResource.id) " .\n                        "OR EXISTS (SELECT 1 FROM {$tmplVarTbl} tv WHERE tv.name = {$tvName} AND {$tvDefaultField} {$sqlOperator} {$tvValue} AND tv.id NOT IN (SELECT tmplvarid FROM {$tmplVarResourceTbl} WHERE contentid = modResource.id)) " .\n                        ")";\n                }\n            } elseif (count($f) == 1) {\n                $tvValue = $modx->quote($f[0]);\n                if ($multiple) {\n                    $filterGroup[] = "EXISTS (SELECT 1 FROM {$tmplVarResourceTbl} tvr JOIN {$tmplVarTbl} tv ON {$tvValueField} {$sqlOperator} {$tvValue} AND tv.id = tvr.tmplvarid WHERE tvr.contentid = modResource.id)";\n                } else {\n                    $filterGroup = "EXISTS (SELECT 1 FROM {$tmplVarResourceTbl} tvr JOIN {$tmplVarTbl} tv ON {$tvValueField} {$sqlOperator} {$tvValue} AND tv.id = tvr.tmplvarid WHERE tvr.contentid = modResource.id)";\n                }\n            }\n        }\n        $conditions[] = $filterGroup;\n    }\n    if (!empty($conditions)) {\n        $firstGroup = true;\n        foreach ($conditions as $cGroup => $c) {\n            if (is_array($c)) {\n                $first = true;\n                foreach ($c as $cond) {\n                    if ($first && !$firstGroup) {\n                        $criteria->condition($criteria->query[\'where\'][0][1], $cond, xPDOQuery::SQL_OR, null, $cGroup);\n                    } else {\n                        $criteria->condition($criteria->query[\'where\'][0][1], $cond, xPDOQuery::SQL_AND, null, $cGroup);\n                    }\n                    $first = false;\n                }\n            } else {\n                $criteria->condition($criteria->query[\'where\'][0][1], $c, $firstGroup ? xPDOQuery::SQL_AND : xPDOQuery::SQL_OR, null, $cGroup);\n            }\n            $firstGroup = false;\n        }\n    }\n}\n/* include/exclude resources, via &resources=`123,-456` prop */\nif (!empty($resources)) {\n    $resourceConditions = array();\n    $resources = explode(\',\',$resources);\n    $include = array();\n    $exclude = array();\n    foreach ($resources as $resource) {\n        $resource = (int)$resource;\n        if ($resource == 0) continue;\n        if ($resource < 0) {\n            $exclude[] = abs($resource);\n        } else {\n            $include[] = $resource;\n        }\n    }\n    if (!empty($include)) {\n        $criteria->where(array(\'OR:modResource.id:IN\' => $include), xPDOQuery::SQL_OR);\n    }\n    if (!empty($exclude)) {\n        $criteria->where(array(\'modResource.id:NOT IN\' => $exclude), xPDOQuery::SQL_AND, null, 1);\n    }\n}\nif (!empty($where)) {\n    $criteria->where($where);\n}\n\n$total = $modx->getCount(\'modResource\', $criteria);\n$modx->setPlaceholder($totalVar, $total);\n\n$fields = array_keys($modx->getFields(\'modResource\'));\nif (empty($includeContent)) {\n    $fields = array_diff($fields, array(\'content\'));\n}\n$columns = $includeContent ? $modx->getSelectColumns(\'modResource\', \'modResource\') : $modx->getSelectColumns(\'modResource\', \'modResource\', \'\', array(\'content\'), true);\n$criteria->select($columns);\nif (!empty($sortbyTV)) {\n    $criteria->leftJoin(\'modTemplateVar\', \'tvDefault\', array(\n        "tvDefault.name" => $sortbyTV\n    ));\n    $criteria->leftJoin(\'modTemplateVarResource\', \'tvSort\', array(\n        "tvSort.contentid = modResource.id",\n        "tvSort.tmplvarid = tvDefault.id"\n    ));\n    if (empty($sortbyTVType)) $sortbyTVType = \'string\';\n    if ($modx->getOption(\'dbtype\') === \'mysql\') {\n        switch ($sortbyTVType) {\n            case \'integer\':\n                $criteria->select("CAST(IFNULL(tvSort.value, tvDefault.default_text) AS SIGNED INTEGER) AS sortTV");\n                break;\n            case \'decimal\':\n                $criteria->select("CAST(IFNULL(tvSort.value, tvDefault.default_text) AS DECIMAL) AS sortTV");\n                break;\n            case \'datetime\':\n                $criteria->select("CAST(IFNULL(tvSort.value, tvDefault.default_text) AS DATETIME) AS sortTV");\n                break;\n            case \'string\':\n            default:\n                $criteria->select("IFNULL(tvSort.value, tvDefault.default_text) AS sortTV");\n                break;\n        }\n    } elseif ($modx->getOption(\'dbtype\') === \'sqlsrv\') {\n        switch ($sortbyTVType) {\n            case \'integer\':\n                $criteria->select("CAST(ISNULL(tvSort.value, tvDefault.default_text) AS BIGINT) AS sortTV");\n                break;\n            case \'decimal\':\n                $criteria->select("CAST(ISNULL(tvSort.value, tvDefault.default_text) AS DECIMAL) AS sortTV");\n                break;\n            case \'datetime\':\n                $criteria->select("CAST(ISNULL(tvSort.value, tvDefault.default_text) AS DATETIME) AS sortTV");\n                break;\n            case \'string\':\n            default:\n                $criteria->select("ISNULL(tvSort.value, tvDefault.default_text) AS sortTV");\n                break;\n        }\n    }\n    $criteria->sortby("sortTV", $sortdirTV);\n}\nif (!empty($sortby)) {\n    if (strpos($sortby, \'{\') === 0) {\n        $sorts = $modx->fromJSON($sortby);\n    } else {\n        $sorts = array($sortby => $sortdir);\n    }\n    if (is_array($sorts)) {\n        foreach($sorts as $sort => $dir){\n            if ($sort == \'resources\' && !empty($resources)) {\n                $sort = \'FIELD(modResource.id, \' . implode($resources,\',\') . \')\';\n            }\n            if ($sortbyEscaped) $sort = $modx->escape($sort);\n            if (!empty($sortbyAlias)) $sort = $modx->escape($sortbyAlias) . ".{$sort}";\n            $criteria->sortby($sort, $dir);\n        }\n    }\n}\nif (!empty($limit)) $criteria->limit($limit, $offset);\n\nif (!empty($debug)) {\n    $criteria->prepare();\n    $modx->log(modX::LOG_LEVEL_ERROR, $criteria->toSQL());\n}\n$collection = $modx->getCollection(\'modResource\', $criteria, $dbCacheFlag);\n\n$idx = !empty($idx) || $idx === \'0\' ? (integer) $idx : 1;\n$first = empty($first) && $first !== \'0\' ? 1 : (integer) $first;\n$last = empty($last) ? (count($collection) + $idx - 1) : (integer) $last;\n\n/* include parseTpl */\ninclude_once $modx->getOption(\'getresources.core_path\',null,$modx->getOption(\'core_path\').\'components/getresources/\').\'include.parsetpl.php\';\n\n$templateVars = array();\nif (!empty($includeTVs) && !empty($includeTVList)) {\n    $templateVars = $modx->getCollection(\'modTemplateVar\', array(\'name:IN\' => $includeTVList));\n}\n/** @var modResource $resource */\nforeach ($collection as $resourceId => $resource) {\n    $tvs = array();\n    if (!empty($includeTVs)) {\n        if (empty($includeTVList)) {\n            $templateVars = $resource->getMany(\'TemplateVars\');\n        }\n        /** @var modTemplateVar $templateVar */\n        foreach ($templateVars as $tvId => $templateVar) {\n            if (!empty($includeTVList) && !in_array($templateVar->get(\'name\'), $includeTVList)) continue;\n            if ($processTVs && (empty($processTVList) || in_array($templateVar->get(\'name\'), $processTVList))) {\n                $tvs[$tvPrefix . $templateVar->get(\'name\')] = $templateVar->renderOutput($resource->get(\'id\'));\n            } else {\n                $value = $templateVar->getValue($resource->get(\'id\'));\n                if ($prepareTVs && method_exists($templateVar, \'prepareOutput\') && (empty($prepareTVList) || in_array($templateVar->get(\'name\'), $prepareTVList))) {\n                    $value = $templateVar->prepareOutput($value);\n                }\n                $tvs[$tvPrefix . $templateVar->get(\'name\')] = $value;\n            }\n        }\n    }\n    $odd = ($idx & 1);\n    $properties = array_merge(\n        $scriptProperties\n        ,array(\n            \'idx\' => $idx\n            ,\'first\' => $first\n            ,\'last\' => $last\n            ,\'odd\' => $odd\n        )\n        ,$includeContent ? $resource->toArray() : $resource->get($fields)\n        ,$tvs\n    );\n    $resourceTpl = false;\n    if ($idx == $first && !empty($tplFirst)) {\n        $resourceTpl = parseTpl($tplFirst, $properties);\n    }\n    if ($idx == $last && empty($resourceTpl) && !empty($tplLast)) {\n        $resourceTpl = parseTpl($tplLast, $properties);\n    }\n    $tplidx = \'tpl_\' . $idx;\n    if (empty($resourceTpl) && !empty($$tplidx)) {\n        $resourceTpl = parseTpl($$tplidx, $properties);\n    }\n    if ($idx > 1 && empty($resourceTpl)) {\n        $divisors = getDivisors($idx);\n        if (!empty($divisors)) {\n            foreach ($divisors as $divisor) {\n                $tplnth = \'tpl_n\' . $divisor;\n                if (!empty($$tplnth)) {\n                    $resourceTpl = parseTpl($$tplnth, $properties);\n                    if (!empty($resourceTpl)) {\n                        break;\n                    }\n                }\n            }\n        }\n    }\n    if ($odd && empty($resourceTpl) && !empty($tplOdd)) {\n        $resourceTpl = parseTpl($tplOdd, $properties);\n    }\n    if (!empty($tplCondition) && !empty($conditionalTpls) && empty($resourceTpl)) {\n        $conTpls = $modx->fromJSON($conditionalTpls);\n        $subject = $properties[$tplCondition];\n        $tplOperator = !empty($tplOperator) ? $tplOperator : \'=\';\n        $tplOperator = strtolower($tplOperator);\n        $tplCon = \'\';\n        foreach ($conTpls as $operand => $conditionalTpl) {\n            switch ($tplOperator) {\n                case \'!=\':\n                case \'neq\':\n                case \'not\':\n                case \'isnot\':\n                case \'isnt\':\n                case \'unequal\':\n                case \'notequal\':\n                    $tplCon = (($subject != $operand) ? $conditionalTpl : $tplCon);\n                    break;\n                case \'<\':\n                case \'lt\':\n                case \'less\':\n                case \'lessthan\':\n                    $tplCon = (($subject < $operand) ? $conditionalTpl : $tplCon);\n                    break;\n                case \'>\':\n                case \'gt\':\n                case \'greater\':\n                case \'greaterthan\':\n                    $tplCon = (($subject > $operand) ? $conditionalTpl : $tplCon);\n                    break;\n                case \'<=\':\n                case \'lte\':\n                case \'lessthanequals\':\n                case \'lessthanorequalto\':\n                    $tplCon = (($subject <= $operand) ? $conditionalTpl : $tplCon);\n                    break;\n                case \'>=\':\n                case \'gte\':\n                case \'greaterthanequals\':\n                case \'greaterthanequalto\':\n                    $tplCon = (($subject >= $operand) ? $conditionalTpl : $tplCon);\n                    break;\n                case \'isempty\':\n                case \'empty\':\n                    $tplCon = empty($subject) ? $conditionalTpl : $tplCon;\n                    break;\n                case \'!empty\':\n                case \'notempty\':\n                case \'isnotempty\':\n                    $tplCon = !empty($subject) && $subject != \'\' ? $conditionalTpl : $tplCon;\n                    break;\n                case \'isnull\':\n                case \'null\':\n                    $tplCon = $subject == null || strtolower($subject) == \'null\' ? $conditionalTpl : $tplCon;\n                    break;\n                case \'inarray\':\n                case \'in_array\':\n                case \'ia\':\n                    $operand = explode(\',\', $operand);\n                    $tplCon = in_array($subject, $operand) ? $conditionalTpl : $tplCon;\n                    break;\n                case \'between\':\n                case \'range\':\n                case \'>=<\':\n                case \'><\':\n                    $operand = explode(\',\', $operand);\n                    $tplCon = ($subject >= min($operand) && $subject <= max($operand)) ? $conditionalTpl : $tplCon;\n                    break;\n                case \'==\':\n                case \'=\':\n                case \'eq\':\n                case \'is\':\n                case \'equal\':\n                case \'equals\':\n                case \'equalto\':\n                default:\n                    $tplCon = (($subject == $operand) ? $conditionalTpl : $tplCon);\n                    break;\n            }\n        }\n        if (!empty($tplCon)) {\n            $resourceTpl = parseTpl($tplCon, $properties);\n        }\n    }\n    if (!empty($tpl) && empty($resourceTpl)) {\n        $resourceTpl = parseTpl($tpl, $properties);\n    }\n    if ($resourceTpl === false && !empty($debug)) {\n        $chunk = $modx->newObject(\'modChunk\');\n        $chunk->setCacheable(false);\n        $output[]= $chunk->process(array(), \'<pre>\' . print_r($properties, true) .\'</pre>\');\n    } else {\n        $output[]= $resourceTpl;\n    }\n    $idx++;\n}\n\n/* output */\n$toSeparatePlaceholders = $modx->getOption(\'toSeparatePlaceholders\', $scriptProperties, false);\nif (!empty($toSeparatePlaceholders)) {\n    $modx->setPlaceholders($output, $toSeparatePlaceholders);\n    return \'\';\n}\n\n$output = implode($outputSeparator, $output);\n\n$tplWrapper = $modx->getOption(\'tplWrapper\', $scriptProperties, false);\n$wrapIfEmpty = $modx->getOption(\'wrapIfEmpty\', $scriptProperties, false);\nif (!empty($tplWrapper) && ($wrapIfEmpty || !empty($output))) {\n    $output = parseTpl($tplWrapper, array_merge($scriptProperties, array(\'output\' => $output)));\n}\n\n$toPlaceholder = $modx->getOption(\'toPlaceholder\', $scriptProperties, false);\nif (!empty($toPlaceholder)) {\n    $modx->setPlaceholder($toPlaceholder, $output);\n    return \'\';\n}\nreturn $output;', 0, 'a:44:{s:3:"tpl";a:7:{s:4:"name";s:3:"tpl";s:4:"desc";s:121:"Name of a chunk serving as a resource template. NOTE: if not provided, properties are dumped to output for each resource.";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";N;s:4:"area";s:0:"";}s:6:"tplOdd";a:7:{s:4:"name";s:6:"tplOdd";s:4:"desc";s:100:"Name of a chunk serving as resource template for resources with an odd idx value (see idx property).";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";N;s:4:"area";s:0:"";}s:8:"tplFirst";a:7:{s:4:"name";s:8:"tplFirst";s:4:"desc";s:89:"Name of a chunk serving as resource template for the first resource (see first property).";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";N;s:4:"area";s:0:"";}s:7:"tplLast";a:7:{s:4:"name";s:7:"tplLast";s:4:"desc";s:87:"Name of a chunk serving as resource template for the last resource (see last property).";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";N;s:4:"area";s:0:"";}s:10:"tplWrapper";a:7:{s:4:"name";s:10:"tplWrapper";s:4:"desc";s:115:"Name of a chunk serving as wrapper template for the Snippet output. This does not work with toSeparatePlaceholders.";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";N;s:4:"area";s:0:"";}s:11:"wrapIfEmpty";a:7:{s:4:"name";s:11:"wrapIfEmpty";s:4:"desc";s:95:"Indicates if empty output should be wrapped by the tplWrapper, if specified. Defaults to false.";s:4:"type";s:13:"combo-boolean";s:7:"options";s:0:"";s:5:"value";b:0;s:7:"lexicon";N;s:4:"area";s:0:"";}s:6:"sortby";a:7:{s:4:"name";s:6:"sortby";s:4:"desc";s:153:"A field name to sort by or JSON object of field names and sortdir for each field, e.g. {"publishedon":"ASC","createdon":"DESC"}. Defaults to publishedon.";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:11:"publishedon";s:7:"lexicon";N;s:4:"area";s:0:"";}s:8:"sortbyTV";a:7:{s:4:"name";s:8:"sortbyTV";s:4:"desc";s:65:"Name of a Template Variable to sort by. Defaults to empty string.";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";N;s:4:"area";s:0:"";}s:12:"sortbyTVType";a:7:{s:4:"name";s:12:"sortbyTVType";s:4:"desc";s:72:"An optional type to indicate how to sort on the Template Variable value.";s:4:"type";s:4:"list";s:7:"options";a:4:{i:0;a:2:{s:4:"text";s:6:"string";s:5:"value";s:6:"string";}i:1;a:2:{s:4:"text";s:7:"integer";s:5:"value";s:7:"integer";}i:2;a:2:{s:4:"text";s:7:"decimal";s:5:"value";s:7:"decimal";}i:3;a:2:{s:4:"text";s:8:"datetime";s:5:"value";s:8:"datetime";}}s:5:"value";s:6:"string";s:7:"lexicon";N;s:4:"area";s:0:"";}s:11:"sortbyAlias";a:7:{s:4:"name";s:11:"sortbyAlias";s:4:"desc";s:58:"Query alias for sortby field. Defaults to an empty string.";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";N;s:4:"area";s:0:"";}s:13:"sortbyEscaped";a:7:{s:4:"name";s:13:"sortbyEscaped";s:4:"desc";s:82:"Determines if the field name specified in sortby should be escaped. Defaults to 0.";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:1:"0";s:7:"lexicon";N;s:4:"area";s:0:"";}s:7:"sortdir";a:7:{s:4:"name";s:7:"sortdir";s:4:"desc";s:41:"Order which to sort by. Defaults to DESC.";s:4:"type";s:4:"list";s:7:"options";a:2:{i:0;a:2:{s:4:"text";s:3:"ASC";s:5:"value";s:3:"ASC";}i:1;a:2:{s:4:"text";s:4:"DESC";s:5:"value";s:4:"DESC";}}s:5:"value";s:4:"DESC";s:7:"lexicon";N;s:4:"area";s:0:"";}s:9:"sortdirTV";a:7:{s:4:"name";s:9:"sortdirTV";s:4:"desc";s:61:"Order which to sort a Template Variable by. Defaults to DESC.";s:4:"type";s:4:"list";s:7:"options";a:2:{i:0;a:2:{s:4:"text";s:3:"ASC";s:5:"value";s:3:"ASC";}i:1;a:2:{s:4:"text";s:4:"DESC";s:5:"value";s:4:"DESC";}}s:5:"value";s:4:"DESC";s:7:"lexicon";N;s:4:"area";s:0:"";}s:5:"limit";a:7:{s:4:"name";s:5:"limit";s:4:"desc";s:55:"Limits the number of resources returned. Defaults to 5.";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:1:"5";s:7:"lexicon";N;s:4:"area";s:0:"";}s:6:"offset";a:7:{s:4:"name";s:6:"offset";s:4:"desc";s:56:"An offset of resources returned by the criteria to skip.";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:1:"0";s:7:"lexicon";N;s:4:"area";s:0:"";}s:9:"tvFilters";a:7:{s:4:"name";s:9:"tvFilters";s:4:"desc";s:778:"Delimited-list of TemplateVar values to filter resources by. Supports two delimiters and two value search formats. THe first delimiter || represents a logical OR and the primary grouping mechanism.  Within each group you can provide a comma-delimited list of values. These values can be either tied to a specific TemplateVar by name, e.g. myTV==value, or just the value, indicating you are searching for the value in any TemplateVar tied to the Resource. An example would be &tvFilters=`filter2==one,filter1==bar%||filter1==foo`. <br />NOTE: filtering by values uses a LIKE query and % is considered a wildcard. <br />ANOTHER NOTE: This only looks at the raw value set for specific Resource, i. e. there must be a value specifically set for the Resource and it is not evaluated.";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";N;s:4:"area";s:0:"";}s:21:"tvFiltersAndDelimiter";a:7:{s:4:"name";s:21:"tvFiltersAndDelimiter";s:4:"desc";s:83:"The delimiter to use to separate logical AND expressions in tvFilters. Default is ,";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:1:",";s:7:"lexicon";N;s:4:"area";s:0:"";}s:20:"tvFiltersOrDelimiter";a:7:{s:4:"name";s:20:"tvFiltersOrDelimiter";s:4:"desc";s:83:"The delimiter to use to separate logical OR expressions in tvFilters. Default is ||";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:2:"||";s:7:"lexicon";N;s:4:"area";s:0:"";}s:5:"depth";a:7:{s:4:"name";s:5:"depth";s:4:"desc";s:88:"Integer value indicating depth to search for resources from each parent. Defaults to 10.";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:2:"10";s:7:"lexicon";N;s:4:"area";s:0:"";}s:7:"parents";a:7:{s:4:"name";s:7:"parents";s:4:"desc";s:57:"Optional. Comma-delimited list of ids serving as parents.";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";N;s:4:"area";s:0:"";}s:14:"includeContent";a:7:{s:4:"name";s:14:"includeContent";s:4:"desc";s:95:"Indicates if the content of each resource should be returned in the results. Defaults to false.";s:4:"type";s:13:"combo-boolean";s:7:"options";s:0:"";s:5:"value";b:0;s:7:"lexicon";N;s:4:"area";s:0:"";}s:10:"includeTVs";a:7:{s:4:"name";s:10:"includeTVs";s:4:"desc";s:124:"Indicates if TemplateVar values should be included in the properties available to each resource template. Defaults to false.";s:4:"type";s:13:"combo-boolean";s:7:"options";s:0:"";s:5:"value";b:0;s:7:"lexicon";N;s:4:"area";s:0:"";}s:13:"includeTVList";a:7:{s:4:"name";s:13:"includeTVList";s:4:"desc";s:96:"Limits included TVs to those specified as a comma-delimited list of TV names. Defaults to empty.";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";N;s:4:"area";s:0:"";}s:10:"showHidden";a:7:{s:4:"name";s:10:"showHidden";s:4:"desc";s:85:"Indicates if Resources that are hidden from menus should be shown. Defaults to false.";s:4:"type";s:13:"combo-boolean";s:7:"options";s:0:"";s:5:"value";b:0;s:7:"lexicon";N;s:4:"area";s:0:"";}s:15:"showUnpublished";a:7:{s:4:"name";s:15:"showUnpublished";s:4:"desc";s:79:"Indicates if Resources that are unpublished should be shown. Defaults to false.";s:4:"type";s:13:"combo-boolean";s:7:"options";s:0:"";s:5:"value";b:0;s:7:"lexicon";N;s:4:"area";s:0:"";}s:11:"showDeleted";a:7:{s:4:"name";s:11:"showDeleted";s:4:"desc";s:75:"Indicates if Resources that are deleted should be shown. Defaults to false.";s:4:"type";s:13:"combo-boolean";s:7:"options";s:0:"";s:5:"value";b:0;s:7:"lexicon";N;s:4:"area";s:0:"";}s:9:"resources";a:7:{s:4:"name";s:9:"resources";s:4:"desc";s:177:"A comma-separated list of resource IDs to exclude or include. IDs with a - in front mean to exclude. Ex: 123,-456 means to include Resource 123, but always exclude Resource 456.";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";N;s:4:"area";s:0:"";}s:10:"processTVs";a:7:{s:4:"name";s:10:"processTVs";s:4:"desc";s:117:"Indicates if TemplateVar values should be rendered as they would on the resource being summarized. Defaults to false.";s:4:"type";s:13:"combo-boolean";s:7:"options";s:0:"";s:5:"value";b:0;s:7:"lexicon";N;s:4:"area";s:0:"";}s:13:"processTVList";a:7:{s:4:"name";s:13:"processTVList";s:4:"desc";s:166:"Limits processed TVs to those specified as a comma-delimited list of TV names; note only includedTVs will be available for processing if specified. Defaults to empty.";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";N;s:4:"area";s:0:"";}s:10:"prepareTVs";a:7:{s:4:"name";s:10:"prepareTVs";s:4:"desc";s:120:"Indicates if TemplateVar values that are not processed fully should be prepared before being returned. Defaults to true.";s:4:"type";s:13:"combo-boolean";s:7:"options";s:0:"";s:5:"value";b:1;s:7:"lexicon";N;s:4:"area";s:0:"";}s:13:"prepareTVList";a:7:{s:4:"name";s:13:"prepareTVList";s:4:"desc";s:164:"Limits prepared TVs to those specified as a comma-delimited list of TV names; note only includedTVs will be available for preparing if specified. Defaults to empty.";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";N;s:4:"area";s:0:"";}s:8:"tvPrefix";a:7:{s:4:"name";s:8:"tvPrefix";s:4:"desc";s:55:"The prefix for TemplateVar properties. Defaults to: tv.";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:3:"tv.";s:7:"lexicon";N;s:4:"area";s:0:"";}s:3:"idx";a:7:{s:4:"name";s:3:"idx";s:4:"desc";s:120:"You can define the starting idx of the resources, which is an property that is incremented as each resource is rendered.";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";N;s:4:"area";s:0:"";}s:5:"first";a:7:{s:4:"name";s:5:"first";s:4:"desc";s:81:"Define the idx which represents the first resource (see tplFirst). Defaults to 1.";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";N;s:4:"area";s:0:"";}s:4:"last";a:7:{s:4:"name";s:4:"last";s:4:"desc";s:129:"Define the idx which represents the last resource (see tplLast). Defaults to the number of resources being summarized + first - 1";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";N;s:4:"area";s:0:"";}s:13:"toPlaceholder";a:7:{s:4:"name";s:13:"toPlaceholder";s:4:"desc";s:85:"If set, will assign the result to this placeholder instead of outputting it directly.";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";N;s:4:"area";s:0:"";}s:22:"toSeparatePlaceholders";a:7:{s:4:"name";s:22:"toSeparatePlaceholders";s:4:"desc";s:130:"If set, will assign EACH result to a separate placeholder named by this param suffixed with a sequential number (starting from 0).";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";N;s:4:"area";s:0:"";}s:5:"debug";a:7:{s:4:"name";s:5:"debug";s:4:"desc";s:68:"If true, will send the SQL query to the MODX log. Defaults to false.";s:4:"type";s:13:"combo-boolean";s:7:"options";s:0:"";s:5:"value";b:0;s:7:"lexicon";N;s:4:"area";s:0:"";}s:5:"where";a:7:{s:4:"name";s:5:"where";s:4:"desc";s:193:"A JSON expression of criteria to build any additional where clauses from, e.g. &where=`{{"alias:LIKE":"foo%", "OR:alias:LIKE":"%bar"},{"OR:pagetitle:=":"foobar", "AND:description:=":"raboof"}}`";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";N;s:4:"area";s:0:"";}s:11:"dbCacheFlag";a:7:{s:4:"name";s:11:"dbCacheFlag";s:4:"desc";s:218:"Determines how result sets are cached if cache_db is enabled in MODX. 0|false = do not cache result set; 1 = cache result set according to cache settings, any other integer value = number of seconds to cache result set";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:1:"0";s:7:"lexicon";N;s:4:"area";s:0:"";}s:7:"context";a:7:{s:4:"name";s:7:"context";s:4:"desc";s:116:"A comma-delimited list of context keys for limiting results. Default is empty, i.e. do not limit results by context.";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";N;s:4:"area";s:0:"";}s:12:"tplCondition";a:7:{s:4:"name";s:12:"tplCondition";s:4:"desc";s:129:"A condition to compare against the conditionalTpls property to map Resources to different tpls based on custom conditional logic.";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";N;s:4:"area";s:0:"";}s:11:"tplOperator";a:7:{s:4:"name";s:11:"tplOperator";s:4:"desc";s:125:"An optional operator to use for the tplCondition when comparing against the conditionalTpls operands. Default is == (equals).";s:4:"type";s:4:"list";s:7:"options";a:10:{i:0;a:2:{s:4:"text";s:11:"is equal to";s:5:"value";s:2:"==";}i:1;a:2:{s:4:"text";s:15:"is not equal to";s:5:"value";s:2:"!=";}i:2;a:2:{s:4:"text";s:9:"less than";s:5:"value";s:1:"<";}i:3;a:2:{s:4:"text";s:21:"less than or equal to";s:5:"value";s:2:"<=";}i:4;a:2:{s:4:"text";s:24:"greater than or equal to";s:5:"value";s:2:">=";}i:5;a:2:{s:4:"text";s:8:"is empty";s:5:"value";s:5:"empty";}i:6;a:2:{s:4:"text";s:12:"is not empty";s:5:"value";s:6:"!empty";}i:7;a:2:{s:4:"text";s:7:"is null";s:5:"value";s:4:"null";}i:8;a:2:{s:4:"text";s:11:"is in array";s:5:"value";s:7:"inarray";}i:9;a:2:{s:4:"text";s:10:"is between";s:5:"value";s:7:"between";}}s:5:"value";s:2:"==";s:7:"lexicon";N;s:4:"area";s:0:"";}s:15:"conditionalTpls";a:7:{s:4:"name";s:15:"conditionalTpls";s:4:"desc";s:121:"A JSON map of conditional operands and tpls to compare against the tplCondition property using the specified tplOperator.";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";N;s:4:"area";s:0:"";}}', '', 0, '');
/*!40000 ALTER TABLE `modx_site_snippets` ENABLE KEYS */;

-- Dumping structure for table petukhov.modx_site_templates
CREATE TABLE IF NOT EXISTS `modx_site_templates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `source` int(10) unsigned NOT NULL DEFAULT 0,
  `property_preprocess` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `templatename` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `editor_type` int(11) NOT NULL DEFAULT 0,
  `category` int(11) NOT NULL DEFAULT 0,
  `icon` varchar(255) NOT NULL DEFAULT '',
  `template_type` int(11) NOT NULL DEFAULT 0,
  `content` mediumtext NOT NULL,
  `locked` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `properties` text DEFAULT NULL,
  `static` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `static_file` varchar(255) NOT NULL DEFAULT '',
  `preview_file` varchar(191) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `templatename` (`templatename`),
  KEY `category` (`category`),
  KEY `locked` (`locked`),
  KEY `static` (`static`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table petukhov.modx_site_templates: 2 rows
/*!40000 ALTER TABLE `modx_site_templates` DISABLE KEYS */;
INSERT INTO `modx_site_templates` (`id`, `source`, `property_preprocess`, `templatename`, `description`, `editor_type`, `category`, `icon`, `template_type`, `content`, `locked`, `properties`, `static`, `static_file`, `preview_file`) VALUES
	(1, 0, 0, 'Landing', '', 0, 0, '', 0, '[[$tplHeadCommon]]\n  </head>\n  <body data-spy="scroll">\n		<section class = "title" id = "title">\n			<a data-scroll href = "#about" class = "scroll-down">\n				<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100">\n					<polygon points="23.1 34.1 51.5 61.7 80 34.1 81.5 35 51.5 64.1 21.5 35 23.1 34.1 " fill="#010101"/>\n				</svg>\n			</a>\n			<div class = "title-table">\n				<div class = "title-row">\n					<div class = "title-cell">\n						<h2>[[*lang:is=`ru`:then=`Евгений Петухов`:else=`Evgenii Petukhov`]]</h2>\n						<p class = "speciality">Full-Stack Web Developer</p>								\n					</div>\n				</div>\n			</div>\n		</section>	\n		<section id = "about" class = "about gradient">\n            [[$tplMainMenu]]\n			<div class = "h3-highlight first">\n				<div class = "outer-container">\n					<div class = "relative-container">\n						<div class = "background-container">\n							<h3>[[*lang:is=`ru`:then=`О себе`:else=`About me`]]</h3>\n						</div>\n					</div>\n				</div>\n			</div>\n			<div class = "container-fluid">\n				<div class = "row">\n					<div class = "activity-box activity-box-background col-md-4">\n						<div class = "box-content">\n							<div class = "box-table">\n								<div class = "box-row">\n									<div class = "box-cell">\n										<div class = "box-heading"><span>15</span> [[*lang:is=`ru`:then=`лет`:else=`years`]]</div>\n										<div class = "box-sub-heading">[[*lang:is=`ru`:then=`опыта разработки ПО`:else=`of software development experience`]]</div>\n									</div>\n								</div>\n							</div>\n						</div>\n					</div>	\n					<div class = "activity-box activity-box-background col-md-4">\n						<div class = "box-content">\n							<div class = "box-table">\n								<div class = "box-row">\n									<div class = "box-cell">\n										<div class = "box-heading"><span>6</span> [[*lang:is=`ru`:then=`лет`:else=`years`]]</div>\n										<div class = "box-sub-heading">[[*lang:is=`ru`:then=`работаю на британского заказчика`:else=`working for a UK company`]]</div>\n									</div>\n								</div>\n							</div>\n						</div>\n					</div>	\n					<div class = "activity-box activity-box-background col-md-4">\n						<div class = "box-content">\n							<div class = "box-table">\n								<div class = "box-row">\n									<div class = "box-cell">\n										<div class = "box-heading">[[*lang:is=`ru`:then=`Живу в Белграде, Сербия`:else=`Live in Belgrade, Sebia`]]</div>\n										<div class = "box-sub-heading">[[*lang:is=`ru`:then=`но готов к переезду в Великобританию, США или страны Евросоюза`:else=`however, ready to move to the UK, EU or US`]]</div>\n									</div>\n								</div>\n							</div>\n						</div>\n					</div>														\n				</div>\n			</div>\n			<div class = "about-text container">\n				<div class = "row">\n				  <div class="col-md-12">\n  					<div class = "face">\n  						<div class = "avatar"><img src = "[[++site_url]]images/avatar.jpg" /></div>\n  						<div class = "name">[[*lang:is=`ru`:then=`Евгений Петухов`:else=`Evgenii Petukhov`]]</div>\n  					</div>\n				  </div>\n				</div>\n				<div class = "row details">\n					<div class = "col-md-8 col-md-offset-2">\n						<p>In a nutshell, I would describe myself as a proactive and reliable person, who is willing to knuckle down to the task when required whilst also being an easy person to get along with.</p>\n						<p>As well as this, I’m a passionate English learner. I’ve been having private English classes with a native speaker since 2016. Passed IELTS in 2017. I definitely have the travel bug. Having visited the UK, Spain, Italy, Hungary, Malta, and Finland, decided to settle in Serbia. I’ve been playing basketball since I was 14. Basketball has really helped me find new connections in Belgrade.</p>\n					</div>\n				</div>\n			</div>		\n		</section>		\n		<section class = "education gradient">\n		    <a id = "education" class = "anchor"></a>\n			<div class = "h3-highlight">\n				<div class = "outer-container">\n					<div class = "relative-container">\n						<div class = "background-container">\n							<h3>[[*lang:is=`ru`:then=`Образование`:else=`Education`]]</h3>\n						</div>\n					</div>\n				</div>\n			</div>		\n			<div class = "container-fluid">\n				<div class = "row">\n					<div class = "col-lg-4 col-lg-offset-4 col-md-6 col-md-offset-3 col-sm-10 col-sm-offset-1">\n						<p class = "sub-heading">[[*lang:is=`ru`:then=`Высшее&colon; закончил с отличием Норильский индустриальный институт. Прошёл курсы Microsoft по web-разработке в учебном центре специалист при МГТУ им. Баумана`:else=`focused on ways to better myself, such as studying online and working on accidental side projects`]]</p>					\n					</div>\n				</div>\n				<div class = "row">\n					<div class = "activity-box activity-box-background col-md-4">\n						<div class = "box-content">\n							<div class = "box-table">\n								<div class = "box-row">\n									<div class = "box-cell">\n										<div class = "box-heading">[[*lang:is=`ru`:then=`высшее образование`:else=`A university degree`]]</div>\n										<div class = "box-sub-heading">[[*lang:is=`ru`:then=`<a href = "http://norvuz.ru" target = "_blank" rel="noopener">перейти на сайт ВУЗа</a>`:else=`in computer science`]]</div>\n									</div>\n								</div>\n							</div>\n						</div>\n					</div>\n					<div class = "activity-box activity-box-background col-md-4">\n						<div class = "box-content">\n							<div class = "box-table">\n								<div class = "box-row">\n									<div class = "box-cell">\n										<div class = "box-heading">[[*lang:is=`ru`:then=`Английский язык`:else=`Fluent English speaker`]]</div>\n										<div class = "box-sub-heading">practice at work on a daily basis</div>\n									</div>\n								</div>\n							</div>\n						</div>\n					</div>					\n					<div class = "activity-box activity-box-background col-md-4">\n						<div class = "box-content">\n							<div class = "box-table">\n								<div class = "box-row">\n									<div class = "box-cell">\n										<div class = "box-heading">[[*lang:is=`ru`:then=`Непрерывное самообучение`:else=`Consistent personal development`]]</div>\n										<div class = "box-sub-heading">[[*lang:is=`ru`:then=`онлайн-курсы и личные проекты`:else=`online courses and side projects`]]</div>\n									</div>\n								</div>\n							</div>\n						</div>\n					</div>					\n				</div>\n			</div>\n			<div class = "education-places">\n				<div class = "container">\n					<div class = "place row">\n						<div class = "place-date col-md-3">2019 <span>[[*lang:is=`ru`:then=`май`:else=`may`]]</span><br/>2016 <span>[[*lang:is=`ru`:then=`май`:else=`may`]]</span></div>\n						<div class = "place-description col-md-9">\n							<p class = "place-description-heading">[[*lang:is=`ru`:then=`EC English, Мальта`:else=`EC English, Malta`]]</p>\n							<p class = "place-description-sub-heading">[[*lang:is=`ru`:then=`курс английского языка`:else=`attended an advanced English language course`]]</p>\n						</div>\n					</div>\n					<div class = "place row">\n						<div class = "place-date col-md-3">2017 <span>[[*lang:is=`ru`:then=`апр`:else=`apr`]]</span></div>\n						<div class = "place-description col-md-9">\n							<p class = "place-description-heading">[[*lang:is=`ru`:then=`IELTS&colon; общий балл 6.5 из 9`:else=`IELTS, overall score&colon; 6.5`]]</p>\n							<p class = "place-description-sub-heading">[[*lang:is=`ru`:then=`сдал экзамен для оценки уровня владения английским языком`:else=`passed the exam in order to assess my english language skills`]]</p>\n						</div>\n					</div>\n					<div class = "place row">\n						<div class = "place-date col-md-3">2016 &minus; [[*lang:is=`ru`:then=`настоящее время`:else=`present time`]]</span></div>\n						<div class = "place-description col-md-9">\n							<p class = "place-description-heading">[[*lang:is=`ru`:then=`Приватные уроки английского`:else=`Private English classes`]]</p>\n							<p class = "place-description-sub-heading">[[*lang:is=`ru`:then=`с носителем языка`:else=`with a native speaker`]]</p>\n						</div>\n					</div>\n					<div class = "place row">\n						<div class = "place-date col-md-3">2012 <span>[[*lang:is=`ru`:then=`авг`:else=`aug`]]</span></div>\n						<div class = "place-description col-md-9">\n							<p class = "place-description-heading">[[*lang:is=`ru`:then=`Курс M10264&colon; Разработка web-приложений в среде Visual Studio`:else=`Course M10264&colon; Developing Web Applications with Visual Studio`]]</p>\n							<p class = "place-description-sub-heading">[[*lang:is=`ru`:then=`Учебный центр &laquo;Специалист&raquo; при МГТУ им. Баумана`:else=`MSTU Training Centre, Moscow`]]</p>\n						</div>\n					</div>					\n					<div class = "place row">\n						<div class = "place-date col-md-3">2003 <span>[[*lang:is=`ru`:then=`сен`:else=`sep`]]</span> &minus; 2008 <span>[[*lang:is=`ru`:then=`июл`:else=`jul`]]</span></div>\n						<div class = "place-description col-md-9">\n							<p class = "place-description-heading">[[*lang:is=`ru`:then=`ГОУ ВПО &laquo;Норильский индустриальный институт&raquo;`:else=`Norilsk Industrial Institute`]]</p>\n							<p class = "place-description-sub-heading">[[*lang:is=`ru`:then=`Специальность&colon; информационные системы и технологии`:else=`a university degree in computer science`]]</p>\n							<p>[[*lang:is=`ru`:then=`Диплом с отличием`:else=``]]</p>\n						</div>\n					</div>\n				</div>				\n			</div>\n		</section>		\n		<section class = "experience gradient">\n		  <a id = "experience" class = "anchor"></a>\n			<div class = "h3-highlight">\n				<div class = "outer-container">\n					<div class = "relative-container">\n						<div class = "background-container">\n							<h3>[[*lang:is=`ru`:then=`Опыт работы`:else=`Experience`]]</h3>\n						</div>\n					</div>\n				</div>\n			</div>		\n			<div class = "container-fluid">\n				<div class = "row">\n					<div class = "col-lg-4 col-lg-offset-4 col-md-6 col-md-offset-3 col-sm-10 col-sm-offset-1">\n						<p class="sub-heading">I’m extremely loyal to the companies I’ve been with. <br />I’ve worked 9 years for Norilsk Nickel and 6 years for Arcadia. <br />Total experience - 15 years.</p>\n					</div>					\n				</div>\n				<div class = "row">\n					<div class = "activity-box activity-box-background col-md-4">\n						<div class = "box-content">\n							<div class = "box-table">\n								<div class = "box-row">\n									<div class = "box-cell">\n										<div class = "box-heading"><span>9</span> [[*lang:is=`ru`:then=`лет`:else=`years`]]</div>\n										<div class = "box-sub-heading">[[*lang:is=`ru`:then=`работы в ЗФ ПАО &laquo;ГМК &laquo;Норильский никель&raquo;`:else=`worked for Norilsk Nickel, IT department`]]</div>\n									</div>\n								</div>\n							</div>\n						</div>\n					</div>\n					<div class = "activity-box activity-box-background col-md-4">\n						<div class = "box-content">\n							<div class = "box-table">\n								<div class = "box-row">\n									<div class = "box-cell">\n										<div class = "box-heading"><span>6</span> [[*lang:is=`ru`:then=`лет`:else=`years`]]</div>\n										<div class = "box-sub-heading">[[*lang:is=`ru`:then=`работаю на британского заказчика`:else=`working for a British company`]]</div>\n									</div>\n								</div>\n							</div>\n						</div>\n					</div>\n					<div class = "activity-box activity-box-background col-md-4">\n						<div class = "box-content">\n							<div class = "box-table">\n								<div class = "box-row">\n									<div class = "box-cell">\n										<div class = "box-heading"><span>3</span> [[*lang:is=`ru`:then=`лет`:else=`years`]]</div>\n										<div class = "box-sub-heading">[[*lang:is=`ru`:then=`работал над собственными проектами`:else=`extra experience: side projects`]]</div>\n									</div>\n								</div>\n							</div>\n						</div>\n					</div>					\n				</div>\n			</div>\n			<div class="about-text container">\n			    <div class="row">\n    			    <div class="col-md-6">\n        				<p>My key specialties include C#, .NET, ASP.NET Core, SQL Server, Entity Framework Core, Apache Kafka, gRPC, Angular, React, TypeScript. </p>\n        				<p>Being a team leader, I’m constantly developing my soft skills, such as leadership, communication skills, priority management, conflict management, and motivational interviewing.</p>\n    			    </div>\n    			    <div class="col-md-6">\n    			        <p>When I feel that I’m stuck in a rut, I study or do something new. It could be an online course or a small side project. Although, at my current position, I work on back-end features most of the time, front-end development calls to all my passions, since it incorporates creativity and problem solving. When I graduated from university, I took on quite a few small side projects, so as to broaden my knowledge in web development. I would love applying responsive design and watching my web pages shrink into mobile screens and still look amazing. It’s oddly satisfying. </p>\n    			    </div>\n			    </div>\n			</div>\n			<div class = "education-places">\n				<div class = "container">\n					<div class = "place row">\n						<div class = "place-date col-md-3">[[*lang:is=`ru`:then=`2023 <span>май</span> &minus; настоящее время`:else=`2023 <span>may</span> &minus; present time`]]</div>\n						<div class = "place-description col-md-9">\n							<p class = "place-description-heading">Gameteq</p>\n							<p class = "place-description-sub-heading">full-stack software engineer</p>\n							[[$tplExperienceGameteqEn]]\n						</div>\n					</div>\n					<div class = "place row">\n						<div class = "place-date col-md-3">[[*lang:is=`ru`:then=`2022 <span>июн</span> &minus; 2023 <span>май</span>`:else=`2022 <span>jun</span> &minus; 2023 <span>may</span>`]]</div>\n						<div class = "place-description col-md-9">\n							<p class = "place-description-heading">[[*lang:is=`ru`:then=`Software country`:else=`Software country, <span>former Arcadia</span>`]]</p>\n							<p class = "place-description-sub-heading">senior software engineer &minus; team leader</p>\n							[[$tplExperienceSoftwareCountryEn]]\n						</div>\n					</div>\n					<div class = "place row">\n						<div class = "place-date col-md-3">[[*lang:is=`ru`:then=`2017 <span>окт</span> &minus; 2022 <span>июн</span>`:else=`2017 <span>oct</span> &minus; 2022 <span>jun</span>`]]</div>\n						<div class = "place-description col-md-9">\n							<p class = "place-description-heading">[[*lang:is=`ru`:then=`АО &laquo;Аркадия&raquo;`:else=`Arcadia, <span>an outsourcing company</span>`]]</p>\n							<p class = "place-description-sub-heading">software engineer</p>\n							[[$tplExperienceArcadiaEn]]\n						</div>\n					</div>\n					<div class = "place row">\n						<div class = "place-date col-md-3">[[*lang:is=`ru`:then=`2008 <span>авг</span> &minus; 2017 <span>март</span>`:else=`2008 <span>aug</span> &minus; 2017 <span>mar</span>`]]</div>\n						<div class = "place-description col-md-9">\n							<p class = "place-description-heading">[[*lang:is=`ru`:then=`ЗФ ПАО &laquo;ГМК &laquo;Норильский никель&raquo;`:else=`Norilsk Nickel, <span>the Polar Division</span>`]]</p>\n							<p class = "place-description-sub-heading">[[*lang:is=`ru`:then=`ведущий инженер-программист`:else=`software engineer`]]</p>\n							[[*lang:is=`ru`:then=`[[$tplExperienceNorilskNickelRu]]`:else=`[[$tplExperienceNorilskNickelEn]]`]]\n						</div>\n					</div>\n				</div>				\n			</div>\n		</section>\n		<section class = "github gradient">\n		    <a id = "github" class = "anchor"></a>\n			<div class = "h3-highlight">\n				<div class = "outer-container">\n					<div class = "relative-container">\n						<div class = "background-container">\n							<h3>GitHub</h3>\n						</div>\n					</div>\n				</div>\n			</div>\n			<div class = "container-fluid">\n				<div class = "row">\n					<div class = "col-lg-4 col-lg-offset-4 col-md-6 col-md-offset-3 col-sm-10 col-sm-offset-1">\n						<p class = "sub-heading">Visit my profile on <a target = "_blank" href="https://github.com/evgenii-petukhov/">GitHub</a>. The most valuable achievements are listed below</p>\n					</div>\n				</div>\n			</div>\n			<div class = "education-places">\n				<div class = "container">\n					<div class = "place row">\n						<div class = "place-date col-md-3">2023 <span>[[*lang:is=`ru`:then=`фев`:else=`feb`]]</span> &minus; [[*lang:is=`ru`:then=`настоящее время`:else=`present time`]]</span></div>\n						<div class = "place-description col-md-9">\n							<p class = "place-description-heading">&#128172; LetsTalk Chat App</p>\n							<p class = "place-description-sub-heading">Developed an instant messaging service with authentication via social media, such as Facebook. The service allows users to send text messages, images, and share links. The back-end implements microservice architecture. Microservices communicate with each other via Apache Kafka.</p>\n							<p class = "place-description-sub-heading">&#128190; <a target = "_blank" href="https://github.com/evgenii-petukhov/LetsTalk.Angular.App">Front-end repository on GitHub (Angular)</a></p>\n							<p class = "place-description-sub-heading">&#128190; <a target = "_blank" href="https://github.com/evgenii-petukhov/LetsTalk.Server">Back-end repository on GitHub (ASP.NET Core)</a></p>\n							<p class = "place-description-sub-heading">&#128308; <a target = "_blank" href="https://chat.epetukhov.cyou/">Live demo</a></p>\n						</div>\n					</div>\n					<div class = "place row">\n						<div class = "place-date col-md-3">2022 <span>[[*lang:is=`ru`:then=`сен`:else=`sep`]]</span> &minus; 2023 <span>[[*lang:is=`ru`:then=`янв`:else=`jan`]]</span></div>\n						<div class = "place-description col-md-9">\n							<p class = "place-description-heading">&#9201; React Time Difference</p>\n							<p class = "place-description-sub-heading">Developed a single-page web application which enables people to compare times in different timezones instantly</p>\n							<p class = "place-description-sub-heading">&#128190; <a target = "_blank" href="https://github.com/evgenii-petukhov/react-time-difference">Repository on GitHub</a></p>\n							<p class = "place-description-sub-heading">&#128308; <a target = "_blank" href="https://petukhov.cyou/react-time-difference/">Live demo</a></p>\n						</div>\n					</div>\n					<div class = "place row">\n						<div class = "place-date col-md-3">2019 <span>[[*lang:is=`ru`:then=`сен`:else=`sep`]]</span></div>\n						<div class = "place-description col-md-9">\n							<p class = "place-description-heading">&#128540; Angular X Social Login (npm package)</p>\n							<p class = "place-description-sub-heading">Contributed to the angularx-social-login GitHub project which entails login via Facebook and Google. Added a new login provider to log in on a website via VK (social media).</p>\n							<p class = "place-description-sub-heading">&#128190; <a target = "_blank" href="https://github.com/abacritt/angularx-social-login">Repository on GitHub</a></p>\n							<p class = "place-description-sub-heading">&#128190; <a target = "_blank" href="https://www.npmjs.com/package/@abacritt/angularx-social-login">Package page on npmjs.com</a></p>\n						</div>\n					</div>\n				</div>				\n			</div>\n			<div class="github-logo">\n			    <a target = "_blank" href="https://github.com/evgenii-petukhov/" title="Visit my profile on GitHub">\n                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" width="48px" height="48px">\n                        <path d="M256 32C132.3 32 32 134.8 32 261.7c0 101.5 64.2 187.5 153.2 217.9 11.2 2.1 15.3-5 15.3-11.1 0-5.5-.2-19.9-.3-39.1-62.3 13.9-75.5-30.8-75.5-30.8-10.2-26.5-24.9-33.6-24.9-33.6-20.3-14.3 1.5-14 1.5-14 22.5 1.6 34.3 23.7 34.3 23.7 20 35.1 52.4 25 65.2 19.1 2-14.8 7.8-25 14.2-30.7-49.7-5.8-102-25.5-102-113.5 0-25.1 8.7-45.6 23-61.6-2.3-5.8-10-29.2 2.2-60.8 0 0 18.8-6.2 61.6 23.5 17.9-5.1 37-7.6 56.1-7.7 19 .1 38.2 2.6 56.1 7.7 42.8-29.7 61.5-23.5 61.5-23.5 12.2 31.6 4.5 55 2.2 60.8 14.3 16.1 23 36.6 23 61.6 0 88.2-52.4 107.6-102.3 113.3 8 7.1 15.2 21.1 15.2 42.5 0 30.7-.3 55.5-.3 63 0 6.1 4 13.3 15.4 11C415.9 449.1 480 363.1 480 261.7 480 134.8 379.7 32 256 32Z" transform="matrix(1.14286 0 0 1.14292 -36.571 -36.573)" fill="#000"/>\n                    </svg>\n			    </a>\n			</div>\n		</section>\n		<section class = "contacts gradient">\n		  <a id = "contacts" class = "anchor"></a>\n			<div class = "h3-highlight">\n				<div class = "outer-container">\n					<div class = "relative-container">\n						<div class = "background-container">\n							<h3>[[*lang:is=`ru`:then=`Контакты`:else=`Contacts`]]</h3>\n						</div>\n					</div>\n				</div>\n			</div>		\n			<div class = "container-fluid">\n				<div class = "row">\n					<div class = "col-lg-4 col-lg-offset-4 col-md-6 col-md-offset-3 col-sm-10 col-sm-offset-1">\n						<p class = "sub-heading">[[*lang:is=`ru`:then=`Вы можете связаться со мной по e-mail, Telegram или написать мне в соц. сети`:else=`send me an e-mail, message me on Telegram or contact me through social media`]]</p>					\n					</div>\n				</div>\n				<div class = "row">\n					<div class = "activity-box activity-box-background col-md-4">\n						<div class = "box-content">\n							<div class = "box-table">\n								<div class = "box-row">\n									<div class = "box-cell">\n										<div class = "box-heading">Telegram</div>\n										<div class = "box-sub-heading">send me a message<br/>@Evgenii_Petukhov</div>\n									</div>\n								</div>\n							</div>\n						</div>\n					</div>\n					<div class = "activity-box activity-box-background col-md-4" id="social-media-box">\n						<div class = "box-content">\n							<div class = "box-table">\n								<div class = "box-row">\n									<div class = "box-cell">\n										<div class = "box-heading">[[*lang:is=`ru`:then=`Социальные сети`:else=`Social media`]]</div>\n										<div class = "box-sub-heading">[[*lang:is=`ru`:then=`давайте дружить`:else=`let\'s start a friendship`]]</div>\n										<div class = "social-media">\n											<ul>\n												<li class = "icon-linkedin">\n													<a href = "https://www.linkedin.com/in/evgenii-petukhov-software-engineer/" target = "_blank" rel="noopener">\n														<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">\n															<rect height="512" rx="64" ry="64" width="512"/>																	\n															<path d="M129.957 200.357h55.194v166.021h-55.194zM157.927 120.303c-18.884 0-31.222 12.415-31.222 28.687 0 15.93 11.963 28.687 30.491 28.687h.357c19.245 0 31.224-12.757 31.224-28.687-.357-16.272-11.978-28.687-30.85-28.687zM320.604 196.453c-29.277 0-42.391 16.101-49.734 27.41v-23.506h-55.18c.732 15.573 0 166.021 0 166.021h55.179V273.66c0-4.963.357-9.924 1.82-13.471 3.982-9.911 13.068-20.178 28.313-20.178 19.959 0 27.955 15.23 27.955 37.539v88.828h55.182v-95.206c0-50.996-27.227-74.719-63.535-74.719z" transform="matrix(1.5538 0 0 1.5538 -140.873 -132.646)" fill="#fff"/>\n														</svg>\n													</a>\n												</li>\n												<li class = "icon-github">\n													<a href = "https://github.com/evgenii-petukhov" target = "_blank" rel="noopener">\n                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">\n                                                            <rect height="512" rx="64" ry="64" width="512"/>\n                                                            <path d="M256 32C132.3 32 32 134.8 32 261.7c0 101.5 64.2 187.5 153.2 217.9 11.2 2.1 15.3-5 15.3-11.1 0-5.5-.2-19.9-.3-39.1-62.3 13.9-75.5-30.8-75.5-30.8-10.2-26.5-24.9-33.6-24.9-33.6-20.3-14.3 1.5-14 1.5-14 22.5 1.6 34.3 23.7 34.3 23.7 20 35.1 52.4 25 65.2 19.1 2-14.8 7.8-25 14.2-30.7-49.7-5.8-102-25.5-102-113.5 0-25.1 8.7-45.6 23-61.6-2.3-5.8-10-29.2 2.2-60.8 0 0 18.8-6.2 61.6 23.5 17.9-5.1 37-7.6 56.1-7.7 19 .1 38.2 2.6 56.1 7.7 42.8-29.7 61.5-23.5 61.5-23.5 12.2 31.6 4.5 55 2.2 60.8 14.3 16.1 23 36.6 23 61.6 0 88.2-52.4 107.6-102.3 113.3 8 7.1 15.2 21.1 15.2 42.5 0 30.7-.3 55.5-.3 63 0 6.1 4 13.3 15.4 11C415.9 449.1 480 363.1 480 261.7 480 134.8 379.7 32 256 32Z" transform="matrix(1.14286 0 0 1.14292 -36.571 -36.573)" fill="#fff"/>\n                                                        </svg>\n													</a>\n												</li>\n												<li class = "icon-fb">\n													<a href = "https://facebook.com/petukhoven" target = "_blank" rel="noopener">\n														<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">\n															<rect height="512" rx="64" ry="64" width="512"/>\n															<path d="M287 456V273.5h61.2l9.2-71.1h-70.4v-45.4c0-20.6 5.7-34.6 35.2-34.6l37.7 0V58.8c-6.5-0.9-28.9-2.8-54.9-2.8-54.3 0-91.4 33.1-91.4 94v52.4h-61.4v71.1h61.4v182.5h73.4z" fill="#fff"/>\n														</svg>\n													</a>\n												</li>\n												<li class = "icon-instagram">\n													<a href = "https://www.instagram.com/evgenii.in.wonderland/" target = "_blank" rel="noopener">\n														<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">\n															<rect height="512" rx="64" ry="64" width="512"/>\n															<path d="M366.4 76H145.6c-38.4 0-69.6 31.2-69.6 69.6v73.5 147.4c0 38.4 31.2 69.6 69.6 69.6H366.4c38.4 0 69.6-31.2 69.6-69.6V219 145.6c0-38.4-31.2-69.6-69.6-69.6zm20 41.5 8 0v7.9 53.1l-60.8 0.2-0.2-61 53.1-0.2zM204.6 219c11.5-16 30.2-26.4 51.4-26.4 21.1 0 39.9 10.5 51.4 26.4 7.5 10.4 12 23.2 12 36.9 0 34.9-28.5 63.4-63.4 63.4-34.9 0-63.4-28.4-63.4-63.4 0-13.8 4.5-26.5 12-36.9zm196.3 147.4c0 19-15.5 34.5-34.5 34.5H145.6c-19 0-34.5-15.5-34.5-34.5V219h53.7c-4.6 11.4-7.3 23.9-7.3 36.9 0 54.3 44.2 98.4 98.4 98.4 54.3 0 98.4-44.2 98.4-98.4 0-13.1-2.6-25.5-7.3-36.9h53.7v147.4z" fill="#fff"/>\n														</svg>\n													</a>\n												</li>\n											</ul>\n										</div>										\n									</div>\n								</div>\n							</div>\n						</div>\n					</div>					\n					<div class = "activity-box activity-box-background col-md-4">\n						<div class = "box-content">\n							<div class = "box-table">\n								<div class = "box-row">\n									<div class = "box-cell">\n										<div class = "box-heading">[[*lang:is=`ru`:then=`Электронная почта`:else=`E-mail`]]</div>\n										<div class = "box-sub-heading">[[*lang:is=`ru`:then=`напишите мне`:else=`send me an e-mail`]]<br/>e-mail: <a href = "mailto:petukhoven@gmail.com">petukhoven@gmail.com</a></div>\n									</div>\n								</div>\n							</div>\n						</div>\n					</div>					\n				</div>\n			</div>\n		</section>\n[[$tplFooter]]', 0, 'a:0:{}', 0, '', ''),
	(4, 1, 0, 'Error 404', '', 0, 0, '', 0, '[[$tplHeadCommon]]\n  </head>\n  <body data-spy="scroll">\n    [[$tplMainMenu]]\n		<div class = "container">\n			<div class = "row">\n				<div class = "col-md-12">\n				  <p>Error 404: page found</p>\n				</div>\n			</div>\n		</div>		\n[[$tplFooter]]', 0, 'a:0:{}', 0, '', '');
/*!40000 ALTER TABLE `modx_site_templates` ENABLE KEYS */;

-- Dumping structure for table petukhov.modx_site_tmplvars
CREATE TABLE IF NOT EXISTS `modx_site_tmplvars` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `source` int(10) unsigned NOT NULL DEFAULT 0,
  `property_preprocess` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `type` varchar(20) NOT NULL DEFAULT '',
  `name` varchar(50) NOT NULL DEFAULT '',
  `caption` varchar(80) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `editor_type` int(11) NOT NULL DEFAULT 0,
  `category` int(11) NOT NULL DEFAULT 0,
  `locked` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `elements` text DEFAULT NULL,
  `rank` int(11) NOT NULL DEFAULT 0,
  `display` varchar(20) NOT NULL DEFAULT '',
  `default_text` mediumtext DEFAULT NULL,
  `properties` text DEFAULT NULL,
  `input_properties` text DEFAULT NULL,
  `output_properties` text DEFAULT NULL,
  `static` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `static_file` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `category` (`category`),
  KEY `locked` (`locked`),
  KEY `rank` (`rank`),
  KEY `static` (`static`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table petukhov.modx_site_tmplvars: 8 rows
/*!40000 ALTER TABLE `modx_site_tmplvars` DISABLE KEYS */;
INSERT INTO `modx_site_tmplvars` (`id`, `source`, `property_preprocess`, `type`, `name`, `caption`, `description`, `editor_type`, `category`, `locked`, `elements`, `rank`, `display`, `default_text`, `properties`, `input_properties`, `output_properties`, `static`, `static_file`) VALUES
	(1, 1, 0, 'text', 'technologies', 'Технологии', '', 0, 1, 0, '', 0, 'default', '', 'a:0:{}', 'a:5:{s:10:"allowBlank";s:4:"true";s:9:"maxLength";s:0:"";s:9:"minLength";s:0:"";s:5:"regex";s:0:"";s:9:"regexText";s:0:"";}', 'a:0:{}', 0, ''),
	(3, 1, 0, 'checkbox', 'hidden-lg', 'Скрывать на десктопе', '', 0, 1, 0, 'Да==1', 0, 'default', '', 'a:0:{}', 'a:2:{s:10:"allowBlank";s:4:"true";s:7:"columns";s:1:"1";}', 'a:0:{}', 0, ''),
	(4, 1, 0, 'checkbox', 'hidden-md', 'Скрывать на планшете (в альбомной ориентации)', '', 0, 1, 0, 'Да==1', 0, 'default', '', 'a:0:{}', 'a:2:{s:10:"allowBlank";s:4:"true";s:7:"columns";s:1:"1";}', 'a:0:{}', 0, ''),
	(5, 1, 0, 'listbox', 'lang', 'Язык', '', 0, 1, 0, 'Русский==ru||Английский==en', 0, 'default', '', 'a:0:{}', 'a:7:{s:10:"allowBlank";s:4:"true";s:9:"listWidth";s:0:"";s:5:"title";s:0:"";s:9:"typeAhead";s:5:"false";s:14:"typeAheadDelay";s:3:"250";s:14:"forceSelection";s:5:"false";s:13:"listEmptyText";s:0:"";}', 'a:0:{}', 0, ''),
	(6, 1, 0, 'text', 'longtitle-en', 'Расширенный заголовок', '', 0, 2, 0, '', 1, 'default', '', 'a:0:{}', 'a:5:{s:10:"allowBlank";s:4:"true";s:9:"maxLength";s:0:"";s:9:"minLength";s:0:"";s:5:"regex";s:0:"";s:9:"regexText";s:0:"";}', 'a:0:{}', 0, ''),
	(7, 1, 0, 'richtext', 'content-en', 'Содержимое', '', 0, 2, 0, '', 2, 'default', '', 'a:0:{}', 'a:0:{}', 'a:0:{}', 0, ''),
	(8, 1, 0, 'text', 'pagetitle-en', 'Заголовок', '', 0, 2, 0, '', 0, 'default', '', 'a:0:{}', 'a:5:{s:10:"allowBlank";s:4:"true";s:9:"maxLength";s:0:"";s:9:"minLength";s:0:"";s:5:"regex";s:0:"";s:9:"regexText";s:0:"";}', 'a:0:{}', 0, ''),
	(11, 1, 0, 'text', 'anchor', 'ID закладки', '', 0, 1, 0, '', 0, 'default', '', 'a:0:{}', 'a:5:{s:10:"allowBlank";s:4:"true";s:9:"maxLength";s:0:"";s:9:"minLength";s:0:"";s:5:"regex";s:0:"";s:9:"regexText";s:0:"";}', 'a:0:{}', 0, '');
/*!40000 ALTER TABLE `modx_site_tmplvars` ENABLE KEYS */;

-- Dumping structure for table petukhov.modx_site_tmplvar_access
CREATE TABLE IF NOT EXISTS `modx_site_tmplvar_access` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tmplvarid` int(10) NOT NULL DEFAULT 0,
  `documentgroup` int(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `tmplvar_template` (`tmplvarid`,`documentgroup`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table petukhov.modx_site_tmplvar_access: 0 rows
/*!40000 ALTER TABLE `modx_site_tmplvar_access` DISABLE KEYS */;
/*!40000 ALTER TABLE `modx_site_tmplvar_access` ENABLE KEYS */;

-- Dumping structure for table petukhov.modx_site_tmplvar_contentvalues
CREATE TABLE IF NOT EXISTS `modx_site_tmplvar_contentvalues` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tmplvarid` int(10) NOT NULL DEFAULT 0,
  `contentid` int(10) NOT NULL DEFAULT 0,
  `value` mediumtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tmplvarid` (`tmplvarid`),
  KEY `contentid` (`contentid`),
  KEY `tv_cnt` (`tmplvarid`,`contentid`)
) ENGINE=MyISAM AUTO_INCREMENT=86 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table petukhov.modx_site_tmplvar_contentvalues: 4 rows
/*!40000 ALTER TABLE `modx_site_tmplvar_contentvalues` DISABLE KEYS */;
INSERT INTO `modx_site_tmplvar_contentvalues` (`id`, `tmplvarid`, `contentid`, `value`) VALUES
	(23, 5, 1, 'en'),
	(24, 5, 2, 'ru'),
	(69, 5, 18, 'en'),
	(71, 6, 18, '404');
/*!40000 ALTER TABLE `modx_site_tmplvar_contentvalues` ENABLE KEYS */;

-- Dumping structure for table petukhov.modx_site_tmplvar_templates
CREATE TABLE IF NOT EXISTS `modx_site_tmplvar_templates` (
  `tmplvarid` int(10) NOT NULL DEFAULT 0,
  `templateid` int(11) NOT NULL DEFAULT 0,
  `rank` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`tmplvarid`,`templateid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table petukhov.modx_site_tmplvar_templates: 5 rows
/*!40000 ALTER TABLE `modx_site_tmplvar_templates` DISABLE KEYS */;
INSERT INTO `modx_site_tmplvar_templates` (`tmplvarid`, `templateid`, `rank`) VALUES
	(5, 1, 0),
	(8, 4, 0),
	(5, 4, 0),
	(6, 1, 0),
	(6, 4, 0);
/*!40000 ALTER TABLE `modx_site_tmplvar_templates` ENABLE KEYS */;

-- Dumping structure for table petukhov.modx_system_eventnames
CREATE TABLE IF NOT EXISTS `modx_system_eventnames` (
  `name` varchar(50) NOT NULL,
  `service` tinyint(4) unsigned NOT NULL DEFAULT 0,
  `groupname` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table petukhov.modx_system_eventnames: 189 rows
/*!40000 ALTER TABLE `modx_system_eventnames` DISABLE KEYS */;
INSERT INTO `modx_system_eventnames` (`name`, `service`, `groupname`) VALUES
	('OnPluginEventBeforeSave', 1, 'Plugin Events'),
	('OnPluginEventSave', 1, 'Plugin Events'),
	('OnPluginEventBeforeRemove', 1, 'Plugin Events'),
	('OnPluginEventRemove', 1, 'Plugin Events'),
	('OnResourceGroupSave', 1, 'Security'),
	('OnResourceGroupBeforeSave', 1, 'Security'),
	('OnResourceGroupRemove', 1, 'Security'),
	('OnResourceGroupBeforeRemove', 1, 'Security'),
	('OnSnippetBeforeSave', 1, 'Snippets'),
	('OnSnippetSave', 1, 'Snippets'),
	('OnSnippetBeforeRemove', 1, 'Snippets'),
	('OnSnippetRemove', 1, 'Snippets'),
	('OnSnipFormPrerender', 1, 'Snippets'),
	('OnSnipFormRender', 1, 'Snippets'),
	('OnBeforeSnipFormSave', 1, 'Snippets'),
	('OnSnipFormSave', 1, 'Snippets'),
	('OnBeforeSnipFormDelete', 1, 'Snippets'),
	('OnSnipFormDelete', 1, 'Snippets'),
	('OnTemplateBeforeSave', 1, 'Templates'),
	('OnTemplateSave', 1, 'Templates'),
	('OnTemplateBeforeRemove', 1, 'Templates'),
	('OnTemplateRemove', 1, 'Templates'),
	('OnTempFormPrerender', 1, 'Templates'),
	('OnTempFormRender', 1, 'Templates'),
	('OnBeforeTempFormSave', 1, 'Templates'),
	('OnTempFormSave', 1, 'Templates'),
	('OnBeforeTempFormDelete', 1, 'Templates'),
	('OnTempFormDelete', 1, 'Templates'),
	('OnTemplateVarBeforeSave', 1, 'Template Variables'),
	('OnTemplateVarSave', 1, 'Template Variables'),
	('OnTemplateVarBeforeRemove', 1, 'Template Variables'),
	('OnTemplateVarRemove', 1, 'Template Variables'),
	('OnTVFormPrerender', 1, 'Template Variables'),
	('OnTVFormRender', 1, 'Template Variables'),
	('OnBeforeTVFormSave', 1, 'Template Variables'),
	('OnTVFormSave', 1, 'Template Variables'),
	('OnBeforeTVFormDelete', 1, 'Template Variables'),
	('OnTVFormDelete', 1, 'Template Variables'),
	('OnTVInputRenderList', 1, 'Template Variables'),
	('OnTVInputPropertiesList', 1, 'Template Variables'),
	('OnTVOutputRenderList', 1, 'Template Variables'),
	('OnTVOutputRenderPropertiesList', 1, 'Template Variables'),
	('OnUserGroupBeforeSave', 1, 'User Groups'),
	('OnUserGroupSave', 1, 'User Groups'),
	('OnUserGroupBeforeRemove', 1, 'User Groups'),
	('OnUserGroupRemove', 1, 'User Groups'),
	('OnBeforeUserGroupFormSave', 1, 'User Groups'),
	('OnUserGroupFormSave', 1, 'User Groups'),
	('OnBeforeUserGroupFormRemove', 1, 'User Groups'),
	('OnDocFormPrerender', 1, 'Resources'),
	('OnDocFormRender', 1, 'Resources'),
	('OnBeforeDocFormSave', 1, 'Resources'),
	('OnDocFormSave', 1, 'Resources'),
	('OnBeforeDocFormDelete', 1, 'Resources'),
	('OnDocFormDelete', 1, 'Resources'),
	('OnDocPublished', 5, 'Resources'),
	('OnDocUnPublished', 5, 'Resources'),
	('OnBeforeEmptyTrash', 1, 'Resources'),
	('OnEmptyTrash', 1, 'Resources'),
	('OnResourceTVFormPrerender', 1, 'Resources'),
	('OnResourceTVFormRender', 1, 'Resources'),
	('OnResourceAutoPublish', 1, 'Resources'),
	('OnResourceDelete', 1, 'Resources'),
	('OnResourceUndelete', 1, 'Resources'),
	('OnResourceBeforeSort', 1, 'Resources'),
	('OnResourceSort', 1, 'Resources'),
	('OnResourceDuplicate', 1, 'Resources'),
	('OnResourceToolbarLoad', 1, 'Resources'),
	('OnResourceRemoveFromResourceGroup', 1, 'Resources'),
	('OnResourceAddToResourceGroup', 1, 'Resources'),
	('OnRichTextEditorRegister', 1, 'RichText Editor'),
	('OnRichTextEditorInit', 1, 'RichText Editor'),
	('OnRichTextBrowserInit', 1, 'RichText Editor'),
	('OnWebLogin', 3, 'Security'),
	('OnBeforeWebLogout', 3, 'Security'),
	('OnWebLogout', 3, 'Security'),
	('OnManagerLogin', 2, 'Security'),
	('OnBeforeManagerLogout', 2, 'Security'),
	('OnManagerLogout', 2, 'Security'),
	('OnBeforeWebLogin', 3, 'Security'),
	('OnWebAuthentication', 3, 'Security'),
	('OnBeforeManagerLogin', 2, 'Security'),
	('OnManagerAuthentication', 2, 'Security'),
	('OnManagerLoginFormRender', 2, 'Security'),
	('OnManagerLoginFormPrerender', 2, 'Security'),
	('OnPageUnauthorized', 1, 'Security'),
	('OnUserFormPrerender', 1, 'Users'),
	('OnUserFormRender', 1, 'Users'),
	('OnBeforeUserFormSave', 1, 'Users'),
	('OnUserFormSave', 1, 'Users'),
	('OnBeforeUserFormDelete', 1, 'Users'),
	('OnUserFormDelete', 1, 'Users'),
	('OnUserNotFound', 1, 'Users'),
	('OnBeforeUserActivate', 1, 'Users'),
	('OnUserActivate', 1, 'Users'),
	('OnBeforeUserDeactivate', 1, 'Users'),
	('OnUserDeactivate', 1, 'Users'),
	('OnBeforeUserDuplicate', 1, 'Users'),
	('OnUserDuplicate', 1, 'Users'),
	('OnUserChangePassword', 1, 'Users'),
	('OnUserBeforeRemove', 1, 'Users'),
	('OnUserBeforeSave', 1, 'Users'),
	('OnUserSave', 1, 'Users'),
	('OnUserRemove', 1, 'Users'),
	('OnUserBeforeAddToGroup', 1, 'User Groups'),
	('OnUserAddToGroup', 1, 'User Groups'),
	('OnUserBeforeRemoveFromGroup', 1, 'User Groups'),
	('OnUserRemoveFromGroup', 1, 'User Groups'),
	('OnWebPagePrerender', 5, 'System'),
	('OnBeforeCacheUpdate', 4, 'System'),
	('OnCacheUpdate', 4, 'System'),
	('OnLoadWebPageCache', 4, 'System'),
	('OnBeforeSaveWebPageCache', 4, 'System'),
	('OnSiteRefresh', 1, 'System'),
	('OnFileManagerDirCreate', 1, 'System'),
	('OnFileManagerDirRemove', 1, 'System'),
	('OnFileManagerDirRename', 1, 'System'),
	('OnFileManagerFileRename', 1, 'System'),
	('OnFileManagerFileRemove', 1, 'System'),
	('OnFileManagerFileUpdate', 1, 'System'),
	('OnFileManagerFileCreate', 1, 'System'),
	('OnFileManagerBeforeUpload', 1, 'System'),
	('OnFileManagerUpload', 1, 'System'),
	('OnFileManagerMoveObject', 1, 'System'),
	('OnFileCreateFormPrerender', 1, 'System'),
	('OnFileEditFormPrerender', 1, 'System'),
	('OnManagerPageInit', 2, 'System'),
	('OnManagerPageBeforeRender', 2, 'System'),
	('OnManagerPageAfterRender', 2, 'System'),
	('OnWebPageInit', 5, 'System'),
	('OnLoadWebDocument', 5, 'System'),
	('OnParseDocument', 5, 'System'),
	('OnWebPageComplete', 5, 'System'),
	('OnBeforeManagerPageInit', 2, 'System'),
	('OnPageNotFound', 1, 'System'),
	('OnHandleRequest', 5, 'System'),
	('OnMODXInit', 5, 'System'),
	('OnElementNotFound', 1, 'System'),
	('OnSiteSettingsRender', 1, 'Settings'),
	('OnInitCulture', 1, 'Internationalization'),
	('OnCategorySave', 1, 'Categories'),
	('OnCategoryBeforeSave', 1, 'Categories'),
	('OnCategoryRemove', 1, 'Categories'),
	('OnCategoryBeforeRemove', 1, 'Categories'),
	('OnChunkSave', 1, 'Chunks'),
	('OnChunkBeforeSave', 1, 'Chunks'),
	('OnChunkRemove', 1, 'Chunks'),
	('OnChunkBeforeRemove', 1, 'Chunks'),
	('OnChunkFormPrerender', 1, 'Chunks'),
	('OnChunkFormRender', 1, 'Chunks'),
	('OnBeforeChunkFormSave', 1, 'Chunks'),
	('OnChunkFormSave', 1, 'Chunks'),
	('OnBeforeChunkFormDelete', 1, 'Chunks'),
	('OnChunkFormDelete', 1, 'Chunks'),
	('OnContextSave', 1, 'Contexts'),
	('OnContextBeforeSave', 1, 'Contexts'),
	('OnContextRemove', 1, 'Contexts'),
	('OnContextBeforeRemove', 1, 'Contexts'),
	('OnContextFormPrerender', 2, 'Contexts'),
	('OnContextFormRender', 2, 'Contexts'),
	('OnPluginSave', 1, 'Plugins'),
	('OnPluginBeforeSave', 1, 'Plugins'),
	('OnPluginRemove', 1, 'Plugins'),
	('OnPluginBeforeRemove', 1, 'Plugins'),
	('OnPluginFormPrerender', 1, 'Plugins'),
	('OnPluginFormRender', 1, 'Plugins'),
	('OnBeforePluginFormSave', 1, 'Plugins'),
	('OnPluginFormSave', 1, 'Plugins'),
	('OnBeforePluginFormDelete', 1, 'Plugins'),
	('OnPluginFormDelete', 1, 'Plugins'),
	('OnPropertySetSave', 1, 'Property Sets'),
	('OnPropertySetBeforeSave', 1, 'Property Sets'),
	('OnPropertySetRemove', 1, 'Property Sets'),
	('OnPropertySetBeforeRemove', 1, 'Property Sets'),
	('OnMediaSourceBeforeFormDelete', 1, 'Media Sources'),
	('OnMediaSourceBeforeFormSave', 1, 'Media Sources'),
	('OnMediaSourceGetProperties', 1, 'Media Sources'),
	('OnMediaSourceFormDelete', 1, 'Media Sources'),
	('OnMediaSourceFormSave', 1, 'Media Sources'),
	('OnMediaSourceDuplicate', 1, 'Media Sources'),
	('OnUserProfileBeforeSave', 1, 'User Profiles'),
	('OnUserProfileSave', 1, 'User Profiles'),
	('OnUserProfileBeforeRemove', 1, 'User Profiles'),
	('OnUserProfileRemove', 1, 'User Profiles'),
	('OnResourceCacheUpdate', 1, 'Resources'),
	('OnBeforeRegisterClientScripts', 5, 'System'),
	('OnPackageInstall', 2, 'Package Manager'),
	('OnPackageUninstall', 2, 'Package Manager'),
	('OnPackageRemove', 2, 'Package Manager');
/*!40000 ALTER TABLE `modx_system_eventnames` ENABLE KEYS */;

-- Dumping structure for table petukhov.modx_system_settings
CREATE TABLE IF NOT EXISTS `modx_system_settings` (
  `key` varchar(50) NOT NULL DEFAULT '',
  `value` text NOT NULL,
  `xtype` varchar(75) NOT NULL DEFAULT 'textfield',
  `namespace` varchar(40) NOT NULL DEFAULT 'core',
  `area` varchar(255) NOT NULL DEFAULT '',
  `editedon` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table petukhov.modx_system_settings: 279 rows
/*!40000 ALTER TABLE `modx_system_settings` DISABLE KEYS */;
INSERT INTO `modx_system_settings` (`key`, `value`, `xtype`, `namespace`, `area`, `editedon`) VALUES
	('access_category_enabled', '1', 'combo-boolean', 'core', 'authentication', NULL),
	('access_context_enabled', '1', 'combo-boolean', 'core', 'authentication', NULL),
	('access_resource_group_enabled', '1', 'combo-boolean', 'core', 'authentication', NULL),
	('allow_forward_across_contexts', '', 'combo-boolean', 'core', 'system', NULL),
	('allow_manager_login_forgot_password', '1', 'combo-boolean', 'core', 'authentication', NULL),
	('allow_multiple_emails', '1', 'combo-boolean', 'core', 'authentication', NULL),
	('allow_tags_in_post', '', 'combo-boolean', 'core', 'system', NULL),
	('archive_with', '', 'combo-boolean', 'core', 'system', NULL),
	('auto_menuindex', '1', 'combo-boolean', 'core', 'site', NULL),
	('auto_check_pkg_updates', '1', 'combo-boolean', 'core', 'system', NULL),
	('auto_check_pkg_updates_cache_expire', '15', 'numberfield', 'core', 'system', '2022-10-04 19:32:09'),
	('automatic_alias', '1', 'combo-boolean', 'core', 'furls', NULL),
	('base_help_url', '//rtfm.modx.com/display/revolution20/', 'textfield', 'core', 'manager', NULL),
	('blocked_minutes', '60', 'numberfield', 'core', 'authentication', '2022-10-04 19:32:09'),
	('upload_translit', '1', 'combo-boolean', 'core', 'file', NULL),
	('cache_alias_map', '1', 'combo-boolean', 'core', 'caching', NULL),
	('cache_context_settings', '1', 'combo-boolean', 'core', 'caching', NULL),
	('cache_db', '0', 'combo-boolean', 'core', 'caching', NULL),
	('cache_db_expires', '0', 'numberfield', 'core', 'caching', '2022-10-04 19:32:09'),
	('cache_db_session', '0', 'combo-boolean', 'core', 'caching', NULL),
	('cache_db_session_lifetime', '', 'numberfield', 'core', 'caching', '2022-10-04 19:32:09'),
	('cache_default', '1', 'combo-boolean', 'core', 'caching', NULL),
	('cache_expires', '0', 'numberfield', 'core', 'caching', '2022-10-04 19:32:09'),
	('cache_format', '0', 'numberfield', 'core', 'caching', '2022-10-04 19:32:09'),
	('cache_handler', 'xPDOFileCache', 'textfield', 'core', 'caching', NULL),
	('cache_lang_js', '1', 'combo-boolean', 'core', 'caching', NULL),
	('cache_lexicon_topics', '1', 'combo-boolean', 'core', 'caching', NULL),
	('cache_noncore_lexicon_topics', '1', 'combo-boolean', 'core', 'caching', NULL),
	('cache_resource', '1', 'combo-boolean', 'core', 'caching', NULL),
	('cache_resource_expires', '0', 'numberfield', 'core', 'caching', '2022-10-04 19:32:09'),
	('cache_scripts', '1', 'combo-boolean', 'core', 'caching', NULL),
	('cache_system_settings', '1', 'combo-boolean', 'core', 'caching', NULL),
	('clear_cache_refresh_trees', '0', 'combo-boolean', 'core', 'caching', NULL),
	('compress_css', '1', 'combo-boolean', 'core', 'manager', NULL),
	('compress_js', '1', 'combo-boolean', 'core', 'manager', NULL),
	('upload_file_exists', '1', 'combo-boolean', 'core', 'file', NULL),
	('confirm_navigation', '1', 'combo-boolean', 'core', 'manager', NULL),
	('container_suffix', '/', 'textfield', 'core', 'furls', NULL),
	('context_tree_sort', '', 'combo-boolean', 'core', 'manager', NULL),
	('context_tree_sortby', 'rank', 'textfield', 'core', 'manager', NULL),
	('context_tree_sortdir', 'ASC', 'textfield', 'core', 'manager', NULL),
	('cultureKey', 'ru', 'modx-combo-language', 'core', 'language', '2016-08-04 07:52:19'),
	('date_timezone', '', 'textfield', 'core', 'system', NULL),
	('debug', '', 'numberfield', 'core', 'system', '2022-10-04 19:32:09'),
	('default_duplicate_publish_option', 'preserve', 'textfield', 'core', 'manager', NULL),
	('default_media_source', '1', 'modx-combo-source', 'core', 'manager', NULL),
	('default_per_page', '20', 'textfield', 'core', 'manager', NULL),
	('default_context', 'web', 'modx-combo-context', 'core', 'site', NULL),
	('default_template', '1', 'modx-combo-template', 'core', 'site', '2023-04-15 03:39:59'),
	('default_content_type', '1', 'modx-combo-content-type', 'core', 'site', NULL),
	('quick_search_result_max', '10', 'numberfield', 'core', 'manager', NULL),
	('emailsender', 'site.messages@petukhov.pro', 'textfield', 'core', 'authentication', '2016-08-12 11:42:15'),
	('emailsubject', 'Your login details', 'textfield', 'core', 'authentication', NULL),
	('enable_dragdrop', '1', 'combo-boolean', 'core', 'manager', NULL),
	('error_page', '18', 'numberfield', 'core', 'site', '2022-10-04 19:32:09'),
	('failed_login_attempts', '5', 'numberfield', 'core', 'authentication', '2022-10-04 19:32:09'),
	('quick_search_in_content', '1', 'combo-boolean', 'core', 'manager', NULL),
	('feed_modx_news', 'https://feeds.feedburner.com/modx-announce', 'textfield', 'core', 'system', '2021-11-26 16:46:59'),
	('feed_modx_news_enabled', '1', 'combo-boolean', 'core', 'system', NULL),
	('feed_modx_security', 'https://forums.modx.com/board.xml?board=294', 'textfield', 'core', 'system', '2021-11-26 16:46:59'),
	('feed_modx_security_enabled', '1', 'combo-boolean', 'core', 'system', NULL),
	('enable_template_picker_in_tree', '1', 'combo-boolean', 'core', 'manager', NULL),
	('manager_logo', '', 'textfield', 'core', 'manager', NULL),
	('login_help_button', '', 'combo-boolean', 'core', 'authentication', NULL),
	('forgot_login_email', '<p>Hello [[+username]],</p>\n<p>A request for a password reset has been issued for your MODX user. If you sent this, you may follow this link and use this password to login. If you did not send this request, please ignore this email.</p>\n\n<p>\n    <strong>Activation Link:</strong> [[+url_scheme]][[+http_host]][[+manager_url]]?modahsh=[[+hash]]<br />\n    <strong>Username:</strong> [[+username]]<br />\n    <strong>Password:</strong> [[+password]]<br />\n</p>\n\n<p>After you log into the MODX Manager, you can change your password again, if you wish.</p>\n\n<p>Regards,<br />Site Administrator</p>', 'textarea', 'core', 'authentication', NULL),
	('form_customization_use_all_groups', '', 'combo-boolean', 'core', 'manager', NULL),
	('forward_merge_excludes', 'type,published,class_key', 'textfield', 'core', 'system', NULL),
	('friendly_alias_lowercase_only', '1', 'combo-boolean', 'core', 'furls', NULL),
	('friendly_alias_max_length', '0', 'numberfield', 'core', 'furls', '2022-10-04 19:32:09'),
	('friendly_alias_realtime', '0', 'combo-boolean', 'core', 'furls', '2016-08-04 12:56:30'),
	('friendly_alias_restrict_chars', 'pattern', 'textfield', 'core', 'furls', NULL),
	('friendly_alias_restrict_chars_pattern', '/[\\0\\x0B\\t\\n\\r\\f\\a&=+%#<>"~:`@\\?\\[\\]\\{\\}\\|\\^\'\\\\]/', 'textfield', 'core', 'furls', NULL),
	('friendly_alias_strip_element_tags', '1', 'combo-boolean', 'core', 'furls', NULL),
	('friendly_alias_translit', 'russian', 'textfield', 'core', 'furls', '2016-08-04 15:30:37'),
	('friendly_alias_translit_class', 'modx.translit.modTransliterate', 'textfield', 'core', 'furls', '2016-08-04 10:46:16'),
	('friendly_alias_translit_class_path', '{core_path}components/translit/model/', 'textfield', 'core', 'furls', '2016-08-04 10:46:16'),
	('friendly_alias_trim_chars', '/.-_', 'textfield', 'core', 'furls', NULL),
	('friendly_alias_word_delimiter', '-', 'textfield', 'core', 'furls', NULL),
	('friendly_alias_word_delimiters', '-_', 'textfield', 'core', 'furls', NULL),
	('friendly_urls', '1', 'combo-boolean', 'core', 'furls', '2016-08-04 17:48:35'),
	('friendly_urls_strict', '0', 'combo-boolean', 'core', 'furls', NULL),
	('use_frozen_parent_uris', '0', 'combo-boolean', 'core', 'furls', NULL),
	('global_duplicate_uri_check', '0', 'combo-boolean', 'core', 'furls', NULL),
	('hidemenu_default', '0', 'combo-boolean', 'core', 'site', NULL),
	('inline_help', '1', 'combo-boolean', 'core', 'manager', NULL),
	('locale', '', 'textfield', 'core', 'language', NULL),
	('log_level', '1', 'numberfield', 'core', 'system', '2022-10-04 19:32:09'),
	('log_target', 'FILE', 'textfield', 'core', 'system', NULL),
	('link_tag_scheme', '-1', 'textfield', 'core', 'site', NULL),
	('lock_ttl', '360', 'numberfield', 'core', 'system', '2022-10-04 19:32:09'),
	('mail_charset', 'UTF-8', 'modx-combo-charset', 'core', 'mail', NULL),
	('mail_encoding', '8bit', 'textfield', 'core', 'mail', NULL),
	('mail_use_smtp', '', 'combo-boolean', 'core', 'mail', NULL),
	('mail_smtp_auth', '', 'combo-boolean', 'core', 'mail', NULL),
	('mail_smtp_helo', '', 'textfield', 'core', 'mail', NULL),
	('mail_smtp_hosts', 'localhost', 'textfield', 'core', 'mail', NULL),
	('mail_smtp_keepalive', '', 'combo-boolean', 'core', 'mail', NULL),
	('mail_smtp_pass', '', 'text-password', 'core', 'mail', NULL),
	('mail_smtp_port', '587', 'numberfield', 'core', 'mail', '2022-10-04 19:32:09'),
	('mail_smtp_prefix', '', 'textfield', 'core', 'mail', NULL),
	('mail_smtp_single_to', '', 'combo-boolean', 'core', 'mail', NULL),
	('mail_smtp_timeout', '10', 'numberfield', 'core', 'mail', '2022-10-04 19:32:09'),
	('mail_smtp_user', '', 'textfield', 'core', 'mail', NULL),
	('manager_date_format', 'Y-m-d', 'textfield', 'core', 'manager', NULL),
	('manager_favicon_url', '', 'textfield', 'core', 'manager', NULL),
	('login_logo', '', 'textfield', 'core', 'authentication', NULL),
	('login_background_image', '', 'textfield', 'core', 'authentication', NULL),
	('manager_tooltip_delay', '2300', 'numberfield', 'core', 'manager', NULL),
	('manager_time_format', 'g:i a', 'textfield', 'core', 'manager', NULL),
	('manager_direction', 'ltr', 'textfield', 'core', 'language', NULL),
	('manager_lang_attribute', 'ru', 'textfield', 'core', 'language', '2016-08-04 07:52:19'),
	('ace.show_invisibles', '0', 'combo-boolean', 'ace', 'general', NULL),
	('manager_login_url_alternate', '', 'textfield', 'core', 'authentication', NULL),
	('manager_theme', 'default', 'modx-combo-manager-theme', 'core', 'manager', NULL),
	('manager_week_start', '0', 'numberfield', 'core', 'manager', '2022-10-04 19:32:09'),
	('modx_browser_tree_hide_files', '1', 'combo-boolean', 'core', 'manager', '2021-11-26 16:46:59'),
	('modx_browser_tree_hide_tooltips', '1', 'combo-boolean', 'core', 'manager', NULL),
	('modx_browser_default_sort', 'name', 'textfield', 'core', 'manager', NULL),
	('modx_browser_default_viewmode', 'grid', 'textfield', 'core', 'manager', NULL),
	('modx_charset', 'UTF-8', 'modx-combo-charset', 'core', 'language', NULL),
	('principal_targets', 'MODX\\Revolution\\modAccessContext,MODX\\Revolution\\modAccessResourceGroup,MODX\\Revolution\\modAccessCategory,MODX\\Revolution\\Sources\\modAccessMediaSource,MODX\\Revolution\\modAccessNamespace', 'textfield', 'core', 'authentication', '2022-10-04 19:32:09'),
	('proxy_auth_type', 'BASIC', 'textfield', 'core', 'proxy', NULL),
	('proxy_host', '', 'textfield', 'core', 'proxy', '2016-08-05 17:54:32'),
	('proxy_password', '', 'text-password', 'core', 'proxy', NULL),
	('proxy_port', '', 'numberfield', 'core', 'proxy', '2022-10-04 19:32:09'),
	('proxy_username', '', 'textfield', 'core', 'proxy', NULL),
	('password_generated_length', '8', 'numberfield', 'core', 'authentication', '2022-10-04 19:32:09'),
	('password_min_length', '8', 'numberfield', 'core', 'authentication', '2022-10-04 19:32:09'),
	('phpthumb_allow_src_above_docroot', '', 'combo-boolean', 'core', 'phpthumb', NULL),
	('phpthumb_cache_maxage', '30', 'numberfield', 'core', 'phpthumb', '2022-10-04 19:32:09'),
	('phpthumb_cache_maxsize', '100', 'numberfield', 'core', 'phpthumb', '2022-10-04 19:32:09'),
	('phpthumb_cache_maxfiles', '10000', 'numberfield', 'core', 'phpthumb', '2022-10-04 19:32:09'),
	('phpthumb_cache_source_enabled', '', 'combo-boolean', 'core', 'phpthumb', NULL),
	('phpthumb_document_root', '', 'textfield', 'core', 'phpthumb', NULL),
	('phpthumb_error_bgcolor', 'CCCCFF', 'textfield', 'core', 'phpthumb', NULL),
	('phpthumb_error_textcolor', 'FF0000', 'textfield', 'core', 'phpthumb', NULL),
	('phpthumb_error_fontsize', '1', 'numberfield', 'core', 'phpthumb', '2022-10-04 19:32:09'),
	('phpthumb_far', 'C', 'textfield', 'core', 'phpthumb', NULL),
	('phpthumb_imagemagick_path', '', 'textfield', 'core', 'phpthumb', NULL),
	('phpthumb_nohotlink_enabled', '1', 'combo-boolean', 'core', 'phpthumb', NULL),
	('phpthumb_nohotlink_erase_image', '1', 'combo-boolean', 'core', 'phpthumb', NULL),
	('phpthumb_nohotlink_valid_domains', '{http_host}', 'textfield', 'core', 'phpthumb', NULL),
	('phpthumb_nohotlink_text_message', 'Off-server thumbnailing is not allowed', 'textfield', 'core', 'phpthumb', NULL),
	('phpthumb_nooffsitelink_enabled', '', 'combo-boolean', 'core', 'phpthumb', NULL),
	('phpthumb_nooffsitelink_erase_image', '1', 'combo-boolean', 'core', 'phpthumb', NULL),
	('phpthumb_nooffsitelink_require_refer', '', 'combo-boolean', 'core', 'phpthumb', NULL),
	('phpthumb_nooffsitelink_text_message', 'Off-server linking is not allowed', 'textfield', 'core', 'phpthumb', NULL),
	('phpthumb_nooffsitelink_valid_domains', '{http_host}', 'textfield', 'core', 'phpthumb', NULL),
	('phpthumb_nooffsitelink_watermark_src', '', 'textfield', 'core', 'phpthumb', NULL),
	('phpthumb_zoomcrop', '0', 'textfield', 'core', 'phpthumb', NULL),
	('publish_default', '1', 'combo-boolean', 'core', 'site', '2016-08-04 15:32:39'),
	('manager_tooltip_enable', '1', 'combo-boolean', 'core', 'manager', NULL),
	('request_controller', 'index.php', 'textfield', 'core', 'gateway', NULL),
	('request_method_strict', '0', 'combo-boolean', 'core', 'gateway', NULL),
	('request_param_alias', 'q', 'textfield', 'core', 'gateway', NULL),
	('request_param_id', 'id', 'textfield', 'core', 'gateway', NULL),
	('mail_smtp_secure', '', 'textfield', 'core', 'mail', NULL),
	('resource_tree_node_name', 'pagetitle', 'textfield', 'core', 'manager', NULL),
	('resource_tree_node_name_fallback', 'pagetitle', 'textfield', 'core', 'manager', NULL),
	('resource_tree_node_tooltip', '', 'textfield', 'core', 'manager', NULL),
	('richtext_default', '1', 'combo-boolean', 'core', 'manager', NULL),
	('search_default', '1', 'combo-boolean', 'core', 'site', NULL),
	('server_offset_time', '0', 'numberfield', 'core', 'system', '2022-10-04 19:32:09'),
	('session_cookie_domain', '', 'textfield', 'core', 'session', NULL),
	('default_username', '(anonymous)', 'textfield', 'core', 'session', NULL),
	('anonymous_sessions', '1', 'combo-boolean', 'core', 'session', NULL),
	('session_cookie_lifetime', '604800', 'numberfield', 'core', 'session', '2022-10-04 19:32:09'),
	('session_cookie_path', '', 'textfield', 'core', 'session', NULL),
	('session_cookie_secure', '', 'combo-boolean', 'core', 'session', NULL),
	('session_cookie_httponly', '1', 'combo-boolean', 'core', 'session', NULL),
	('session_gc_maxlifetime', '604800', 'textfield', 'core', 'session', NULL),
	('session_handler_class', 'modSessionHandler', 'textfield', 'core', 'session', NULL),
	('session_name', '', 'textfield', 'core', 'session', NULL),
	('set_header', '1', 'combo-boolean', 'core', 'system', NULL),
	('send_poweredby_header', '1', 'combo-boolean', 'core', 'system', '2016-08-04 07:52:19'),
	('show_tv_categories_header', '1', 'combo-boolean', 'core', 'manager', NULL),
	('signupemail_message', '<p>Hello [[+uid]],</p>\n    <p>Here are your login details for the [[+sname]] MODX Manager:</p>\n\n    <p>\n        <strong>Username:</strong> [[+uid]]<br />\n        <strong>Password:</strong> [[+pwd]]<br />\n    </p>\n\n    <p>Once you log into the MODX Manager at [[+surl]], you can change your password.</p>\n\n    <p>Regards,<br />Site Administrator</p>', 'textarea', 'core', 'authentication', NULL),
	('site_name', 'Evgenii Petukhov CV Web Site', 'textfield', 'core', 'site', '2016-08-04 10:40:48'),
	('site_start', '1', 'numberfield', 'core', 'site', '2022-10-04 19:32:09'),
	('site_status', '1', 'combo-boolean', 'core', 'site', NULL),
	('site_unavailable_message', 'The site is currently unavailable', 'textfield', 'core', 'site', NULL),
	('site_unavailable_page', '0', 'numberfield', 'core', 'site', '2022-10-04 19:32:09'),
	('symlink_merge_fields', '1', 'combo-boolean', 'core', 'site', NULL),
	('syncsite_default', '1', 'combo-boolean', 'core', 'caching', NULL),
	('topmenu_show_descriptions', '1', 'combo-boolean', 'core', 'manager', NULL),
	('tree_default_sort', 'menuindex', 'textfield', 'core', 'manager', NULL),
	('tree_root_id', '0', 'numberfield', 'core', 'manager', NULL),
	('tvs_below_content', '0', 'combo-boolean', 'core', 'manager', NULL),
	('unauthorized_page', '1', 'numberfield', 'core', 'site', '2022-10-04 19:32:09'),
	('upload_files', 'txt,html,htm,xml,js,css,zip,gz,rar,z,tgz,tar,htaccess,mp3,mp4,aac,wav,au,wmv,avi,mpg,mpeg,pdf,doc,docx,xls,xlsx,ppt,pptx,jpg,jpeg,png,tiff,svg,svgz,gif,psd,ico,bmp,odt,ods,odp,odb,odg,odf,md,ttf,woff,eot,webp,woff2', 'textfield', 'core', 'file', '2021-11-26 16:46:59'),
	('upload_images', 'jpg,jpeg,png,gif,psd,ico,bmp,tiff,svg,svgz,webp', 'textfield', 'core', 'file', '2021-11-26 16:46:59'),
	('upload_maxsize', '2097152000', 'numberfield', 'core', 'file', '2022-10-04 19:32:09'),
	('upload_media', 'mp3,wav,au,wmv,avi,mpg,mpeg', 'textfield', 'core', 'file', NULL),
	('use_alias_path', '1', 'combo-boolean', 'core', 'furls', '2016-08-04 10:47:59'),
	('use_editor', '1', 'combo-boolean', 'core', 'editor', NULL),
	('use_multibyte', '1', 'combo-boolean', 'core', 'language', '2016-08-04 07:52:19'),
	('use_weblink_target', '', 'combo-boolean', 'core', 'site', NULL),
	('websignupemail_message', '<p>Hello [[+uid]],</p>\n\n    <p>Here are your login details for [[+sname]]:</p>\n\n    <p><strong>Username:</strong> [[+uid]]<br />\n    <strong>Password:</strong> [[+pwd]]</p>\n\n    <p>Once you log into [[+sname]] at [[+surl]], you can change your password.</p>\n\n    <p>Regards,<br />\n    Site Administrator</p>', 'textarea', 'core', 'authentication', NULL),
	('welcome_screen', '', 'combo-boolean', 'core', 'manager', '2016-08-04 07:52:52'),
	('welcome_screen_url', '//misc.modx.com/revolution/welcome.25.html ', 'textfield', 'core', 'manager', NULL),
	('welcome_action', 'welcome', 'textfield', 'core', 'manager', NULL),
	('welcome_namespace', 'core', 'textfield', 'core', 'manager', NULL),
	('which_editor', 'TinyMCE RTE', 'modx-combo-rte', 'core', 'editor', '2022-10-04 20:07:40'),
	('which_element_editor', 'Ace', 'modx-combo-rte', 'core', 'editor', '2016-08-04 10:33:54'),
	('xhtml_urls', '1', 'combo-boolean', 'core', 'site', NULL),
	('enable_gravatar', '1', 'combo-boolean', 'core', 'manager', NULL),
	('mgr_tree_icon_context', 'tree-context', 'textfield', 'core', 'manager', NULL),
	('mgr_source_icon', 'icon-folder-open-o', 'textfield', 'core', 'manager', NULL),
	('main_nav_parent', 'topnav', 'textfield', 'core', 'manager', NULL),
	('user_nav_parent', 'usernav', 'textfield', 'core', 'manager', NULL),
	('auto_isfolder', '1', 'combo-boolean', 'core', 'site', NULL),
	('manager_use_fullname', '', 'combo-boolean', 'core', 'manager', NULL),
	('parser_recurse_uncacheable', '1', 'combo-boolean', 'core', 'system', NULL),
	('preserve_menuindex', '1', 'combo-boolean', 'core', 'manager', NULL),
	('settings_version', '3.0.1-pl', 'textfield', 'core', 'system', '2022-10-04 19:32:30'),
	('settings_distro', 'traditional', 'textfield', 'core', 'system', NULL),
	('ace.fold_widgets', '1', 'combo-boolean', 'ace', 'general', NULL),
	('ace.tab_size', '4', 'textfield', 'ace', 'general', NULL),
	('ace.soft_tabs', '1', 'combo-boolean', 'ace', 'general', NULL),
	('ace.word_wrap', '', 'combo-boolean', 'ace', 'general', NULL),
	('ace.font_size', '13px', 'textfield', 'ace', 'general', NULL),
	('ace.theme', 'chrome', 'textfield', 'ace', 'general', NULL),
	('tinymcerte.headers_format', '[{"title": "Header 1", "format": "h1"},{"title": "Header 2", "format": "h2"},{"title": "Header 3", "format": "h3"},{"title": "Header 4", "format": "h4"},{"title": "Header 5", "format": "h5"},{"title": "Header 6", "format": "h6"}]', 'textarea', 'tinymcerte', 'tinymcerte.style_formats', NULL),
	('tinymcerte.toolbar2', '', 'textfield', 'tinymcerte', 'tinymcerte.toolbar', NULL),
	('tinymcerte.toolbar3', '', 'textfield', 'tinymcerte', 'tinymcerte.toolbar', NULL),
	('tinymcerte.style_formats', '[{"title": "Headers", "items": "headers_format"},{"title": "Inline", "items": "inline_format"},{"title": "Blocks", "items": "blocks_format"},{"title": "Alignment", "items": "alignment_format"}]', 'textarea', 'tinymcerte', 'tinymcerte.style_formats', NULL),
	('tinymcerte.max_height', '500', 'textfield', 'tinymcerte', 'tinymcerte.default', NULL),
	('tinymcerte.toolbar1', 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image', 'textfield', 'tinymcerte', 'tinymcerte.toolbar', NULL),
	('tinymcerte.links_across_contexts', '', 'combo-boolean', 'tinymcerte', 'tinymcerte.default', NULL),
	('tinymcerte.settings', '', 'textfield', 'tinymcerte', 'tinymcerte.default', NULL),
	('tinymcerte.enable_link_list', '1', 'combo-boolean', 'tinymcerte', 'tinymcerte.default', NULL),
	('tinymcerte.valid_elements', '', 'textfield', 'tinymcerte', 'tinymcerte.default', NULL),
	('tinymcerte.remove_script_host', '1', 'combo-boolean', 'tinymcerte', 'tinymcerte.default', NULL),
	('tinymcerte.relative_urls', '1', 'combo-boolean', 'tinymcerte', 'tinymcerte.default', NULL),
	('tinymcerte.external_config', '', 'textfield', 'tinymcerte', 'tinymcerte.default', NULL),
	('tinymcerte.skin', 'modx', 'textfield', 'tinymcerte', 'tinymcerte.default', NULL),
	('tinymcerte.image_class_list', '', 'textfield', 'tinymcerte', 'tinymcerte.default', NULL),
	('tinymcerte.content_css', '', 'textfield', 'tinymcerte', 'tinymcerte.default', NULL),
	('tinymcerte.browser_spellcheck', '', 'combo-boolean', 'tinymcerte', 'tinymcerte.default', NULL),
	('tinymcerte.link_class_list', '', 'textfield', 'tinymcerte', 'tinymcerte.default', NULL),
	('tinymcerte.paste_as_text', '', 'combo-boolean', 'tinymcerte', 'tinymcerte.default', NULL),
	('tinymcerte.object_resizing', '1', 'textfield', 'tinymcerte', 'tinymcerte.default', NULL),
	('tinymcerte.image_advtab', '1', 'combo-boolean', 'tinymcerte', 'tinymcerte.default', NULL),
	('tinymcerte.statusbar', '1', 'combo-boolean', 'tinymcerte', 'tinymcerte.default', NULL),
	('tinymcerte.menubar', 'file edit insert view format table tools', 'textfield', 'tinymcerte', 'tinymcerte.default', NULL),
	('tinymcerte.plugins', 'autoresize advlist autolink lists charmap print preview anchor visualblocks searchreplace code fullscreen insertdatetime media table paste modxlink modximage', 'textfield', 'tinymcerte', 'tinymcerte.default', NULL),
	('automatic_template_assignment', 'parent', 'textfield', 'core', 'site', NULL),
	('use_context_resource_table', '1', 'combo-boolean', 'core', 'caching', NULL),
	('cache_resource_clear_partial', '0', 'combo-boolean', 'core', 'caching', NULL),
	('default_media_source_type', 'MODX\\Revolution\\Sources\\modFileMediaSource', 'modx-combo-source-type', 'core', 'manager', '2022-10-04 19:32:09'),
	('log_deprecated', '1', 'combo-boolean', 'core', 'system', NULL),
	('mail_smtp_autotls', '1', 'combo-boolean', 'core', 'mail', NULL),
	('session_cookie_samesite', '', 'textfield', 'core', 'session', NULL),
	('static_elements_automate_templates', '0', 'combo-boolean', 'core', 'static_elements', NULL),
	('static_elements_automate_tvs', '0', 'combo-boolean', 'core', 'static_elements', NULL),
	('static_elements_automate_chunks', '0', 'combo-boolean', 'core', 'static_elements', NULL),
	('static_elements_automate_snippets', '0', 'combo-boolean', 'core', 'static_elements', NULL),
	('static_elements_automate_plugins', '0', 'combo-boolean', 'core', 'static_elements', NULL),
	('static_elements_default_mediasource', '0', 'modx-combo-source', 'core', 'static_elements', NULL),
	('static_elements_default_category', '0', 'modx-combo-category', 'core', 'static_elements', NULL),
	('static_elements_basepath', '', 'textfield', 'core', 'static_elements', NULL),
	('resource_static_allow_absolute', '0', 'combo-boolean', 'core', 'static_resources', NULL),
	('resource_static_path', '{assets_path}', 'textfield', 'core', 'static_resources', NULL),
	('upload_check_exists', '1', 'combo-boolean', 'core', 'file', NULL),
	('passwordless_activated', '', 'combo-boolean', 'core', 'authentication', NULL),
	('log_snippet_not_found', '1', 'combo-boolean', 'core', 'site', NULL),
	('error_log_filename', 'error.log', 'textfield', 'core', 'system', NULL),
	('error_log_filepath', '', 'textfield', 'core', 'system', NULL),
	('access_policies_version', '1.0', 'textfield', 'core', 'system', NULL),
	('passwordless_expiration', '3600', 'textfield', 'core', 'authentication', NULL),
	('static_elements_html_extension', '.tpl', 'textfield', 'core', 'static_elements', NULL),
	('ace.snippets', '', 'textarea', 'ace', 'general', NULL),
	('ace.height', '', 'textfield', 'ace', 'general', NULL),
	('ace.grow', '', 'textfield', 'ace', 'general', NULL),
	('ace.html_elements_mime', '', 'textfield', 'ace', 'general', NULL),
	('tinymcerte.inline_format', '[{"title": "Bold", "icon": "bold", "format": "bold"},{"title": "Italic", "icon": "italic", "format": "italic"},{"title": "Underline", "icon": "underline", "format": "underline"},{"title": "Strikethrough", "icon": "strike-through", "format": "strikethrough"},{"title": "Superscript", "icon": "superscript", "format": "superscript"},{"title": "Subscript", "icon": "subscript", "format": "subscript"},{"title": "Code", "icon": "sourcecode", "format": "code"}]', 'textarea', 'tinymcerte', 'tinymcerte.style_formats', NULL),
	('tinymcerte.blocks_format', '[{"title": "Paragraph", "format": "p"},{"title": "Blockquote", "format": "blockquote"},{"title": "Div", "format": "div"},{"title": "Pre", "format": "pre"}]', 'textarea', 'tinymcerte', 'tinymcerte.style_formats', NULL),
	('tinymcerte.alignment_format', '[{"title": "Left", "icon": "align-left", "format": "alignleft"},{"title": "Center", "icon": "align-center", "format": "aligncenter"},{"title": "Right", "icon": "align-right", "format": "alignright"},{"title": "Justify", "icon": "align-justify", "format": "alignjustify"}]', 'textarea', 'tinymcerte', 'tinymcerte.style_formats', NULL),
	('tinymcerte.style_formats_merge', '', 'combo-boolean', 'tinymcerte', 'tinymcerte.style_formats', NULL);
/*!40000 ALTER TABLE `modx_system_settings` ENABLE KEYS */;

-- Dumping structure for table petukhov.modx_transport_packages
CREATE TABLE IF NOT EXISTS `modx_transport_packages` (
  `signature` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `updated` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `installed` datetime DEFAULT NULL,
  `state` tinyint(1) unsigned NOT NULL DEFAULT 1,
  `workspace` int(10) unsigned NOT NULL DEFAULT 0,
  `provider` int(10) unsigned NOT NULL DEFAULT 0,
  `disabled` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `source` tinytext DEFAULT NULL,
  `manifest` text DEFAULT NULL,
  `attributes` mediumtext DEFAULT NULL,
  `package_name` varchar(255) NOT NULL,
  `metadata` text DEFAULT NULL,
  `version_major` smallint(5) unsigned NOT NULL DEFAULT 0,
  `version_minor` smallint(5) unsigned NOT NULL DEFAULT 0,
  `version_patch` smallint(5) unsigned NOT NULL DEFAULT 0,
  `release` varchar(100) NOT NULL DEFAULT '',
  `release_index` smallint(5) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`signature`),
  KEY `workspace` (`workspace`),
  KEY `provider` (`provider`),
  KEY `disabled` (`disabled`),
  KEY `package_name` (`package_name`),
  KEY `version_major` (`version_major`),
  KEY `version_minor` (`version_minor`),
  KEY `version_patch` (`version_patch`),
  KEY `release` (`release`),
  KEY `release_index` (`release_index`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table petukhov.modx_transport_packages: 6 rows
/*!40000 ALTER TABLE `modx_transport_packages` DISABLE KEYS */;
INSERT INTO `modx_transport_packages` (`signature`, `created`, `updated`, `installed`, `state`, `workspace`, `provider`, `disabled`, `source`, `manifest`, `attributes`, `package_name`, `metadata`, `version_major`, `version_minor`, `version_patch`, `release`, `release_index`) VALUES
	('tinymcerte-2.0.9-pl', '2022-10-04 22:07:24', '2022-10-04 20:07:40', '2022-10-04 22:07:40', 0, 1, 1, 0, 'tinymcerte-2.0.9-pl.transport.zip', NULL, 'a:9:{s:7:"license";s:15215:"GNU GENERAL PUBLIC LICENSE\n   Version 2, June 1991\n--------------------------\n\nCopyright (C) 1989, 1991 Free Software Foundation, Inc.\n59 Temple Place, Suite 330, Boston, MA  02111-1307  USA\n\nEveryone is permitted to copy and distribute verbatim copies\nof this license document, but changing it is not allowed.\n\nPreamble\n--------\n\n  The licenses for most software are designed to take away your\nfreedom to share and change it.  By contrast, the GNU General Public\nLicense is intended to guarantee your freedom to share and change free\nsoftware--to make sure the software is free for all its users.  This\nGeneral Public License applies to most of the Free Software\nFoundation\'s software and to any other program whose authors commit to\nusing it.  (Some other Free Software Foundation software is covered by\nthe GNU Library General Public License instead.)  You can apply it to\nyour programs, too.\n\n  When we speak of free software, we are referring to freedom, not\nprice.  Our General Public Licenses are designed to make sure that you\nhave the freedom to distribute copies of free software (and charge for\nthis service if you wish), that you receive source code or can get it\nif you want it, that you can change the software or use pieces of it\nin new free programs; and that you know you can do these things.\n\n  To protect your rights, we need to make restrictions that forbid\nanyone to deny you these rights or to ask you to surrender the rights.\nThese restrictions translate to certain responsibilities for you if you\ndistribute copies of the software, or if you modify it.\n\n  For example, if you distribute copies of such a program, whether\ngratis or for a fee, you must give the recipients all the rights that\nyou have.  You must make sure that they, too, receive or can get the\nsource code.  And you must show them these terms so they know their\nrights.\n\n  We protect your rights with two steps: (1) copyright the software, and\n(2) offer you this license which gives you legal permission to copy,\ndistribute and/or modify the software.\n\n  Also, for each author\'s protection and ours, we want to make certain\nthat everyone understands that there is no warranty for this free\nsoftware.  If the software is modified by someone else and passed on, we\nwant its recipients to know that what they have is not the original, so\nthat any problems introduced by others will not reflect on the original\nauthors\' reputations.\n\n  Finally, any free program is threatened constantly by software\npatents.  We wish to avoid the danger that redistributors of a free\nprogram will individually obtain patent licenses, in effect making the\nprogram proprietary.  To prevent this, we have made it clear that any\npatent must be licensed for everyone\'s free use or not licensed at all.\n\n  The precise terms and conditions for copying, distribution and\nmodification follow.\n\n\nGNU GENERAL PUBLIC LICENSE\nTERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION\n---------------------------------------------------------------\n\n  0. This License applies to any program or other work which contains\na notice placed by the copyright holder saying it may be distributed\nunder the terms of this General Public License.  The "Program", below,\nrefers to any such program or work, and a "work based on the Program"\nmeans either the Program or any derivative work under copyright law:\nthat is to say, a work containing the Program or a portion of it,\neither verbatim or with modifications and/or translated into another\nlanguage.  (Hereinafter, translation is included without limitation in\nthe term "modification".)  Each licensee is addressed as "you".\n\nActivities other than copying, distribution and modification are not\ncovered by this License; they are outside its scope.  The act of\nrunning the Program is not restricted, and the output from the Program\nis covered only if its contents constitute a work based on the\nProgram (independent of having been made by running the Program).\nWhether that is true depends on what the Program does.\n\n  1. You may copy and distribute verbatim copies of the Program\'s\nsource code as you receive it, in any medium, provided that you\nconspicuously and appropriately publish on each copy an appropriate\ncopyright notice and disclaimer of warranty; keep intact all the\nnotices that refer to this License and to the absence of any warranty;\nand give any other recipients of the Program a copy of this License\nalong with the Program.\n\nYou may charge a fee for the physical act of transferring a copy, and\nyou may at your option offer warranty protection in exchange for a fee.\n\n  2. You may modify your copy or copies of the Program or any portion\nof it, thus forming a work based on the Program, and copy and\ndistribute such modifications or work under the terms of Section 1\nabove, provided that you also meet all of these conditions:\n\n    a) You must cause the modified files to carry prominent notices\n    stating that you changed the files and the date of any change.\n\n    b) You must cause any work that you distribute or publish, that in\n    whole or in part contains or is derived from the Program or any\n    part thereof, to be licensed as a whole at no charge to all third\n    parties under the terms of this License.\n\n    c) If the modified program normally reads commands interactively\n    when run, you must cause it, when started running for such\n    interactive use in the most ordinary way, to print or display an\n    announcement including an appropriate copyright notice and a\n    notice that there is no warranty (or else, saying that you provide\n    a warranty) and that users may redistribute the program under\n    these conditions, and telling the user how to view a copy of this\n    License.  (Exception: if the Program itself is interactive but\n    does not normally print such an announcement, your work based on\n    the Program is not required to print an announcement.)\n\nThese requirements apply to the modified work as a whole.  If\nidentifiable sections of that work are not derived from the Program,\nand can be reasonably considered independent and separate works in\nthemselves, then this License, and its terms, do not apply to those\nsections when you distribute them as separate works.  But when you\ndistribute the same sections as part of a whole which is a work based\non the Program, the distribution of the whole must be on the terms of\nthis License, whose permissions for other licensees extend to the\nentire whole, and thus to each and every part regardless of who wrote it.\n\nThus, it is not the intent of this section to claim rights or contest\nyour rights to work written entirely by you; rather, the intent is to\nexercise the right to control the distribution of derivative or\ncollective works based on the Program.\n\nIn addition, mere aggregation of another work not based on the Program\nwith the Program (or with a work based on the Program) on a volume of\na storage or distribution medium does not bring the other work under\nthe scope of this License.\n\n  3. You may copy and distribute the Program (or a work based on it,\nunder Section 2) in object code or executable form under the terms of\nSections 1 and 2 above provided that you also do one of the following:\n\n    a) Accompany it with the complete corresponding machine-readable\n    source code, which must be distributed under the terms of Sections\n    1 and 2 above on a medium customarily used for software interchange; or,\n\n    b) Accompany it with a written offer, valid for at least three\n    years, to give any third party, for a charge no more than your\n    cost of physically performing source distribution, a complete\n    machine-readable copy of the corresponding source code, to be\n    distributed under the terms of Sections 1 and 2 above on a medium\n    customarily used for software interchange; or,\n\n    c) Accompany it with the information you received as to the offer\n    to distribute corresponding source code.  (This alternative is\n    allowed only for noncommercial distribution and only if you\n    received the program in object code or executable form with such\n    an offer, in accord with Subsection b above.)\n\nThe source code for a work means the preferred form of the work for\nmaking modifications to it.  For an executable work, complete source\ncode means all the source code for all modules it contains, plus any\nassociated interface definition files, plus the scripts used to\ncontrol compilation and installation of the executable.  However, as a\nspecial exception, the source code distributed need not include\nanything that is normally distributed (in either source or binary\nform) with the major components (compiler, kernel, and so on) of the\noperating system on which the executable runs, unless that component\nitself accompanies the executable.\n\nIf distribution of executable or object code is made by offering\naccess to copy from a designated place, then offering equivalent\naccess to copy the source code from the same place counts as\ndistribution of the source code, even though third parties are not\ncompelled to copy the source along with the object code.\n\n  4. You may not copy, modify, sublicense, or distribute the Program\nexcept as expressly provided under this License.  Any attempt\notherwise to copy, modify, sublicense or distribute the Program is\nvoid, and will automatically terminate your rights under this License.\nHowever, parties who have received copies, or rights, from you under\nthis License will not have their licenses terminated so long as such\nparties remain in full compliance.\n\n  5. You are not required to accept this License, since you have not\nsigned it.  However, nothing else grants you permission to modify or\ndistribute the Program or its derivative works.  These actions are\nprohibited by law if you do not accept this License.  Therefore, by\nmodifying or distributing the Program (or any work based on the\nProgram), you indicate your acceptance of this License to do so, and\nall its terms and conditions for copying, distributing or modifying\nthe Program or works based on it.\n\n  6. Each time you redistribute the Program (or any work based on the\nProgram), the recipient automatically receives a license from the\noriginal licensor to copy, distribute or modify the Program subject to\nthese terms and conditions.  You may not impose any further\nrestrictions on the recipients\' exercise of the rights granted herein.\nYou are not responsible for enforcing compliance by third parties to\nthis License.\n\n  7. If, as a consequence of a court judgment or allegation of patent\ninfringement or for any other reason (not limited to patent issues),\nconditions are imposed on you (whether by court order, agreement or\notherwise) that contradict the conditions of this License, they do not\nexcuse you from the conditions of this License.  If you cannot\ndistribute so as to satisfy simultaneously your obligations under this\nLicense and any other pertinent obligations, then as a consequence you\nmay not distribute the Program at all.  For example, if a patent\nlicense would not permit royalty-free redistribution of the Program by\nall those who receive copies directly or indirectly through you, then\nthe only way you could satisfy both it and this License would be to\nrefrain entirely from distribution of the Program.\n\nIf any portion of this section is held invalid or unenforceable under\nany particular circumstance, the balance of the section is intended to\napply and the section as a whole is intended to apply in other\ncircumstances.\n\nIt is not the purpose of this section to induce you to infringe any\npatents or other property right claims or to contest validity of any\nsuch claims; this section has the sole purpose of protecting the\nintegrity of the free software distribution system, which is\nimplemented by public license practices.  Many people have made\ngenerous contributions to the wide range of software distributed\nthrough that system in reliance on consistent application of that\nsystem; it is up to the author/donor to decide if he or she is willing\nto distribute software through any other system and a licensee cannot\nimpose that choice.\n\nThis section is intended to make thoroughly clear what is believed to\nbe a consequence of the rest of this License.\n\n  8. If the distribution and/or use of the Program is restricted in\ncertain countries either by patents or by copyrighted interfaces, the\noriginal copyright holder who places the Program under this License\nmay add an explicit geographical distribution limitation excluding\nthose countries, so that distribution is permitted only in or among\ncountries not thus excluded.  In such case, this License incorporates\nthe limitation as if written in the body of this License.\n\n  9. The Free Software Foundation may publish revised and/or new versions\nof the General Public License from time to time.  Such new versions will\nbe similar in spirit to the present version, but may differ in detail to\naddress new problems or concerns.\n\nEach version is given a distinguishing version number.  If the Program\nspecifies a version number of this License which applies to it and "any\nlater version", you have the option of following the terms and conditions\neither of that version or of any later version published by the Free\nSoftware Foundation.  If the Program does not specify a version number of\nthis License, you may choose any version ever published by the Free Software\nFoundation.\n\n  10. If you wish to incorporate parts of the Program into other free\nprograms whose distribution conditions are different, write to the author\nto ask for permission.  For software which is copyrighted by the Free\nSoftware Foundation, write to the Free Software Foundation; we sometimes\nmake exceptions for this.  Our decision will be guided by the two goals\nof preserving the free status of all derivatives of our free software and\nof promoting the sharing and reuse of software generally.\n\nNO WARRANTY\n-----------\n\n  11. BECAUSE THE PROGRAM IS LICENSED FREE OF CHARGE, THERE IS NO WARRANTY\nFOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW.  EXCEPT WHEN\nOTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES\nPROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED\nOR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF\nMERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  THE ENTIRE RISK AS\nTO THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU.  SHOULD THE\nPROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING,\nREPAIR OR CORRECTION.\n\n  12. IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING\nWILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY AND/OR\nREDISTRIBUTE THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES,\nINCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING\nOUT OF THE USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED\nTO LOSS OF DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY\nYOU OR THIRD PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER\nPROGRAMS), EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE\nPOSSIBILITY OF SUCH DAMAGES.\n\n---------------------------\nEND OF TERMS AND CONDITIONS\n";s:6:"readme";s:639:"# TinyMCE Rich Text Editor\n\nTinyMCE 5 for MODX Revolution.\n\n- Author: John Peca <john@modx.com>\n- Upgrade to TinyMCE 5: Thomas Jakobi <office@treehillstudio.com>\n- License: GNU GPLv2\n\n## Features\n\nTinyMCE is a platform independent web based Javascript HTML WYSIWYG editor. It\nallows non-technical users to format content without knowing how to code. This\nrelease was done as a companion project for the https://a11y.modx.com to provide\nan accessible RTE. It is based on the TinyMCE 5 code base.\n\n## Installation\n\nMODX Package Management\n\n## Usage\n\nInstall via package manager.\n\n## GitHub Repository\n\nhttps://github.com/modxcms/tinymce-rte\n";s:9:"changelog";s:4857:"# Changelog\n\nAll notable changes to this project will be documented in this file.\n\nThe format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),\nand this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).\n\n## [2.0.9] - 2022-09-28\n\n### Fixed\n\n- Fix `max_height` system setting not casted right\n\n## [2.0.8] - 2022-08-01\n\n### Added\n\n- Add a default `max_height` system setting to restrict the height of the editor with an enabled `autoresize` plugin\n\n### Fixed\n\n- Fill the TinyMCE `document_base_url` with the `site_url` context setting instead of the `site_url` system setting [#121]\n\n## [2.0.7] - 2022-03-18\n\n### Fixed\n\n- Avoid and log an invalid TinyMCE configuration\n- Get the right manager language in MODX 3.x\n\n## [2.0.6] - 2022-02-16\n\n### Added\n\n- Change `lightgray` skin system setting to `modx` during an update\n- Add `autoresize` plugin per default\n\n## [2.0.5] - 2021-12-27\n\n### Added\n\n- Unescape escaped regex strings (i.e. to allow javascript regex filters in an external config) [#117] \n\n### Fix\n\n- Escape MySQL reserved word rank [#119]\n\n## [2.0.4] - 2021-12-03\n\n### Added\n\n- Load the TinyMCE configuration even if the resource is not using a rich text editor\n- Allow drop of MODX elements to the editor content in richtext template variables\n\n### Fix\n\n- Fix an uncaught type error when the current resource has richtext disabled and uses ContentBlocks\n\n## [2.0.3] - 2021-10-01\n\n### Fix\n\n- Fix setting the link text after selecting a resource\n\n## [2.0.2] - 2021-09-30\n\n### Changed\n\n- Update TinyMCE to 5.9.2\n- Restored compatibility for PHP 7.1 and less\n\n## [2.0.1] - 2021-05-14\n\n### Changed\n\n- Update TinyMCE to 5.8.0\n- Improve the configuration output in the manager html code\n\n### Fixed\n\n- Compatibility with moregallery and Collections\n\n## [2.0.0] - 2021-03-19\n\n### Added\n\n- MODX skintool.json for http://skin.tiny.cloud/t5/\n- MODX 3 compatibility\n- link_list_enable system setting\n\n### Changed\n\n- Upgrade TinyMCE to 5\n- Refactored modxlink TinyMCE plugin to use the nested link_list option\n- Refactored modximage TinyMCE plugin\n- Recursive merge the external config with the config\n- Remove the deprecated file_browser_callback and use the file_picker_callback \n- Allow direct JSON based style_formats items\n\n## [1.4.0] - 2020-09-11\n\n### Added\n\n- Build the modx skin with the internal tinymce grunt workflow\n\n### Changed\n\n- Extend/Fix the modx skin styles\n- Fix an issue with the table tool buttons\n\n## [1.3.4] - 2020-08-12\n\n### Added\n\n- The modx skin extends the lightgray skin, that way the css changes in the lightgray skin are available after a TinyMCE update\n\n### Changed\n\n- Some lexicon changes/improvements\n- Upgrade TinyMCE to 4.9.11\n\n### Removed\n\n- Removed some unnecessary files\n\n## [1.3.3] - 2020-02-04\n\n### Changed\n\n- Bugfix for not using full width when the editor is moved to a new tab [#86]\n- Upgrade TinyMCE to 4.9.7\n\n## [1.3.2] - 2019-06-13\n\n### Changed\n\n- Bugfix for showing only an english user interface\n\n## [1.3.1] - 2019-06-05\n\n### Added\n\n- Added field displaying resource pagetitle of MODX link [#83]\n- Added image_caption option for TinyMCE [#60]\n\n### Changed\n\n- Expanding the locale list [#82]\n- Get settings from a JSON encoded array in tinymcerte.settings system setting\n- Make the entity_encoding configurable [#79]\n- Upgrade TinyMCE to 4.9.4\n\n## [1.3.0] - 2019-05-22\n\n### Added\n\n- Manage TinyMCE release download by npm\n- Add Gruntfile.js that copies the current release of TinyMCE to the corresponding folders\n- Add version info to the registered assets\n- Adding Russian translation\n\n### Changed\n\n- Upgrade TinyMCE to 4.8.3\n\n## [1.2.1] - 2017-12-16\n\n### Added\n\n- Added language strings for the system settings added in 1.2.0\n\n### Changed\n\n- Escaped special HTML chars in the modxlink plugin\n- Fixing \'Media browser does not close when clicking on close\'\n\n## [1.2.0] - 2017-05-21\n\n### Added\n\n- Added `relative_urls` & `remove_script_host` settings\n- Added system setting to define \'valid_elements\'\n- Added \'links_across_contexts\' setting to limit links to the current context resources\n- Added support for configured default Media Source in context settings\n- CMPs can now pass any TinyMCE configuration property using the `OnRichTextEditorInit` system event\n\n### Changed\n\n- Plugin now makes use of `modManagerController::addJavascript` instead of `modX::regClientStartupScript`\n- Upgraded to TinyMCE 4.5.7\n\n## [1.1.1] - 2016-01-20\n\n### Added\n\n- Add tel: prefix\n- Add modximage - left/right image positioning\n- Add modx skin (Credits goes to fourroses666)\n- Add skin system setting\n\n### Changed\n\n- Allow base path parsing in the external_config system setting\n- Sync tinymce and textarea\n\n## [1.1.0] - 2015-07-13\n\n### Added\n\n- Add autocomplete search for links\n- Add external config\n- Support for link classes\n\n## [1.0.0] - 2015-02-23\n\n### Added\n\n- Initial release\n";s:8:"requires";a:2:{s:3:"php";s:5:">=5.6";s:4:"modx";s:5:">=2.7";}s:9:"signature";s:19:"tinymcerte-2.0.9-pl";s:6:"action";s:26:"Workspace/Packages/Install";s:8:"register";s:3:"mgr";s:5:"topic";s:47:"/workspace/package/install/tinymcerte-2.0.9-pl/";s:14:"package_action";i:0;}', 'TinyMCE Rich Text Editor', 'a:38:{s:2:"id";s:24:"63341ec136f70a737b6fdaf2";s:7:"package";s:24:"54eb652ddc532f725a02bf64";s:12:"display_name";s:19:"tinymcerte-2.0.9-pl";s:4:"name";s:24:"TinyMCE Rich Text Editor";s:7:"version";s:5:"2.0.9";s:13:"version_major";s:1:"2";s:13:"version_minor";s:1:"0";s:13:"version_patch";s:1:"9";s:7:"release";s:2:"pl";s:8:"vrelease";s:2:"pl";s:14:"vrelease_index";s:0:"";s:6:"author";s:4:"jako";s:11:"description";s:340:"<p>TinyMCE is a platform independent web based Javascript HTML WYSIWYG editor. It allows non-technical users to format content without knowing how to code. This release was done as a companion project for the <a href="https://a11y.modx.com">https://a11y.modx.com</a> to provide an accessible RTE. It is based on the TinyMCE 5 code base.</p>";s:12:"instructions";s:46:"Download and install via MODX package manager.";s:9:"changelog";s:50:"- Fix `max_height` system setting not casted right";s:9:"createdon";s:24:"2022-09-28T10:15:29+0000";s:9:"createdby";s:4:"jako";s:8:"editedon";s:24:"2022-10-04T19:50:46+0000";s:10:"releasedon";s:24:"2022-09-28T10:15:29+0000";s:9:"downloads";s:6:"136295";s:8:"approved";s:4:"true";s:7:"audited";s:5:"false";s:8:"featured";s:5:"false";s:10:"deprecated";s:5:"false";s:7:"license";s:5:"GPLv2";s:7:"smf_url";s:0:"";s:10:"repository";s:24:"4d4c3fa6b2b0830da9000001";s:8:"supports";s:3:"2.7";s:8:"location";s:61:"https://modx.com/extras/download/?id=63345720cf552c1ff72c9b02";s:9:"signature";s:19:"tinymcerte-2.0.9-pl";s:11:"supports_db";s:5:"mysql";s:16:"minimum_supports";s:3:"2.7";s:9:"breaks_at";s:8:"10000000";s:10:"screenshot";s:105:"http://modx.s3.amazonaws.com/extras/54eb652ddc532f725a02bf64/ss-Bildschirmfoto 2021-12-03 um 20.14.25.png";s:4:"file";a:7:{s:2:"id";s:24:"63345720cf552c1ff72c9b02";s:7:"version";s:24:"63341ec136f70a737b6fdaf2";s:8:"filename";s:33:"tinymcerte-2.0.9-pl.transport.zip";s:9:"downloads";s:3:"469";s:6:"lastip";s:14:"109.62.185.224";s:9:"transport";s:4:"true";s:8:"location";s:61:"https://modx.com/extras/download/?id=63345720cf552c1ff72c9b02";}s:17:"package-signature";s:19:"tinymcerte-2.0.9-pl";s:10:"categories";s:15:"richtexteditors";s:4:"tags";s:0:"";}', 2, 0, 9, 'pl', 0),
	('translit-1.0.0-beta', '2016-08-04 07:46:06', '2022-10-04 16:40:07', '2016-08-04 07:46:16', 0, 1, 0, 0, 'translit-1.0.0-beta.transport.zip', NULL, 'a:2:{s:7:"license";s:15218:"GNU GENERAL PUBLIC LICENSE\n   Version 2, June 1991\n--------------------------\n\nCopyright (C) 1989, 1991 Free Software Foundation, Inc.\n59 Temple Place, Suite 330, Boston, MA  02111-1307  USA\n\nEveryone is permitted to copy and distribute verbatim copies\nof this license document, but changing it is not allowed.\n\nPreamble\n--------\n\n  The licenses for most software are designed to take away your\nfreedom to share and change it.  By contrast, the GNU General Public\nLicense is intended to guarantee your freedom to share and change free\nsoftware--to make sure the software is free for all its users.  This\nGeneral Public License applies to most of the Free Software\nFoundation\'s software and to any other program whose authors commit to\nusing it.  (Some other Free Software Foundation software is covered by\nthe GNU Library General Public License instead.)  You can apply it to\nyour programs, too.\n\n  When we speak of free software, we are referring to freedom, not\nprice.  Our General Public Licenses are designed to make sure that you\nhave the freedom to distribute copies of free software (and charge for\nthis service if you wish), that you receive source code or can get it\nif you want it, that you can change the software or use pieces of it\nin new free programs; and that you know you can do these things.\n\n  To protect your rights, we need to make restrictions that forbid\nanyone to deny you these rights or to ask you to surrender the rights.\nThese restrictions translate to certain responsibilities for you if you\ndistribute copies of the software, or if you modify it.\n\n  For example, if you distribute copies of such a program, whether\ngratis or for a fee, you must give the recipients all the rights that\nyou have.  You must make sure that they, too, receive or can get the\nsource code.  And you must show them these terms so they know their\nrights.\n\n  We protect your rights with two steps: (1) copyright the software, and\n(2) offer you this license which gives you legal permission to copy,\ndistribute and/or modify the software.\n\n  Also, for each author\'s protection and ours, we want to make certain\nthat everyone understands that there is no warranty for this free\nsoftware.  If the software is modified by someone else and passed on, we\nwant its recipients to know that what they have is not the original, so\nthat any problems introduced by others will not reflect on the original\nauthors\' reputations.\n\n  Finally, any free program is threatened constantly by software\npatents.  We wish to avoid the danger that redistributors of a free\nprogram will individually obtain patent licenses, in effect making the\nprogram proprietary.  To prevent this, we have made it clear that any\npatent must be licensed for everyone\'s free use or not licensed at all.\n\n  The precise terms and conditions for copying, distribution and\nmodification follow.\n\n\nGNU GENERAL PUBLIC LICENSE\nTERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION\n---------------------------------------------------------------\n\n  0. This License applies to any program or other work which contains\na notice placed by the copyright holder saying it may be distributed\nunder the terms of this General Public License.  The "Program", below,\nrefers to any such program or work, and a "work based on the Program"\nmeans either the Program or any derivative work under copyright law:\nthat is to say, a work containing the Program or a portion of it,\neither verbatim or with modifications and/or translated into another\nlanguage.  (Hereinafter, translation is included without limitation in\nthe term "modification".)  Each licensee is addressed as "you".\n\nActivities other than copying, distribution and modification are not\ncovered by this License; they are outside its scope.  The act of\nrunning the Program is not restricted, and the output from the Program\nis covered only if its contents constitute a work based on the\nProgram (independent of having been made by running the Program).\nWhether that is true depends on what the Program does.\n\n  1. You may copy and distribute verbatim copies of the Program\'s\nsource code as you receive it, in any medium, provided that you\nconspicuously and appropriately publish on each copy an appropriate\ncopyright notice and disclaimer of warranty; keep intact all the\nnotices that refer to this License and to the absence of any warranty;\nand give any other recipients of the Program a copy of this License\nalong with the Program.\n\nYou may charge a fee for the physical act of transferring a copy, and\nyou may at your option offer warranty protection in exchange for a fee.\n\n  2. You may modify your copy or copies of the Program or any portion\nof it, thus forming a work based on the Program, and copy and\ndistribute such modifications or work under the terms of Section 1\nabove, provided that you also meet all of these conditions:\n\n    a) You must cause the modified files to carry prominent notices\n    stating that you changed the files and the date of any change.\n\n    b) You must cause any work that you distribute or publish, that in\n    whole or in part contains or is derived from the Program or any\n    part thereof, to be licensed as a whole at no charge to all third\n    parties under the terms of this License.\n\n    c) If the modified program normally reads commands interactively\n    when run, you must cause it, when started running for such\n    interactive use in the most ordinary way, to print or display an\n    announcement including an appropriate copyright notice and a\n    notice that there is no warranty (or else, saying that you provide\n    a warranty) and that users may redistribute the program under\n    these conditions, and telling the user how to view a copy of this\n    License.  (Exception: if the Program itself is interactive but\n    does not normally print such an announcement, your work based on\n    the Program is not required to print an announcement.)\n\nThese requirements apply to the modified work as a whole.  If\nidentifiable sections of that work are not derived from the Program,\nand can be reasonably considered independent and separate works in\nthemselves, then this License, and its terms, do not apply to those\nsections when you distribute them as separate works.  But when you\ndistribute the same sections as part of a whole which is a work based\non the Program, the distribution of the whole must be on the terms of\nthis License, whose permissions for other licensees extend to the\nentire whole, and thus to each and every part regardless of who wrote it.\n\nThus, it is not the intent of this section to claim rights or contest\nyour rights to work written entirely by you; rather, the intent is to\nexercise the right to control the distribution of derivative or\ncollective works based on the Program.\n\nIn addition, mere aggregation of another work not based on the Program\nwith the Program (or with a work based on the Program) on a volume of\na storage or distribution medium does not bring the other work under\nthe scope of this License.\n\n  3. You may copy and distribute the Program (or a work based on it,\nunder Section 2) in object code or executable form under the terms of\nSections 1 and 2 above provided that you also do one of the following:\n\n    a) Accompany it with the complete corresponding machine-readable\n    source code, which must be distributed under the terms of Sections\n    1 and 2 above on a medium customarily used for software interchange; or,\n\n    b) Accompany it with a written offer, valid for at least three\n    years, to give any third party, for a charge no more than your\n    cost of physically performing source distribution, a complete\n    machine-readable copy of the corresponding source code, to be\n    distributed under the terms of Sections 1 and 2 above on a medium\n    customarily used for software interchange; or,\n\n    c) Accompany it with the information you received as to the offer\n    to distribute corresponding source code.  (This alternative is\n    allowed only for noncommercial distribution and only if you\n    received the program in object code or executable form with such\n    an offer, in accord with Subsection b above.)\n\nThe source code for a work means the preferred form of the work for\nmaking modifications to it.  For an executable work, complete source\ncode means all the source code for all modules it contains, plus any\nassociated interface definition files, plus the scripts used to\ncontrol compilation and installation of the executable.  However, as a\nspecial exception, the source code distributed need not include\nanything that is normally distributed (in either source or binary\nform) with the major components (compiler, kernel, and so on) of the\noperating system on which the executable runs, unless that component\nitself accompanies the executable.\n\nIf distribution of executable or object code is made by offering\naccess to copy from a designated place, then offering equivalent\naccess to copy the source code from the same place counts as\ndistribution of the source code, even though third parties are not\ncompelled to copy the source along with the object code.\n\n  4. You may not copy, modify, sublicense, or distribute the Program\nexcept as expressly provided under this License.  Any attempt\notherwise to copy, modify, sublicense or distribute the Program is\nvoid, and will automatically terminate your rights under this License.\nHowever, parties who have received copies, or rights, from you under\nthis License will not have their licenses terminated so long as such\nparties remain in full compliance.\n\n  5. You are not required to accept this License, since you have not\nsigned it.  However, nothing else grants you permission to modify or\ndistribute the Program or its derivative works.  These actions are\nprohibited by law if you do not accept this License.  Therefore, by\nmodifying or distributing the Program (or any work based on the\nProgram), you indicate your acceptance of this License to do so, and\nall its terms and conditions for copying, distributing or modifying\nthe Program or works based on it.\n\n  6. Each time you redistribute the Program (or any work based on the\nProgram), the recipient automatically receives a license from the\noriginal licensor to copy, distribute or modify the Program subject to\nthese terms and conditions.  You may not impose any further\nrestrictions on the recipients\' exercise of the rights granted herein.\nYou are not responsible for enforcing compliance by third parties to\nthis License.\n\n  7. If, as a consequence of a court judgment or allegation of patent\ninfringement or for any other reason (not limited to patent issues),\nconditions are imposed on you (whether by court order, agreement or\notherwise) that contradict the conditions of this License, they do not\nexcuse you from the conditions of this License.  If you cannot\ndistribute so as to satisfy simultaneously your obligations under this\nLicense and any other pertinent obligations, then as a consequence you\nmay not distribute the Program at all.  For example, if a patent\nlicense would not permit royalty-free redistribution of the Program by\nall those who receive copies directly or indirectly through you, then\nthe only way you could satisfy both it and this License would be to\nrefrain entirely from distribution of the Program.\n\nIf any portion of this section is held invalid or unenforceable under\nany particular circumstance, the balance of the section is intended to\napply and the section as a whole is intended to apply in other\ncircumstances.\n\nIt is not the purpose of this section to induce you to infringe any\npatents or other property right claims or to contest validity of any\nsuch claims; this section has the sole purpose of protecting the\nintegrity of the free software distribution system, which is\nimplemented by public license practices.  Many people have made\ngenerous contributions to the wide range of software distributed\nthrough that system in reliance on consistent application of that\nsystem; it is up to the author/donor to decide if he or she is willing\nto distribute software through any other system and a licensee cannot\nimpose that choice.\n\nThis section is intended to make thoroughly clear what is believed to\nbe a consequence of the rest of this License.\n\n  8. If the distribution and/or use of the Program is restricted in\ncertain countries either by patents or by copyrighted interfaces, the\noriginal copyright holder who places the Program under this License\nmay add an explicit geographical distribution limitation excluding\nthose countries, so that distribution is permitted only in or among\ncountries not thus excluded.  In such case, this License incorporates\nthe limitation as if written in the body of this License.\n\n  9. The Free Software Foundation may publish revised and/or new versions\nof the General Public License from time to time.  Such new versions will\nbe similar in spirit to the present version, but may differ in detail to\naddress new problems or concerns.\n\nEach version is given a distinguishing version number.  If the Program\nspecifies a version number of this License which applies to it and "any\nlater version", you have the option of following the terms and conditions\neither of that version or of any later version published by the Free\nSoftware Foundation.  If the Program does not specify a version number of\nthis License, you may choose any version ever published by the Free Software\nFoundation.\n\n  10. If you wish to incorporate parts of the Program into other free\nprograms whose distribution conditions are different, write to the author\nto ask for permission.  For software which is copyrighted by the Free\nSoftware Foundation, write to the Free Software Foundation; we sometimes\nmake exceptions for this.  Our decision will be guided by the two goals\nof preserving the free status of all derivatives of our free software and\nof promoting the sharing and reuse of software generally.\n\nNO WARRANTY\n-----------\n\n  11. BECAUSE THE PROGRAM IS LICENSED FREE OF CHARGE, THERE IS NO WARRANTY\nFOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW.  EXCEPT WHEN\nOTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES\nPROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED\nOR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF\nMERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  THE ENTIRE RISK AS\nTO THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU.  SHOULD THE\nPROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING,\nREPAIR OR CORRECTION.\n\n  12. IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING\nWILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY AND/OR\nREDISTRIBUTE THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES,\nINCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING\nOUT OF THE USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED\nTO LOSS OF DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY\nYOU OR THIRD PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER\nPROGRAMS), EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE\nPOSSIBILITY OF SUCH DAMAGES.\n\n---------------------------\nEND OF TERMS AND CONDITIONS";s:6:"readme";s:453:"--------------------\nExtension: translit\n--------------------\nVersion: 1.0.0-beta\nReleased: October 20, 2010\nSince: October 20, 2010\nAuthor: Jason Coward <jason@modx.com>\n\nA MODx Revolution Core Extension, the translit package provides a custom transliteration service class. When installed,\nthis is automatically available for the core Friendly URL alias transliteration process for Resources. You can also use\nthe service in your plugins and snippets.";}', 'translit', NULL, 1, 0, 0, 'beta', 0),
	('if-1.1.1-pl', '2016-08-04 13:18:07', '2022-10-04 16:40:07', '2016-08-04 13:18:13', 0, 1, 0, 0, 'if-1.1.1-pl.transport.zip', NULL, 'a:2:{s:7:"license";s:15218:"GNU GENERAL PUBLIC LICENSE\n   Version 2, June 1991\n--------------------------\n\nCopyright (C) 1989, 1991 Free Software Foundation, Inc.\n59 Temple Place, Suite 330, Boston, MA  02111-1307  USA\n\nEveryone is permitted to copy and distribute verbatim copies\nof this license document, but changing it is not allowed.\n\nPreamble\n--------\n\n  The licenses for most software are designed to take away your\nfreedom to share and change it.  By contrast, the GNU General Public\nLicense is intended to guarantee your freedom to share and change free\nsoftware--to make sure the software is free for all its users.  This\nGeneral Public License applies to most of the Free Software\nFoundation\'s software and to any other program whose authors commit to\nusing it.  (Some other Free Software Foundation software is covered by\nthe GNU Library General Public License instead.)  You can apply it to\nyour programs, too.\n\n  When we speak of free software, we are referring to freedom, not\nprice.  Our General Public Licenses are designed to make sure that you\nhave the freedom to distribute copies of free software (and charge for\nthis service if you wish), that you receive source code or can get it\nif you want it, that you can change the software or use pieces of it\nin new free programs; and that you know you can do these things.\n\n  To protect your rights, we need to make restrictions that forbid\nanyone to deny you these rights or to ask you to surrender the rights.\nThese restrictions translate to certain responsibilities for you if you\ndistribute copies of the software, or if you modify it.\n\n  For example, if you distribute copies of such a program, whether\ngratis or for a fee, you must give the recipients all the rights that\nyou have.  You must make sure that they, too, receive or can get the\nsource code.  And you must show them these terms so they know their\nrights.\n\n  We protect your rights with two steps: (1) copyright the software, and\n(2) offer you this license which gives you legal permission to copy,\ndistribute and/or modify the software.\n\n  Also, for each author\'s protection and ours, we want to make certain\nthat everyone understands that there is no warranty for this free\nsoftware.  If the software is modified by someone else and passed on, we\nwant its recipients to know that what they have is not the original, so\nthat any problems introduced by others will not reflect on the original\nauthors\' reputations.\n\n  Finally, any free program is threatened constantly by software\npatents.  We wish to avoid the danger that redistributors of a free\nprogram will individually obtain patent licenses, in effect making the\nprogram proprietary.  To prevent this, we have made it clear that any\npatent must be licensed for everyone\'s free use or not licensed at all.\n\n  The precise terms and conditions for copying, distribution and\nmodification follow.\n\n\nGNU GENERAL PUBLIC LICENSE\nTERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION\n---------------------------------------------------------------\n\n  0. This License applies to any program or other work which contains\na notice placed by the copyright holder saying it may be distributed\nunder the terms of this General Public License.  The "Program", below,\nrefers to any such program or work, and a "work based on the Program"\nmeans either the Program or any derivative work under copyright law:\nthat is to say, a work containing the Program or a portion of it,\neither verbatim or with modifications and/or translated into another\nlanguage.  (Hereinafter, translation is included without limitation in\nthe term "modification".)  Each licensee is addressed as "you".\n\nActivities other than copying, distribution and modification are not\ncovered by this License; they are outside its scope.  The act of\nrunning the Program is not restricted, and the output from the Program\nis covered only if its contents constitute a work based on the\nProgram (independent of having been made by running the Program).\nWhether that is true depends on what the Program does.\n\n  1. You may copy and distribute verbatim copies of the Program\'s\nsource code as you receive it, in any medium, provided that you\nconspicuously and appropriately publish on each copy an appropriate\ncopyright notice and disclaimer of warranty; keep intact all the\nnotices that refer to this License and to the absence of any warranty;\nand give any other recipients of the Program a copy of this License\nalong with the Program.\n\nYou may charge a fee for the physical act of transferring a copy, and\nyou may at your option offer warranty protection in exchange for a fee.\n\n  2. You may modify your copy or copies of the Program or any portion\nof it, thus forming a work based on the Program, and copy and\ndistribute such modifications or work under the terms of Section 1\nabove, provided that you also meet all of these conditions:\n\n    a) You must cause the modified files to carry prominent notices\n    stating that you changed the files and the date of any change.\n\n    b) You must cause any work that you distribute or publish, that in\n    whole or in part contains or is derived from the Program or any\n    part thereof, to be licensed as a whole at no charge to all third\n    parties under the terms of this License.\n\n    c) If the modified program normally reads commands interactively\n    when run, you must cause it, when started running for such\n    interactive use in the most ordinary way, to print or display an\n    announcement including an appropriate copyright notice and a\n    notice that there is no warranty (or else, saying that you provide\n    a warranty) and that users may redistribute the program under\n    these conditions, and telling the user how to view a copy of this\n    License.  (Exception: if the Program itself is interactive but\n    does not normally print such an announcement, your work based on\n    the Program is not required to print an announcement.)\n\nThese requirements apply to the modified work as a whole.  If\nidentifiable sections of that work are not derived from the Program,\nand can be reasonably considered independent and separate works in\nthemselves, then this License, and its terms, do not apply to those\nsections when you distribute them as separate works.  But when you\ndistribute the same sections as part of a whole which is a work based\non the Program, the distribution of the whole must be on the terms of\nthis License, whose permissions for other licensees extend to the\nentire whole, and thus to each and every part regardless of who wrote it.\n\nThus, it is not the intent of this section to claim rights or contest\nyour rights to work written entirely by you; rather, the intent is to\nexercise the right to control the distribution of derivative or\ncollective works based on the Program.\n\nIn addition, mere aggregation of another work not based on the Program\nwith the Program (or with a work based on the Program) on a volume of\na storage or distribution medium does not bring the other work under\nthe scope of this License.\n\n  3. You may copy and distribute the Program (or a work based on it,\nunder Section 2) in object code or executable form under the terms of\nSections 1 and 2 above provided that you also do one of the following:\n\n    a) Accompany it with the complete corresponding machine-readable\n    source code, which must be distributed under the terms of Sections\n    1 and 2 above on a medium customarily used for software interchange; or,\n\n    b) Accompany it with a written offer, valid for at least three\n    years, to give any third party, for a charge no more than your\n    cost of physically performing source distribution, a complete\n    machine-readable copy of the corresponding source code, to be\n    distributed under the terms of Sections 1 and 2 above on a medium\n    customarily used for software interchange; or,\n\n    c) Accompany it with the information you received as to the offer\n    to distribute corresponding source code.  (This alternative is\n    allowed only for noncommercial distribution and only if you\n    received the program in object code or executable form with such\n    an offer, in accord with Subsection b above.)\n\nThe source code for a work means the preferred form of the work for\nmaking modifications to it.  For an executable work, complete source\ncode means all the source code for all modules it contains, plus any\nassociated interface definition files, plus the scripts used to\ncontrol compilation and installation of the executable.  However, as a\nspecial exception, the source code distributed need not include\nanything that is normally distributed (in either source or binary\nform) with the major components (compiler, kernel, and so on) of the\noperating system on which the executable runs, unless that component\nitself accompanies the executable.\n\nIf distribution of executable or object code is made by offering\naccess to copy from a designated place, then offering equivalent\naccess to copy the source code from the same place counts as\ndistribution of the source code, even though third parties are not\ncompelled to copy the source along with the object code.\n\n  4. You may not copy, modify, sublicense, or distribute the Program\nexcept as expressly provided under this License.  Any attempt\notherwise to copy, modify, sublicense or distribute the Program is\nvoid, and will automatically terminate your rights under this License.\nHowever, parties who have received copies, or rights, from you under\nthis License will not have their licenses terminated so long as such\nparties remain in full compliance.\n\n  5. You are not required to accept this License, since you have not\nsigned it.  However, nothing else grants you permission to modify or\ndistribute the Program or its derivative works.  These actions are\nprohibited by law if you do not accept this License.  Therefore, by\nmodifying or distributing the Program (or any work based on the\nProgram), you indicate your acceptance of this License to do so, and\nall its terms and conditions for copying, distributing or modifying\nthe Program or works based on it.\n\n  6. Each time you redistribute the Program (or any work based on the\nProgram), the recipient automatically receives a license from the\noriginal licensor to copy, distribute or modify the Program subject to\nthese terms and conditions.  You may not impose any further\nrestrictions on the recipients\' exercise of the rights granted herein.\nYou are not responsible for enforcing compliance by third parties to\nthis License.\n\n  7. If, as a consequence of a court judgment or allegation of patent\ninfringement or for any other reason (not limited to patent issues),\nconditions are imposed on you (whether by court order, agreement or\notherwise) that contradict the conditions of this License, they do not\nexcuse you from the conditions of this License.  If you cannot\ndistribute so as to satisfy simultaneously your obligations under this\nLicense and any other pertinent obligations, then as a consequence you\nmay not distribute the Program at all.  For example, if a patent\nlicense would not permit royalty-free redistribution of the Program by\nall those who receive copies directly or indirectly through you, then\nthe only way you could satisfy both it and this License would be to\nrefrain entirely from distribution of the Program.\n\nIf any portion of this section is held invalid or unenforceable under\nany particular circumstance, the balance of the section is intended to\napply and the section as a whole is intended to apply in other\ncircumstances.\n\nIt is not the purpose of this section to induce you to infringe any\npatents or other property right claims or to contest validity of any\nsuch claims; this section has the sole purpose of protecting the\nintegrity of the free software distribution system, which is\nimplemented by public license practices.  Many people have made\ngenerous contributions to the wide range of software distributed\nthrough that system in reliance on consistent application of that\nsystem; it is up to the author/donor to decide if he or she is willing\nto distribute software through any other system and a licensee cannot\nimpose that choice.\n\nThis section is intended to make thoroughly clear what is believed to\nbe a consequence of the rest of this License.\n\n  8. If the distribution and/or use of the Program is restricted in\ncertain countries either by patents or by copyrighted interfaces, the\noriginal copyright holder who places the Program under this License\nmay add an explicit geographical distribution limitation excluding\nthose countries, so that distribution is permitted only in or among\ncountries not thus excluded.  In such case, this License incorporates\nthe limitation as if written in the body of this License.\n\n  9. The Free Software Foundation may publish revised and/or new versions\nof the General Public License from time to time.  Such new versions will\nbe similar in spirit to the present version, but may differ in detail to\naddress new problems or concerns.\n\nEach version is given a distinguishing version number.  If the Program\nspecifies a version number of this License which applies to it and "any\nlater version", you have the option of following the terms and conditions\neither of that version or of any later version published by the Free\nSoftware Foundation.  If the Program does not specify a version number of\nthis License, you may choose any version ever published by the Free Software\nFoundation.\n\n  10. If you wish to incorporate parts of the Program into other free\nprograms whose distribution conditions are different, write to the author\nto ask for permission.  For software which is copyrighted by the Free\nSoftware Foundation, write to the Free Software Foundation; we sometimes\nmake exceptions for this.  Our decision will be guided by the two goals\nof preserving the free status of all derivatives of our free software and\nof promoting the sharing and reuse of software generally.\n\nNO WARRANTY\n-----------\n\n  11. BECAUSE THE PROGRAM IS LICENSED FREE OF CHARGE, THERE IS NO WARRANTY\nFOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW.  EXCEPT WHEN\nOTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES\nPROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED\nOR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF\nMERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  THE ENTIRE RISK AS\nTO THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU.  SHOULD THE\nPROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING,\nREPAIR OR CORRECTION.\n\n  12. IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING\nWILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY AND/OR\nREDISTRIBUTE THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES,\nINCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING\nOUT OF THE USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED\nTO LOSS OF DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY\nYOU OR THIRD PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER\nPROGRAMS), EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE\nPOSSIBILITY OF SUCH DAMAGES.\n\n---------------------------\nEND OF TERMS AND CONDITIONS";s:6:"readme";s:455:"-----------\nSnippet: If\n-----------\nVersion: 1.0\nCreated: October 29, 2009\nAuthor: Jason Coward <jason@modx.com>\n        Shaun McCormick <shaun@modx.com>\n\nA simple Conditional snippet.\n    \nExamples:\n[[If?\n    &subject=`[[+name]]`\n    &operator=`notempty`\n    &then=`Hello, [[+name]]!`\n    &else=`Hello, anonymous!`\n]]\n\n[[If?\n    &subject=`[[*scoreTV]]`\n    &operator=`GT`\n    &operand=`100`\n    &then=`You win!`\n    &else=`Not high enough. Try again!`\n]]";}', 'if', NULL, 1, 1, 1, 'pl', 0),
	('ace-1.9.3-pl', '2022-10-04 22:00:01', '2022-10-04 20:00:50', '2022-10-04 22:00:50', 0, 1, 1, 0, 'ace-1.9.3-pl.transport.zip', NULL, 'a:8:{s:7:"license";s:15218:"GNU GENERAL PUBLIC LICENSE\n   Version 2, June 1991\n--------------------------\n\nCopyright (C) 1989, 1991 Free Software Foundation, Inc.\n59 Temple Place, Suite 330, Boston, MA  02111-1307  USA\n\nEveryone is permitted to copy and distribute verbatim copies\nof this license document, but changing it is not allowed.\n\nPreamble\n--------\n\n  The licenses for most software are designed to take away your\nfreedom to share and change it.  By contrast, the GNU General Public\nLicense is intended to guarantee your freedom to share and change free\nsoftware--to make sure the software is free for all its users.  This\nGeneral Public License applies to most of the Free Software\nFoundation\'s software and to any other program whose authors commit to\nusing it.  (Some other Free Software Foundation software is covered by\nthe GNU Library General Public License instead.)  You can apply it to\nyour programs, too.\n\n  When we speak of free software, we are referring to freedom, not\nprice.  Our General Public Licenses are designed to make sure that you\nhave the freedom to distribute copies of free software (and charge for\nthis service if you wish), that you receive source code or can get it\nif you want it, that you can change the software or use pieces of it\nin new free programs; and that you know you can do these things.\n\n  To protect your rights, we need to make restrictions that forbid\nanyone to deny you these rights or to ask you to surrender the rights.\nThese restrictions translate to certain responsibilities for you if you\ndistribute copies of the software, or if you modify it.\n\n  For example, if you distribute copies of such a program, whether\ngratis or for a fee, you must give the recipients all the rights that\nyou have.  You must make sure that they, too, receive or can get the\nsource code.  And you must show them these terms so they know their\nrights.\n\n  We protect your rights with two steps: (1) copyright the software, and\n(2) offer you this license which gives you legal permission to copy,\ndistribute and/or modify the software.\n\n  Also, for each author\'s protection and ours, we want to make certain\nthat everyone understands that there is no warranty for this free\nsoftware.  If the software is modified by someone else and passed on, we\nwant its recipients to know that what they have is not the original, so\nthat any problems introduced by others will not reflect on the original\nauthors\' reputations.\n\n  Finally, any free program is threatened constantly by software\npatents.  We wish to avoid the danger that redistributors of a free\nprogram will individually obtain patent licenses, in effect making the\nprogram proprietary.  To prevent this, we have made it clear that any\npatent must be licensed for everyone\'s free use or not licensed at all.\n\n  The precise terms and conditions for copying, distribution and\nmodification follow.\n\n\nGNU GENERAL PUBLIC LICENSE\nTERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION\n---------------------------------------------------------------\n\n  0. This License applies to any program or other work which contains\na notice placed by the copyright holder saying it may be distributed\nunder the terms of this General Public License.  The "Program", below,\nrefers to any such program or work, and a "work based on the Program"\nmeans either the Program or any derivative work under copyright law:\nthat is to say, a work containing the Program or a portion of it,\neither verbatim or with modifications and/or translated into another\nlanguage.  (Hereinafter, translation is included without limitation in\nthe term "modification".)  Each licensee is addressed as "you".\n\nActivities other than copying, distribution and modification are not\ncovered by this License; they are outside its scope.  The act of\nrunning the Program is not restricted, and the output from the Program\nis covered only if its contents constitute a work based on the\nProgram (independent of having been made by running the Program).\nWhether that is true depends on what the Program does.\n\n  1. You may copy and distribute verbatim copies of the Program\'s\nsource code as you receive it, in any medium, provided that you\nconspicuously and appropriately publish on each copy an appropriate\ncopyright notice and disclaimer of warranty; keep intact all the\nnotices that refer to this License and to the absence of any warranty;\nand give any other recipients of the Program a copy of this License\nalong with the Program.\n\nYou may charge a fee for the physical act of transferring a copy, and\nyou may at your option offer warranty protection in exchange for a fee.\n\n  2. You may modify your copy or copies of the Program or any portion\nof it, thus forming a work based on the Program, and copy and\ndistribute such modifications or work under the terms of Section 1\nabove, provided that you also meet all of these conditions:\n\n    a) You must cause the modified files to carry prominent notices\n    stating that you changed the files and the date of any change.\n\n    b) You must cause any work that you distribute or publish, that in\n    whole or in part contains or is derived from the Program or any\n    part thereof, to be licensed as a whole at no charge to all third\n    parties under the terms of this License.\n\n    c) If the modified program normally reads commands interactively\n    when run, you must cause it, when started running for such\n    interactive use in the most ordinary way, to print or display an\n    announcement including an appropriate copyright notice and a\n    notice that there is no warranty (or else, saying that you provide\n    a warranty) and that users may redistribute the program under\n    these conditions, and telling the user how to view a copy of this\n    License.  (Exception: if the Program itself is interactive but\n    does not normally print such an announcement, your work based on\n    the Program is not required to print an announcement.)\n\nThese requirements apply to the modified work as a whole.  If\nidentifiable sections of that work are not derived from the Program,\nand can be reasonably considered independent and separate works in\nthemselves, then this License, and its terms, do not apply to those\nsections when you distribute them as separate works.  But when you\ndistribute the same sections as part of a whole which is a work based\non the Program, the distribution of the whole must be on the terms of\nthis License, whose permissions for other licensees extend to the\nentire whole, and thus to each and every part regardless of who wrote it.\n\nThus, it is not the intent of this section to claim rights or contest\nyour rights to work written entirely by you; rather, the intent is to\nexercise the right to control the distribution of derivative or\ncollective works based on the Program.\n\nIn addition, mere aggregation of another work not based on the Program\nwith the Program (or with a work based on the Program) on a volume of\na storage or distribution medium does not bring the other work under\nthe scope of this License.\n\n  3. You may copy and distribute the Program (or a work based on it,\nunder Section 2) in object code or executable form under the terms of\nSections 1 and 2 above provided that you also do one of the following:\n\n    a) Accompany it with the complete corresponding machine-readable\n    source code, which must be distributed under the terms of Sections\n    1 and 2 above on a medium customarily used for software interchange; or,\n\n    b) Accompany it with a written offer, valid for at least three\n    years, to give any third party, for a charge no more than your\n    cost of physically performing source distribution, a complete\n    machine-readable copy of the corresponding source code, to be\n    distributed under the terms of Sections 1 and 2 above on a medium\n    customarily used for software interchange; or,\n\n    c) Accompany it with the information you received as to the offer\n    to distribute corresponding source code.  (This alternative is\n    allowed only for noncommercial distribution and only if you\n    received the program in object code or executable form with such\n    an offer, in accord with Subsection b above.)\n\nThe source code for a work means the preferred form of the work for\nmaking modifications to it.  For an executable work, complete source\ncode means all the source code for all modules it contains, plus any\nassociated interface definition files, plus the scripts used to\ncontrol compilation and installation of the executable.  However, as a\nspecial exception, the source code distributed need not include\nanything that is normally distributed (in either source or binary\nform) with the major components (compiler, kernel, and so on) of the\noperating system on which the executable runs, unless that component\nitself accompanies the executable.\n\nIf distribution of executable or object code is made by offering\naccess to copy from a designated place, then offering equivalent\naccess to copy the source code from the same place counts as\ndistribution of the source code, even though third parties are not\ncompelled to copy the source along with the object code.\n\n  4. You may not copy, modify, sublicense, or distribute the Program\nexcept as expressly provided under this License.  Any attempt\notherwise to copy, modify, sublicense or distribute the Program is\nvoid, and will automatically terminate your rights under this License.\nHowever, parties who have received copies, or rights, from you under\nthis License will not have their licenses terminated so long as such\nparties remain in full compliance.\n\n  5. You are not required to accept this License, since you have not\nsigned it.  However, nothing else grants you permission to modify or\ndistribute the Program or its derivative works.  These actions are\nprohibited by law if you do not accept this License.  Therefore, by\nmodifying or distributing the Program (or any work based on the\nProgram), you indicate your acceptance of this License to do so, and\nall its terms and conditions for copying, distributing or modifying\nthe Program or works based on it.\n\n  6. Each time you redistribute the Program (or any work based on the\nProgram), the recipient automatically receives a license from the\noriginal licensor to copy, distribute or modify the Program subject to\nthese terms and conditions.  You may not impose any further\nrestrictions on the recipients\' exercise of the rights granted herein.\nYou are not responsible for enforcing compliance by third parties to\nthis License.\n\n  7. If, as a consequence of a court judgment or allegation of patent\ninfringement or for any other reason (not limited to patent issues),\nconditions are imposed on you (whether by court order, agreement or\notherwise) that contradict the conditions of this License, they do not\nexcuse you from the conditions of this License.  If you cannot\ndistribute so as to satisfy simultaneously your obligations under this\nLicense and any other pertinent obligations, then as a consequence you\nmay not distribute the Program at all.  For example, if a patent\nlicense would not permit royalty-free redistribution of the Program by\nall those who receive copies directly or indirectly through you, then\nthe only way you could satisfy both it and this License would be to\nrefrain entirely from distribution of the Program.\n\nIf any portion of this section is held invalid or unenforceable under\nany particular circumstance, the balance of the section is intended to\napply and the section as a whole is intended to apply in other\ncircumstances.\n\nIt is not the purpose of this section to induce you to infringe any\npatents or other property right claims or to contest validity of any\nsuch claims; this section has the sole purpose of protecting the\nintegrity of the free software distribution system, which is\nimplemented by public license practices.  Many people have made\ngenerous contributions to the wide range of software distributed\nthrough that system in reliance on consistent application of that\nsystem; it is up to the author/donor to decide if he or she is willing\nto distribute software through any other system and a licensee cannot\nimpose that choice.\n\nThis section is intended to make thoroughly clear what is believed to\nbe a consequence of the rest of this License.\n\n  8. If the distribution and/or use of the Program is restricted in\ncertain countries either by patents or by copyrighted interfaces, the\noriginal copyright holder who places the Program under this License\nmay add an explicit geographical distribution limitation excluding\nthose countries, so that distribution is permitted only in or among\ncountries not thus excluded.  In such case, this License incorporates\nthe limitation as if written in the body of this License.\n\n  9. The Free Software Foundation may publish revised and/or new versions\nof the General Public License from time to time.  Such new versions will\nbe similar in spirit to the present version, but may differ in detail to\naddress new problems or concerns.\n\nEach version is given a distinguishing version number.  If the Program\nspecifies a version number of this License which applies to it and "any\nlater version", you have the option of following the terms and conditions\neither of that version or of any later version published by the Free\nSoftware Foundation.  If the Program does not specify a version number of\nthis License, you may choose any version ever published by the Free Software\nFoundation.\n\n  10. If you wish to incorporate parts of the Program into other free\nprograms whose distribution conditions are different, write to the author\nto ask for permission.  For software which is copyrighted by the Free\nSoftware Foundation, write to the Free Software Foundation; we sometimes\nmake exceptions for this.  Our decision will be guided by the two goals\nof preserving the free status of all derivatives of our free software and\nof promoting the sharing and reuse of software generally.\n\nNO WARRANTY\n-----------\n\n  11. BECAUSE THE PROGRAM IS LICENSED FREE OF CHARGE, THERE IS NO WARRANTY\nFOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW.  EXCEPT WHEN\nOTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES\nPROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED\nOR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF\nMERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  THE ENTIRE RISK AS\nTO THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU.  SHOULD THE\nPROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING,\nREPAIR OR CORRECTION.\n\n  12. IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING\nWILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY AND/OR\nREDISTRIBUTE THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES,\nINCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING\nOUT OF THE USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED\nTO LOSS OF DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY\nYOU OR THIRD PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER\nPROGRAMS), EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE\nPOSSIBILITY OF SUCH DAMAGES.\n\n---------------------------\nEND OF TERMS AND CONDITIONS";s:6:"readme";s:271:"--------------------\nExtra: Ace\n--------------------\nSince: March 29th, 2012\nAuthor: Danil Kostin <danya.postfactum@gmail.com>\nLicense: GNU GPLv2 (or later at your option)\n\nIntegrates Ace Code Editor into MODx Revolution.\n\nPress Ctrl+Alt+H to see all available shortcuts.";s:9:"changelog";s:4624:"Changelog for Ace integration into MODx Revolution.\n\nAce 1.9.3 [15.05.2022]\n====================================\n- Updated: Move settings into an own file [#12]\n- Fixed: Ace TV input [#11]\n- Fixed: Prevent PHP warning: Undefined variable $field [#10]\n\nAce 1.9.2\n====================================\n- Updated: Corrected search form [#8]\n- Added: TV input Ace field [#9]\n\nAce 1.9.1\n====================================\n- Fixed: Changed fonts\n- Updated: emmet.js with the support flex css styles and many other combinations\n\nAce 1.9.0\n====================================\n- Added: autodetecting file mode by modelist.js [#7]\n- Added: new modes from ace-builds for version 1.2.0\n\nAce 1.8.0\n====================================\n- Added: autocompletion for php functions.\n\nAce 1.7.0\n====================================\n- Added: new system setting "ace.grow".\n- Added: new system setting "ace.html_elements_mime".\n\nAce 1.6.5\n====================================\n- Added: "Twig" syntax for support of Twig in chunks.\n- Changed: Plugin is not static anymore.\n\nAce 1.6.4\n====================================\n- Fixed: Support of emmet in smarty mode. Again.\n\nAce 1.6.3\n====================================\n- Fixed: Support of emmet in smarty mode.\n\nAce 1.6.2\n====================================\n- Fixed: Editor mode handling.\n- Added: "Markdown" syntax for mime type "text/x-markdown".\n\nAce 1.6.1\n====================================\n- Fixed : Work with enabled system setting "compress_js".\n\nAce 1.6.0\n====================================\n- Added: "Smarty" syntax for support of Fenom in chunks.\n- Updated: Ace to version 1.2.0.\n\nAce 1.5.1\n====================================\n- Fixed: Bug with narrowing of the textarea.\n\nAce 1.5.0\n====================================\n- Changed: Assets are moved back to /assets/\n- Fixed: MODx tag completions (was completely broken)\n- Added: Editor height setting\n\nAce 1.4.3\n====================================\n- Added: MODx tag completions (Ctrl+Space)\n- Fixed: Issue caused AjaxManager (MODx Manager speed booster plugin) tree drag\'n\'drop bug\n\nAce 1.4.2\n====================================\n- Added: Undo coalescing\n- Changed: Mac fullscreen command is bound to Command+F12\n- Added: Drag delay (allow to start new selection inside current one) for Mac\n- Fixed: Use file extension of static chunks to detect code syntax\n\n\nAce 1.4.1\n====================================\n- Fixed: Tab handling\n- Fixed: Emmet shortcut listing by Ctr+Alt+H\n- Added: Expandable snippets support (see ace.snippets setting)\n- Added: Emmet wrap_with_abbreviation command (Alt+W)\n\nAce 1.4.0\n====================================\n- Added: Emmet (aka Zen Coding) support\n- Added: Terminal dark theme\n- Added: Hotkey table (Ctrl+Alt+H)\n- Fixed: Resource overview fatal error\n- Changed: Assets are moved to /manager/assets/components/\n\nAce 1.3.3\n====================================\n- Added: PHP live syntax check\n- Added: Chaos dark theme\n- Added: Setting show_invisibles\n\n\nAce 1.3.2\n====================================\n- Fixed: The bug while installing the Ace\n- Fixed: Broken word_wrap setting\n- Added: Tab settings (tab size, soft tab)\n- Added: Now completele compatible with AjaxManager extra\n\n\nAce 1.3.1\n====================================\n- Changed: Plugin content now is stored in static file\n\n\nAce 1.3.0\n====================================\n- Added: German translation\n- Added: MODx tags highlighting\n- Added: Ambiance and xcode themes\n- Added: less/scss syntax highlighting\n- Added: Fullwindow mode (Ctrl + F11)\n- Changed: Editor now ignores `wich_editor` setting. Set `use_editor` to false to use ACE for Resources.\n\n\nAce 1.2.1\n====================================\n- Changed: Assets are moved to manager folder\n- Added: Font size setting\n- Added: "GitHub" theme\n- Added: Support of html5 drag\'n\'drop (accepting of dropped text)\n- Added: XML / HTML tag autoclosing\n- Fixed: broken en lexicon and php 5.3 incompatibility\n\n\nAce 1.2.0\n====================================\n- Removed: Some unnecessary options\n- Changed: Editor options are moved to system settings\n- Fixed: Multiple little editor bugs\n- Added: Add missing "OnFileEditFormPrerender" event to MODx\n- Added: Multiline editing\n- Added: Advanced find/replace window\n\n\nAce 1.1.0\n====================================\n- Fixed: Fatal error on document create event\n- Fixed: Changing of properties has no effect\n- Added: File edition support\n- Added: MODx tree elements drag\'n\'drop support\n- Added: Auto-assigning which_element_editor to Ace\n\n\nAce 1.0.0\n====================================\n- Added: Plugin properties to adjust how Ace behaves\n- Initial commit";s:9:"signature";s:12:"ace-1.9.3-pl";s:6:"action";s:26:"Workspace/Packages/Install";s:8:"register";s:3:"mgr";s:5:"topic";s:40:"/workspace/package/install/ace-1.9.3-pl/";s:14:"package_action";i:0;}', 'Ace', 'a:38:{s:2:"id";s:24:"628075bba8801031285d3f42";s:7:"package";s:24:"4f6e2782f245544fe8000014";s:12:"display_name";s:12:"ace-1.9.3-pl";s:4:"name";s:3:"Ace";s:7:"version";s:5:"1.9.3";s:13:"version_major";s:1:"1";s:13:"version_minor";s:1:"9";s:13:"version_patch";s:1:"3";s:7:"release";s:2:"pl";s:8:"vrelease";s:2:"pl";s:14:"vrelease_index";s:0:"";s:6:"author";s:10:"ibochkarev";s:11:"description";s:376:"<p>New feature: modx tag code autocompletion! Press Ctrl+Space to get code suggestions with descriptions.</p><p>Works for snippets, chunks, system settings, tvs and resource fields, filters and properties.</p><p>Property sets, lexicon entries are not supported. Unfortunately, I have no idea how to retrieve chunk-specific placeholders, so there is no placeholder support.</p>";s:12:"instructions";s:341:"<p></p><p>Install via Package Management.</p><p>Set editor theme you wish in system settings (change namespace to "ace").</p><p>If you want to use this editor for resources, just set system option <i>use_editor</i> to <b>false</b> (global usage), or <i>richtext</i> setting of certain resource to <b>false</b> (specific usage).</p><p></p>";s:9:"changelog";s:4571:"Ace 1.9.3 [15.05.2022]\n====================================\n- Updated: Move settings into an own file [#12]\n- Fixed: Ace TV input [#11]\n- Fixed: Prevent PHP warning: Undefined variable $field [#10]\n\nAce 1.9.2\n====================================\n- Updated: Corrected search form [#8]\n- Added: TV input Ace field [#9]\n\nAce 1.9.1\n====================================\n- Fixed: Changed fonts\n- Updated: emmet.js with the support flex css styles and many other combinations\n\nAce 1.9.0\n====================================\n- Added: autodetecting file mode by modelist.js [#7]\n- Added: new modes from ace-builds for version 1.2.0\n\nAce 1.8.0\n====================================\n- Added: autocompletion for php functions.\n\nAce 1.7.0\n====================================\n- Added: new system setting "ace.grow".\n- Added: new system setting "ace.html_elements_mime".\n\nAce 1.6.5\n====================================\n- Added: "Twig" syntax for support of Twig in chunks.\n- Changed: Plugin is not static anymore.\n\nAce 1.6.4\n====================================\n- Fixed: Support of emmet in smarty mode. Again.\n\nAce 1.6.3\n====================================\n- Fixed: Support of emmet in smarty mode.\n\nAce 1.6.2\n====================================\n- Fixed: Editor mode handling.\n- Added: "Markdown" syntax for mime type "text/x-markdown".\n\nAce 1.6.1\n====================================\n- Fixed : Work with enabled system setting "compress_js".\n\nAce 1.6.0\n====================================\n- Added: "Smarty" syntax for support of Fenom in chunks.\n- Updated: Ace to version 1.2.0.\n\nAce 1.5.1\n====================================\n- Fixed: Bug with narrowing of the textarea.\n\nAce 1.5.0\n====================================\n- Changed: Assets are moved back to /assets/\n- Fixed: MODx tag completions (was completely broken)\n- Added: Editor height setting\n\nAce 1.4.3\n====================================\n- Added: MODx tag completions (Ctrl+Space)\n- Fixed: Issue caused AjaxManager (MODx Manager speed booster plugin) tree drag\'n\'drop bug\n\nAce 1.4.2\n====================================\n- Added: Undo coalescing\n- Changed: Mac fullscreen command is bound to Command+F12\n- Added: Drag delay (allow to start new selection inside current one) for Mac\n- Fixed: Use file extension of static chunks to detect code syntax\n\n\nAce 1.4.1\n====================================\n- Fixed: Tab handling\n- Fixed: Emmet shortcut listing by Ctr+Alt+H\n- Added: Expandable snippets support (see ace.snippets setting)\n- Added: Emmet wrap_with_abbreviation command (Alt+W)\n\nAce 1.4.0\n====================================\n- Added: Emmet (aka Zen Coding) support\n- Added: Terminal dark theme\n- Added: Hotkey table (Ctrl+Alt+H)\n- Fixed: Resource overview fatal error\n- Changed: Assets are moved to /manager/assets/components/\n\nAce 1.3.3\n====================================\n- Added: PHP live syntax check\n- Added: Chaos dark theme\n- Added: Setting show_invisibles\n\n\nAce 1.3.2\n====================================\n- Fixed: The bug while installing the Ace\n- Fixed: Broken word_wrap setting\n- Added: Tab settings (tab size, soft tab)\n- Added: Now completele compatible with AjaxManager extra\n\n\nAce 1.3.1\n====================================\n- Changed: Plugin content now is stored in static file\n\n\nAce 1.3.0\n====================================\n- Added: German translation\n- Added: MODx tags highlighting\n- Added: Ambiance and xcode themes\n- Added: less/scss syntax highlighting\n- Added: Fullwindow mode (Ctrl + F11)\n- Changed: Editor now ignores `wich_editor` setting. Set `use_editor` to false to use ACE for Resources.\n\n\nAce 1.2.1\n====================================\n- Changed: Assets are moved to manager folder\n- Added: Font size setting\n- Added: "GitHub" theme\n- Added: Support of html5 drag\'n\'drop (accepting of dropped text)\n- Added: XML / HTML tag autoclosing\n- Fixed: broken en lexicon and php 5.3 incompatibility\n\n\nAce 1.2.0\n====================================\n- Removed: Some unnecessary options\n- Changed: Editor options are moved to system settings\n- Fixed: Multiple little editor bugs\n- Added: Add missing "OnFileEditFormPrerender" event to MODx\n- Added: Multiline editing\n- Added: Advanced find/replace window\n\n\nAce 1.1.0\n====================================\n- Fixed: Fatal error on document create event\n- Fixed: Changing of properties has no effect\n- Added: File edition support\n- Added: MODx tree elements drag\'n\'drop support\n- Added: Auto-assigning which_element_editor to Ace\n\n\nAce 1.0.0\n====================================\n- Added: Plugin properties to adjust how Ace behaves\n- Initial commit";s:9:"createdon";s:24:"2022-05-15T03:38:35+0000";s:9:"createdby";s:10:"ibochkarev";s:8:"editedon";s:24:"2022-10-04T19:49:12+0000";s:10:"releasedon";s:24:"2022-05-15T03:38:35+0000";s:9:"downloads";s:6:"345953";s:8:"approved";s:4:"true";s:7:"audited";s:5:"false";s:8:"featured";s:5:"false";s:10:"deprecated";s:5:"false";s:7:"license";s:5:"GPLv2";s:7:"smf_url";s:0:"";s:10:"repository";s:24:"4d4c3fa6b2b0830da9000001";s:8:"supports";s:1:"2";s:8:"location";s:61:"https://modx.com/extras/download/?id=628075bba8801031285d3f43";s:9:"signature";s:12:"ace-1.9.3-pl";s:11:"supports_db";s:5:"mysql";s:16:"minimum_supports";s:1:"2";s:9:"breaks_at";s:8:"10000000";s:10:"screenshot";s:68:"http://modx.s3.amazonaws.com/extras/4f6e2782f245544fe8000014/ace.png";s:4:"file";a:7:{s:2:"id";s:24:"628075bba8801031285d3f43";s:7:"version";s:24:"628075bba8801031285d3f42";s:8:"filename";s:26:"ace-1.9.3-pl.transport.zip";s:9:"downloads";s:5:"11750";s:6:"lastip";s:14:"109.62.185.224";s:9:"transport";s:4:"true";s:8:"location";s:61:"https://modx.com/extras/download/?id=628075bba8801031285d3f43";}s:17:"package-signature";s:12:"ace-1.9.3-pl";s:10:"categories";s:15:"richtexteditors";s:4:"tags";s:0:"";}', 1, 9, 3, 'pl', 0),
	('getresources-1.7.0-pl', '2022-10-04 22:00:25', '2022-10-04 20:01:01', '2022-10-04 22:01:01', 0, 1, 1, 0, 'getresources-1.7.0-pl.transport.zip', NULL, 'a:8:{s:7:"license";s:15218:"GNU GENERAL PUBLIC LICENSE\n   Version 2, June 1991\n--------------------------\n\nCopyright (C) 1989, 1991 Free Software Foundation, Inc.\n59 Temple Place, Suite 330, Boston, MA  02111-1307  USA\n\nEveryone is permitted to copy and distribute verbatim copies\nof this license document, but changing it is not allowed.\n\nPreamble\n--------\n\n  The licenses for most software are designed to take away your\nfreedom to share and change it.  By contrast, the GNU General Public\nLicense is intended to guarantee your freedom to share and change free\nsoftware--to make sure the software is free for all its users.  This\nGeneral Public License applies to most of the Free Software\nFoundation\'s software and to any other program whose authors commit to\nusing it.  (Some other Free Software Foundation software is covered by\nthe GNU Library General Public License instead.)  You can apply it to\nyour programs, too.\n\n  When we speak of free software, we are referring to freedom, not\nprice.  Our General Public Licenses are designed to make sure that you\nhave the freedom to distribute copies of free software (and charge for\nthis service if you wish), that you receive source code or can get it\nif you want it, that you can change the software or use pieces of it\nin new free programs; and that you know you can do these things.\n\n  To protect your rights, we need to make restrictions that forbid\nanyone to deny you these rights or to ask you to surrender the rights.\nThese restrictions translate to certain responsibilities for you if you\ndistribute copies of the software, or if you modify it.\n\n  For example, if you distribute copies of such a program, whether\ngratis or for a fee, you must give the recipients all the rights that\nyou have.  You must make sure that they, too, receive or can get the\nsource code.  And you must show them these terms so they know their\nrights.\n\n  We protect your rights with two steps: (1) copyright the software, and\n(2) offer you this license which gives you legal permission to copy,\ndistribute and/or modify the software.\n\n  Also, for each author\'s protection and ours, we want to make certain\nthat everyone understands that there is no warranty for this free\nsoftware.  If the software is modified by someone else and passed on, we\nwant its recipients to know that what they have is not the original, so\nthat any problems introduced by others will not reflect on the original\nauthors\' reputations.\n\n  Finally, any free program is threatened constantly by software\npatents.  We wish to avoid the danger that redistributors of a free\nprogram will individually obtain patent licenses, in effect making the\nprogram proprietary.  To prevent this, we have made it clear that any\npatent must be licensed for everyone\'s free use or not licensed at all.\n\n  The precise terms and conditions for copying, distribution and\nmodification follow.\n\n\nGNU GENERAL PUBLIC LICENSE\nTERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION\n---------------------------------------------------------------\n\n  0. This License applies to any program or other work which contains\na notice placed by the copyright holder saying it may be distributed\nunder the terms of this General Public License.  The "Program", below,\nrefers to any such program or work, and a "work based on the Program"\nmeans either the Program or any derivative work under copyright law:\nthat is to say, a work containing the Program or a portion of it,\neither verbatim or with modifications and/or translated into another\nlanguage.  (Hereinafter, translation is included without limitation in\nthe term "modification".)  Each licensee is addressed as "you".\n\nActivities other than copying, distribution and modification are not\ncovered by this License; they are outside its scope.  The act of\nrunning the Program is not restricted, and the output from the Program\nis covered only if its contents constitute a work based on the\nProgram (independent of having been made by running the Program).\nWhether that is true depends on what the Program does.\n\n  1. You may copy and distribute verbatim copies of the Program\'s\nsource code as you receive it, in any medium, provided that you\nconspicuously and appropriately publish on each copy an appropriate\ncopyright notice and disclaimer of warranty; keep intact all the\nnotices that refer to this License and to the absence of any warranty;\nand give any other recipients of the Program a copy of this License\nalong with the Program.\n\nYou may charge a fee for the physical act of transferring a copy, and\nyou may at your option offer warranty protection in exchange for a fee.\n\n  2. You may modify your copy or copies of the Program or any portion\nof it, thus forming a work based on the Program, and copy and\ndistribute such modifications or work under the terms of Section 1\nabove, provided that you also meet all of these conditions:\n\n    a) You must cause the modified files to carry prominent notices\n    stating that you changed the files and the date of any change.\n\n    b) You must cause any work that you distribute or publish, that in\n    whole or in part contains or is derived from the Program or any\n    part thereof, to be licensed as a whole at no charge to all third\n    parties under the terms of this License.\n\n    c) If the modified program normally reads commands interactively\n    when run, you must cause it, when started running for such\n    interactive use in the most ordinary way, to print or display an\n    announcement including an appropriate copyright notice and a\n    notice that there is no warranty (or else, saying that you provide\n    a warranty) and that users may redistribute the program under\n    these conditions, and telling the user how to view a copy of this\n    License.  (Exception: if the Program itself is interactive but\n    does not normally print such an announcement, your work based on\n    the Program is not required to print an announcement.)\n\nThese requirements apply to the modified work as a whole.  If\nidentifiable sections of that work are not derived from the Program,\nand can be reasonably considered independent and separate works in\nthemselves, then this License, and its terms, do not apply to those\nsections when you distribute them as separate works.  But when you\ndistribute the same sections as part of a whole which is a work based\non the Program, the distribution of the whole must be on the terms of\nthis License, whose permissions for other licensees extend to the\nentire whole, and thus to each and every part regardless of who wrote it.\n\nThus, it is not the intent of this section to claim rights or contest\nyour rights to work written entirely by you; rather, the intent is to\nexercise the right to control the distribution of derivative or\ncollective works based on the Program.\n\nIn addition, mere aggregation of another work not based on the Program\nwith the Program (or with a work based on the Program) on a volume of\na storage or distribution medium does not bring the other work under\nthe scope of this License.\n\n  3. You may copy and distribute the Program (or a work based on it,\nunder Section 2) in object code or executable form under the terms of\nSections 1 and 2 above provided that you also do one of the following:\n\n    a) Accompany it with the complete corresponding machine-readable\n    source code, which must be distributed under the terms of Sections\n    1 and 2 above on a medium customarily used for software interchange; or,\n\n    b) Accompany it with a written offer, valid for at least three\n    years, to give any third party, for a charge no more than your\n    cost of physically performing source distribution, a complete\n    machine-readable copy of the corresponding source code, to be\n    distributed under the terms of Sections 1 and 2 above on a medium\n    customarily used for software interchange; or,\n\n    c) Accompany it with the information you received as to the offer\n    to distribute corresponding source code.  (This alternative is\n    allowed only for noncommercial distribution and only if you\n    received the program in object code or executable form with such\n    an offer, in accord with Subsection b above.)\n\nThe source code for a work means the preferred form of the work for\nmaking modifications to it.  For an executable work, complete source\ncode means all the source code for all modules it contains, plus any\nassociated interface definition files, plus the scripts used to\ncontrol compilation and installation of the executable.  However, as a\nspecial exception, the source code distributed need not include\nanything that is normally distributed (in either source or binary\nform) with the major components (compiler, kernel, and so on) of the\noperating system on which the executable runs, unless that component\nitself accompanies the executable.\n\nIf distribution of executable or object code is made by offering\naccess to copy from a designated place, then offering equivalent\naccess to copy the source code from the same place counts as\ndistribution of the source code, even though third parties are not\ncompelled to copy the source along with the object code.\n\n  4. You may not copy, modify, sublicense, or distribute the Program\nexcept as expressly provided under this License.  Any attempt\notherwise to copy, modify, sublicense or distribute the Program is\nvoid, and will automatically terminate your rights under this License.\nHowever, parties who have received copies, or rights, from you under\nthis License will not have their licenses terminated so long as such\nparties remain in full compliance.\n\n  5. You are not required to accept this License, since you have not\nsigned it.  However, nothing else grants you permission to modify or\ndistribute the Program or its derivative works.  These actions are\nprohibited by law if you do not accept this License.  Therefore, by\nmodifying or distributing the Program (or any work based on the\nProgram), you indicate your acceptance of this License to do so, and\nall its terms and conditions for copying, distributing or modifying\nthe Program or works based on it.\n\n  6. Each time you redistribute the Program (or any work based on the\nProgram), the recipient automatically receives a license from the\noriginal licensor to copy, distribute or modify the Program subject to\nthese terms and conditions.  You may not impose any further\nrestrictions on the recipients\' exercise of the rights granted herein.\nYou are not responsible for enforcing compliance by third parties to\nthis License.\n\n  7. If, as a consequence of a court judgment or allegation of patent\ninfringement or for any other reason (not limited to patent issues),\nconditions are imposed on you (whether by court order, agreement or\notherwise) that contradict the conditions of this License, they do not\nexcuse you from the conditions of this License.  If you cannot\ndistribute so as to satisfy simultaneously your obligations under this\nLicense and any other pertinent obligations, then as a consequence you\nmay not distribute the Program at all.  For example, if a patent\nlicense would not permit royalty-free redistribution of the Program by\nall those who receive copies directly or indirectly through you, then\nthe only way you could satisfy both it and this License would be to\nrefrain entirely from distribution of the Program.\n\nIf any portion of this section is held invalid or unenforceable under\nany particular circumstance, the balance of the section is intended to\napply and the section as a whole is intended to apply in other\ncircumstances.\n\nIt is not the purpose of this section to induce you to infringe any\npatents or other property right claims or to contest validity of any\nsuch claims; this section has the sole purpose of protecting the\nintegrity of the free software distribution system, which is\nimplemented by public license practices.  Many people have made\ngenerous contributions to the wide range of software distributed\nthrough that system in reliance on consistent application of that\nsystem; it is up to the author/donor to decide if he or she is willing\nto distribute software through any other system and a licensee cannot\nimpose that choice.\n\nThis section is intended to make thoroughly clear what is believed to\nbe a consequence of the rest of this License.\n\n  8. If the distribution and/or use of the Program is restricted in\ncertain countries either by patents or by copyrighted interfaces, the\noriginal copyright holder who places the Program under this License\nmay add an explicit geographical distribution limitation excluding\nthose countries, so that distribution is permitted only in or among\ncountries not thus excluded.  In such case, this License incorporates\nthe limitation as if written in the body of this License.\n\n  9. The Free Software Foundation may publish revised and/or new versions\nof the General Public License from time to time.  Such new versions will\nbe similar in spirit to the present version, but may differ in detail to\naddress new problems or concerns.\n\nEach version is given a distinguishing version number.  If the Program\nspecifies a version number of this License which applies to it and "any\nlater version", you have the option of following the terms and conditions\neither of that version or of any later version published by the Free\nSoftware Foundation.  If the Program does not specify a version number of\nthis License, you may choose any version ever published by the Free Software\nFoundation.\n\n  10. If you wish to incorporate parts of the Program into other free\nprograms whose distribution conditions are different, write to the author\nto ask for permission.  For software which is copyrighted by the Free\nSoftware Foundation, write to the Free Software Foundation; we sometimes\nmake exceptions for this.  Our decision will be guided by the two goals\nof preserving the free status of all derivatives of our free software and\nof promoting the sharing and reuse of software generally.\n\nNO WARRANTY\n-----------\n\n  11. BECAUSE THE PROGRAM IS LICENSED FREE OF CHARGE, THERE IS NO WARRANTY\nFOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW.  EXCEPT WHEN\nOTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES\nPROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED\nOR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF\nMERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  THE ENTIRE RISK AS\nTO THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU.  SHOULD THE\nPROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING,\nREPAIR OR CORRECTION.\n\n  12. IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING\nWILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY AND/OR\nREDISTRIBUTE THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES,\nINCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING\nOUT OF THE USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED\nTO LOSS OF DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY\nYOU OR THIRD PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER\nPROGRAMS), EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE\nPOSSIBILITY OF SUCH DAMAGES.\n\n---------------------------\nEND OF TERMS AND CONDITIONS";s:6:"readme";s:335:"--------------------\nSnippet: getResources\n--------------------\nVersion: 1.7.0-pl\nReleased: April 29, 2021\nSince: December 28, 2009\nAuthor: Jason Coward <jason@opengeek.com>\n\nA general purpose Resource listing and summarization snippet for MODX Revolution.\n\nOfficial Documentation:\nhttps://docs.modx.com/current/en/extras/getresources\n";s:9:"changelog";s:3660:"Changelog for getResources.\n\ngetResources 1.7.0-pl (April 29, 2021)\n====================================\n- [#104] Replace deprecated each() usage\n- [#97] Add sortby resources to apply input order\n\ngetResources 1.6.1-pl (December 30, 2013)\n====================================\n- Allow tvFilter values to contain filter operators\n- Allow 0-based idx\n- Pass scriptProperties to wrapperTpl\n- [#30][#80] Only dump properties for invalid tpl when debug enabled\n\ngetResources 1.6.0-pl (February 19, 2013)\n====================================\n- Add tplWrapper for specifying a wrapper template\n\ngetResources 1.5.1-pl (August 23, 2012)\n====================================\n- Add tplOperator property to default properties\n- [#73] Add between tplOperator to conditionalTpls\n\ngetResources 1.5.0-pl (June 15, 2012)\n====================================\n- [#58] Add tplCondition/conditionalTpls support\n- [#67] Add odd property\n- [#60] Allow custom delimiters for tvFilters\n- [#63] Give tplFirst/tplLast precedence over tpl_X/tpl_nX\n- Automatically prepare TV values for media-source dependent TVs\n\ngetResources 1.4.2-pl (December 9, 2011)\n====================================\n- [#25] Add new operators to tvFilters\n- [#37] Consider default values with tvFilters\n- [#57] Fix tpl overrides and improve order\n\ngetResources 1.4.1-pl (December 8, 2011)\n====================================\n- [#57] Add support for factor-based tpls\n- [#54], [#55] Fix processTVList feature\n\ngetResources 1.4.0-pl (September 21, 2011)\n====================================\n- [#50] Use children of parents from other contexts\n- [#45] Add dbCacheFlag to control db caching of getCollection, default to false\n- [#49] Allow comma-delimited list of TV names as includeTVList or processTVList\n\ngetResources 1.3.1-pl (July 14, 2011)\n====================================\n- [#43] Allow 0 as idx property\n- [#9] Fix tvFilters grouping\n- [#46] Fix criteria issue with &resources property\n\ngetResources 1.3.0-pl (March 28, 2011)\n====================================\n- [#33] sortbyTVType: Allow numeric and datetime TV sorting via SQL CAST()\n- [#24] Fix typos in list property options\n- [#4] Support multiple sortby fields via JSON object\n- Use get() instead to toArray() if includeContent is false\n- [#22] Add &toSeparatePlaceholders property for splitting output\n\ngetResources 1.2.2-pl (October 18, 2010)\n====================================\n- [#19] Fix sortbyTV returning duplicate rows\n\ngetResources 1.2.1-pl (October 11, 2010)\n====================================\n- Remove inadvertent call to modX::setLogTarget(\'ECHO\')\n\ngetResources 1.2.0-pl (September 25, 2010)\n====================================\n- Fix error when &parents is not set\n- Allow empty &sortby\n- Add ability to sort by a single Template Variable value (or default value)\n\ngetResources 1.1.0-pl (July 30, 2010)\n====================================\n- Added &toPlaceholder property for assigning results to a placeholder\n- Added &resources property for including/excluding specific resources\n- Added &showDeleted property\n- Allow multiple contexts to be passed into &context\n- Added &showUnpublish property\n- Added getresources.core_path reference for easier development\n- [#ADDON-135] Make output separator configurable via outputSeparator property\n- Add where property to allow ad hoc criteria in JSON format\n\ngetResources 1.0.0-ga (December 29, 2009)\n====================================\n- [#ADDON-81] Allow empty tvPrefix property.\n- [#ADDON-89] Allow parents property to have a value of 0.\n- Changed default value of sortbyAlias to empty string and added sortbyEscaped property with default of 0.\n- Added changelog, license, and readme.\n";s:9:"signature";s:21:"getresources-1.7.0-pl";s:6:"action";s:26:"Workspace/Packages/Install";s:8:"register";s:3:"mgr";s:5:"topic";s:49:"/workspace/package/install/getresources-1.7.0-pl/";s:14:"package_action";i:0;}', 'getResources', 'a:38:{s:2:"id";s:24:"608ad48e5338cc65734841d2";s:7:"package";s:24:"4d556c3db2b083396d000abe";s:12:"display_name";s:21:"getresources-1.7.0-pl";s:4:"name";s:12:"getResources";s:7:"version";s:5:"1.7.0";s:13:"version_major";s:1:"1";s:13:"version_minor";s:1:"7";s:13:"version_patch";s:1:"0";s:7:"release";s:2:"pl";s:8:"vrelease";s:2:"pl";s:14:"vrelease_index";s:1:"0";s:6:"author";s:8:"opengeek";s:11:"description";s:115:"This release of getResources provides PHP 8 support and adds a sortby feature for resources to be sorted as listed.";s:12:"instructions";s:31:"Install via Package Management.";s:9:"changelog";s:91:"- [#104] Replace deprecated each() usage\n- [#97] Add sortby resources to apply input order\n";s:9:"createdon";s:24:"2021-04-29T15:45:18+0000";s:9:"createdby";s:8:"opengeek";s:8:"editedon";s:24:"2022-10-04T19:21:41+0000";s:10:"releasedon";s:24:"2021-04-29T15:45:18+0000";s:9:"downloads";s:6:"284644";s:8:"approved";s:4:"true";s:7:"audited";s:4:"true";s:8:"featured";s:4:"true";s:10:"deprecated";s:5:"false";s:7:"license";s:5:"GPLv2";s:7:"smf_url";s:0:"";s:10:"repository";s:24:"4d4c3fa6b2b0830da9000001";s:8:"supports";s:1:"2";s:8:"location";s:61:"https://modx.com/extras/download/?id=608ad48e5338cc65734841d3";s:9:"signature";s:21:"getresources-1.7.0-pl";s:11:"supports_db";s:12:"mysql,sqlsrv";s:16:"minimum_supports";s:1:"2";s:9:"breaks_at";s:8:"10000000";s:10:"screenshot";s:0:"";s:4:"file";a:7:{s:2:"id";s:24:"608ad48e5338cc65734841d3";s:7:"version";s:24:"608ad48e5338cc65734841d2";s:8:"filename";s:35:"getresources-1.7.0-pl.transport.zip";s:9:"downloads";s:5:"12579";s:6:"lastip";s:14:"92.204.239.110";s:9:"transport";s:4:"true";s:8:"location";s:61:"https://modx.com/extras/download/?id=608ad48e5338cc65734841d3";}s:17:"package-signature";s:21:"getresources-1.7.0-pl";s:10:"categories";s:32:"blogging,content,navigation,news";s:4:"tags";s:57:"blog,blogging,resources,getr,getresource,resource,listing";}', 1, 7, 0, 'pl', 0),
	('migx-3.0.0-alpha5', '2022-10-04 22:00:41', '2022-10-04 20:01:12', '2022-10-04 22:01:12', 0, 1, 1, 0, 'migx-3.0.0-alpha5.transport.zip', NULL, 'a:8:{s:7:"license";s:15218:"GNU GENERAL PUBLIC LICENSE\n   Version 2, June 1991\n--------------------------\n\nCopyright (C) 1989, 1991 Free Software Foundation, Inc.\n59 Temple Place, Suite 330, Boston, MA  02111-1307  USA\n\nEveryone is permitted to copy and distribute verbatim copies\nof this license document, but changing it is not allowed.\n\nPreamble\n--------\n\n  The licenses for most software are designed to take away your\nfreedom to share and change it.  By contrast, the GNU General Public\nLicense is intended to guarantee your freedom to share and change free\nsoftware--to make sure the software is free for all its users.  This\nGeneral Public License applies to most of the Free Software\nFoundation\'s software and to any other program whose authors commit to\nusing it.  (Some other Free Software Foundation software is covered by\nthe GNU Library General Public License instead.)  You can apply it to\nyour programs, too.\n\n  When we speak of free software, we are referring to freedom, not\nprice.  Our General Public Licenses are designed to make sure that you\nhave the freedom to distribute copies of free software (and charge for\nthis service if you wish), that you receive source code or can get it\nif you want it, that you can change the software or use pieces of it\nin new free programs; and that you know you can do these things.\n\n  To protect your rights, we need to make restrictions that forbid\nanyone to deny you these rights or to ask you to surrender the rights.\nThese restrictions translate to certain responsibilities for you if you\ndistribute copies of the software, or if you modify it.\n\n  For example, if you distribute copies of such a program, whether\ngratis or for a fee, you must give the recipients all the rights that\nyou have.  You must make sure that they, too, receive or can get the\nsource code.  And you must show them these terms so they know their\nrights.\n\n  We protect your rights with two steps: (1) copyright the software, and\n(2) offer you this license which gives you legal permission to copy,\ndistribute and/or modify the software.\n\n  Also, for each author\'s protection and ours, we want to make certain\nthat everyone understands that there is no warranty for this free\nsoftware.  If the software is modified by someone else and passed on, we\nwant its recipients to know that what they have is not the original, so\nthat any problems introduced by others will not reflect on the original\nauthors\' reputations.\n\n  Finally, any free program is threatened constantly by software\npatents.  We wish to avoid the danger that redistributors of a free\nprogram will individually obtain patent licenses, in effect making the\nprogram proprietary.  To prevent this, we have made it clear that any\npatent must be licensed for everyone\'s free use or not licensed at all.\n\n  The precise terms and conditions for copying, distribution and\nmodification follow.\n\n\nGNU GENERAL PUBLIC LICENSE\nTERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION\n---------------------------------------------------------------\n\n  0. This License applies to any program or other work which contains\na notice placed by the copyright holder saying it may be distributed\nunder the terms of this General Public License.  The "Program", below,\nrefers to any such program or work, and a "work based on the Program"\nmeans either the Program or any derivative work under copyright law:\nthat is to say, a work containing the Program or a portion of it,\neither verbatim or with modifications and/or translated into another\nlanguage.  (Hereinafter, translation is included without limitation in\nthe term "modification".)  Each licensee is addressed as "you".\n\nActivities other than copying, distribution and modification are not\ncovered by this License; they are outside its scope.  The act of\nrunning the Program is not restricted, and the output from the Program\nis covered only if its contents constitute a work based on the\nProgram (independent of having been made by running the Program).\nWhether that is true depends on what the Program does.\n\n  1. You may copy and distribute verbatim copies of the Program\'s\nsource code as you receive it, in any medium, provided that you\nconspicuously and appropriately publish on each copy an appropriate\ncopyright notice and disclaimer of warranty; keep intact all the\nnotices that refer to this License and to the absence of any warranty;\nand give any other recipients of the Program a copy of this License\nalong with the Program.\n\nYou may charge a fee for the physical act of transferring a copy, and\nyou may at your option offer warranty protection in exchange for a fee.\n\n  2. You may modify your copy or copies of the Program or any portion\nof it, thus forming a work based on the Program, and copy and\ndistribute such modifications or work under the terms of Section 1\nabove, provided that you also meet all of these conditions:\n\n    a) You must cause the modified files to carry prominent notices\n    stating that you changed the files and the date of any change.\n\n    b) You must cause any work that you distribute or publish, that in\n    whole or in part contains or is derived from the Program or any\n    part thereof, to be licensed as a whole at no charge to all third\n    parties under the terms of this License.\n\n    c) If the modified program normally reads commands interactively\n    when run, you must cause it, when started running for such\n    interactive use in the most ordinary way, to print or display an\n    announcement including an appropriate copyright notice and a\n    notice that there is no warranty (or else, saying that you provide\n    a warranty) and that users may redistribute the program under\n    these conditions, and telling the user how to view a copy of this\n    License.  (Exception: if the Program itself is interactive but\n    does not normally print such an announcement, your work based on\n    the Program is not required to print an announcement.)\n\nThese requirements apply to the modified work as a whole.  If\nidentifiable sections of that work are not derived from the Program,\nand can be reasonably considered independent and separate works in\nthemselves, then this License, and its terms, do not apply to those\nsections when you distribute them as separate works.  But when you\ndistribute the same sections as part of a whole which is a work based\non the Program, the distribution of the whole must be on the terms of\nthis License, whose permissions for other licensees extend to the\nentire whole, and thus to each and every part regardless of who wrote it.\n\nThus, it is not the intent of this section to claim rights or contest\nyour rights to work written entirely by you; rather, the intent is to\nexercise the right to control the distribution of derivative or\ncollective works based on the Program.\n\nIn addition, mere aggregation of another work not based on the Program\nwith the Program (or with a work based on the Program) on a volume of\na storage or distribution medium does not bring the other work under\nthe scope of this License.\n\n  3. You may copy and distribute the Program (or a work based on it,\nunder Section 2) in object code or executable form under the terms of\nSections 1 and 2 above provided that you also do one of the following:\n\n    a) Accompany it with the complete corresponding machine-readable\n    source code, which must be distributed under the terms of Sections\n    1 and 2 above on a medium customarily used for software interchange; or,\n\n    b) Accompany it with a written offer, valid for at least three\n    years, to give any third party, for a charge no more than your\n    cost of physically performing source distribution, a complete\n    machine-readable copy of the corresponding source code, to be\n    distributed under the terms of Sections 1 and 2 above on a medium\n    customarily used for software interchange; or,\n\n    c) Accompany it with the information you received as to the offer\n    to distribute corresponding source code.  (This alternative is\n    allowed only for noncommercial distribution and only if you\n    received the program in object code or executable form with such\n    an offer, in accord with Subsection b above.)\n\nThe source code for a work means the preferred form of the work for\nmaking modifications to it.  For an executable work, complete source\ncode means all the source code for all modules it contains, plus any\nassociated interface definition files, plus the scripts used to\ncontrol compilation and installation of the executable.  However, as a\nspecial exception, the source code distributed need not include\nanything that is normally distributed (in either source or binary\nform) with the major components (compiler, kernel, and so on) of the\noperating system on which the executable runs, unless that component\nitself accompanies the executable.\n\nIf distribution of executable or object code is made by offering\naccess to copy from a designated place, then offering equivalent\naccess to copy the source code from the same place counts as\ndistribution of the source code, even though third parties are not\ncompelled to copy the source along with the object code.\n\n  4. You may not copy, modify, sublicense, or distribute the Program\nexcept as expressly provided under this License.  Any attempt\notherwise to copy, modify, sublicense or distribute the Program is\nvoid, and will automatically terminate your rights under this License.\nHowever, parties who have received copies, or rights, from you under\nthis License will not have their licenses terminated so long as such\nparties remain in full compliance.\n\n  5. You are not required to accept this License, since you have not\nsigned it.  However, nothing else grants you permission to modify or\ndistribute the Program or its derivative works.  These actions are\nprohibited by law if you do not accept this License.  Therefore, by\nmodifying or distributing the Program (or any work based on the\nProgram), you indicate your acceptance of this License to do so, and\nall its terms and conditions for copying, distributing or modifying\nthe Program or works based on it.\n\n  6. Each time you redistribute the Program (or any work based on the\nProgram), the recipient automatically receives a license from the\noriginal licensor to copy, distribute or modify the Program subject to\nthese terms and conditions.  You may not impose any further\nrestrictions on the recipients\' exercise of the rights granted herein.\nYou are not responsible for enforcing compliance by third parties to\nthis License.\n\n  7. If, as a consequence of a court judgment or allegation of patent\ninfringement or for any other reason (not limited to patent issues),\nconditions are imposed on you (whether by court order, agreement or\notherwise) that contradict the conditions of this License, they do not\nexcuse you from the conditions of this License.  If you cannot\ndistribute so as to satisfy simultaneously your obligations under this\nLicense and any other pertinent obligations, then as a consequence you\nmay not distribute the Program at all.  For example, if a patent\nlicense would not permit royalty-free redistribution of the Program by\nall those who receive copies directly or indirectly through you, then\nthe only way you could satisfy both it and this License would be to\nrefrain entirely from distribution of the Program.\n\nIf any portion of this section is held invalid or unenforceable under\nany particular circumstance, the balance of the section is intended to\napply and the section as a whole is intended to apply in other\ncircumstances.\n\nIt is not the purpose of this section to induce you to infringe any\npatents or other property right claims or to contest validity of any\nsuch claims; this section has the sole purpose of protecting the\nintegrity of the free software distribution system, which is\nimplemented by public license practices.  Many people have made\ngenerous contributions to the wide range of software distributed\nthrough that system in reliance on consistent application of that\nsystem; it is up to the author/donor to decide if he or she is willing\nto distribute software through any other system and a licensee cannot\nimpose that choice.\n\nThis section is intended to make thoroughly clear what is believed to\nbe a consequence of the rest of this License.\n\n  8. If the distribution and/or use of the Program is restricted in\ncertain countries either by patents or by copyrighted interfaces, the\noriginal copyright holder who places the Program under this License\nmay add an explicit geographical distribution limitation excluding\nthose countries, so that distribution is permitted only in or among\ncountries not thus excluded.  In such case, this License incorporates\nthe limitation as if written in the body of this License.\n\n  9. The Free Software Foundation may publish revised and/or new versions\nof the General Public License from time to time.  Such new versions will\nbe similar in spirit to the present version, but may differ in detail to\naddress new problems or concerns.\n\nEach version is given a distinguishing version number.  If the Program\nspecifies a version number of this License which applies to it and "any\nlater version", you have the option of following the terms and conditions\neither of that version or of any later version published by the Free\nSoftware Foundation.  If the Program does not specify a version number of\nthis License, you may choose any version ever published by the Free Software\nFoundation.\n\n  10. If you wish to incorporate parts of the Program into other free\nprograms whose distribution conditions are different, write to the author\nto ask for permission.  For software which is copyrighted by the Free\nSoftware Foundation, write to the Free Software Foundation; we sometimes\nmake exceptions for this.  Our decision will be guided by the two goals\nof preserving the free status of all derivatives of our free software and\nof promoting the sharing and reuse of software generally.\n\nNO WARRANTY\n-----------\n\n  11. BECAUSE THE PROGRAM IS LICENSED FREE OF CHARGE, THERE IS NO WARRANTY\nFOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW.  EXCEPT WHEN\nOTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES\nPROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED\nOR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF\nMERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  THE ENTIRE RISK AS\nTO THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU.  SHOULD THE\nPROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING,\nREPAIR OR CORRECTION.\n\n  12. IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING\nWILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY AND/OR\nREDISTRIBUTE THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES,\nINCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING\nOUT OF THE USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED\nTO LOSS OF DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY\nYOU OR THIRD PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER\nPROGRAMS), EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE\nPOSSIBILITY OF SUCH DAMAGES.\n\n---------------------------\nEND OF TERMS AND CONDITIONS";s:6:"readme";s:1392:"--------------------\nMIGX\n--------------------\nVersion: 2.1.0\nAuthor: Bruno Perner <b.perner@gmx.de>\n--------------------\n\n* MIGX (multiItemsGridTv for modx) is a custom-tv-input-type for adding multiple items into one TV-value and a snippet for listing this items on your frontend.\n* It has a configurable grid and a configurable tabbed editor-window to add and edit items.\n* Each item can have multiple fields. For each field you can use another tv-input-type.\n\nFeel free to suggest ideas/improvements/bugs on GitHub:\nhttp://github.com/Bruno17/multiItemsGridTV/issues\n\nInstallation:\n\ninstall by package-management.\nCreate a new menu:\nSystem -> Actions \n\nActions-tree:\nmigx -> right-click -> create Acton here\ncontroller: index\nnamespace: migx\nlanguage-topics: migx:default,file\n\nmenu-tree:\nComponents -> right-click -> place action here\nlexicon-key: migx\naction: migx - index\nparameters: &configs=migxconfigs||packagemanager||setup\n\nclear cache\ngo to components -> migx -> setup-tab -> setup\n\nIf you are upgrading from MIGX - versions before 2.0\ngo to tab upgrade. click upgrade.\nThis will add a new autoincrementing field MIGX_id to all your MIGX-TV-items\nThe getImageList-snippet needs this field to work correctly.\n\n\nAllways after upgrading MIGX of any Version:\ngo to components -> migx -> setup-tab -> setup\n\nthis will upgrade the migx-configs-table and add new fields, if necessary.\n\n\n";s:9:"changelog";s:11763:"Changelog for MIGX.\n\nMIGX 2.13.0\n==============\nadd snippet migxGetObject and property toPlaceholders\nadd property createChunk to create a chunk with all possible placeholders\nFix imageupload on PHP7 due to stray break statement\nFix for missing custom prefix in some packagemanager tasks\nUpdated image paths to use dynamic assets path\nFix the export if a join setting exist\nFinnish Translation\nadd userid to migxResourceMediapath snippet\nremove duplicate sort scriptProperty in export.php\n[getlist.php] add getcustomconfigs - hook, groupby, selectfields, specialfields\nparse MODX-tags in field-configs\npackagemanager fix new folder permission\nUse a textarea instead of an input in MIGX TV\nReduce indexed varchar fields for utf8mb4 support in mysql\nfix duplicate entries in csv-export\nAdd CSV Import functionality\nImprove CSV Export functionality\nadd toJson property to migxLoopCollection\nFix transparency in .png in renderimage column renderer\nOptimize loadfromxource functionality, especially for MIGX within MIGXdb\ndeep nested saving when nested windows are open and saving without closing the window\nexport/import single MIGX - items\n\nMIGX 2.12.0\n==============\nselect db-fields from defined class and its joins for formtabs and columns\nadd categories and a category-filter to MIGX Configs\nmultiple grouping-levels for &groupingField\nhooksnippet getformnames\nadd snippet migxAutoPublish for publishing by Cronjobs\nadd beforesave - hook to update-processor\nallow string in where-property\nadd a default schema-template, used at create package\nand some bugfixes\n\nMIGX 2.11.0\n==============\ngroupingfield, preparesnippet, _total, _count, improve @CODE\nhooksnippet beforecreateform\n\nMIGX 2.10.0\n==============\nhooksnippet getcustomconfigs for importcsvmigx\nsupport layout-rows/columns in formtabs\ncontextmenu \'flat formtabs\'\nmultiupload to db, resizeOnUpload - plugin\n[packagemanager] Add Extension Package - feature\nrespect joinalias in export.php and handlepositionselector.php\npossible to use TV-value in migxresourcemediapath - snippet\n[getImageList] inherit_children_tvname\nMIGXdb add item before/after\n\nMIGX 2.9.7\n==============\nadd emtpyTpl\nfix some MIGX-grid width - issues\nimport csv to MIGX\n\nMIGX 2.9.6\n==============\nhooksnippet getcustomconfigs for export-processor\nfix missing formtabs after saving\n\nMIGX 2.9.5\n==============\ncolumn-actionbuttons also for MIGX\nexport/import MIGX-items from/into MIGX-TV\nget tinymcewrapper working\nmore config-options for combo-filter\nFix and simplify Redactor implementation to use MODx.loadRTE()\n\nMIGX 2.9.4\n==============\n[migxResourcemediapath] add context_key as path option\nbutton for \'alter fields from schema\'\nfix MIGX-grid width\ncustom grid for MIGX-TV\nrespect context-default-media-source - setting\n\nMIGX 2.9.3\n==============\nbasic support for new TinyMCE RTE\nfix assetsUrl/connectorUrl - settings\nMIGX-TV MODX multifile-uploader/autocreate items  \nMIGX-TV configurable contextmenues\n\nMIGX 2.9.2\n==============\nsome smaller fixes\n\nMIGX 2.9.1\n==============\nadd hook-snippet setting\nsome handleRelated - update - functions\ndestroy updatewindow on close\n\nMIGX 2.9\n==============\n[migxLoopCollection] allow use of foreign database\nSottwell\'s improvements on migxresourcemediapath\nnew snippet: migxGetCollectionTree\naccess to foreign database from default processors\nimprovements on multiple formtabs\nmake inline-editing for MIGX - grid possible\noption to add MIGX-items directly without modal\nlistbox-cell-editor\nmovetotop,movetobottom - buttons for MIGX-grid\ncell-editing for MIgXdb - grids\noption to add MIGXdb-items directly without modal\n[getImageList] &inheritFrom - inherit MIGX-items from parents or other resources\nsome migxredactor - fixes \n\nMIGX 2.8.1\n==============\nlets disable the \'Add Item\' - button\nnew configs gridperpage and sortconfig\nwrapperTpl for getImageList and migxLoopCollection\n\nMIGX 2.8.0\n==============\nresolve tables on install\nrender cross, also when empty string\nreusable activaterelations - processors\n[migxLoopCollection] tpl_nN\n[#154] clean TV-value, if no MIGX-items \nadditional db-storage of formtabs and fields\nget menue working in MODX 2.3\nimprove description_is_code \n\n\nMIGX 2.6.8\n==============\nsome other small fixes\nrestrictive condition by processed MIGX-tags for formfields\nFilter-Button for Reset all filters to default-value\nextend date-filter\nmake cmp main caption translatable \nMigx::prepareJoins - optional rightjoin \n\nMIGX 2.6.7\n==============\nadd date - filter \nadd handlepositionselector - processor \nadd snippet exportmigx2db\n\nMIGX 2.6.6\n==============\nfixes only\n\nMIGX 2.6.5\n==============\nfixes only\n\nMIGX 2.6.4\n==============\n[redactor-field] get and use file-properties from a redactor-inputTV\nadd renderImageFromHtml - renderer\n\nMIGX 2.6.3\n==============\nconfigurable redactor-field with configs-configuration, make redactor work in MIGXdb - CMP\n\nMIGX 2.6.2\n==============\nfix issue with imported configs-field, if not an array \n\nMIGX 2.6.1\n==============\nMake Formfields translatable\n\nMIGX 2.6\n==============\n[getImageList] output inner arrays as json-string\nadded polish translation\n[getImageList] splits, build summaries\n make grid-columns translatable, let user add custom-lexicons from custom php-config-files \n\n\nMIGX 2.5.11\n==============\nadd simple MIGXdb - validation (only required for now)\nsome fixes\n\n\nMIGX 2.5.9\n==============\nlet us create new indexes, with altered field-def from schema \noptimize input-option-values-handling, see:http://forums.modx.com/thread/79757/sortable-editable-list-of-checkboxes?page=4#dis-post-483240\n\n\nMIGX 2.5.8\n\n==============\nAdded \'showScreenshot\' (big image in popup) \nAdded template-field for direct template-input for renderChunk\nAdded position - selector for new MIGX - items\nFix for not removed rte-editors when using formswitcher\nsend current store-params to iframe-window\n\n\nMIGX 2.5.6\n\n==============\n\nAdd support for the modmore.com Redactor editor \nsome work on multiuploader for MIGXdb\nmore eNotice - fixes\n\n\nMIGX 2.5.2\n\n==============\nread input-options into MIGX-TV\nrespect filter in default - export.php\nfix for empty value in TV - configs not loading renderers etc.\nfix changed processaction-param after export2csv \nstopEvent() by onClick - event\n\nMIGX 2.5.1\n\n==============\nfix bug with renderChunk - renderer\n\nMIGX 2.5\n\n==============\nget different rtes working - support for tinymce, ckeditor \nsome settings for MIGXfe\ncs - lexicons, \nsome eNotice - fixes\nfix with to big integers on TV-id (set phptype to string)\nlimit MIGX-record-count\n\n\nMIGX 2.4.2\n\n==============\ncolumnButtons for the migx - grid \nlittle form-layout-mods\nadded the option to have the first formtab outside the other tabs above of them.\n\nadded the option to use the TV-description-field as parsed code-areas in the formtabs, modx-tags are parsed there - \nsnippets, chunks, output-filters can be used there. All fields of the record can be used as placeholders.\n\nmigxupdate for MIGXfe\ndefault-values for MIGXdb-filters\nupdate co_id in iframe-window\nadd a searchbox to MIGX-Configurator\nread configs directly from exported configs-files from custom-packages - directory by using configname:packagename - configs\n\n\nMIGX 2.4.1\n\n==============\nsome new snippets:\ngetDayliMIGXrecord\nmigxgetrelations\n\nadded beta treecombo-filter-option for example to filter resources in MIGXdb by resourcetree\nadd window-title configuration, make window-caption dynamic (its possible to use placeholders now)\nhide tabs in form, when only one tab\nadded selectposition - renderer\n\n\nMIGX 2.4.0\n\n==============\nnew renderer - switchStatusOptions\nnew renderer - renderChunk\ngetImageList - added \'contains\' and \'snippet\' - where-filters\nadd duplicate-contextmenue to MIGXdb \nnew property for getImageList: &reverse\ngive TVs in each CMP-tab a unique id\nrefresh grid after closing iframe-window\nadded tpl_n{n} tplFirst tplLast tpl_n tpl_oneresult properties to getImageList\nexport jsonarray-fields as separate fields in csv-export\nalias, breadcrumb and ultimateparent for migxREsourceMediaPath\nAdded TV - description - field to configuration\n\n\nMIGX 2.3.1\n\n==============\nsome eNotice - error - fixes\nadd type - configuration to gridcolumns, now its possible to sort also numeric on the MIGX - grid: see https://github.com/Bruno17/MIGX/issues/41\n\nMIGX 2.3.0\n\n==============\nadd multifile - uploader, upload to MIGX - mediasource\nadd load from mediasource - button to MIGX\nadd migxResourcePath - snippet\nadd iframe - support - its now possible to create chunks with snippet-calls and show the result in an iframe-window. used by multifile-uploader.\n\n\nMIGX 2.2.3\n\n==============\nconfirmation before overriding schema-files\nsome additions for childresources-management by MIGXdb\nswitch between multiple forms - configurations\nadd renderDate - renderer , thanks to Jako\nadditional send all store-baseParams when opening the form-window. This way we can have different forms depending on filters for example.\nadd parent-property for dynamic filter-comboboxes\nadd getliste-where for default getlist-processor\nexport formtabs as clean json in editraw-mode\n\n\nMIGX 2.2.2\n\n==============\nadd migxLoopCollection-snippet\nmove prepareJoins into a migx-method\nconfirmation before remove db-record, getcombo did not use idfield \nallow empty prefix \nadd possibility to use tables without \'deleted\' - field and default-getlist-processor\nfix Call-time pass-by-reference errors\nget tinyMCE to work on richtext-TV-inputs in MIGXdb - CMPs \nfix prefix not sended to writeSchema\ngrid add cls \'main-wrapper\' to give it a bit padding, thanks to jako\n\n\nMIGX 2.2.0\n\n==============\n\nexport/import configuration-objects as json to/from files in custom-package-directories \nnew configs: getlist - defaultsort, joins, gridload_mode (by button, auto) \ngrid-smarty-template now can be also in custom-package-directories\nreworked handling of joined objects in default update-php \nadd connected_object_id baseparam to migx-grid\nadded snippet migxLoopCollection\n\n\nMIGX 2.1.1\n\n==============\n\n  fix for migx-snippet not working with multiple calls on one page\n  resource_id as script-property for getlist-processor when used with migx-snippet\n\nMIGX 2.1.0\n\n==============\n\n  add &sort to the getImageList - snippet\n  add new snippet \'migx\' to get items from db-tables, can use the same configurations and getList - processors as the MIGXdb - manager\n  make it possible to have config-files for grids and processors in another package-folder for easier packaging together custom - packages\n  more MIGXdb - configurations\n\n\nMIGX 2.0.1\n\n==============\n\n  more E_NOTICE - Error - fixes\n  Fix Missing Add - Item - Replacement - String \n\nMIGX 2.0.0\n\n==============\n\n- pl\n\n  fix for Revo 2.2.2\n  fix some E_NOTICE - errors\n\n- new in beta5\n\n  Configure multiple CMP - tabs\n  packagemanager ported to extjs - tab\n  added MIGX-setup/upgrade - tab\n  added configurable text and combo - filterboxes\n\n- new in beta3\n\n  This is a preview-version of MIGXdb\n  MIGXdb can now also be used as configurable CMP\n  MIGX - configurator for tabs, columns, MIGXdb-TV and MIGXdb-CMP\n  Package-manager, create and edit schemas and package-tables\n\n- new:\n  better compatibility with revo 2.2\n  working with mediasources\n  introduced MIGXdb\n\n\nMIGX 1.2.0\n==============\n- new:\n  merge scriptProperties to Placeholders \n  basic compatibility for modx 2.2 \n  autoinc-field: MIGX_id\n  autoResourceFolders - functionality, autoCreate directory for each resource\n  \n  \n- fixed:\n  url-TV support\n  context-based base_path issues\n  remove remaining []\n  remove Tiny-instances when switching form\n  enter on textarea closes window\n  fireResourceFormChange for drag,remove,duplicate \n\nMIGX 1.1.0\n==============\n- new:\n  &docidVarKey\n  &processTVs\n  \n- fixed:\n  context-filepath-issue\n\nMIGX 1.0.0\n==============\n- Initial release.";s:9:"signature";s:17:"migx-3.0.0-alpha5";s:6:"action";s:26:"Workspace/Packages/Install";s:8:"register";s:3:"mgr";s:5:"topic";s:45:"/workspace/package/install/migx-3.0.0-alpha5/";s:14:"package_action";i:0;}', 'MIGX', 'a:38:{s:2:"id";s:24:"625c4b0eb2fbc9264e2a5032";s:7:"package";s:24:"4db018def24554690c000005";s:12:"display_name";s:17:"migx-3.0.0-alpha5";s:4:"name";s:4:"MIGX";s:7:"version";s:5:"3.0.0";s:13:"version_major";s:1:"3";s:13:"version_minor";s:1:"0";s:13:"version_patch";s:1:"0";s:7:"release";s:6:"alpha5";s:8:"vrelease";s:5:"alpha";s:14:"vrelease_index";s:1:"5";s:6:"author";s:7:"bruno17";s:11:"description";s:672:"<p>MIGX (multiItemsGridTv for modx) is a custom-tv-input-type for adding multiple items into one TV-value and a snippet for listing this items on your frontend.</p><p>It has a cofigurable grid and a configurable tabbed editor-window to add and edit items.</p><p>Each item can have multiple fields. For each field you can use another tv-input-type.</p><p>MIGXdb can manage (resource-related) custom-db-table-items in a TV and can help to create CMPs for custom-db-tables</p><p>See the official documentation here: <a href="http://rtfm.modx.com/display/addon/MIGX" style="color: rgb(15, 112, 150); " title="" target="">http://rtfm.modx.com/display/addon/MIGX</a></p><p></p>";s:12:"instructions";s:5844:"<p></p><p style="margin: 10px 0px 20px; padding: 0px; border-width: 0px; outline-width: 0px; font-size: 13px; vertical-align: baseline; background-color: transparent; line-height: 1.4;">Installation:Install via Package Management.</p><p style="margin: 10px 0px 20px; padding: 0px; border-width: 0px; outline-width: 0px; font-size: 13px; vertical-align: baseline; background-color: transparent; line-height: 1.4;">For MIGX and MIGXdb - Configuration - Management:</p><p style="margin-top: 10px; margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 0px; border-left-width: 0px; border-style: initial; border-color: initial; outline-width: 0px; outline-style: initial; outline-color: initial; font-size: 13px; vertical-align: baseline; background-image: initial; background-attachment: initial; background-origin: initial; background-clip: initial; background-color: transparent; line-height: 1.4; background-position: initial initial; background-repeat: initial initial; ">Create a new menu:System -> Actions Actions-tree:migx -> right-click -> create Acton herecontroller: indexnamespace: migxlanguage-topics: migx:default,filemenu-tree:Components -> right-click -> place action herelexicon-key: migxaction: migx - indexparameters: &configs=migxconfigs||packagemanager||setupclear cachego to components -> migx -> setup-tab -> setupIf you are upgrading from MIGX - versions before 2.0go to tab upgrade. click upgrade.This will add a new autoincrementing field MIGX_id to all your MIGX-TV-itemsThe getImageList-snippet needs this field to work correctly.</p><p style="margin-top: 10px; margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 0px; border-left-width: 0px; border-style: initial; border-color: initial; outline-width: 0px; outline-style: initial; outline-color: initial; font-size: 13px; vertical-align: baseline; background-image: initial; background-attachment: initial; background-origin: initial; background-clip: initial; background-color: transparent; line-height: 1.4; background-position: initial initial; background-repeat: initial initial; "><b style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 0px; border-left-width: 0px; border-style: initial; border-color: initial; outline-width: 0px; outline-style: initial; outline-color: initial; font-size: 13px; vertical-align: baseline; background-image: initial; background-attachment: initial; background-origin: initial; background-clip: initial; background-color: transparent; background-position: initial initial; background-repeat: initial initial; ">Note:</b> Make sure to remove older versions of multiItemsGridTv and the multiitemsgridTv-namespace, if you had them tried from Github.</p><p style="margin-top: 10px; margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 0px; border-left-width: 0px; border-style: initial; border-color: initial; outline-width: 0px; outline-style: initial; outline-color: initial; font-size: 13px; vertical-align: baseline; background-image: initial; background-attachment: initial; background-origin: initial; background-clip: initial; background-color: transparent; line-height: 1.4; background-position: initial initial; background-repeat: initial initial; "><b style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 0px; border-left-width: 0px; border-style: initial; border-color: initial; outline-width: 0px; outline-style: initial; outline-color: initial; font-size: 13px; vertical-align: baseline; background-image: initial; background-attachment: initial; background-origin: initial; background-clip: initial; background-color: transparent; background-position: initial initial; background-repeat: initial initial; ">Note</b>: Input Options for the MIGX only work for Revolution 2.1.0-rc2 and later.</p><p style="margin-top: 10px; margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 0px; border-left-width: 0px; border-style: initial; border-color: initial; outline-width: 0px; outline-style: initial; outline-color: initial; font-size: 13px; vertical-align: baseline; background-image: initial; background-attachment: initial; background-origin: initial; background-clip: initial; background-color: transparent; line-height: 1.4; background-position: initial initial; background-repeat: initial initial; "></p><p style="margin-top: 10px; margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 0px; border-left-width: 0px; border-style: initial; border-color: initial; outline-width: 0px; outline-style: initial; outline-color: initial; font-size: 13px; vertical-align: baseline; background-image: initial; background-attachment: initial; background-origin: initial; background-clip: initial; background-color: transparent; line-height: 1.4; background-position: initial initial; background-repeat: initial initial; "></p>";s:9:"changelog";s:1194:"MIGX 3.0.0\n============\nBasic Compatibility for MODX 3\n\n\nMIGX 2.13.0\n==============\n\nadd snippet migxGetObject and property toPlaceholders\n\nadd property createChunk to create a chunk with all possible placeholders\n\nFix imageupload on PHP7 due to stray break statement\n\nFix for missing custom prefix in some packagemanager tasks\n\nUpdated image paths to use dynamic assets path\n\nFix the export if a join setting exist\n\nFinnish Translation\n\nadd userid to migxResourceMediapath snippet\n\nremove duplicate sort scriptProperty in export.php\n\n[getlist.php] add getcustomconfigs - hook, groupby, selectfields, specialfields\n\nparse MODX-tags in field-configs\n\npackagemanager fix new folder permission\n\nUse a textarea instead of an input in MIGX TV\n\nReduce indexed varchar fields for utf8mb4 support in mysql\n\nfix duplicate entries in csv-export\n\nAdd CSV Import functionality\n\nImprove CSV Export functionality\n\nadd toJson property to migxLoopCollection\n\nFix transparency in .png in renderimage column renderer\n\nOptimize loadfromxource functionality, especially for MIGX within MIGXdb\n\ndeep nested saving when nested windows are open and saving without closing the window\n\nexport/import single MIGX - items";s:9:"createdon";s:24:"2022-04-17T17:14:54+0000";s:9:"createdby";s:7:"bruno17";s:8:"editedon";s:24:"2022-10-04T19:22:08+0000";s:10:"releasedon";s:24:"2022-04-17T17:14:54+0000";s:9:"downloads";s:6:"259617";s:8:"approved";s:4:"true";s:7:"audited";s:5:"false";s:8:"featured";s:5:"false";s:10:"deprecated";s:5:"false";s:7:"license";s:5:"GPLv2";s:7:"smf_url";s:0:"";s:10:"repository";s:24:"4d4c3fa6b2b0830da9000001";s:8:"supports";s:3:"2.2";s:8:"location";s:61:"https://modx.com/extras/download/?id=625c4b0eb2fbc9264e2a5033";s:9:"signature";s:17:"migx-3.0.0-alpha5";s:11:"supports_db";s:5:"mysql";s:16:"minimum_supports";s:3:"2.2";s:9:"breaks_at";s:1:"3";s:10:"screenshot";s:70:"http://modx.s3.amazonaws.com/extras/4db018def24554690c000005/migx1.JPG";s:4:"file";a:7:{s:2:"id";s:24:"625c4b0eb2fbc9264e2a5033";s:7:"version";s:24:"625c4b0eb2fbc9264e2a5032";s:8:"filename";s:31:"migx-3.0.0-alpha5.transport.zip";s:9:"downloads";s:4:"9466";s:6:"lastip";s:14:"92.204.239.110";s:9:"transport";s:4:"true";s:8:"location";s:61:"https://modx.com/extras/download/?id=625c4b0eb2fbc9264e2a5033";}s:17:"package-signature";s:17:"migx-3.0.0-alpha5";s:10:"categories";s:15:"content,gallery";s:4:"tags";s:46:"migx,multiitems,multitv,migxdb,CMP,MIGX,MIGXdb";}', 3, 0, 0, 'alpha', 5);
/*!40000 ALTER TABLE `modx_transport_packages` ENABLE KEYS */;

-- Dumping structure for table petukhov.modx_transport_providers
CREATE TABLE IF NOT EXISTS `modx_transport_providers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` mediumtext DEFAULT NULL,
  `service_url` tinytext DEFAULT NULL,
  `username` varchar(255) NOT NULL DEFAULT '',
  `api_key` varchar(255) NOT NULL DEFAULT '',
  `created` datetime NOT NULL,
  `updated` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `priority` tinyint(4) NOT NULL DEFAULT 10,
  `properties` mediumtext NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `api_key` (`api_key`),
  KEY `username` (`username`),
  KEY `active` (`active`),
  KEY `priority` (`priority`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table petukhov.modx_transport_providers: 1 rows
/*!40000 ALTER TABLE `modx_transport_providers` DISABLE KEYS */;
INSERT INTO `modx_transport_providers` (`id`, `name`, `description`, `service_url`, `username`, `api_key`, `created`, `updated`, `active`, `priority`, `properties`) VALUES
	(1, 'modx.com', 'The official MODX transport provider for 3rd party components.', 'https://rest.modx.com/extras/', '', '', '2022-04-28 10:20:05', '2022-10-04 19:32:10', 1, 10, '');
/*!40000 ALTER TABLE `modx_transport_providers` ENABLE KEYS */;

-- Dumping structure for table petukhov.modx_users
CREATE TABLE IF NOT EXISTS `modx_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL DEFAULT '',
  `password` varchar(255) NOT NULL DEFAULT '',
  `cachepwd` varchar(255) NOT NULL DEFAULT '',
  `class_key` varchar(100) NOT NULL DEFAULT 'MODX\\Revolution\\modUser',
  `active` tinyint(1) unsigned NOT NULL DEFAULT 1,
  `remote_key` varchar(255) DEFAULT NULL,
  `remote_data` text DEFAULT NULL,
  `hash_class` varchar(100) NOT NULL DEFAULT 'MODX\\Revolution\\Hashing\\modNative',
  `salt` varchar(100) NOT NULL DEFAULT '',
  `primary_group` int(10) unsigned NOT NULL DEFAULT 0,
  `session_stale` text DEFAULT NULL,
  `sudo` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `createdon` int(20) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  KEY `class_key` (`class_key`),
  KEY `remote_key` (`remote_key`),
  KEY `primary_group` (`primary_group`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table petukhov.modx_users: 1 rows
/*!40000 ALTER TABLE `modx_users` DISABLE KEYS */;
INSERT INTO `modx_users` (`id`, `username`, `password`, `cachepwd`, `class_key`, `active`, `remote_key`, `remote_data`, `hash_class`, `salt`, `primary_group`, `session_stale`, `sudo`, `createdon`) VALUES
	(1, 'root', 'UYRWA/9mr0y6dqWTEXgB/tCTcOG9ZOjnnaP3hxXRhyY=', '', 'MODX\\Revolution\\modUser', 1, NULL, NULL, 'MODX\\Revolution\\Hashing\\modPBKDF2', '0110bee5789643693dd97c4e67da546e', 1, 'a:2:{i:0;s:3:"mgr";i:1;s:3:"web";}', 1, 1470279139);
/*!40000 ALTER TABLE `modx_users` ENABLE KEYS */;

-- Dumping structure for table petukhov.modx_user_attributes
CREATE TABLE IF NOT EXISTS `modx_user_attributes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `internalKey` int(10) NOT NULL,
  `fullname` varchar(100) NOT NULL DEFAULT '',
  `email` varchar(100) NOT NULL DEFAULT '',
  `phone` varchar(100) NOT NULL DEFAULT '',
  `mobilephone` varchar(100) NOT NULL DEFAULT '',
  `blocked` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `blockeduntil` int(11) NOT NULL DEFAULT 0,
  `blockedafter` int(11) NOT NULL DEFAULT 0,
  `logincount` int(11) NOT NULL DEFAULT 0,
  `lastlogin` int(11) NOT NULL DEFAULT 0,
  `thislogin` int(11) NOT NULL DEFAULT 0,
  `failedlogincount` int(10) NOT NULL DEFAULT 0,
  `sessionid` varchar(100) NOT NULL DEFAULT '',
  `dob` int(10) NOT NULL DEFAULT 0,
  `gender` tinyint(1) NOT NULL DEFAULT 0,
  `address` text NOT NULL,
  `country` varchar(255) NOT NULL DEFAULT '',
  `city` varchar(255) NOT NULL DEFAULT '',
  `state` varchar(25) NOT NULL DEFAULT '',
  `zip` varchar(25) NOT NULL DEFAULT '',
  `fax` varchar(100) NOT NULL DEFAULT '',
  `photo` varchar(255) NOT NULL DEFAULT '',
  `comment` text NOT NULL,
  `website` varchar(255) NOT NULL DEFAULT '',
  `extended` text DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `internalKey` (`internalKey`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table petukhov.modx_user_attributes: 1 rows
/*!40000 ALTER TABLE `modx_user_attributes` DISABLE KEYS */;
INSERT INTO `modx_user_attributes` (`id`, `internalKey`, `fullname`, `email`, `phone`, `mobilephone`, `blocked`, `blockeduntil`, `blockedafter`, `logincount`, `lastlogin`, `thislogin`, `failedlogincount`, `sessionid`, `dob`, `gender`, `address`, `country`, `city`, `state`, `zip`, `fax`, `photo`, `comment`, `website`, `extended`) VALUES
	(1, 1, 'Администратор по умолчанию', 'evgenii@petukhov.pro', '', '', 0, 0, 0, 56, 1681526014, 1693900244, 0, 'la53vebfeb0hcpe78q16dua2k4', 0, 0, '', '', '', '', '', '', '', '', '', NULL);
/*!40000 ALTER TABLE `modx_user_attributes` ENABLE KEYS */;

-- Dumping structure for table petukhov.modx_user_group_roles
CREATE TABLE IF NOT EXISTS `modx_user_group_roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` mediumtext DEFAULT NULL,
  `authority` int(10) unsigned NOT NULL DEFAULT 9999,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `authority` (`authority`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table petukhov.modx_user_group_roles: 2 rows
/*!40000 ALTER TABLE `modx_user_group_roles` DISABLE KEYS */;
INSERT INTO `modx_user_group_roles` (`id`, `name`, `description`, `authority`) VALUES
	(1, 'Member', NULL, 9999),
	(2, 'Super User', NULL, 0);
/*!40000 ALTER TABLE `modx_user_group_roles` ENABLE KEYS */;

-- Dumping structure for table petukhov.modx_user_group_settings
CREATE TABLE IF NOT EXISTS `modx_user_group_settings` (
  `group` int(10) unsigned NOT NULL DEFAULT 0,
  `key` varchar(50) NOT NULL,
  `value` text DEFAULT NULL,
  `xtype` varchar(75) NOT NULL DEFAULT 'textfield',
  `namespace` varchar(40) NOT NULL DEFAULT 'core',
  `area` varchar(255) NOT NULL DEFAULT '',
  `editedon` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`group`,`key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table petukhov.modx_user_group_settings: 0 rows
/*!40000 ALTER TABLE `modx_user_group_settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `modx_user_group_settings` ENABLE KEYS */;

-- Dumping structure for table petukhov.modx_user_messages
CREATE TABLE IF NOT EXISTS `modx_user_messages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(15) NOT NULL DEFAULT '',
  `subject` varchar(255) NOT NULL DEFAULT '',
  `message` text NOT NULL,
  `sender` int(10) NOT NULL DEFAULT 0,
  `recipient` int(10) NOT NULL DEFAULT 0,
  `private` tinyint(4) NOT NULL DEFAULT 0,
  `date_sent` datetime DEFAULT NULL,
  `read` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table petukhov.modx_user_messages: 0 rows
/*!40000 ALTER TABLE `modx_user_messages` DISABLE KEYS */;
/*!40000 ALTER TABLE `modx_user_messages` ENABLE KEYS */;

-- Dumping structure for table petukhov.modx_user_settings
CREATE TABLE IF NOT EXISTS `modx_user_settings` (
  `user` int(11) NOT NULL DEFAULT 0,
  `key` varchar(50) NOT NULL DEFAULT '',
  `value` text DEFAULT NULL,
  `xtype` varchar(75) NOT NULL DEFAULT 'textfield',
  `namespace` varchar(40) NOT NULL DEFAULT 'core',
  `area` varchar(255) NOT NULL DEFAULT '',
  `editedon` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`user`,`key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table petukhov.modx_user_settings: 0 rows
/*!40000 ALTER TABLE `modx_user_settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `modx_user_settings` ENABLE KEYS */;

-- Dumping structure for table petukhov.modx_workspaces
CREATE TABLE IF NOT EXISTS `modx_workspaces` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `path` varchar(255) NOT NULL DEFAULT '',
  `created` datetime NOT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `attributes` mediumtext DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `path` (`path`),
  KEY `name` (`name`),
  KEY `active` (`active`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table petukhov.modx_workspaces: 1 rows
/*!40000 ALTER TABLE `modx_workspaces` DISABLE KEYS */;
INSERT INTO `modx_workspaces` (`id`, `name`, `path`, `created`, `active`, `attributes`) VALUES
	(1, 'Default MODX workspace', '{core_path}', '2016-08-04 04:52:18', 1, NULL);
/*!40000 ALTER TABLE `modx_workspaces` ENABLE KEYS */;

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
